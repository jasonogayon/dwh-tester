#### To-Do(s):

1. Review image uploading process for deskop/mobile on-hold reservations
2. Write test for checking HE activity logs load speed
3. Write new desktop tests (update LOS, update private rate plan max sell, update inclusions, update add-ons, update valid days for check-in)
4. Write new mobile version of existing desktop tests (room availability, room rates, room stop sell, highlight, update CC details, policies, copies)

Author: **Jason B. Ogayon (Software Tester)**