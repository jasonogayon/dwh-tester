Feature: Check Payment Page No Prepayment Copy

    Guests is notified that payment charging for a Pay Upon Arrival reservation is done at the hotel.


    @copy @payment @noprepayment
    Scenario: Payment Page, Prepayment Heading Copy for Pay Upon Arrival Reservations
        Given an existing HPP hotel property
        When guest reads the no prepayment copy
        Then a copy of "Please note that charging will be made when you arrive at the hotel. Your credit card may be pre-authorized prior to your arrival." is displayed
