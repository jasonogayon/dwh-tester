Feature: Check Payment Page Digicert Copy

    Guests is notified that Digicert plays a part in securing the confirmation of a reservation. The copy depends on the hotel type.


    @copy @payment @digicert @dwh
    Scenario: Payment Page, Footer Copy for Digicert Certification, DWH Property
        Given an existing Global Collect hotel property
        When guest reads the digicert copy
        Then a copy of "DigiCert® secures the confirmation of your reservation." is displayed

    @copy @payment @digicert @hpp
    Scenario: Payment Page, Footer Copy for Digicert Certification, HPP Property
        Given an existing HPP hotel property
        When guest reads the digicert copy
        Then a copy of "DigiCert® secures the confirmation of your reservation." is displayed
