Feature: Check Best Price Guarantee Copy

    Guests is notified of a best price guarantee for the rates displayed in the booking engine.


    @copy @bpg @showrooms
    Scenario: ShowRooms Page, Best Price Guarantee Copy
        Given an existing Global Collect hotel property
        When guest checks the best price guarantee link in the rate plan page
        Then a copy of "will make available the best" is displayed
        And a copy of "guest room price for the hotel (the" is also displayed
        And a copy of "are the websites owned or" is also displayed
        And a copy of "operated by or on behalf of" is also displayed
        And a copy of "bearing the logo and" is also displayed
        And a copy of "the unlikely event that a lower price at" is also displayed
        And a copy of "upon its receipt of a claim that satisfies these Best" is also displayed
        And a copy of "Price Guarantee terms and conditions (the" is also displayed
        And a copy of "will honor that Competing Price and provide the" is also displayed
        And a copy of "individual that submitted the valid claim one of the following: (1)" is also displayed
        And a copy of "an additional 10% discount off the Competing Price per room per" is also displayed
        And a copy of "night; or (2) a voucher that the guest can use during their stay, the" is also displayed
        And a copy of "amount of the voucher will be equal to the difference between the" is also displayed
        And a copy of "original reservation rate and the competing price." is also displayed
        And a copy of "For a claim to be eligible under the Best Price Guarantee:" is also displayed
        And a copy of "claim must be submitted prior to, or within 24 hours after, making" is also displayed
        And a copy of "The claim must include:" is also displayed
        And a copy of "Booking Confirmation Number" is also displayed
        And a copy of "Full Name on the reservation" is also displayed
        And a copy of "Lower rate found (with currency)" is also displayed
        And a copy of "Room type/ Rate plan (i.e Deluxe Room with Buffet Breakfast)" is also displayed
        And a copy of "Url of the website where the lower price was found" is also displayed
        And a copy of "Reservation date" is also displayed
        And a copy of "Stay dates" is also displayed
        And a copy of "NOTE: You don't need to make a booking on the competitor website," is also displayed
        And a copy of "just email the complete details of your claim" is also displayed
        And a copy of "to support@healthyhotelier.club." is also displayed
        And a copy of "We will get in touch with you within 48 hours from the receipt of the" is also displayed
        And a copy of "claim to verify its validity." is also displayed
        And a copy of "the Competing Price to be valid, it must be a currently available" is also displayed
        And a copy of "lower published online room price for" is also displayed
        And a copy of "same stay dates, the same number of guests, the same room type, with" is also displayed
        And a copy of "a similar view and room size, and include similar or additional" is also displayed
        And a copy of "value-added amenities (e.g., free breakfast)." is also displayed
        And a copy of "compare the total room cost of a stay, and multiple" is also displayed
        And a copy of "claims for a stay consisting of two or more nights in the same week" is also displayed
        And a copy of "will convert any Competing Price offered in a different" is also displayed
        And a copy of "currency than the price made available through the" is also displayed
        And a copy of "Website, and may deny claims where it determines that the" is also displayed
        And a copy of "difference between the price is due to exchange rate fluctuations." is also displayed
        And a copy of "Surcharges, Booking fees, extra adult fees, fees for children," is also displayed
        And a copy of "rollaway charges will be included in the price comparison." is also displayed
        And a copy of "estimated value of value-added amenities (e.g.,free breakfast, use" is also displayed
        And a copy of "of Wifi, vouchers) offered as part of a Competing Price will be" is also displayed
        And a copy of "excluded from the price comparison, and will not be provided by" is also displayed
        And a copy of "when honoring a lower price." is also displayed
        And a copy of "may deny claims where the difference between the Competing" is also displayed
        And a copy of "Best Price Guarantee does not apply to:" is also displayed
        And a copy of "or negotiated prices (e.g., corporate discount rates, group rates," is also displayed
        And a copy of "meeting rates);" is also displayed
        And a copy of "requiring membership in a club or other organization, offered" is also displayed
        And a copy of "pursuant to direct mail or email solicitations, requiring discount" is also displayed
        And a copy of "codes or coupons, or otherwise not intended for the general public;" is also displayed
        And a copy of "prices (e.g., prices that include a combination of a room and" is also displayed
        And a copy of "airfare, an overnight cruise, car rental);" is also displayed
        And a copy of "offered by opaque providers (e.g., Hotwire, Priceline) that do not" is also displayed
        And a copy of "provide the name or location of the hotel until after a reservation" is also displayed
        And a copy of "has been made; and" is also displayed
        And a copy of "offered on on-request websites that do not provide immediate hotel" is also displayed
        And a copy of "confirmations (e.g., Asiaweb)." is also displayed
        And a copy of "Best Price Guarantee does not apply to existing reservations that" is also displayed
        And a copy of "are not booked through a" is also displayed
        And a copy of "is not responsible for any fees associated with cancelling" is also displayed
        And a copy of "a reservation made through a different channel (e.g., a call center," is also displayed
        And a copy of "a valid Best Price Guarantee claim is submitted without an existing" is also displayed
        And a copy of "reservation, the individual making the valid claim will be contacted" is also displayed
        And a copy of "and must make a reservation in the manner" is also displayed
        And a copy of "within 24 hours from receipt of" is also displayed
        And a copy of "the communication or local check-in time at" is also displayed
        And a copy of "Hotel. Failure to make a reservation in the required time period" is also displayed
        And a copy of "will invalidate the claim." is also displayed
        And a copy of "Best Price Guarantee will be suspended during times where the" is also displayed
        And a copy of "Websites or certain prices are not available due to an" is also displayed
        And a copy of "outage, a technical issue or a circumstance beyond" is also displayed
        And a copy of "reasonable control." is also displayed
        And a copy of "Best Price Guarantee reward will only be provided if the individual" is also displayed
        And a copy of "making the valid claim stays in the reserved guest room." is also displayed
        And a copy of "has the sole right and discretion to determine the validity" is also displayed
        And a copy of "of any claim and will not review documentation provided by the" is also displayed
        And a copy of "individual submitting a claim as part of its validation process." is also displayed
        And a copy of "reserves the right to deny a claim, if it cannot" is also displayed
        And a copy of "independently verify the availability of a Competing Price at the" is also displayed
        And a copy of "time it processes the claim." is also displayed
        And a copy of "may at any time and without notice terminate or restrict a" is also displayed
        And a copy of "person's ability to submit a claim under or otherwise benefit from" is also displayed
        And a copy of "the Best Price Guarantee, if in its sole discretion" is also displayed
        And a copy of "determines that such person has: (1) acted in a manner inconsistent" is also displayed
        And a copy of "with applicable laws or ordinances; (2) acted in a fraudulent or" is also displayed
        And a copy of "abusive manner, (3) submitted multiple invalid Best Price Guarantee" is also displayed
        And a copy of "claims; (4) failed to stay at Webdriver -D  Hotels after" is also displayed
        And a copy of "receiving approved Best Price Guarantee Claims; or (5) breached any" is also displayed
        And a copy of "of these BPG Terms." is also displayed
        And a copy of "disputes arising out of or related to the Best Price Guarantee or" is also displayed
        And a copy of "these BPG Terms shall be handled individually without any class" is also displayed
        And a copy of "action, and shall be governed by, construed and enforced in" is also displayed
        And a copy of "accordance with the laws of Philippines." is also displayed
        And a copy of "where prohibited by law." is also displayed
        And a copy of "reserves the right to" is also displayed
        And a copy of "amend, revise, supplement, suspend or discontinue the Best Price" is also displayed
        And a copy of "Guarantee or these BPG Terms at anytime in its sole discretion and" is also displayed
        And a copy of "without prior notice." is also displayed
        And a copy of "Revised: June 29, 2018" is also displayed

    @copy @bpg @payinfo
    Scenario: PayInfo Page, Best Price Guarantee Copy
        Given an existing Global Collect hotel property
        When guest checks the best price guarantee link in the review page
        Then a copy of "will make available the best" is displayed
        And a copy of "guest room price for the hotel (the" is also displayed
        And a copy of "are the websites owned or" is also displayed
        And a copy of "operated by or on behalf of" is also displayed
        And a copy of "bearing the logo and" is also displayed
        And a copy of "the unlikely event that a lower price at" is also displayed
        And a copy of "upon its receipt of a claim that satisfies these Best" is also displayed
        And a copy of "Price Guarantee terms and conditions (the" is also displayed
        And a copy of "will honor that Competing Price and provide the" is also displayed
        And a copy of "individual that submitted the valid claim one of the following: (1)" is also displayed
        And a copy of "an additional 10% discount off the Competing Price per room per" is also displayed
        And a copy of "night; or (2) a voucher that the guest can use during their stay, the" is also displayed
        And a copy of "amount of the voucher will be equal to the difference between the" is also displayed
        And a copy of "original reservation rate and the competing price." is also displayed
        And a copy of "For a claim to be eligible under the Best Price Guarantee:" is also displayed
        And a copy of "claim must be submitted prior to, or within 24 hours after, making" is also displayed
        And a copy of "The claim must include:" is also displayed
        And a copy of "Booking Confirmation Number" is also displayed
        And a copy of "Full Name on the reservation" is also displayed
        And a copy of "Lower rate found (with currency)" is also displayed
        And a copy of "Room type/ Rate plan (i.e Deluxe Room with Buffet Breakfast)" is also displayed
        And a copy of "Url of the website where the lower price was found" is also displayed
        And a copy of "Reservation date" is also displayed
        And a copy of "Stay dates" is also displayed
        And a copy of "NOTE: You don't need to make a booking on the competitor website," is also displayed
        And a copy of "just email the complete details of your claim" is also displayed
        And a copy of "to support@healthyhotelier.club." is also displayed
        And a copy of "We will get in touch with you within 48 hours from the receipt of the" is also displayed
        And a copy of "claim to verify its validity." is also displayed
        And a copy of "the Competing Price to be valid, it must be a currently available" is also displayed
        And a copy of "lower published online room price for" is also displayed
        And a copy of "same stay dates, the same number of guests, the same room type, with" is also displayed
        And a copy of "a similar view and room size, and include similar or additional" is also displayed
        And a copy of "value-added amenities (e.g., free breakfast)." is also displayed
        And a copy of "compare the total room cost of a stay, and multiple" is also displayed
        And a copy of "claims for a stay consisting of two or more nights in the same week" is also displayed
        And a copy of "will convert any Competing Price offered in a different" is also displayed
        And a copy of "currency than the price made available through the" is also displayed
        And a copy of "Website, and may deny claims where it determines that the" is also displayed
        And a copy of "difference between the price is due to exchange rate fluctuations." is also displayed
        And a copy of "Surcharges, Booking fees, extra adult fees, fees for children," is also displayed
        And a copy of "rollaway charges will be included in the price comparison." is also displayed
        And a copy of "estimated value of value-added amenities (e.g.,free breakfast, use" is also displayed
        And a copy of "of Wifi, vouchers) offered as part of a Competing Price will be" is also displayed
        And a copy of "excluded from the price comparison, and will not be provided by" is also displayed
        And a copy of "when honoring a lower price." is also displayed
        And a copy of "may deny claims where the difference between the Competing" is also displayed
        And a copy of "Best Price Guarantee does not apply to:" is also displayed
        And a copy of "or negotiated prices (e.g., corporate discount rates, group rates," is also displayed
        And a copy of "meeting rates);" is also displayed
        And a copy of "requiring membership in a club or other organization, offered" is also displayed
        And a copy of "pursuant to direct mail or email solicitations, requiring discount" is also displayed
        And a copy of "codes or coupons, or otherwise not intended for the general public;" is also displayed
        And a copy of "prices (e.g., prices that include a combination of a room and" is also displayed
        And a copy of "airfare, an overnight cruise, car rental);" is also displayed
        And a copy of "offered by opaque providers (e.g., Hotwire, Priceline) that do not" is also displayed
        And a copy of "provide the name or location of the hotel until after a reservation" is also displayed
        And a copy of "has been made; and" is also displayed
        And a copy of "offered on on-request websites that do not provide immediate hotel" is also displayed
        And a copy of "confirmations (e.g., Asiaweb)." is also displayed
        And a copy of "Best Price Guarantee does not apply to existing reservations that" is also displayed
        And a copy of "are not booked through a" is also displayed
        And a copy of "is not responsible for any fees associated with cancelling" is also displayed
        And a copy of "a reservation made through a different channel (e.g., a call center," is also displayed
        And a copy of "a valid Best Price Guarantee claim is submitted without an existing" is also displayed
        And a copy of "reservation, the individual making the valid claim will be contacted" is also displayed
        And a copy of "and must make a reservation in the manner" is also displayed
        And a copy of "within 24 hours from receipt of" is also displayed
        And a copy of "the communication or local check-in time at" is also displayed
        And a copy of "Hotel. Failure to make a reservation in the required time period" is also displayed
        And a copy of "will invalidate the claim." is also displayed
        And a copy of "Best Price Guarantee will be suspended during times where the" is also displayed
        And a copy of "Websites or certain prices are not available due to an" is also displayed
        And a copy of "outage, a technical issue or a circumstance beyond" is also displayed
        And a copy of "reasonable control." is also displayed
        And a copy of "Best Price Guarantee reward will only be provided if the individual" is also displayed
        And a copy of "making the valid claim stays in the reserved guest room." is also displayed
        And a copy of "has the sole right and discretion to determine the validity" is also displayed
        And a copy of "of any claim and will not review documentation provided by the" is also displayed
        And a copy of "individual submitting a claim as part of its validation process." is also displayed
        And a copy of "reserves the right to deny a claim, if it cannot" is also displayed
        And a copy of "independently verify the availability of a Competing Price at the" is also displayed
        And a copy of "time it processes the claim." is also displayed
        And a copy of "may at any time and without notice terminate or restrict a" is also displayed
        And a copy of "person's ability to submit a claim under or otherwise benefit from" is also displayed
        And a copy of "the Best Price Guarantee, if in its sole discretion" is also displayed
        And a copy of "determines that such person has: (1) acted in a manner inconsistent" is also displayed
        And a copy of "with applicable laws or ordinances; (2) acted in a fraudulent or" is also displayed
        And a copy of "abusive manner, (3) submitted multiple invalid Best Price Guarantee" is also displayed
        And a copy of "claims; (4) failed to stay at Webdriver -D  Hotels after" is also displayed
        And a copy of "receiving approved Best Price Guarantee Claims; or (5) breached any" is also displayed
        And a copy of "of these BPG Terms." is also displayed
        And a copy of "disputes arising out of or related to the Best Price Guarantee or" is also displayed
        And a copy of "these BPG Terms shall be handled individually without any class" is also displayed
        And a copy of "action, and shall be governed by, construed and enforced in" is also displayed
        And a copy of "accordance with the laws of Philippines." is also displayed
        And a copy of "where prohibited by law." is also displayed
        And a copy of "reserves the right to" is also displayed
        And a copy of "amend, revise, supplement, suspend or discontinue the Best Price" is also displayed
        And a copy of "Guarantee or these BPG Terms at anytime in its sole discretion and" is also displayed
        And a copy of "without prior notice." is also displayed
        And a copy of "Revised: June 29, 2018" is also displayed
