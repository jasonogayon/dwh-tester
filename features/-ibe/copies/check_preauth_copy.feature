Feature: Check Payment Page Pre-Authorization Copy

    Guests can read up on why there is a need for pre-authorization for Pay Upon Arrival reservations.


    @copy @payment @preauth
    Scenario: Payment Page, Pre-Authorization Copy for Pay Upon Arrival Reservations
        Given an existing HPP hotel property
        When guest reads the pre-authorization copy
        Then a copy of "Why is there a need to pre-authorize my credit card?" is displayed
        And a copy of "To ensure that the credit card you are using is valid and hasn" is also displayed
        And a copy of "t been reported lost or stolen, the hotel may contact your credit card company. During this time, the hotel will also check to see if your credit card has sufficient funds available to cover your reservation costs. Rest assured that the hotel will not proceed with the charge. The time at which your card will be charged will depend on the terms and conditions of your reservation." is also displayed
        And a copy of "Will my credit card be charged for pre-authorization?" is also displayed
        And a copy of "There is NO charge for pre-authorization. A pre-authorization is a temporary hold of a specific amount of the available balance on your credit card. This is to ensure that your credit card is valid, has sufficient funds and hasn" is also displayed
        And a copy of "t been reported lost or stolen." is also displayed
        And a copy of "How will I know if my credit card has been pre-authorized?" is also displayed
        And a copy of "Pending transactions will appear on your credit card summary. It will indicate a temporary deduction of your available balance by the full amount of your reservation. You may contact the hotel or your credit card company to verify if your credit card has been pre-authorized." is also displayed
        And a copy of "When does the pre-authorization get released from my credit card?" is also displayed
        And a copy of "The length of the hold will vary, and your credit card company can better explain the terms and conditions of their pre-authorization procedures. The terms vary, so it" is also displayed
        And a copy of "s best to contact them for specific details." is also displayed
        And a copy of "Will the hold always equal the exact amount of my reservation?" is also displayed
        And a copy of "Yes, the hotel will pre-authorize your card for the full amount of your reservation." is also displayed
