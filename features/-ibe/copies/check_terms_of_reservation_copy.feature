Feature: Check Payment Page Terms of Reservation Copies

    Guests should be able to read the terms of a reservation before payment. The copies depend on the refund and prepayment policies.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.


    @copy @payment @terms @dwh @public @cc @nonrefundable
    Scenario: Payment Page, Terms of Reservation Copy, Public DWH Non-Refundable
        Given an existing Global Collect hotel property
        When guest reads the terms of reservation copy for a Public Full Prepayment Non-Refundable rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the Internet Booking Engine of Webdriver -D powered by HEALTHYHOTELIER.CLUB PTE. LTD." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept and agree to these terms on behalf of all members of your party when you make a reservation with Webdriver -D. The property and HEALTHYHOTELIER.CLUB PTE. LTD will not be liable or held responsible for any authorized, unauthorized or wrong charge of your credit card pertaining to this reservation." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, full payment will be charged by Webdriver -D to the mode of payment of your choice. This charge is non-refundable, unless otherwise stated in the reservation policies, and completes your online reservation (subject to the terms of reservation). HEALTHYHOTELIER.CLUB PTE. LTD does not make, accept, or process payments on behalf of Webdriver -D at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a confirmation email containing your Transaction ID. Please save and print this for your reference and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Modification, Cancellation and No-Show Terms" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the policy of your selected promo  as stated in the reservation policies section of the Internet Booking Engine. You may be charged for modification and cancellation as per the stated policy of your reservation. Please note, however, that certain rates or special promos are not eligible for modification and cancellation. In the event that you do not check-in before the time specified in the no-show policy section, you will be qualified as a no-show and corresponding fees will be collected via a charge to the credit card on record. We will be in contact with you to request your assistance with the payment of the no-show fee in case the credit card we have on record does not work. City/tourist tax may still be charged in the event of a no-show or cancellation with penalty. Please check the reservation policies, including additional information and hotel policies thoroughly before making a reservation with Webdriver -D. These information are made available on the promo details pages of the Internet Booking Engine during the reservation process, and in the confirmation page and confirmation email after the reservation is confirmed. If you wish to review, adjust, or cancel your reservation, please open your confirmation email and follow the instructions provided. You may also contact Guest Support for adjustments and disputes whose details are supplied in your confirmation email." is also displayed
        And a copy of "6. Secure Reservation Processing" is also displayed
        And a copy of "The credit card and guest information collected from the Internet Booking Engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "7. Card Fraud Control" is also displayed
        And a copy of "The guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used, a valid government issued photo ID of the card owner along with an authorization letter stating that the credit card holder has allowed the guest to use his/her card for the reservation. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "8. No Liability" is also displayed
        And a copy of "Neither Webdriver -D, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for any loss of or damage to property arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and Internet Booking Engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "9. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising from making a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "10. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -D.The English version of our terms of reservation may have been translated into other languages. The translated version is a courtesy translation only and you cannot derive any rights from the translated version. In the event of a dispute about the contents or interpretation of these terms of reservation or inconsistency or discrepancy between the English version and any other language version of these terms of reservation, the English language version to the extent permitted by law shall apply, prevail and be conclusive. The English version is available on the Internet Booking Engine by selecting the English language.If any provision of these terms of reservation is or becomes invalid, unenforceable or non-binding, you shall remain bound by all other provisions hereof. In such event, such invalid provision shall nonetheless be enforced to the fullest extent permitted by applicable law, and you will at least agree to accept a similar effect as the invalid, unenforceable or non-binding provision, given the contents and purpose of these terms of reservation." is also displayed
        And a copy of "11. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed
        And a copy of "12. About HEALTHYHOTELIER.CLUB PTE. LTD" is also displayed
        And a copy of "The online reservation service is powered by HEALTHYHOTELIER.CLUB PTE. LTD, a private company, incorporated under the laws of Singapore and having its headquarters at 100 Tras Street #16-01 100 AM Singapore 079027, registered with the trade register of the Accounting and Corporate Regulatory Authority of Singapore under registration number 201534248H. You may contact HEALTHYHOTELIER.CLUB PTE. LTD at +632-8928030." is also displayed

    @copy @payment @terms @dwh @public @cc @refundable
    Scenario: Payment Page, Terms of Reservation Copy, Public DWH Refundable
        Given an existing Global Collect hotel property
        When guest reads the terms of reservation copy for a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the Internet Booking Engine of Webdriver -D powered by HEALTHYHOTELIER.CLUB PTE. LTD." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept and agree to these terms on behalf of all members of your party when you make a reservation with Webdriver -D. The property and HEALTHYHOTELIER.CLUB PTE. LTD will not be liable or held responsible for any authorized, unauthorized or wrong charge of your credit card pertaining to this reservation." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, full payment will be charged by Webdriver -D to the mode of payment of your choice. This charge is non-refundable, unless otherwise stated in the reservation policies, and completes your online reservation (subject to the terms of reservation). HEALTHYHOTELIER.CLUB PTE. LTD does not make, accept, or process payments on behalf of Webdriver -D at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a confirmation email containing your Transaction ID. Please save and print this for your reference and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Modification, Cancellation and No-Show Terms" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the policy of your selected promo  as stated in the reservation policies section of the Internet Booking Engine. You may be charged for modification and cancellation as per the stated policy of your reservation. Please note, however, that certain rates or special promos are not eligible for modification and cancellation. In the event that you do not check-in before the time specified in the no-show policy section, you will be qualified as a no-show and corresponding fees will be collected via a charge to the credit card on record. We will be in contact with you to request your assistance with the payment of the no-show fee in case the credit card we have on record does not work. City/tourist tax may still be charged in the event of a no-show or cancellation with penalty. Please check the reservation policies, including additional information and hotel policies thoroughly before making a reservation with Webdriver -D. These information are made available on the promo details pages of the Internet Booking Engine during the reservation process, and in the confirmation page and confirmation email after the reservation is confirmed. If you wish to review, adjust, or cancel your reservation, please open your confirmation email and follow the instructions provided. You may also contact Guest Support for adjustments and disputes whose details are supplied in your confirmation email." is also displayed
        And a copy of "6. Secure Reservation Processing" is also displayed
        And a copy of "The credit card and guest information collected from the Internet Booking Engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "7. Card Fraud Control" is also displayed
        And a copy of "The guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used, a valid government issued photo ID of the card owner along with an authorization letter stating that the credit card holder has allowed the guest to use his/her card for the reservation. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "8. No Liability" is also displayed
        And a copy of "Neither Webdriver -D, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for any loss of or damage to property arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and Internet Booking Engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "9. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising from making a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "10. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -D.The English version of our terms of reservation may have been translated into other languages. The translated version is a courtesy translation only and you cannot derive any rights from the translated version. In the event of a dispute about the contents or interpretation of these terms of reservation or inconsistency or discrepancy between the English version and any other language version of these terms of reservation, the English language version to the extent permitted by law shall apply, prevail and be conclusive. The English version is available on the Internet Booking Engine by selecting the English language.If any provision of these terms of reservation is or becomes invalid, unenforceable or non-binding, you shall remain bound by all other provisions hereof. In such event, such invalid provision shall nonetheless be enforced to the fullest extent permitted by applicable law, and you will at least agree to accept a similar effect as the invalid, unenforceable or non-binding provision, given the contents and purpose of these terms of reservation." is also displayed
        And a copy of "11. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed
        And a copy of "12. About HEALTHYHOTELIER.CLUB PTE. LTD" is also displayed
        And a copy of "The online reservation service is powered by HEALTHYHOTELIER.CLUB PTE. LTD, a private company, incorporated under the laws of Singapore and having its headquarters at 100 Tras Street #16-01 100 AM Singapore 079027, registered with the trade register of the Accounting and Corporate Regulatory Authority of Singapore under registration number 201534248H. You may contact HEALTHYHOTELIER.CLUB PTE. LTD at +632-8928030." is also displayed

    @copy @payment @terms @hpp @public @cc @noprepay
    Scenario: Payment Page, Terms of Reservation Copy, Public HPP Pay Upon Arrival
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Payment for Reservation" is also displayed
        And a copy of "A credit card is required to guarantee your reservation. Your card will be checked for validity. No charge will be made. The total cost of your reservation will be paid at the hotel. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Arrival)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: \"no-show\" and the corresponding fees will be collected via a charge to the card on record." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @public @cc @nonrefundable
    Scenario: Payment Page, Terms of Reservation Copy, Public HPP Pay Upon Booking, Non-Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H within 1 to 2 days to any of the credit or debit cards supported by the payment system. This charge, if successful, is nonrefundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: \"no-show\" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @public @cc @refundable
    Scenario: Payment Page, Terms of Reservation Copy, Public HPP Pay Upon Booking, Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H within 1 to 2 days to any of the credit or debit cards supported by the payment system. This charge, if successful, is refundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: \"no-show\" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @bdo @public @cc @nonrefundable
    Scenario: Payment Page, Terms of Reservation Copy, Public BDO Pay Upon Booking, Non-Refundable
        Given an existing BDO hotel property
        When guest reads the terms of reservation copy for a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -B powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be collected upfront through Webdriver -B website, as an advanced payment charged to any of the credit cards supported by the payment system. In order to keep your credit card details secure, we are using a 3D secure system, via BDO Secure Payment. This system guarantees that your credit card details are not available to us or any other user of the internet apart from the secure server which does the authorization on our behalf. Third party credit card transaction must submit a photocopy of the credit card used (back and front), a signed letter of authorization from the credit card owner plus a photocopy of any government issued ID such as passport, driver license, etc of the credit card owner. Signature should match in all the required documents. Non compliance will be subject to full payment upon check-in." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it, along with the credit card used in this transaction, to the hotel upon check-in. NO record or email, NO check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. You will have to contact Webdriver -B if you need to modify or cancel this reservation. Webdriver -B reserves the right to charge your credit card for modification, cancellation and no-show fees as per the stated policy for your reservation. In case of a refund, aside from the applicable penalty fee, a refund processing fee wil be charged." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: \"no-show\" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "Payment transactions are securely processed by BDO Secure Payment. A processing fee will be charged for bookings requesting for refund, as per the stated policy for your reservation." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -B and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -B, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -B." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @bdo @public @cc @refundable
    Scenario: Payment Page, Terms of Reservation Copy, Public BDO Pay Upon Booking, Refundable
        Given an existing BDO hotel property
        When guest reads the terms of reservation copy for a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -B powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be collected upfront through Webdriver -B website, as an advanced payment charged to any of the credit cards supported by the payment system. In order to keep your credit card details secure, we are using a 3D secure system, via BDO Secure Payment. This system guarantees that your credit card details are not available to us or any other user of the internet apart from the secure server which does the authorization on our behalf. Third party credit card transaction must submit a photocopy of the credit card used (back and front), a signed letter of authorization from the credit card owner plus a photocopy of any government issued ID such as passport, driver license, etc of the credit card owner. Signature should match in all the required documents. Non compliance will be subject to full payment upon check-in." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it, along with the credit card used in this transaction, to the hotel upon check-in. NO record or email, NO check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. You will have to contact Webdriver -B if you need to modify or cancel this reservation. Webdriver -B reserves the right to charge your credit card for modification, cancellation and no-show fees as per the stated policy for your reservation. In case of a refund, aside from the applicable penalty fee, a refund processing fee wil be charged." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: \"no-show\" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "Payment transactions are securely processed by BDO Secure Payment. A processing fee will be charged for bookings requesting for refund, as per the stated policy for your reservation." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -B and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -B, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -B." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @dwh @private @cc @nonrefundable @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private DWH Non-Refundable
        Given an existing Global Collect hotel property
        When guest reads the terms of reservation copy for a Private Full Prepayment Non-Refundable rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the Internet Booking Engine of Webdriver -D powered by HEALTHYHOTELIER.CLUB PTE. LTD." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept and agree to these terms on behalf of all members of your party when you make a reservation with Webdriver -D. The property and HEALTHYHOTELIER.CLUB PTE. LTD will not be liable or held responsible for any authorized, unauthorized or wrong charge of your credit card pertaining to this reservation." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, full payment will be charged by Webdriver -D to the mode of payment of your choice. This charge is non-refundable, unless otherwise stated in the reservation policies, and completes your online reservation (subject to the terms of reservation). HEALTHYHOTELIER.CLUB PTE. LTD does not make, accept, or process payments on behalf of Webdriver -D at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. We reserve the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding fees will be collected via a charge to the card on record. Should that card not work for this purpose, we will be in contact with you to request your assistance with your payment of the no-show fee." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used, a valid government issued photo ID of the card owner along with an authorization letter stating that the credit card holder has allowed the guest to use his/her card for the reservation. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "9. No Liability" is also displayed
        And a copy of "Neither Webdriver -D, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "10. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "11. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -D." is also displayed
        And a copy of "12. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @cc @noprepay @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Arrival
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Arrival with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Payment for Reservation" is also displayed
        And a copy of "A credit card is required to guarantee your reservation. Your card will be checked for validity. No charge will be made. The total cost of your reservation will be paid at the hotel. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Arrival)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding fees will be collected via a charge to the card on record." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @cc @nonrefundable @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Booking, Non-Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H within 1 to 2 days to any of the credit or debit cards supported by the payment system. This charge, if successful, is nonrefundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @cc @refundable @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Booking, Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H within 1 to 2 days to any of the credit or debit cards supported by the payment system. This charge, if successful, is refundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. The hotel reserves the right to charge your credit/ debit card for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @account @noprepay @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Arrival
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Arrival with Lead Time Penalty rate plan for account payment
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Payment for Reservation" is also displayed
        And a copy of "No prepayment will be charged. The total cost of your reservation will be paid at the hotel. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Arrival)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. You will have to contact Webdriver -H if you need to modify your reservation. Webdriver -H reserves the right to charge your private account for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding fees will be collected via a charge to the card on record." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @account @nonrefundable @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Booking, Non-Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan for account payment
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H to your private account. This charge is nonrefundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. You will have to contact Webdriver -H if you need to modify your reservation. Webdriver -H reserves the right to charge your private account for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed

    @copy @payment @terms @hpp @private @account @refundable @flaky
    Scenario: Payment Page, Terms of Reservation Copy, Private HPP Pay Upon Booking, Refundable
        Given an existing HPP hotel property
        When guest reads the terms of reservation copy for a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for account payment
        Then a copy of "1. Scope of Terms" is displayed
        And a copy of "These terms govern all reservations made through the internet booking engine of Webdriver -H powered by DirectWithHotels." is also displayed
        And a copy of "2. Acceptance" is also displayed
        And a copy of "You accept these terms on behalf of all members of your party." is also displayed
        And a copy of "3. Advance Payment for Reservation" is also displayed
        And a copy of "To guarantee your reservation, a prepayment will be charged by Webdriver -H to your private account. This charge is refundable unless otherwise specified, and completes your online reservation, subject to the terms of reservation." is also displayed
        And a copy of "The remaining balance of your total room charge if any, along with any charges incurred during your stay, will be collected upon check-out. DirectWithHotels does not make, accept or process payment to or on behalf of the Webdriver -H at the time of reservation." is also displayed
        And a copy of "4. Confirmed Reservation" is also displayed
        And a copy of "The reservation is considered confirmed when you receive a Confirmation Page containing your Transaction ID." is also displayed
        And a copy of "Please save and/or print this for your records and present it to the hotel upon check-in." is also displayed
        And a copy of "5. Terms of Modifying or Cancelling a Reservation (Pay Upon Booking)" is also displayed
        And a copy of "Modification and cancellation may be allowed depending on the rate plan policy as stated in the policy section. You will have to contact Webdriver -H if you need to modify your reservation. Webdriver -H reserves the right to charge your private account for modification, cancellation and no-show fees as per the stated policy for your reservation." is also displayed
        And a copy of "6. No-Show Terms" is also displayed
        And a copy of "In the event that you do not check-in before the time specified in the Cancellation and No-Show Policies section, you will be qualified as a: "no-show" and the corresponding penalty fee will apply." is also displayed
        And a copy of "7. Secure Reservation Processing" is also displayed
        And a copy of "The guest information collected from the internet booking engine are protected and securely transmitted via a secure channel." is also displayed
        And a copy of "8. Payment for Use of Internet Booking Engine" is also displayed
        And a copy of "To help develop, maintain & enhance this direct booking online technology, as well as provide you 24/7 pre-arrival support," is also displayed
        And a copy of "a service fee is charged; to simplify, this service fee is included in the room-rate and settled directly between Webdriver -H and DirectWithHotels." is also displayed
        And a copy of "9. Card Fraud Control" is also displayed
        And a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used and a valid government issued PHOTO ID of the card owner. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is also displayed
        And a copy of "10. No Liability" is also displayed
        And a copy of "Neither Webdriver -H, nor our online reservation provider nor any of our directors, employees, affiliates or other representatives, will be liable for loss or damages arising out of or in connection with your use of any information, products, services and/or the materials offered through this website and internet booking engine, including but not limited to, loss of data, income, profit or opportunity, loss of or damage to property and claims of third parties, or any indirect or consequential loss or damages." is also displayed
        And a copy of "11. Limitation of Financial Liability" is also displayed
        And a copy of "In the highly unlikely event of an issue arising as pertains to a reservation, any liability to the Hotel and the online reservation provider for our website is limited to the prepayment paid at the time of reservation." is also displayed
        And a copy of "12. Governing Law" is also displayed
        And a copy of "Your online reservation is governed by the laws of the Singapore and is subject to the exclusive jurisdiction of the courts. These terms represent the entire agreement between you and Webdriver -H." is also displayed
        And a copy of "13. Privacy Policy" is also displayed
        And a copy of "All information collected are used only to secure the reservation. Please read our  privacy policy statement  for more information." is also displayed
