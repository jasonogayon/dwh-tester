Feature: Check Payment Page Reservation Provider Copy

    Guests is notified that the reservation payment that will appear in the credit card statement will be under HealthyHotelier Club or DirectWithHotels Ltd, unless the property processes the payment themselves. Copy depends on whether Asiapay or GC/Ingenico processes the payment.

    Tests Marked as Not Ready:
    - It seems that the reservation provider copy for Asiapay Full Non-Refundable rate plans are not being displayed
    - Starting with the No-Credit-Card booking feature, the reservation provider copy for Ingenico rate plans are also not being displayed


    @copy @payment @provider @gc
    Scenario: Payment Page, Reservation Provider Copy, Global Collect/Ingenico Property
        Given an existing Global Collect hotel property
        When guest reads the reservation provider copy
        Then a copy of "*The amount will appear on your credit/debit card statement under the name of our reservation system provider, HOTEL-RESERVEDIRECT." is displayed

    @copy @payment @provider @ap
    Scenario: Payment Page, Reservation Provider Copy, Asiapay Property
        Given an existing Asiapay hotel property
        When guest reads the reservation provider copy
        Then a copy of "*The amount will appear on your credit/debit card statement under the name of our reservation system provider, DIRECTWITHHOTELS LIMITED." is displayed

    @copy @payment @provider @hpp
    Scenario: Payment Page, Reservation Provider Copy, HPP Property
        Given an existing HPP hotel property
        When guest views the desktop IBE payment page
        Then the reservation provider copy is not displayed