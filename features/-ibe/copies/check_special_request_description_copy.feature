Feature: Check Payment Page Special Request Description Copy

    Guests is notified that they can indicate their special requests, including flight details and preferred transfer schedule, to the hotelier through their room booking.


    @copy @payment @request
    Scenario: Payment Page, Special Request Description Copy
        Given an existing Global Collect hotel property
        When guest reads the special request description copy
        Then a copy of "Please indicate your special/accessibility requests here. You may also include your FLIGHT DETAILS, LOYALTY PROGRAM NUMBER, preferred schedule for Airport Transfer, Boat Transfer, Island Transfer and other preferences." is displayed
