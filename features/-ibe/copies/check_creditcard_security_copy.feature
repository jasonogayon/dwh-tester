Feature: Check Payment Page Credit Card Security Copies

    Guests can read up on how DirectWithHotels is securing their credit card information when paying for a reservation. The copies depend on the hotel type.


    @copy @payment @cc_fraud
    Scenario: Payment Page, Display Copy for Card Owner CC Fraud Description Popup
        Given an existing Global Collect hotel property
        When guest reads the credit card fraud copy
        Then a copy of "To protect credit card owners from fraud, the guest is required to present the actual credit card used in making the online booking and a valid government issued PHOTO ID of the card owner upon check-in. The hotel reserves the right to refuse the guest to check-in for failure to comply with this requirement. If the card owner is not the one staying at the hotel, the guest is required to present clear photocopies of the front and back of the credit card used, a valid government issued photo ID of the card owner along with an authorization letter stating that the credit card holder has allowed the guest to use his/her card for the reservation. The hotel may cancel a confirmed reservation if a booking is suspected to be made using a fraudulent credit card." is displayed

    @copy @payment @cc_cvv @dwh
    Scenario: Payment Page, Display Copy for CC CVV Description Popup, Pay Upon Booking
        Given an existing Global Collect hotel property
        When guest reads the credit card cvv copy
        Then a copy of "To avoid incidences of credit/debit card fraud on Internet purchases, online sellers require consumers to enter their card's Security Code before confirming a sale. These numbers are not embossed on the physical card so that it cannot be printed on receipts, thus assuring cardholders that no one can easily transact using their account numbers alone." is displayed
        And a copy of "Security Codes are three-digit numbers, printed at the back of credit/ debit cards." is also displayed
        And a copy of "Security Codes can also be the four-digit numbers, printed in front of some credit cards, such as American Express." is also displayed

    @copy @payment @cc_cvv @hpp
    Scenario: Payment Page, Display Copy for CC CVV Description Popup, Pay Upon Arrival
        Given an existing HPP hotel property
        When guest reads the credit card cvv copy
        Then a copy of "To avoid incidences of credit/debit card fraud we require your card's Security Code before confirming your reservation. These numbers are not embossed on the physical card so that it cannot be printed on receipts, thus assuring cardholders that no one can easily transact using their account numbers alone." is displayed
        And a copy of "Security Codes are three-digit numbers, printed at the back of credit/ debit cards." is also displayed
        And a copy of "Security Codes can also be the four-digit numbers, printed in front of some credit cards, such as American Express." is also displayed

    @copy @payment @cc_security @dwh
    Scenario: Payment Page, Display Copy for CC Security Description Popup, DWH Property
        Given an existing Global Collect hotel property
        When guest reads the credit card security copy
        Then a copy of "DirectWithHotels provides the booking engine and payment processing facility for Webdriver -D to securely process payment and offer instant confirmation in its own website." is displayed
        And a copy of "DirectWithHotels has partnered with DigiCert®, to provide SSL technology, which is an industry standard and is designed to prevent someone from capturing and viewing your personal information. All of your order information, including your card number, is transmitted through the Internet using this Secure Sockets Layer (SSL) technology, which causes your browser to encrypt your order information before transmitting it to our secure server." is also displayed
        And a copy of "The system and technology infrastructure used are designed to meet security requirements of the Payment Card Industry Data Security Standards (PCI DSS) which is an international information security standard for organizations that handle cardholder information for the major debit, credit, prepaid, e-purse, ATM, and POS cards. The standard was created to increase controls around cardholder data to reduce credit card fraud via its exposure." is also displayed
        And a copy of "Moreover, DirectWithHotels and Webdriver -D do not store or have access to your card information. All this ensures the security of your card as you make your reservation online." is also displayed

    @copy @payment @cc_security @hpp
    Scenario: Payment Page, Display Copy for CC Security Description Popup, HPP Property, Pay Upon Arrival
        Given an existing HPP hotel property
        When guest reads the credit card security copy for a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then a copy of "DirectWithHotels provides the booking engine facility for Webdriver -H to offer instant confirmation in its own website." is displayed
        And a copy of "DirectWithHotels has partnered with DigiCert®, to provide SSL technology, which is an industry standard and is designed to prevent someone from capturing and viewing your personal information. All your reservation information, including your card number, is transmitted through the Internet using this Secure Sockets Layer (SSL) technology, which causes your browser to encrypt your information before transmitting it to our secure server." is also displayed
        And a copy of "The system and technology infrastructure used are designed to meet security requirements of the Payment Card Industry Data Security Standards (PCI DSS) which is an international information security standard for organizations that handle cardholder information for the major debit, credit, prepaid, e-purse, ATM, and POS cards. The standard was created to increase controls around cardholder data to reduce credit card fraud via its exposure." is also displayed
        And a copy of "Moreover, DirectWithHotels does not store or have access to your card information. All this ensures the security of your card as you make your reservation online." is also displayed

    @copy @payment @cc_security @hpp
    Scenario: Payment Page, Display Copy for CC Security Description Popup, HPP Property, Pay Upon Booking
        Given an existing HPP hotel property
        When guest reads the credit card security copy for a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then a copy of "DirectWithHotels provides the booking engine and payment processing facility for Webdriver -H to securely process payment and offer instant confirmation in its own website." is displayed
        And a copy of "DirectWithHotels has partnered with DigiCert®, to provide SSL technology, which is an industry standard and is designed to prevent someone from capturing and viewing your personal information. All of your order information, including your card number, is transmitted through the Internet using this Secure Sockets Layer (SSL) technology, which causes your browser to encrypt your order information before transmitting it to our secure server." is also displayed
        And a copy of "The system and technology infrastructure used are designed to meet security requirements of the Payment Card Industry Data Security Standards (PCI DSS) which is an international information security standard for organizations that handle cardholder information for the major debit, credit, prepaid, e-purse, ATM, and POS cards. The standard was created to increase controls around cardholder data to reduce credit card fraud via its exposure." is also displayed
        And a copy of "Moreover, DirectWithHotels and Webdriver -H do not store or have access to your card information. All this ensures the security of your card as you make your reservation online." is also displayed
