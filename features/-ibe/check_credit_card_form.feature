Feature: Check Credit Card Form for No-Credit-Card Reservations

    Guest does not need to provide credit card information, hence the credit card form is not displayed, for no-credit-card reservations.


    @ccform @desktop @nocc @withcc
    Scenario: Payment Page, Credit Card Form, Public DWH Non-Refundable, With Credit Card
        Given an existing Global Collect hotel property
        When guest views the desktop IBE payment page for a Public Full Prepayment Non-Refundable rate plan
        Then guest sees that the credit card form is displayed

    @ccform @desktop @nocc
    Scenario: Payment Page, Credit Card Form, Public DWH Non-Refundable, No Credit Card
        Given an existing Global Collect hotel property
        And guest support enables the no-credit-card booking feature for the property
        When guest views the desktop IBE payment page for a Public Full Prepayment Non-Refundable rate plan
        Then guest sees that the credit card form is not displayed
