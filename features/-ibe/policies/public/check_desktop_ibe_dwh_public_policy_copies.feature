Feature: Check Desktop IBE Policies Copies, DWH Public Rate Plans

    Guests should be able to read the reservation policies for a desired public promo. The copies depend on the various types of policies, including prepayment, refund, modification, cancellation, and no-show.

    Background:
        Given an existing Global Collect hotel property with public rate plans


    @policies @desktop @public @dwh @partial @leadtime
    Scenario: ShowRooms Reservation Policies, DWH Partial, Lead Time Early Modification
        When guest reads the policies for a Public Partial Prepayment with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only 10% prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @partial @leadtime
    Scenario: ShowRooms Reservation Policies, DWH Partial, Lead Time Late Modification
        When guest reads the policies for a Public Partial Prepayment with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @partial @firstnight
    Scenario: ShowRooms Reservation Policies, DWH Partial, First Night Effective Immediately
        When guest reads the policies for a Public Partial Prepayment with First Night Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged the cost of the first night" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged the cost of the first night" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged the first night of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @partial @nocharge
    Scenario: ShowRooms Reservation Policies, DWH Partial, No Charge
        When guest reads the policies for a Public Partial Prepayment with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @full @nonrefundable
    Scenario: ShowRooms Reservation Policies, DWH Full Non-Refundable
        When guest reads the policies for a Public Full Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Full prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Your 100% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @full @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, DWH Full Refundable, Lead Time Early Modification
        When guest reads the policies for a Public Full Prepayment Refundable with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Full prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed
        And a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @full @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, DWH Full Refundable, Lead Time Late Modification
        When guest reads the policies for a Public Full Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @dwh @full @refundable @nocharge
    Scenario: ShowRooms Reservation Policies, DWH Full Refundable, No Charge
        When guest reads the policies for a Public Full Prepayment Refundable with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed


    # No Credit Card
    # --------------

    @policies @desktop @public @dwh @nocc
    Scenario: ShowRooms Reservation Policies, DWH Full Non-Refundable, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        When guest reads the policies for a Public Full Prepayment Non-Refundable rate plan on stay dates from TODAY to TOMORROW
        Then a copy of "Full prepayment is required to confirm your reservation" is not displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed
        And a copy of "Your 100% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is not displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed
        And a copy of "No prepayment will be charged. Payment for your stay will be made at the hotel" is displayed
        And a copy of "Allowed. Free of charge" is displayed

    @policies @desktop @public @dwh @nocc
    Scenario: ShowRooms Reservation Policies, DWH Full Non-Refundable, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        But hotelier disables the no-credit-card booking feature for the Public Full Prepayment Non-Refundable rate plan
        When guest reads the policies for a Public Full Prepayment Non-Refundable rate plan on stay dates from TODAY to TOMORROW
        Then a copy of "Full prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Your 100% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed
