Feature: Check Desktop IBE Policies Copies, HPP Public Rate Plans

    Guests should be able to read the reservation policies for a desired public promo. The copies depend on the various types of policies, including prepayment, refund, modification, cancellation, and no-show.

    Background:
        Given an existing HPP hotel property with public rate plans


    @policies @desktop @public @hpp @arrival @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Pay Upon Arrival, Lead Time Early Modification
        When guest reads the policies for a Public Pay upon Arrival with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "No prepayment will be charged. Payment for your stay will be made at the hotel" is displayed
        And a copy of "Please note that your credit card may be pre-authorized prior to your arrival" is displayed
        And a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @arrival @leadtime
    Scenario: ShowRooms Reservation Policies, HPP HPP Pay Upon Arrival, Lead Time Late Modification
        When guest reads the policies for a Public Pay upon Arrival with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @arrival @firstnight
    Scenario: ShowRooms Reservation Policies, HPP Pay Upon Arrival, First Night Penalty
        When guest reads the policies for a Public Pay upon Arrival with First Night Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "No prepayment will be charged. Payment for your stay will be made at the hotel" is displayed
        And a copy of "Please note that your credit card may be pre-authorized prior to your arrival" is displayed
        And a copy of "If you choose to modify, you will be charged the cost of the first night." is displayed
        And a copy of "If you choose to cancel, you will be charged the cost of the first night." is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged the first night of the reservation." is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @arrival @fullcharge
    Scenario: ShowRooms Reservation Policies, HPP Pay Upon Arrival, Full Charge Penalty
        When guest reads the policies for a Public Pay upon Arrival with Full Charge Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "No prepayment will be charged. Payment for your stay will be made at the hotel" is displayed
        And a copy of "Please note that your credit card may be pre-authorized prior to your arrival" is displayed
        And a copy of "If you choose to modify, you will be charged the total room charges." is displayed
        And a copy of "If you choose to cancel, you will be charged the total room charges." is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged the total room cost of the reservation." is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @arrival @nocharge
    Scenario: ShowRooms Reservation Policies, HPP HPP Pay Upon Arrival, No Charge
        When guest reads the policies for a Public Pay upon Arrival with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Non-Refundable Effective Immediately
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only 10% prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "If you choose to modify, you will be charged 10% of the total room charges" is displayed
        And a copy of "If you choose to cancel, you will be charged 10% of the total room charges" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Non-Refundable Not Allowed
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Not Allowed" is displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @partial @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, Lead Time Early Modification
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only 10% is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @partial @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, Lead Time Late Modification
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @partial @refundable @nocharge
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, No Charge
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Refundable with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @firstnight @nonrefundable
    Scenario: ShowRooms Prepayment Policy, HPP First Night Pay Upon Booking Non-Refundable
        When guest reads the policies for a Public Pay upon Booking First Night Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only first night is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is not displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is not displayed

    @policies @desktop @public @hpp @firstnight @refundable
    Scenario: ShowRooms Prepayment Policy, HPP First Night Pay Upon Booking Refundable
        When guest reads the policies for a Public Pay upon Booking First Night Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "Only first night is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed

    @policies @desktop @public @hpp @full @nonrefundable
    Scenario: ShowRooms Prepayment Policy, HPP Full Pay Upon Booking Non-Refundable
        When guest reads the policies for a Public Pay upon Booking Full Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Full prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed

    @policies @desktop @public @hpp @full @refundable
    Scenario: ShowRooms Prepayment Policy, HPP Full Pay Upon Booking Refundable
        When guest reads the policies for a Public Pay upon Booking Full Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "Full prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is not displayed
