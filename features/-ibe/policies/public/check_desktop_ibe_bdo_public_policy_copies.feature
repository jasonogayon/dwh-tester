Feature: Check Desktop IBE Policies Copies, BDO Public Rate Plans

    Guests should be able to read the reservation policies for a desired public promo. The copies depend on the various types of policies, including prepayment, refund, modification, cancellation, and no-show.

    Background:
        Given an existing BDO hotel property with public rate plans


    @policies @desktop @public @bdo @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, BDO Partial Pay Upon Booking Non-Refundable Effective Immediately
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only 10% prepayment is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "If you choose to modify, your 10% prepayment will be forfeited" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is displayed
        And a copy of "If you choose to cancel, your 10% prepayment will be forfeited" is displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed

    @policies @desktop @public @bdo @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, BDO Partial Pay Upon Booking Non-Refundable Not Allowed
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Not Allowed" is displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed

    @policies @desktop @public @bdo @partial @refundable @leadtime @flaky
    Scenario: ShowRooms Reservation Policies, BDO Partial Pay Upon Booking Refundable, Lead Time Early Modification
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Only 10% is required to confirm your reservation" is displayed
        And a copy of "This is nonrefundable" is not displayed
        And a copy of "The remaining balance shall be paid at the hotel" is displayed
        And a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise your prepayment will be forfeited" is displayed
        And a copy of "In case of a refund, you will be charged an additional Php 500.00 as refund processing fee" is displayed
        And a copy of "Please note that you will need to contact the hotel to modify your reservation" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise your prepayment will be forfeited" is displayed
        And a copy of "In case of a refund, you will be charged an additional Php 500.00 as refund processing fee" is displayed
        And a copy of "Please note that you will need to contact the hotel to cancel your reservation" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 10% of the total room cost of the reservation" is displayed

    @policies @desktop @public @bdo @partial @refundable @leadtime @flaky
    Scenario: ShowRooms Reservation Policies, BDO Partial Pay Upon Booking Refundable, Lead Time Late Modification
        When guest reads the policies for a Public Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed