Feature: Check Desktop IBE Policies Copies, HPP Private Rate Plans

    Guests should be able to read the reservation policies for a desired private promo. The copies depend on the various types of policies, including prepayment, refund, modification, cancellation, and no-show.

    Background:
        Given an existing HPP hotel property with private rate plans


    @policies @desktop @private @hpp @noprepay @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Pay Upon Arrival, Lead Time Early Modification
        When guest reads the policies for a Private Pay upon Arrival with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed

    @policies @desktop @private @hpp @noprepay @leadtime
    Scenario: ShowRooms Reservation Policies, HPP HPP Pay Upon Arrival, Lead Time Late Modification
        When guest reads the policies for a Private Pay upon Arrival with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed

    @policies @desktop @private @hpp @noprepay @nocharge
    Scenario: ShowRooms Reservation Policies, HPP HPP Pay Upon Arrival, No Charge
        When guest reads the policies for a Private Pay upon Arrival with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed

    @policies @desktop @private @hpp @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Non-Refundable Effective Immediately
        When guest reads the policies for a Private Pay upon Booking 10% Prepayment Non-Refundable rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "If you choose to modify, you will be charged 10% of the total room charges" is displayed
        And a copy of "If you choose to cancel, you will be charged 10% of the total room charges" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed

    @policies @desktop @private @hpp @partial @nonrefundable
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Non-Refundable Not Allowed
        When guest reads the policies for a Private Pay upon Booking 10% Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "Not Allowed" is displayed
        And a copy of "Not Allowed" is displayed
        And a copy of "Your 10% prepayment will be forfeited if you do not arrive within 24 hours of the check-in date and time of the hotel" is displayed

    @policies @desktop @private @hpp @partial @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, Lead Time Early Modification
        When guest reads the policies for a Private Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from 4 DAYS FROM NOW to 6 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed

    @policies @desktop @private @hpp @partial @refundable @leadtime
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, Lead Time Late Modification
        When guest reads the policies for a Private Pay upon Booking 10% Prepayment Refundable with Lead Time Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is not displayed
        And a copy of "If you choose to modify, you will be charged 30% of the total room charges" is displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is not displayed
        And a copy of "If you choose to cancel, you will be charged 30% of the total room charges" is displayed
        And a copy of "Guests who do not arrive within 24 hours of the check-in date and time of the hotel will be charged 30% of the total room cost of the reservation" is displayed

    @policies @desktop @private @hpp @partial @refundable @nocharge
    Scenario: ShowRooms Reservation Policies, HPP Partial Pay Upon Booking Refundable, No Charge
        When guest reads the policies for a Private Pay upon Booking 10% Prepayment Refundable with No Penalty rate plan on stay dates from TOMORROW to 3 DAYS FROM NOW
        Then a copy of "We don't charge you a modification fee if you choose to modify before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "We don't charge you a cancellation fee if you choose to cancel before" is displayed
        And a copy of "Otherwise you will be charged 30% of the total room cost" is not displayed
        And a copy of "You will not be charged for a No Show" is displayed
