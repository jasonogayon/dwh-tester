Feature: Rate Mixing

    Guest can book a room package with mixed rates from at least two rate plans with same breakfast type.

    Background:
        Given an existing Global Collect with Rate Mixing hotel property


    @ratemix @desktop
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable, Rate Mixing
        When hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property
        And stop sells the rate plan for dates from TODAY to 7 DAYS FROM NOW
        And hotel manager also adds a Nightly Full Non-Refundable public rate plan
        And stop sells the rate plan for dates from 8 DAYS FROM NOW to 14 DAYS FROM NOW
        Then guest sees that a "Best Rates Without Breakfast" mixed rate plan is displayed on the desktop IBE for stay dates from 4 DAYS FROM NOW to 11 DAYS FROM NOW
        And guest can book rooms for the mixed rate plan on the desktop IBE
        And the reservation is logged as mixed on the Hotelier Extranet

    @ratemix @mobile @flaky
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable, Rate Mixing
        When hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property
        And stop sells the rate plan for dates from TODAY to 7 DAYS FROM NOW
        And hotel manager also adds a Nightly Full Non-Refundable public rate plan
        And stop sells the rate plan for dates from 8 DAYS FROM NOW to 14 DAYS FROM NOW
        Then guest sees that a "Best Rates Without Breakfast" mixed rate plan is displayed on the mobile IBE for stay dates from 4 DAYS FROM NOW to 11 DAYS FROM NOW
        And guest can book rooms for the mixed rate plan on the mobile IBE
        And the reservation is logged as mixed on the Hotelier Extranet