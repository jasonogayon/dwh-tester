Feature: Create Cancelled Reservations on the Desktop IBE, HPP

    Guests should be able to cancel a room booking for HPP hotels.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the cancellation policy calls for it.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing HPP hotel property


    @create_rsvsn @desktop @cancel @public @hpp @arrival @early
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @late
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @arrival @none
    Scenario: Create Desktop Public Cancelled Reservation, HPP Pay upon Arrival Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @early
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @late
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @firstnight
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @firstnight
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @fullcharge
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @fullcharge
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @hpp @partial @refundable @none
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @cancel @private @hpp @arrival @early @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @late @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @firstnight @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @firstnight @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @fullcharge @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @fullcharge @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @arrival @none @flaky
    Scenario: Create Desktop Private Cancelled Reservation, HPP Pay upon Arrival Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @early @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @late @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @firstnight @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @firstnight @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @fullcharge @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @fullcharge @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @hpp @partial @refundable @none @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet
