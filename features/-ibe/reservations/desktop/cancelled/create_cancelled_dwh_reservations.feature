Feature: Create Cancelled Reservations on the Desktop IBE, DWH

    Guests should be able to cancel a room booking for DWH hotels.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the cancellation policy calls for it.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing Global Collect hotel property


    @create_rsvsn @desktop @cancel @public @dwh @partial @early
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @late
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @partial @none
    Scenario: Create Desktop Public Cancelled Reservation, DWH Partial Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @nonrefundable
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @onhold @pending @flaky
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Non-Refundable Rate Plan, On-Hold Pending
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from 2 DAYS FROM NOW to 3 DAYS FROM NOW using a different credit card owner
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet
        And hotelier sees that the amount remittable for the reservation is 0

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @early
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @late
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with First Night Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @firstnight
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Full Charge Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @fullcharge
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @dwh @full @refundable @none
    Scenario: Create Desktop Public Cancelled Reservation, DWH Full Refundable Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with No Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @cancel @private @dwh @full @nonrefundable @flaky
    Scenario: Create Desktop Private Cancelled Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Full Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet


    # No Credit Card Reservations
    # ---------------------------

    @create_rsvsn @desktop @cancel @public @nocc @dwh
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, Lead Time Penalty, Late Cancellation, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from TODAY to TOMORROW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And credit card details are not displayed on the desktop IBE confirmation page
        And the prepayment copy displayed on the desktop IBE confirmation page is "Prepayment: No prepayment will be charged. Payment for your stay will be made at the hotel."
        And the cancellation policy copy displayed on the desktop IBE confirmation page is "Cancellation: Allowed. Free of charge."
        And the modification policy copy is not displayed on the desktop IBE confirmation page
        And the no-show policy copy is not displayed on the desktop IBE confirmation page
        And the refund policy copy is also not displayed on the desktop IBE confirmation page
        And the card fraud policy copy is also not displayed on the desktop IBE confirmation page
        And the desktop reservation is logged in the Hotelier Extranet
        And credit card details are also not displayed in the reservation details
