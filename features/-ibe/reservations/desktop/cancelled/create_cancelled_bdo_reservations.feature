Feature: Create Cancelled Reservations on the Desktop IBE, BDO

    Guests should be able to cancel a room booking for BDO hotels.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the cancellation policy calls for it.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing BDO hotel property


    @create_rsvsn @desktop @cancel @public @bdo @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @bdo @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @bdo @partial @refundable @early
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @bdo @partial @refundable @late
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @cancel @private @bdo @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @bdo @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @bdo @partial @refundable @early @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @private @bdo @partial @refundable @late @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet
