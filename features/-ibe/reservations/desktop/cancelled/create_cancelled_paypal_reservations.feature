Feature: Create Cancelled Reservations on the Desktop IBE, Paypal

    Guests should be able to cancel a room booking for Paypal hotels.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the cancellation policy calls for it.

    Background:
        Given an existing Paypal Only hotel property


    @create_rsvsn @desktop @cancel @public @pp @full @nonrefundable @flaky
    Scenario: Create Desktop Public Confirmed Reservation, Paypal Full Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Full Prepayment Non-Refundable rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @pp @full @refundable @early @flaky
    Scenario: Create Desktop Public Confirmed Reservation, Paypal Full Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @cancel @public @pp @full @refundable @late @flaky
    Scenario: Create Desktop Public Confirmed Reservation, Paypal Full Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Late Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Full Prepayment Refundable with Lead Time Penalty rate plan
        And cancels the reservation
        Then guest is notified that the desktop reservation has been cancelled
        And the desktop reservation is logged in the Hotelier Extranet
