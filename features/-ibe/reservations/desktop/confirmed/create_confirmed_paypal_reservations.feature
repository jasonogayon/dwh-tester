Feature: Create Confirmed Reservations on the Desktop IBE, Paypal

    Guests should be able to book a hotel room on a desired package via the desktop/tablet booking engine.
    For Paypal hotels, payment for bookings will be through Paypal.

    Background:
        Given an existing Paypal Only hotel property


    @create_rsvsn @desktop @confirm @public @pp @full @nonrefundable @flaky
    Scenario: Create Desktop Public Confirmed Reservation, Paypal Full Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @pp @full @refundable @flaky
    Scenario: Create Desktop Public Confirmed Reservation, Paypal Full Pay Upon Booking Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet
