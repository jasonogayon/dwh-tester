Feature: Create Confirmed Reservations on the Desktop IBE, BDO

    Guests should be able to book a hotel room on a desired package via the desktop/tablet booking engine.
    For BDO hotels, payment for bookings will be through BDO's payment system.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing BDO hotel property


    @create_rsvsn @desktop @confirm @public @bdo @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @bdo @partial @refundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @confirm @private @bdo @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @private @bdo @partial @refundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet
