Feature: Create Confirmed Reservations on the Desktop IBE, DWH

    Guests should be able to book a hotel room on a desired package via the desktop/tablet booking engine.
    For DWH hotels, payment for bookings will be direct through Global Collect.

    Tests Marked as Not Ready:
    - File submit process for uploading on-hold reservation credentials fail because of a PHP Error
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing Global Collect hotel property


    @create_rsvsn @desktop @confirm @public @dwh @partial
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the reservation's confirmed estimated time of arrival is the same as that which is set by the guest
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @dwh @full @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @dwh @onhold @pending
    Scenario: Create Desktop Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, On-Hold Pending
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from 2 DAYS FROM NOW to 3 DAYS FROM NOW using a different credit card owner
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @dwh @onhold @upload
    Scenario: Create Desktop Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, On-Hold With Images Uploaded
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from 2 DAYS FROM NOW to 3 DAYS FROM NOW using a different credit card owner and credentials sent
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @dwh @full @refundable
    Scenario: Create Desktop Public Confirmed Reservation, DWH Full Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet


    # With HTML Tags
    # --------------

    @create_rsvsn @desktop @confirm @public @html @dwh
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, With HTML Tags
        When hotel manager adds a Nightly HTML Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest can book a desktop IBE reservation for 2 Accommodation rooms on the created rate plan
        And the desktop reservation is logged in the Hotelier Extranet
        And hotelier sees that the reservation rate plan name contains HTML tags


    # With Extra Beds
    # ---------------

    @create_rsvsn @desktop @confirm @public @extrabed @dwh
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, With Extra Bed
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan with extra beds
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet
        And hotelier sees that the reservation details includes extra beds information


    # With Add-ons
    # ------------

    @create_rsvsn @desktop @confirm @public @addon @dwh @flaky
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, With Add-ons
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan with add-ons
        Then guest is notified that the desktop reservation has been confirmed
        And guest sees that the reservation confirmation includes add-ons information
        And the desktop reservation is logged in the Hotelier Extranet
        And hotelier sees that the reservation details includes add-ons information
        And hotelier can modify the included add-ons of the reservation


    # With Promo Code
    # ---------------

    @create_rsvsn @desktop @confirm @public @promocode @dwh @flaky
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, With Promo Code
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan with "RESERVATIONZ" Promo Code
        Then guest sees that the reservation confirmation includes promo code information
        And hotelier sees that the reservation details includes promo code information


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @confirm @private @dwh @flaky
    Scenario: Create Desktop Private Confirmed Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Private Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet


    # No Credit Card Reservations
    # ---------------------------

    @create_rsvsn @desktop @confirm @public @nocc @dwh
    Scenario: Create Desktop Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from TODAY to TOMORROW
        Then guest is notified that the desktop reservation has been confirmed
        And credit card details are not displayed on the desktop IBE confirmation page
        And the prepayment copy displayed on the desktop IBE confirmation page is "Prepayment: No prepayment will be charged. Payment for your stay will be made at the hotel."
        And the cancellation policy copy displayed on the desktop IBE confirmation page is "Cancellation: Allowed. Free of charge."
        And the modification policy copy is not displayed on the desktop IBE confirmation page
        And the no-show policy copy is not displayed on the desktop IBE confirmation page
        And the refund policy copy is also not displayed on the desktop IBE confirmation page
        And the card fraud policy copy is also not displayed on the desktop IBE confirmation page
        And the desktop reservation is logged in the Hotelier Extranet
        And credit card details are also not displayed in the reservation details
