Feature: Create Confirmed Reservations on the Desktop IBE, HPP

    Guests should be able to book a hotel room on a desired package via the desktop/tablet booking engine.
    For HPP hotels, payment for bookings will be through the hotel.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing HPP hotel property


    @create_rsvsn @desktop @confirm @public @hpp @arrival
    Scenario: Create Desktop Public Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @hpp @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @public @hpp @partial @refundable
    Scenario: Create Desktop Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @confirm @private @hpp @arrival @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @private @hpp @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @confirm @private @hpp @partial @refundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 3 Occupancy rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been confirmed
        And the desktop reservation is logged in the Hotelier Extranet
