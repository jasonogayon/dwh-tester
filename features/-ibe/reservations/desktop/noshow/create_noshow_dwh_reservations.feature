Feature: Create No-Show Reservations on the Desktop IBE, DWH

    The guest support team should be able to mark a room booking as no-show.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the no-show policy calls for it.

    Tests Marked as Not Ready:
    - For First Night Penalty and Full Charge Penalty DWH no-show reservations, it seems that there is no penalty displayed in the confirmation page and refund is always the full amount of the reservation, even though the HE reservation details display the correct penalty and refund amounts for those reservations
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing Global Collect hotel property


    @create_rsvsn @desktop @noshow @public @dwh @partial @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @partial @firstnight @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with First Night Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @partial @firstnight @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @partial @fullcharge @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Full Charge Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @partial @fullcharge @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @partial @none
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with No Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @nonrefundable
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @firstnight @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, First Night Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with First Night Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @firstnight @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, First Night Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with First Night Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @fullcharge @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, Full Charge Penalty, 1 Night
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Full Charge Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @fullcharge @flaky
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, Full Charge Penalty, 2 Nights
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Full Charge Penalty rate plan for stay dates from TOMORROW to 3 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @dwh @full @refundable @none
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, No Penalty
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with No Penalty rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @noshow @private @dwh @full @nonrefundable @flaky
    Scenario: Create Desktop Private No-Show Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Full Prepayment Non-Refundable rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet


    # No-Show Waived
    # --------------

    @create_rsvsn @desktop @noshow @waived @public @dwh @partial @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show waived
        Then guest is notified that the desktop reservation has been marked as no-show waived
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @waived @public @dwh @full @refundable @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show waived
        Then guest is notified that the desktop reservation has been marked as no-show waived
        And the desktop reservation is logged in the Hotelier Extranet


    # No-Show Invalid
    # ---------------

    @create_rsvsn @desktop @noshow @invalid @public @dwh @partial @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Partial Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show invalid
        Then guest is notified that the desktop reservation has been marked as no-show invalid
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @invalid @public @dwh @full @refundable @early
    Scenario: Create Desktop Public No-Show Reservation, DWH Full Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show invalid
        Then guest is notified that the desktop reservation has been marked as no-show invalid
        And the desktop reservation is logged in the Hotelier Extranet


    # No Credit Card Reservations
    # ---------------------------

    @create_rsvsn @desktop @noshow @public @nocc @dwh
    Scenario: Create Desktop Public Confirmed Reservation, DWH Partial Rate Plan, Lead Time Penalty, Late Cancellation, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from TODAY to TOMORROW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And credit card details are not displayed on the desktop IBE confirmation page
        And the prepayment copy displayed on the desktop IBE confirmation page is "Prepayment: No prepayment will be charged. Payment for your stay will be made at the hotel."
        And the cancellation policy copy displayed on the desktop IBE confirmation page is "Cancellation: Allowed. Free of charge."
        And the modification policy copy is not displayed on the desktop IBE confirmation page
        And the no-show policy copy is not displayed on the desktop IBE confirmation page
        And the refund policy copy is also not displayed on the desktop IBE confirmation page
        And the card fraud policy copy is also not displayed on the desktop IBE confirmation page
        And the desktop reservation is logged in the Hotelier Extranet
        And credit card details are also not displayed in the reservation details
