Feature: Create No-Show Reservations on the Desktop IBE, BDO

    The guest support team should be able to mark a room booking as no-show.
    Prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the no-show policy calls for it.

    Tests Marked as Not Ready:
    - Since the Rate Plan Mixing/Bancnet release candidate branch, it takes a long time to load even a single private rate plan directly through the showRooms URL.

    Background:
        Given an existing BDO hotel property


    @create_rsvsn @desktop @noshow @public @bdo @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @bdo @partial @nonrefundable
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @public @bdo @partial @refundable @early @flaky
    Scenario: Create Desktop Public Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet


    # Private Reservations
    # --------------------

    @create_rsvsn @desktop @noshow @private @bdo @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @private @bdo @partial @nonrefundable @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @noshow @private @bdo @partial @refundable @early @flaky
    Scenario: Create Desktop Private Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan, Lead Time Penalty, Early Cancellation
        When guest books a desktop reservation for 2 Accommodation rooms on a Private Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 4 DAYS FROM NOW to 7 DAYS FROM NOW
        And guest support marks the reservation as no-show
        Then guest is notified that the desktop reservation has been marked as no-show
        And the desktop reservation is logged in the Hotelier Extranet
