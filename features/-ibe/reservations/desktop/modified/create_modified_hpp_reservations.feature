Feature: Create Modified Reservations on the Desktop IBE, HPP

    Guests should be able to modify an existing room booking.
    Previous prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the modify policy calls for it.

    Tests Marked as Not Ready:
    - For Pay upon Booking to Pay upon Arrival (B2A) modified reservations, the penalty displayed in the confirmation page is incorrect.

    Background:
        Given an existing HPP hotel property


    @create_rsvsn @desktop @modify @public @hpp @a2a @early
    Scenario: Create Desktop Public Modified Reservation, HPP A2A, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @hpp @a2a @late
    Scenario: Create Desktop Public Modified Reservation, HPP A2A, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet


    @create_rsvsn @desktop @modify @public @hpp @a2b @early
    Scenario: Create Desktop Public Modified Reservation, HPP A2B, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @hpp @a2b @late
    Scenario: Create Desktop Public Modified Reservation, HPP A2B, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet


    @create_rsvsn @desktop @modify @public @hpp @b2b @early
    Scenario: Create Desktop Public Modified Reservation, HPP B2B, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @hpp @b2b @late
    Scenario: Create Desktop Public Modified Reservation, HPP B2B, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet


    @create_rsvsn @desktop @modify @public @hpp @b2a @early @flaky
    Scenario: Create Desktop Public Modified Reservation, HPP B2A, Lead Time Penalty, Early Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @hpp @b2a @late @flaky
    Scenario: Create Desktop Public Modified Reservation, HPP B2A, Lead Time Penalty, Late Modification
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet
