Feature: Create Modified Reservations on the Desktop IBE, DWH

    Guests should be able to modify an existing room booking.
    Previous prepayment made should be refunded to the guest if refundable, while a certain amount of penalty will be charged if the modify policy calls for it.

    Tests Marked as Not Ready:
    - For Full Refundable to Any (FR2*) modified reservations with penalty, the dwh revenue (and thus the hotel revenue too) displayed in the reservation details is incorrect.

    Background:
        Given an existing Global Collect hotel property


    @create_rsvsn @desktop @modify @public @dwh @p2p @early
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @p2p @late
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @p2fn @early
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @p2fn @late
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @p2fr @early
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @p2fr @late
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet


    @create_rsvsn @desktop @modify @public @dwh @fr2p @early
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @fr2p @late @flaky
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @fr2fn @early
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @fr2fn @late @flaky
    Scenario: Create Desktop Public Modified Reservation, DWH Partial Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @fr2fr @early
    Scenario: Create Desktop Public Modified Reservation, DWH Full Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from 5 DAYS FROM NOW to 7 DAYS FROM NOW
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet

    @create_rsvsn @desktop @modify @public @dwh @fr2fr @early @flaky
    Scenario: Create Desktop Public Modified Reservation, DWH Full Refundable Rate Plan
        When guest books a desktop reservation for 2 Accommodation and 1 Occupancy rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        And modifies the reservation to 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the desktop reservation has been modified
        And the desktop reservation is logged in the Hotelier Extranet
