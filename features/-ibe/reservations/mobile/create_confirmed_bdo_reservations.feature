Feature: Create Confirmed Reservations on the Mobile IBE, BDO

    Guests should be able to book a hotel room on a desired package via the mobile booking engine.
    For BDO hotels, payment for bookings will be through BDO's payment system.

    Background:
        Given an existing BDO hotel property


    @create_rsvsn @mobile @confirm @public @bdo @partial @nonrefundable
    Scenario: Create Mobile Public Confirmed Reservation, BDO Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @bdo @partial @refundable
    Scenario: Create Mobile Public Confirmed Reservation, BDO Partial Pay Upon Booking Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet
