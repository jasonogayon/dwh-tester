Feature: Create Confirmed Reservations on the Mobile IBE, DWH

    Guests should be able to book a hotel room on a desired package via the mobile booking engine.
    For DWH hotels, payment for bookings will be direct through Global Collect.

    Tests Marked as Not Ready:
    - File submit process for uploading on-hold reservation credentials fail because of a PHP Error

    Background:
        Given an existing Global Collect hotel property


    @create_rsvsn @mobile @confirm @public @dwh @partial
    Scenario: Create Mobile Public Confirmed Reservation, DWH Partial Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @dwh @full @nonrefundable
    Scenario: Create Mobile Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @dwh @onhold @pending
    Scenario: Create Mobile Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, On-Hold Pending
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from 2 DAYS FROM NOW to 3 DAYS FROM NOW using a different credit card owner
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @dwh @onhold @upload @flaky
    Scenario: Create Mobile Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, On-Hold With Images Uploaded
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Full Prepayment Non-Refundable rate plan for stay dates from 2 DAYS FROM NOW to 3 DAYS FROM NOW using a different credit card owner and credentials sent
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @dwh @full @refundable
    Scenario: Create Mobile Public Confirmed Reservation, DWH Full Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet


    # With HTML Tags
    # --------------

    @create_rsvsn @mobile @confirm @public @html @dwh @flaky
    Scenario: Create Mobile Public Confirmed Reservation, DWH Partial Rate Plan, With HTML Tags
        When hotel manager adds a Nightly HTML Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest can book a mobile IBE reservation for 2 Accommodation rooms on the created rate plan
        And the mobile reservation is logged in the Hotelier Extranet
        And hotelier sees that the reservation rate plan name contains HTML tags


    # No Credit Card Reservations
    # ---------------------------

    @create_rsvsn @mobile @confirm @public @nocc @dwh
    Scenario: Create Mobile Public Confirmed Reservation, DWH Full Non-Refundable Rate Plan, No Credit Card
        And guest support enables the no-credit-card booking feature for the property
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Full Prepayment Refundable with Lead Time Penalty rate plan for stay dates from TODAY to TOMORROW
        Then guest is notified that the mobile reservation has been confirmed
        And credit card details are not displayed on the mobile IBE confirmation page
        And the prepayment copy displayed on the mobile IBE confirmation page is "Prepayment: No prepayment will be charged. Payment for your stay will be made at the hotel."
        And the cancellation policy copy displayed on the mobile IBE confirmation page is "Cancellation: Allowed. Free of charge."
        And the modification policy copy is not displayed on the mobile IBE confirmation page
        And the no-show policy copy is not displayed on the mobile IBE confirmation page
        And the refund policy copy is also not displayed on the mobile IBE confirmation page
        And the card fraud policy copy is also not displayed on the mobile IBE confirmation page
        And the mobile reservation is logged in the Hotelier Extranet
        And credit card details are also not displayed in the reservation details
        And reservation is found in the filtered reservations with no-credit-card guarantee
