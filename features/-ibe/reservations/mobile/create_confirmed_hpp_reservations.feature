Feature: Create Confirmed Reservations on the Mobile IBE, HPP

    Guests should be able to book a hotel room on a desired package via the mobile booking engine.
    For HPP hotels, payment for bookings will be through the hotel.

    Background:
        Given an existing HPP hotel property


    @create_rsvsn @mobile @confirm @public @hpp @arrival
    Scenario: Create Mobile Public Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the reservation's confirmed estimated time of arrival is the same as that which is set by the guest
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @hpp @partial @nonrefundable
    Scenario: Create Mobile Public Confirmed Reservation, HPP Partial Pay Upon Booking Non-Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Non-Refundable rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet

    @create_rsvsn @mobile @confirm @public @hpp @partial @refundable
    Scenario: Create Mobile Public Confirmed Reservation, HPP Partial Pay Upon Booking Refundable Rate Plan
        When guest books a mobile reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then guest is notified that the mobile reservation has been confirmed
        And the mobile reservation is logged in the Hotelier Extranet
