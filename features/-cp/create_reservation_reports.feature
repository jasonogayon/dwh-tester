Feature: Create Reports From Control Panel Reservations Data

    This is a tool for retrieving reservations data from the Control Panel and building custom reports of those data.
    Currently, the following reservations data are available:
        - Total Number of Confirmed Reservations Per Day, categorized by Payment Processor
        - Number of Confirmed Reservations with Extra Beds
        - Number of Confirmed Reservations with Add-ons
        - Number of Confirmed Reservations that are Private
        - Number of Confirmed Reservations made via Mobile
        - Number of Confirmed Reservations by PGW Method
        - Number of Active Hotels, categorized by Country, Payment Processor, Percent Commission, Currency, or Market
        - Number of Active Hotels with Full Payment, Private Accounts, or Rate Mixing Enabled


    @get_data @no_rsvsn
    Scenario: Get Total Number of Confirmed Reservations for a Date
        * guest support can get the total number of confirmed reservations YESTERDAY

    @get_data @extra_bed @addon @private
    Scenario: Get Extra Bed, Add-on, and Private Account Data from Confirmed Reservations
        * guest support can get the number of confirmed reservations YESTERDAY with extra bed, add-on, or are private

    @get_data @rsvsn
    Scenario: Get Information from Confirmed Reservations for a Date
        * guest support can get information from confirmed reservations YESTERDAY

    @get_data @mobile
    Scenario: Get Mobile Data from Confirmed Reservations
        * guest support can get the number of mobile reservations YESTERDAY

    @get_data @active_hotels
    Scenario: Get Total Number of Active Hotels
        * guest support can get the total number of active hotels
