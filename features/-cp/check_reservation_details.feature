Feature: CP Reservations and Reservation Details

    Guest support sees that the number of occupants in the most recent reservations is the same as the total number of rooms booked.
    The reservation details window should also display without PHP Errors.

    Note: Test for the number of occupants in the reservation details only runs for confirmed reservations.


    @cp @room_occupants
    Scenario: Number of Occupants in a Reservation is the Same as the Total Number of Rooms Booked
        Given 25 most recent reservations in the Control Panel
        Then guest support sees that the number of occupants for those reservations is the same as the total number of rooms booked
