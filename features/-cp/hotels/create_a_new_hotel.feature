Feature: Create New Hotel Property

    Sales partner can create a new hotel property via the SPI.
    Guest support can update the newly created hotel into GI Activated status.
    Guest support can add hotel information to the newly created hotel since they have access to the Hotelier Extranet.


    @create_hotel @gc
    Scenario: Create a new Hotel, Global Collect
        Given a new Global Collect hotel property with property information, amenities, facilities, rooms, contacts, add-ons, advisories, extra bed information, users, taxes and surcharges, and private accounts

    @create_hotel @ap
    Scenario: Create a new Hotel, Asiapay
        Given a new Asiapay hotel property with private accounts, nightly public rate plans, inactive fixed nights public rate plans, inactive free nights public rate plans, and private rate plans

    @create_hotel @hpp
    Scenario: Create a new Hotel, HPP
        Given a new HPP hotel property with users, and private accounts

    @create_hotel @bdo
    Scenario: Create a new Hotel, BDO
        Given a new BDO hotel property with users, and private accounts

    @create_hotel @bp
    Scenario: Create a new Hotel, BDO-Paypal in PHP
        Given a new BDO-Paypal in PHP hotel property with users, and private accounts

    @create_hotel @pp
    Scenario: Create a new Hotel, Paypal Only
        Given a new Paypal Only hotel property with users, and private accounts

    @create_hotel @bn
    Scenario: Create a new Hotel, Bancnet with Rate Mixing
        Given a new Bancnet with Rate Mixing hotel property with users, and private accounts
