Feature: Get Information From Existing Test Hotels

    Guest support can view information from existing hotel properties since they have access to the Hotelier Extranet.


    @existing @gc
    Scenario: Get an existing Hotel, Global Collect
        Given an existing Global Collect hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans

    @existing @hpp
    Scenario: Get an existing Hotel, HPP
        Given an existing HPP hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans

    @existing @bdo
    Scenario: Get an existing Hotel, BDO
        Given an existing BDO hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans

    @existing @bp
    Scenario: Get an existing Hotel, BDO-Paypal in PHP
        Given an existing BDO-Paypal hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans

    @existing @pp
    Scenario: Get an existing Hotel, Paypal Only
        Given an existing Paypal Only hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans