Feature: Unsubscribed Hotel Properties

    Guests are unable to make room bookings for unsubscribed hotels.


    @unsubscribe
    Scenario: Booking Engine Access for Unsubscribed Hotels
        Given an existing Unsubscribed hotel property
        Then guest is notified that the hotel's reservation system is no longer available on both the desktop and mobile booking engines
