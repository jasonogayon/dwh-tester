Feature: Booking Engine Dynamic Sub-Domain URLs

    Administrators can set up dynamic desktop and mobile domain URLs for a hotel's booking engine.


    @domain_url
    Scenario: Booking Engine Dynamic Domain URL for a Property, Desktop
        Given an existing Asiapay hotel property
        When an administrator adds a 'uat2-reservations.directwithhotels.com' subdomain for the hotel's desktop booking engine
        And an administrator adds a 'uat2-m-neo.directwithhotels.com' subdomain for the hotel's mobile booking engine
        Then guest is redirected to the provided desktop subdomain when visiting the desktop booking engine
        And guest is redirected to the provided mobile subdomain when visiting the mobile booking engine
