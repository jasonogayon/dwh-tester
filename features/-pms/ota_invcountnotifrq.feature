Feature: Property Management System, OTA_InvCountNotifRQ


    @pms @invcountnotifrq @valid @all
    Scenario: OTA_InvCountNotifRQ, Valid Request, All
        When hotelier sends an OTA_InvCountNotifRQ All PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @invcountnotifrq @valid @adjustment
    Scenario: OTA_InvCountNotifRQ, Valid Request, Adjustment
        When hotelier sends an OTA_InvCountNotifRQ Adjustment PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @invcountnotifrq @valid @allocation
    Scenario: OTA_InvCountNotifRQ, Valid Request, Allocation
        When hotelier sends an OTA_InvCountNotifRQ Allocation PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @invcountnotifrq @valid @none
    Scenario: OTA_InvCountNotifRQ, Valid Request, None
        When hotelier sends an OTA_InvCountNotifRQ None PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @invcountnotifrq @invalid @client
    Scenario: OTA_InvCountNotifRQ, Request with Invalid Client
        When hotelier sends an OTA_InvCountNotifRQ Initial with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @invcountnotifrq @invalid @key
    Scenario: OTA_InvCountNotifRQ, Request with Invalid Key
        When hotelier sends an OTA_InvCountNotifRQ Initial with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @invcountnotifrq @invalid @hotel
    Scenario: OTA_InvCountNotifRQ, Request with Invalid Hotel
        When hotelier sends an OTA_InvCountNotifRQ Initial with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Empty or missing HotelCode attribute"

    @pms @invcountnotifrq @invalid @room
    Scenario: OTA_InvCountNotifRQ, Request with Invalid Room
        When hotelier sends an OTA_InvCountNotifRQ Initial with invalid room PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Invalid InvCode attribute"
