Feature: Property Management System, OTA_HotelRatePlanNotifRQ


    @pms @rateplannotifrq @valid @initial @open @flaky
    Scenario: OTA_HotelRatePlanNotifRQ, Valid Request, Initial Open
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial Open PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @rateplannotifrq @valid @initial @closed @summaryonly @flaky
    Scenario: OTA_HotelRatePlanNotifRQ, Valid Request, Initial Closed (Stop Sell)
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial Closed PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @rateplannotifrq @valid @active @open
    Scenario: OTA_HotelRatePlanNotifRQ, Valid Request, Active Open
        When hotelier sends an OTA_HotelRatePlanNotifRQ Active Open PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @rateplannotifrq @valid @active @close @summaryonly
    Scenario: OTA_HotelRatePlanNotifRQ, Valid Request, Active Closed
        When hotelier sends an OTA_HotelRatePlanNotifRQ Active Closed PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @rateplannotifrq @invalid @client
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Client
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @rateplannotifrq @invalid @key
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Key
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @rateplannotifrq @invalid @hotel
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Hotel
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Missing or invalid HotelCode attribute"

    @pms @rateplannotifrq @invalid @rateplan
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Rate Plan
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid rateplan PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Invalid RatePlanCode attribute"

    @pms @rateplannotifrq @invalid @room
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Room
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid room PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Unable to process InvCode"

    @pms @rateplannotifrq @invalid @currency
    Scenario: OTA_HotelRatePlanNotifRQ, Request with Invalid Currency
        When hotelier sends an OTA_HotelRatePlanNotifRQ Initial with invalid currency PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Validation"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "attribute 'CurrencyCode': '0' is not a valid value of the atomic type"
