Feature: Property Management System, OTA_ReadRQ

    Tests Marked as Not Ready:
    - LastUpdateDate valid request test returns a retrieving reservation error

    @pms @readrq @valid @all
    Scenario: OTA_ReadRQ, Valid Request, CreateDate All
        When hotelier sends an OTA_ReadRQ All PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @readrq @valid @undelivered
    Scenario: OTA_ReadRQ, Valid Request, CreateDate Undelivered
        When hotelier sends an OTA_ReadRQ Undelivered PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @readrq @valid @previouslydelivered
    Scenario: OTA_ReadRQ, Valid Request, CreateDate PreviouslyDelivered
        When hotelier sends an OTA_ReadRQ PreviouslyDelivered PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @readrq @valid @lastupdatedate
    Scenario: OTA_ReadRQ, Valid Request, LastUpdateDate
        When hotelier sends an OTA_ReadRQ All with LastUpdateDate PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @readrq @invalid @client
    Scenario: OTA_ReadRQ, Request with Invalid Client
        When hotelier sends an OTA_ReadRQ Undelivered with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @readrq @invalid @key
    Scenario: OTA_ReadRQ, Request with Invalid Key
        When hotelier sends an OTA_ReadRQ PreviouslyDelivered with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @readrq @invalid @hotel
    Scenario: OTA_ReadRQ, Request with Invalid Hotel
        When hotelier sends an OTA_ReadRQ All with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Missing or invalid HotelCode attribute"
