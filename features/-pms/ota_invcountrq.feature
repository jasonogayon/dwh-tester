Feature: Property Management System, OTA_InvCountRQ


    @pms @invcountrq @valid @all
    Scenario: OTA_InvCountRQ, Valid Request, All
        When hotelier sends an OTA_InvCountRQ All PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an Inventories element with the HotelCode attribute
        And the PMS response also contains an StatusApplicationControl element with the InvCode attribute
        And the PMS response also contains an InvCount element with the Count attribute

    @pms @invcountrq @valid @selected
    Scenario: OTA_InvCountRQ, Valid Request, Selected
        When hotelier sends an OTA_InvCountRQ Selected PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an Inventories element with the HotelCode attribute
        And the PMS response also contains an StatusApplicationControl element with the InvCode attribute
        And the PMS response also contains an InvCount element with the Count attribute

    @pms @invcountrq @valid @all @summaryonly
    Scenario: OTA_InvCountRQ, Valid Request, All (Summary Only)
        When hotelier sends an OTA_InvCountRQ All Summary Only PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an Inventories element with the HotelCode attribute
        And the PMS response also contains an StatusApplicationControl element with the InvCode attribute
        And the PMS response also contains an InvCount element with the Count attribute

    @pms @invcountrq @valid @selected @summaryonly
    Scenario: OTA_InvCountRQ, Valid Request, Selected (Summary Only)
        When hotelier sends an OTA_InvCountRQ Selected Summary Only PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an Inventories element with the HotelCode attribute
        And the PMS response also contains an StatusApplicationControl element with the InvCode attribute
        And the PMS response also contains an InvCount element with the Count attribute

    @pms @invcountrq @invalid @client
    Scenario: OTA_InvCountRQ, Request with Invalid Client
        When hotelier sends an OTA_InvCountRQ All with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @invcountrq @invalid @key
    Scenario: OTA_InvCountRQ, Request with Invalid Key
        When hotelier sends an OTA_InvCountRQ All with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @invcountrq @invalid @hotel
    Scenario: OTA_InvCountRQ, Request with Invalid Hotel
        When hotelier sends an OTA_InvCountRQ All with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Could not retrieve details for the hotel"

    @pms @invcountrq @invalid @room
    Scenario: OTA_InvCountRQ, Request with Invalid Room
        When hotelier sends an OTA_InvCountRQ Selected with invalid room PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Invalid RoomID"
