Feature: Property Management System, OTA_NotifReportRQ


    @pms @notifreportrq @valid
    Scenario: OTA_NotifReportRQ, Valid Request
        When hotelier sends an OTA_NotifReportRQ PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @notifreportrq @invalid @client
    Scenario: OTA_NotifReportRQ, Request with Invalid Client
        When hotelier sends an OTA_NotifReportRQ with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @notifreportrq @invalid @key
    Scenario: OTA_NotifReportRQ, Request with Invalid Key
        When hotelier sends an OTA_NotifReportRQ with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"
