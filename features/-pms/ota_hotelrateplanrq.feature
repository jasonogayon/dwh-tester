Feature: Property Management System, OTA_HotelRatePlanRQ


    @pms @rateplanrq @valid @all
    Scenario: OTA_HotelRatePlanRQ, Valid Request, All
        When hotelier sends an OTA_HotelRatePlanRQ All PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an RatePlans element with the HotelCode attribute
        And the PMS response also contains an RatePlan element with the RatePlanCode attribute
        And the PMS response also contains an Rate element with the InvCode attribute
        And the PMS response also contains an BaseByGuestAmt element with the AmountAfterTax attribute

    @pms @rateplanrq @valid @selected
    Scenario: OTA_HotelRatePlanRQ, Valid Request, Selected
        When hotelier sends an OTA_HotelRatePlanRQ Selected PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an RatePlans element with the HotelCode attribute
        And the PMS response also contains an RatePlan element with the RatePlanCode attribute
        And the PMS response also contains an Rate element with the InvCode attribute
        And the PMS response also contains an BaseByGuestAmt element with the AmountAfterTax attribute

    @pms @rateplanrq @valid @all @summaryonly
    Scenario: OTA_HotelRatePlanRQ, Valid Request, All (Summary Only)
        When hotelier sends an OTA_HotelRatePlanRQ All Summary Only PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an RatePlan element with the RatePlanCode attribute
        And the PMS response also contains an Rate element with the InvCode attribute
        And the PMS response also contains an RateDescription element with the Name attribute
        And the PMS response also contains an Description element with the Name attribute

    @pms @rateplanrq @valid @selected @summaryonly
    Scenario: OTA_HotelRatePlanRQ, Valid Request, Selected (Summary Only)
        When hotelier sends an OTA_HotelRatePlanRQ Selected Summary Only PMS request
        Then hotelier receives a PMS response containing a <Success/> element
        And the PMS response also contains an RatePlan element with the RatePlanCode attribute
        And the PMS response also contains an Rate element with the InvCode attribute
        And the PMS response also contains an RateDescription element with the Name attribute
        And the PMS response also contains an Description element with the Name attribute

    @pms @rateplanrq @invalid @client
    Scenario: OTA_HotelRatePlanRQ, Request with Invalid Client
        When hotelier sends an OTA_HotelRatePlanRQ All with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @rateplanrq @invalid @key
    Scenario: OTA_HotelRatePlanRQ, Request with Invalid Key
        When hotelier sends an OTA_HotelRatePlanRQ All with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @rateplanrq @invalid @hotel
    Scenario: OTA_HotelRatePlanRQ, Request with Invalid Hotel
        When hotelier sends an OTA_HotelRatePlanRQ All with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Could not retrieve details for the hotel"

    @pms @rateplanrq @invalid @rateplanrq
    Scenario: OTA_HotelRatePlanRQ, Request with Invalid Rate Plan
        When hotelier sends an OTA_HotelRatePlanRQ Selected with invalid rateplan PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "RatePlanCode Invalid"
