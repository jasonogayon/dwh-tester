Feature: Property Management System, OTA_HotelRateAmountNotifRQ


    @pms @rateamountrq @valid
    Scenario: OTA_HotelRateAmountNotifRQ, Valid Request
        When hotelier sends an OTA_HotelRateAmountNotifRQ PMS request
        Then hotelier receives a PMS response containing a <Success/> element

    @pms @rateamountrq @invalid @client
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Client
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid client PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Bad password"

    @pms @rateamountrq @invalid @key
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Key
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid key PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Security"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "Request is from a source that lacks message privilege"

    @pms @rateamountrq @invalid @rateplan
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Rate Plan
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid rateplan PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Rate plan error"

    @pms @rateamountrq @invalid @room
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Room
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid room PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Rate plan error"

    @pms @rateamountrq @invalid @currency
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Currency
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid currency PMS request
        Then hotelier receives a PMS response containing an OTA_ErrorRS element with the ErrorCode attribute value of "Validation"
        And the PMS response also contains an OTA_ErrorRS element with the ErrorMessage attribute value of "attribute 'CurrencyCode': '0' is not a valid value of the atomic type"

    @pms @rateamountrq @invalid @hotel
    Scenario: OTA_HotelRateAmountNotifRQ, Request with Invalid Hotel
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid hotel PMS request
        Then hotelier receives a PMS response containing an Error element with the Code attribute value of "448"
        And the PMS response also contains an Error element with the ShortText attribute value of "Invalid or missing HotelCode attribute"
