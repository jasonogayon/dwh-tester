include RestHelper
include PayloadHelper
include DataLookup

# Create a New Hotel Property
# ---------------------------

Given(/^a new (.*?) hotel property(?: with (.*?))?$/) do |hotel, information|
  @property = build_default_hotelinfo(hotel)
  step "sales partner creates a new hotel property"
  step "sales partner endorses the newly created hotel property"
  step "guest support searches for the newly created hotel property"
  step "guest support updates the property to GI Activated status"
  information.split(",").each { |info| step "guest support can add #{info.gsub('and', '').strip}" } unless information.nil?
end

When(/^sales partner creates a new hotel property$/) do
  step "sales partner views the E-Form page"
  step "sales partner sends subscription agreement information"
  step "sales partner sends hotel information"
  step "sales partner sends taxes information"
  step "sales partner sends policies information"
  step "sales partner sends images information and checks if hotel property is created"
end

Given(/^an existing (.*?)hotel property(?: with (.*?))?$/) do |hotel, information|
  hotel = "Global Collect" if hotel.nil?
  @property = build_default_hotelinfo(hotel)
  @property.update(Hash[hotel_type: "#{@property[:hotel_type]} Rate Mixing"]) if hotel.include?("Rate Mixing")
  @property.update(Hash[id: property_id.nil? ? get_hotel_id(@property[:hotel_type], @property[:is_asiapay]) : property_id.to_i])
  if use_data == :true || !property_id.nil?
    step "guest support searches for the existing hotel property"
    information.split(",").each { |info| step "guest support can get the hotel's #{info.gsub('and', '').strip} from the hotelier extranet" } unless information.nil?
  else
    step "guest support logs into the Control Panel"
    puts "#{@property[:id].to_s.ljust(10)} #{get_hotel_name(@property[:hotel_type], @property[:is_asiapay])}"
  end
end
