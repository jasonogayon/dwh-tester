# Sales Partner Interface Log-in
# ------------------------------

When(/^sales partner logs on the Sales Partner Interface$/) do
  if browser == :none
    @session = []
    payload = build_login_payload(FigNewton.username_spi, FigNewton.password_spi)
    post(FigNewton.url_cp_login, payload, nil).cookies.each_value { |v| @session.push(v) }
    @headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }
  else
    raise "Not yet implemented using a browser"
  end
end
