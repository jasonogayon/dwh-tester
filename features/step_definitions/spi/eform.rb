# E-Form
# ------

When(/^sales partner views the E-Form page$/) do
  if browser == :none
    @session = []
    begin
      get(FigNewton.url_eform, nil).cookies.each_value { |v| @session.push(v) }
    rescue SocketError
      raise "Error: The site #{FigNewton.url_eform} seems to be unreachable"
    end
    @headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner sends subscription agreement information$/) do
  if browser == :none
    url = FigNewton.url_eform_savecurrent
    payload = build_subscription_agreement_payload(@property)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to send subscription agreement information in E-Form"
    @eform_id = response["data"]["eform_id"]
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner sends hotel information$/) do
  if browser == :none
    url = FigNewton.url_eform_savecurrent
    payload = build_hotelinfo_payload(@eform_id, @property)
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to send hotel information in E-Form"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner sends taxes information$/) do
  if browser == :none
    url = FigNewton.url_eform_savecurrent
    payload = build_taxes_payload(@eform_id, @property)
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to send taxes information in E-Form"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner sends policies information$/) do
  if browser == :none
    url = FigNewton.url_eform_savecurrent
    payload = build_policies_payload(@eform_id, @property)
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to send policies information in E-Form"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner sends images information and checks if hotel property is created$/) do
  if browser == :none
    url = FigNewton.url_eform_savecurrent
    payload = build_images_payload(@eform_id, @property)
    expect(JSON.parse(post(url, payload, @headers))["data"]["completed"]).to be(1), "Error: Failed to create new hotel property via E-Form"
  else
    raise "Not yet implemented using a browser"
  end
end
