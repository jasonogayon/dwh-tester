# Hotel Endorsement
# -----------------

When(/^sales partner endorses the newly created hotel property$/) do
  if browser == :none
    step "sales partner logs on the Sales Partner Interface"
    step "sales partner gets the ID of the newly created hotel property"
    url = FigNewton.url_spi_endorse_hotel
    payload = build_endorsement_payload(@property)
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to endorse the new hotel property"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^sales partner gets the ID of the newly created hotel property$/) do
  if browser == :none
    response = JSON.parse(get(FigNewton.url_spi_get_endorsable_hotels, @headers))
    @property.update(Hash[spi_id: response["data"][0]["id"]])
  else
    raise "Not yet implemented using a browser"
  end
end
