# Impact Marketing Solutions
# --------------------------

When(/^rewards member sends an IMS (.*?) request$/) do |request_type|
  @ims = Hash[url: FigNewton.ims, type: request_type.downcase]
  @social_id = (rand(100000000..900000000)).floor + 1
  @ims.update(Hash[request: build_ims_request(@ims[:type], get_ims_info(@ims[:type], @social_id))])
  step "rewards member retrieves IMS response"
end

When(/^rewards member logs in and sends an IMS (.*?) request$/) do |request_type|
  @ims = Hash[url: FigNewton.ims, type: request_type.downcase]
  type = request_type.include?("change password") ? "change password" : "valid login"
  key = JSON.parse(post(@ims[:url], build_ims_request("valid login", get_ims_info(type, @social_id))))["result"]["key"]
  key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" if request_type.include? "incorrect session key"
  puts "Member successfully logged in. Proceeding to next request ..."
  @ims.update(Hash[request: build_ims_request(@ims[:type], get_ims_info(@ims[:type]), key)])
  step "rewards member retrieves IMS response"
end

Then(/^(?:the )?rewards member can log in via the (.*?) account$/) do |account_type|
  @ims.update(Hash[type: "signin via #{account_type}"])
  @ims.update(Hash[request: build_ims_request(@ims[:type], get_ims_info(@ims[:type], @social_id))])
  step "rewards member retrieves IMS response"
end

Then(/^(?:rewards member receives an IMS response containing|the IMS response also contains)? a "(.*?)" key(?: with a "(.*?)" value)?$/) do |key, value|
  response = @ims[:response]
  expect(response[key]).to eq(value)
end

Then(/^(?:rewards member receives an IMS response containing|the IMS response also contains)? a non-null "(.*?)" field(?: inside "(.*?)")?$/) do |key, parent_key|
  response = @ims[:response]
  expect(parent_key.nil? ? response[key].size : response[parent_key][key].size).to be > 0
end

When(/^rewards member retrieves IMS response$/) do
  begin
    @ims.update(Hash[response: JSON.parse(post(@ims[:url], @ims[:request]))["result"]])
    puts "REQUEST:\n#{@ims[:request]}\n\nRESPONSE:\n#{JSON.pretty_generate(@ims[:response])}"
  rescue JSON::ParserError
    puts "\nREQUEST:\n#{@ims[:request]}\n"
    raise "Response is not a valid JSON. Response is #{@ims[:response].nil? ? "an empty string" : @ims[:response]}"
  end
end
