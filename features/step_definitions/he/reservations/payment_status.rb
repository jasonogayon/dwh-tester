# Mark a Reservation Payment Status as Charged or Pre-Authorized
# --------------------------------------------------------------

Then(/^hotelier can mark the reservation as (?:charged|pre-authorized)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_update_payment_status
    payload = build_rsvsn_id_payload(@property[:reservation][:reservation_id])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to mark the reservation's payment status, #{response}"
    step "hotelier searches for the reservation"
    expect(@property[:reservation][:he_search][0]["payment_status"]).to eq(1)
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier sees that the reservation is already marked as charged$/) do
  if browser == :none
    step "hotelier searches for the reservation"
    expect_payment_status = @property[:reservation][:is_dwh] ? nil : 1
    expect(@property[:reservation][:he_search][0]["payment_status"]).to eq(expect_payment_status)
  else
    raise "Not yet implemented using a browser"
  end
end
