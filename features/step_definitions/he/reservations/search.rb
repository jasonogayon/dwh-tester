# Search Reservation
# ------------------

Then(/^hotelier searches for the reservation$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_search_reservation
    payload = build_search_payload(@property)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to find reservation via search, #{response}"
    @property[:reservation].update(Hash[he_search: response["data"]])
  else
    raise "Not yet implemented using a browser"
  end
end
