# Acknowledge a Reservation
# -------------------------

Then(/^hotelier can acknowledge the reservation$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_acknowledge_reservation
    payload = build_rsvsn_id_payload(@property[:reservation][:reservation_id])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to acknowledge reservation, #{response}"
    step "hotelier searches for the reservation"
    expect(@property[:reservation][:he_search][0]["acknowledged_flag"]).to eq(1)
    expect(@property[:reservation][:he_search][0]["acknowledged_date"]).not_to be(nil)
    expect(@property[:reservation][:he_search][0]["acknowledged_by"]).not_to be(nil)
  else
    raise "Not yet implemented using a browser"
  end
end
