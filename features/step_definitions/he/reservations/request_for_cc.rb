# Request Alternate Credit Card from Guest
# ----------------------------------------

Then(/^hotelier can request for alternate credit card details from the guest$/) do
  if browser == :none
    deadline = get_date("TOMORROW")[0]
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_request_alternate_cc
    payload = build_request_for_alternate_cc_payload(@property[:reservation][:reservation_id], deadline)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to request alternate credit card details for reservation, #{response}"
    step "hotelier searches for the reservation"
    expect(@property[:reservation][:he_search][0]["cc_requested_by"]).not_to be(nil)
    expect(@property[:reservation][:he_search][0]["cc_requested_date"]).not_to be(nil)
    expect(@property[:reservation][:he_search][0]["cc_submission_due_date"]).to eq(deadline)
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier cannot request for alternate credit card details from the guest$/) do
  if browser == :none
    deadline = get_date("TOMORROW")[0]
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_request_alternate_cc
    payload = build_request_for_alternate_cc_payload(@property[:reservation][:reservation_id], deadline)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(false), "Error: Request for alternate credit card details should not have succeeded for this reservation"
    expect(response["error_message"]).to eq("Invalid payment type: Payment type must be Hotel Processed")
  else
    raise "Not yet implemented using a browser"
  end
end
