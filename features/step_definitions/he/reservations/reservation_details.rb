# View Reservation Details
# ------------------------

Then(/^hotelier views the reservation's details log$/) do
  if browser == :none
    url = FigNewton.url_he_get_reservation_logs
    payload = build_rsvsn_details_payload(@property[:id], @property[:reservation][:reservation_id])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to view reservation's details log, #{response}"
    @property[:reservation].update(Hash[he_data: response["data"]])
    @property[:reservation].update(Hash[he_details: response["data"]["logs"]])
  else
    raise "Not yet implemented using a browser"
  end
end

# Amount Remittable
# -----------------

Then(/^hotelier sees that the amount remittable for the reservation is (.*?)$/) do |amount_remittable|
  if browser == :none
    is_nocc = @property[:nocc_sameday] || @property[:nocc_nextday]
    reservation = @property[:reservation]
    log_id = reservation[:confirmation_details][:charge_total_addon_cost].to_i > 0 ? (reservation[:is_confirmed] ? 1 : 2) : (reservation[:is_confirmed] ? 0 : 1)
    log_id = 3 if reservation[:is_dwh] && reservation[:is_noshow] && reservation[:rateplan_details][:is_refundable] && !is_nocc
    details = get_he_rsvsn_details(reservation[:he_details][log_id])
    expect(details[:hotel_remittable].to_f).to eq(amount_remittable.to_f)
    expect(details[:hotel_remittable_usd].to_f).to eq(amount_remittable.to_f)
  else
    raise "Not yet implemented using a browser"
  end
end

# With HTML Tags
# --------------

Then(/^hotelier sees that the reservation rate plan name contains HTML tags$/) do
  if browser == :none
    rateplan = @property[:public_rateplans][0]
    details = get_he_rsvsn_details(@property[:reservation][:he_details][0])
    expect(details[:rateplan_info]).to include(rateplan[:name].gsub("<", "&lt;"))
  else
    raise "Not yet implemented using a browser"
  end
end

# Extra Beds
# ----------

Then(/^hotelier sees that the reservation details includes extra beds information$/) do
  if browser == :none
    expect(JSON.parse(@property[:reservation][:he_details][0]["extra_bed_details"]).size).to be > 0
  else
    raise "Not yet implemented using a browser"
  end
end

# Add-ons
# -------

Then(/^hotelier sees that the reservation details includes add-ons information$/) do
  if browser == :none
    expect(JSON.parse(@property[:reservation][:he_details][0]["add_on_details"])).not_to be("null")
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can modify the included add-ons of the reservation$/) do
  if browser == :none
    url = FigNewton.url_he_modify_addon
    payload = build_modify_addon_payload(@property)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to modify included add-ons in the reservation, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

# Promo Code
# ----------

Then(/^hotelier sees that the reservation details includes promo code information$/) do
  if browser == :none
    step "guest support logs into the Control Panel"
    step "guest support logs into the Hotelier Extranet directly from CP"
    step "hotelier views the reservation's details log"
    expect(JSON.parse(@property[:reservation][:he_details][0]["rate_plan_info"])["promo_code"]).to eq("RESERVATION")
  else
    raise "Not yet implemented using a browser"
  end
end

# Rate Mixing
# -----------

Then(/^the reservation is logged as mixed on the Hotelier Extranet$/) do
  if browser == :none
    step "hotelier views the reservation's details log"
    rsvsn_details = @property[:reservation][:he_data]
    expect(rsvsn_details["is_mixed"]).to eq(1)
    expect(rsvsn_details["mixed_info"]).to eq("{\"with_breakfast\": 0}")
    expect(rsvsn_details["rate_plan_name"]).to eq("Best Rates Without Breakfast")
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

Then(/^credit card details are also not displayed in the reservation details$/) do
  begin
    if browser == :none
      step "guest support logs into the Control Panel"
      step "guest support logs into the Hotelier Extranet directly from CP"
      step "hotelier views the reservation's details log"
      rsvsn_details = @property[:reservation][:he_data]
      expect(rsvsn_details["ccard_name"]).to eq("")
      expect(rsvsn_details["ccard_type"]).to eq("")
      expect(rsvsn_details["ccard_no"]).to eq("")
      expect(rsvsn_details["expiry"]).to eq("")
      expect(rsvsn_details["deposit_amount"]).to eq("0")
      expect(rsvsn_details["balance_amount"]).to eq(rsvsn_details["total_amount"])
      expect(rsvsn_details["payment_type"]).to eq("200")
      expect(rsvsn_details["payment_history"][0]["payment_processor"]).to eq("Hotel Processed - No credit card")
      expect(rsvsn_details["hotel_remittable"]).to eq("0")
      expect(rsvsn_details["show_request_cc"]).to eq(0) unless @property[:reservation][:is_confirmed]
      expect(rsvsn_details["show_request_to_cancel"]).to be(true) if @property[:reservation][:is_confirmed]
      expect(rsvsn_details["hotel_policies"]).to include("\"no_cc_booking_flag\":true")
      expect(rsvsn_details["reservation_policy_description"]).to include("\"prepayment\": \"No prepayment will be charged. Payment for your stay will be made at the hotel.\"")
      expect(rsvsn_details["reservation_policy_description"]).to include("\"modification\": \"Not allowed.\"")
      expect(rsvsn_details["reservation_policy_description"]).to include("\"cancellation\": \"Allowed. Free of charge.\"")
      expect(rsvsn_details["reservation_policy_description"]).to include("\"noshow\": \"\"")
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "guest support disables the no-credit-card booking feature for the property"
  end
end

Then(/^reservation is found in the filtered reservations with no-credit-card guarantee$/) do
  begin
    if browser == :none
      url = FigNewton.url_he_search_reservation
      payload = build_filter_nocc_bookings(@property)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to filter no credit card bookings, #{response}"
      expect(response["data"][0]["confirmation_no"].to_i).to eq(@property[:reservation][:confirmation_details][:confirmation_no].to_i)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "guest support disables the no-credit-card booking feature for the property"
  end
end
