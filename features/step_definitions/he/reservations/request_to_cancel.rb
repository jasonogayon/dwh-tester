# Request a Reservation for Cancellation
# --------------------------------------

Then(/^hotelier can request for the reservation to be cancelled$/) do
  if browser == :none
    remark = "Requesting to cancel reservation for testing purposes."
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_request_to_cancel
    payload = build_request_to_cancel_payload(@property[:reservation][:reservation_id], remark)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to request reservation for cancellation, #{response}"
    step "hotelier views the reservation's details log"
    expect(@property[:reservation][:he_data]["request_to_cancel_flag"]).to be(1)
    expect(@property[:reservation][:he_data]["show_request_to_cancel"]).to be(false)
    expect(@property[:reservation][:he_data]["hotel_remarks"]).to eq(remark)
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier cannot request for the reservation to be cancelled$/) do
  if browser == :none
    reservation_id = @property[:reservation][:reservation_id]
    remark = "Requesting to cancel reservation for testing purposes."
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_request_to_cancel
    payload = build_request_to_cancel_payload(reservation_id, remark)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(false), "Error: Request to cancel should not have succeeded for this reservation"
    expect(response["error_message"]).to eq("This reservation is not under No Prepayment scheme")
  else
    raise "Not yet implemented using a browser"
  end
end
