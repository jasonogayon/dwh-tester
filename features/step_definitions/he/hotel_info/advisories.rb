# Hotel Policies
# --------------

Then(/^guest support can add advisories(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Advisories ------"
    @property[:advisories].each do |advisory|
      step "hotel manager can add an advisory: #{advisory}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add an advisory: (.*?)$/) do |advisory|
  if browser == :none
    advisory = eval(advisory)
    url = FigNewton.url_he_add_advisory
    payload = build_advisory_payload(advisory)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{advisory[:name]} advisory into HE, #{response}"
    step "hotelier can view the advisory: #{advisory[:name]}"
    @property[:advisories].select { |policy| policy[:name] == advisory[:name] }[0].update(Hash[id: @advisory_id])
    @property[:advisories].select { |policy| policy[:name] == advisory[:name] }[0][:new_advisory_details].update(Hash[id: @advisory_id])
    step "hotel manager can update the newly added advisory" if advisory[:to_edit]
    step "hotel manager can remove the newly added advisory" if advisory[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the advisory: (.*?)$/) do |advisory_name|
  if browser == :none
    url = FigNewton.url_he_view_advisories
    advisories = JSON.parse(get(url, @headers))
    expect(advisories["success"]).to be(true), "Error: Failed to get advisories from HE"
    advisories["data"].each do |advisory|
      @advisory_id = advisory["advisory_cd"] if advisory["advisory_name"].include? advisory_name
    end
    puts "#{@advisory_id.ljust(10)} #{advisory_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added advisory$/) do
  if browser == :none
    new_advisory_details = @property[:advisories].select { |policy| policy[:id] == @advisory_id }[0][:new_advisory_details]
    url = FigNewton.url_he_edit_advisory
    payload = build_advisory_payload(new_advisory_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update advisory from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added advisory$/) do
  if browser == :none
    url = FigNewton.url_he_remove_advisory
    payload = build_remove_advisory_payload(@advisory_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete advisory from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's advisories(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_advisories
    advisories = JSON.parse(get(url, @headers))
    expect(advisories["success"]).to be(true), "Error: Failed to view advisories from HE, #{advisories}"
    @property[:advisories] = advisories["data"]
    if !property_id.nil? && !@property[:advisories].empty?
      puts "\n----- Retrieved Hotel Advisories ------"
      @property[:advisories].each { |advisory| puts "#{advisory['advisory_cd'].ljust(10)} #{advisory['advisory_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

Then(/^guest support enables the no-credit-card booking feature for the property$/) do
  step "guest support enables the same-day no-credit-card booking feature for the property"
  step "guest support enables the next-day no-credit-card booking feature for the property"
end

Then(/^guest support enables the all-time no-credit-card booking feature for the property$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_edit_advisory_nocc
    payload = build_no_credit_card_payload(@property, "all-time")
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to enable all-time no-credit-card booking in HE, #{response}"
    @property.update(Hash[nocc_sameday: true, nocc_nextday: true, nocc_alltime: true])
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support enables the same-day no-credit-card booking feature for the property$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_edit_advisory_nocc
    payload = build_no_credit_card_payload(@property, "same-day")
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to enable same-day no-credit-card booking in HE, #{response}"
    @property.update(Hash[nocc_sameday: true])
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support enables the next-day no-credit-card booking feature for the property$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_edit_advisory_nocc
    payload = build_no_credit_card_payload(@property, "next-day")
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to enable next-day no-credit-card booking in HE, #{response}"
    @property.update(Hash[nocc_nextday: true])
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support disables the no-credit-card booking feature for the property$/) do
  if browser == :none
    url = FigNewton.url_he_edit_advisory_nocc
    payload = build_no_credit_card_payload(@property)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to disable no-credit-card booking in HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end
