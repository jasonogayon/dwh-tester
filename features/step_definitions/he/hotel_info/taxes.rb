# Taxes and Surcharges
# --------------------

Then(/^guest support can add taxes (?:and|or)? surcharges(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Taxes and Fees ------"
    @property[:taxes].each do |tax|
      step "hotel manager can add a tax or surcharge: #{tax}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a tax or surcharge: (.*?)$/) do |tax|
  if browser == :none
    tax = eval(tax)
    url = FigNewton.url_he_add_tax_surcharge
    payload = build_tax_payload(tax)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add tax into HE, #{response}"
    step "hotelier can view the tax or surcharge: #{tax[:name]}"
    @property[:taxes].select { |tx| tx[:name] == tax[:name] }[0].update(Hash[id: @tax_id])
    @property[:taxes].select { |tx| tx[:name] == tax[:name] }[0][:new_tax_details].update(Hash[id: @tax_id])
    step "hotel manager can update the newly added tax or surcharge" if tax[:to_edit]
    step "hotel manager can remove the newly added tax or surcharge" if tax[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end
Then(/^hotelier can view the tax or surcharge: (.*?)$/) do |tax_name|
  if browser == :none
    url = FigNewton.url_he_view_taxes_surcharges
    taxes = JSON.parse(get(url, @headers))
    expect(taxes["success"]).to be(true), "Error: Failed to get taxes/surcharges from HE"
    taxes["data"].each do |tax|
      @tax_id = tax["id"] if tax["tax_surcharge_name"].include? tax_name
    end
    puts "#{@tax_id.ljust(10)} #{tax_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added tax or surcharge$/) do
  if browser == :none
    new_tax_details = @property[:taxes].select { |tx| tx[:id] == @tax_id }[0][:new_tax_details]
    url = FigNewton.url_he_edit_taxes_surcharges
    payload = build_tax_payload(new_tax_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update tax from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added tax or surcharge$/) do
  if browser == :none
    url = FigNewton.url_he_remove_tax_surcharge
    payload = build_remove_tax_payload(@tax_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete tax/surcharge from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's taxes(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_taxes_surcharges
    taxes = JSON.parse(get(url, @headers))
    expect(taxes["success"]).to be(true), "Error: Failed to view taxes from HE, #{taxes}"
    @property[:taxes] = taxes["data"]
    if !property_id.nil? && !@property[:taxes].empty?
      puts "\n----- Retrieved Hotel Taxes and Fees ------"
      @property[:taxes].each do |tax|
        tax_type = tax["type_id"].to_i == 1 ? "Tax" : "Surcharge #{tax['taxable_name']}"
        puts "#{tax['id'].ljust(10)} #{tax['tax_surcharge_name']} (#{tax['charge_amount']}% #{tax_type})"
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end
