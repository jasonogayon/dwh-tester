# Property Information
# --------------------

Then(/^guest support can add property information(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_edit_property_info
    payload = build_propertyinfo_payload(@property[:property_info])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add property information into HE, #{response}"
    step "hotelier views the property information"
    puts "\n----- Property Information Updated -----"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier views the property information$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_read_property_info
    response = JSON.parse(post(url, nil, @headers))
    expect(response["success"]).to be(true), "Error: Failed to view property information from HE, #{response}"
    expect(response["data"]["address"]).to eq(@property[:property_info][:address])
    expect(response["data"]["telephone"]).to eq(@property[:property_info][:phone])
    expect(response["data"]["email"]).to eq(@property[:property_info][:email])
    expect(response["data"]["longitude"]).to eq(@property[:property_info][:longitude].to_s)
    expect(response["data"]["latitude"]).to eq(@property[:property_info][:latitude].to_s)
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's property information(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_read_property_info
    response = JSON.parse(post(url, nil, @headers))
    expect(response["success"]).to be(true), "Error: Failed to view property information from HE, #{response}"
    expect(response["data"]["id"].to_i).to eq(@property[:id]), "Error: Property information being retrieved is for a different property, #{response}"
    @property[:property_info].update(Hash[
      address:        response["data"]["address"],
      phone:          response["data"]["telephone"],
      email:          response["data"]["email"],
      longitude:      response["data"]["longitude"].to_f,
      latitude:       response["data"]["latitude"].to_f,
      check_in_time:  response["data"]["check_in_time"].to_i,
      check_out_time: response["data"]["check_out_time"].to_i
    ])
  else
    raise "Not yet implemented using a browser"
  end
end
