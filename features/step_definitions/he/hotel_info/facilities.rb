# Facility
# --------

Then(/^guest support can add facilities(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Facilities ------"
    @property[:facilities].each do |facility|
      step "hotel manager can add a facility: #{facility}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a facility: (.*?)$/) do |facility|
  if browser == :none
    facility = eval(facility)
    url = FigNewton.url_he_add_facility
    payload = build_facility_payload(facility)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{facility} facility into HE, #{response}"
    step "hotelier can view the facility: #{facility[:name]}"
    @property[:facilities].select { |fa| fa[:name] == facility[:name] }[0].update(Hash[id: @facility_id])
    step "hotel manager can remove the newly added facility" if facility[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the facility: (.*?)$/) do |facility_name|
  if browser == :none
    url = FigNewton.url_he_view_facilities
    facilities = JSON.parse(get(url, @headers))
    expect(facilities["success"]).to be(true), "Error: Failed to get facilities information from HE"
    facilities["data"].each do |fa|
      @facility_id = fa["facility_cd"] if fa["facility_name"].include? facility_name
    end
    puts "#{@facility_id.ljust(10)} #{facility_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added facility$/) do
  if browser == :none
    url = FigNewton.url_he_remove_facility
    payload = build_remove_facility_payload(@facility_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete facility from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's facilities(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_facilities
    facilities = JSON.parse(get(url, @headers))
    expect(facilities["success"]).to be(true), "Error: Failed to view facilities from HE, #{facilities}"
    @property[:facilities] = facilities["data"]
    if !property_id.nil? && !@property[:facilities].empty?
      puts "\n----- Retrieved Hotel Facilities ------"
      @property[:facilities].each { |facility| puts "#{facility['facility_cd'].ljust(10)} #{facility['facility_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
