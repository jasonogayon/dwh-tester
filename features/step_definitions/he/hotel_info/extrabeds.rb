# Extra Bed
# ---------

Then(/^guest support can add extra bed information(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    @property[:extrabeds].update(Hash[room_standard: @property[:rooms].select { |rm| rm[:name].downcase.include?("standard") }[0][:id]])
    @property[:extrabeds].update(Hash[room_deluxe: @property[:rooms].select { |rm| rm[:name].downcase.include?("deluxe") }[0][:id]])
    url = FigNewton.url_he_edit_extrabed
    payload = build_extrabed_payload(@property[:id], @property[:extrabeds])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add extra bed information into HE, #{response}"
    puts "\n----- Extra Beds Information Updated -----"
  else
    raise "Not yet implemented using a browser"
  end
end
