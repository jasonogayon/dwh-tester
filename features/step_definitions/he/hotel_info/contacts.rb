# Contacts
# --------

Then(/^guest support can add contacts(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Contacts ------"
    @property[:contacts].each do |contact|
      step "hotel manager can add a contact: #{contact}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a contact: (.*?)?$/) do |contact|
  if browser == :none
    contact = eval(contact)
    url = FigNewton.url_he_add_contact
    payload = build_contact_payload(contact)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add contact into HE, #{response}"
    step "hotelier can view the contact: #{contact[:name]}"
    @property[:contacts].select { |ct| ct[:name] == contact[:name] }[0].update(Hash[id: @contact_id])
    @property[:contacts].select { |ct| ct[:name] == contact[:name] }[0][:new_contact_details].update(Hash[id: @contact_id])
    step "hotel manager can update the newly added contact" if contact[:to_edit]
    step "hotel manager can remove the newly added contact" if contact[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the contact: (.*?)$/) do |contact_name|
  if browser == :none
    url = FigNewton.url_he_view_contacts
    contacts = JSON.parse(get(url, @headers))
    expect(contacts["success"]).to be(true), "Error: Failed to get contact information from HE"
    contacts["data"].each do |contact|
      @contact_id = contact["contact_cd"] if contact["contact_name"].include? contact_name
    end
    puts "#{@contact_id.ljust(10)} #{contact_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added contact$/) do
  if browser == :none
    new_contact_details = @property[:contacts].select { |rm| rm[:id] == @contact_id }[0][:new_contact_details]
    url = FigNewton.url_he_edit_contact
    payload = build_contact_payload(new_contact_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update contact from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added contact$/) do
  if browser == :none
    url = FigNewton.url_he_remove_contact
    payload = build_remove_contact_payload(@contact_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete contact from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's contacts(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_contacts
    contacts = JSON.parse(get(url, @headers))
    expect(contacts["success"]).to be(true), "Error: Failed to view contacts from HE, #{contacts}"
    @property[:contacts] = contacts["data"]
    if !property_id.nil? && !@property[:contacts].empty?
      puts "\n----- Retrieved Hotel Contacts ------"
      @property[:contacts].each { |contact| puts "#{contact['contact_cd'].ljust(10)} #{contact['contact_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
