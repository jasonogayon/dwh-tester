# Amenity
# -------

Then(/^guest support can add amenities(?: )?(?:into the hotelier extranet|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Amenities ------"
    @property[:amenities].each do |amenity|
      step "hotel manager can add an amenity: #{amenity}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add an amenity: (.*?)$/) do |amenity|
  if browser == :none
    amenity = eval(amenity)
    url = FigNewton.url_he_add_amenity
    payload = build_amenity_payload(amenity)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{amenity} amenity into HE, #{response}"
    step "hotelier can view the amenity: #{amenity[:name]}"
    @property[:amenities].select { |am| am[:name] == amenity[:name] }[0].update(Hash[id: @amenity_id])
    step "hotel manager can remove the newly added amenity" if amenity[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the amenity: (.*?)$/) do |amenity_name|
  if browser == :none
    url = FigNewton.url_he_view_amenities
    amenities = JSON.parse(get(url, @headers))
    expect(amenities["success"]).to be(true), "Error: Failed to get amenities information from HE"
    amenities["data"].each do |am|
      @amenity_id = am["amenity_cd"] if am["amenity_name"].include? amenity_name
    end
    puts "#{@amenity_id.ljust(10)} #{amenity_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added amenity$/) do
  if browser == :none
    url = FigNewton.url_he_remove_amenity
    payload = build_remove_amenity_payload(@amenity_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete amenity from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's amenities(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_amenities
    amenities = JSON.parse(get(url, @headers))
    expect(amenities["success"]).to be(true), "Error: Failed to view amenities from HE, #{amenities}"
    @property[:amenities] = amenities["data"]
    if !property_id.nil? && !@property[:amenities].empty?
      puts "\n----- Retrieved Hotel Amenities ------"
      @property[:amenities].each { |amenity| puts "#{amenity['amenity_cd'].ljust(10)} #{amenity['amenity_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
