# Room
# ----

Then(/^guest support can add rooms(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Rooms ------"
    @property[:rooms].each do |room|
      step "hotel manager can add a room: #{room}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a room: (.*?)$/) do |room|
  if browser == :none
    room = eval(room)
    url = FigNewton.url_he_add_room
    payload = build_room_payload(room)
    response = post_multipart(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{room[:name]} room into HE, #{response}"
    step "hotelier can view the room: #{room[:name]}"
    @property[:rooms].select { |rm| rm[:name] == room[:name] }[0].update(Hash[id: @room_id])
    @property[:rooms].select { |rm| rm[:name] == room[:name] }[0][:new_room_details].update(Hash[id: @room_id])
    step "hotel manager can update the newly added room" if room[:to_edit]
    step "hotel manager can remove the newly added room" if room[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the room: (.*?)$/) do |room_name|
  if browser == :none
    url = FigNewton.url_he_view_rooms
    rooms = JSON.parse(get(url, @headers))
    expect(rooms["success"]).to be(true), "Error: Failed to get room information from HE"
    rooms["data"].each do |room|
      @room_id = room["room_cd"] if room["room_name"].include? room_name
    end
    puts "#{@room_id.ljust(10)} #{room_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added room$/) do
  if browser == :none
    new_room_details = @property[:rooms].select { |rm| rm[:id] == @room_id }[0][:new_room_details]
    url = FigNewton.url_he_edit_room
    payload = build_room_edit_payload(@room_id, new_room_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update room from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added room$/) do
  if browser == :none
    url = FigNewton.url_he_remove_room
    payload = build_remove_room_payload(@room_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete room from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager adds rooms if there aren't any$/) do
  if browser == :none
    url = FigNewton.url_he_view_rooms
    rooms = JSON.parse(get(url, @headers))
    expect(rooms["success"]).to be(true), "Error: Failed to get room information from HE"
    step "guest support can add rooms into the new property" if rooms["total"].to_i < 1
    @property[:rooms] = rooms["data"] if rooms["total"].to_i > 0
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's rooms(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_rooms
    rooms = JSON.parse(get(url, @headers))
    expect(rooms["success"]).to be(true), "Error: Failed to view rooms from HE, #{rooms}"
    @property[:rooms] = rooms["data"]
    if !property_id.nil? && !@property[:rooms].empty?
      puts "\n----- Retrieved Hotel Rooms ------"
      @property[:rooms].each { |room| puts "#{room['room_cd'].ljust(10)} #{room['room_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
