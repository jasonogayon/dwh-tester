# Add-ons
# -------

Then(/^guest support can add add-ons(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Reservation Add-ons ------"
    @property[:addons].each do |addon|
      step "hotel manager can add an add-on: #{addon}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add an add-on: (.*?)$/) do |addon|
  if browser == :none
    addon = eval(addon)
    url = FigNewton.url_he_add_addon
    payload = build_addon_payload(addon)
    response = post_multipart(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{addon[:name]} addon into HE, #{response}"
    step "hotelier can view the add-on: #{addon[:name]}"
    @property[:addons].select { |ao| ao[:name] == addon[:name] }[0].update(Hash[id: @addon_id])
    @property[:addons].select { |ao| ao[:name] == addon[:name] }[0][:new_addon_details].update(Hash[id: @addon_id])
    step "hotel manager can update the newly added add-on" if addon[:to_edit]
    step "hotel manager can remove the newly added add-on" if addon[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the add-on: (.*?)$/) do |addon_name|
  if browser == :none
    url = FigNewton.url_he_view_addons
    addons = JSON.parse(get(url, @headers))
    expect(addons["success"]).to be(true), "Error: Failed to get addons information from HE"
    addons["data"].each do |addon|
      @addon_id = addon["id"] if addon["name"].include? addon_name
    end
    puts "#{@addon_id.to_s.ljust(10)} #{addon_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added add-on$/) do
  if browser == :none
    new_addon_details = @property[:addons].select { |ao| ao[:id] == @addon_id }[0][:new_addon_details]
    url = FigNewton.url_he_edit_addon
    payload = build_addon_payload(new_addon_details)
    response = post_multipart(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update add-on from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added add-on$/) do
  if browser == :none
    url = FigNewton.url_he_remove_addon
    payload = build_remove_addon_payload(@addon_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete add-on from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's add-ons(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_addons
    addons = JSON.parse(get(url, @headers))
    expect(addons["success"]).to be(true), "Error: Failed to view add-ons from HE, #{addons}"
    @property[:addons] = addons["data"]
    if !property_id.nil? && !@property[:addons].empty?
      puts "\n----- Retrieved Hotel Add-ons ------"
      @property[:addons].each { |addon| puts "#{addon['id'].to_s.ljust(10)} #{addon['name']} (#{@property[:currency_code]} #{addon['rate']})" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
