# Private Accounts
# ----------------

Then(/^guest support can add private accounts(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel Private Accounts ------"
    @property[:private_accounts].each do |account|
      step "hotel manager can add a private account: #{account}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a private account: (.*?)$/) do |account|
  if browser == :none
    account = eval(account)
    url = FigNewton.url_he_add_private_account
    payload = build_privateaccount_payload(account)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{account[:name]} private account into HE, #{response}"
    step "hotelier can view the private account: #{account[:name]}"
    @property[:private_accounts].select { |act| act[:name] == account[:name] }[0].update(Hash[id: @account_id])
    @property[:private_accounts].select { |act| act[:name] == account[:name] }[0][:new_account_details].update(Hash[id: @account_id])
    step "hotel manager can update the newly added private account" if account[:to_edit]
    step "hotel manager can remove the newly added private account" if account[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the private account: (.*?)$/) do |account_name|
  if browser == :none
    url = FigNewton.url_he_view_private_accounts
    private_accounts = JSON.parse(get(url, @headers))
    expect(private_accounts["success"]).to be(true), "Error: Failed to get private accounts from HE"
    private_accounts["data"].each do |account|
      @account_id = account["id"] if account["account_name"].include? account_name
    end
    puts "#{@account_id.to_s.ljust(10)} #{account_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added private account$/) do
  if browser == :none
    new_account_details = @property[:private_accounts].select { |act| act[:id] == @account_id }[0][:new_account_details]
    url = FigNewton.url_he_edit_private_account
    payload = build_privateaccount_edit_payload(new_account_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update account from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added private account$/) do
  if browser == :none
    url = FigNewton.url_he_remove_private_account
    payload = build_remove_privateaccount_payload(@account_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete account from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager adds private accounts if there aren't any$/) do
  if browser == :none
    url = FigNewton.url_he_view_private_accounts
    private_accounts = JSON.parse(get(url, @headers))
    expect(private_accounts["success"]).to be(true), "Error: Failed to get private_accounts from HE"
    if private_accounts["total"].to_i < 1
      step "guest support can add private accounts into the new property"
      private_accounts = JSON.parse(get(url, @headers))
    end
    @property[:private_accounts] = private_accounts["data"]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's private accounts(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_private_accounts
    private_accounts = JSON.parse(get(url, @headers))
    expect(private_accounts["success"]).to be(true), "Error: Failed to view private accounts from HE, #{private_accounts}"
    @property[:private_accounts] = private_accounts["data"]
    if !property_id.nil? && !@property[:private_accounts].empty?
      puts "\n----- Retrieved Hotel Private Accounts ------"
      @property[:private_accounts].each { |account| puts "#{account['id'].to_s.ljust(10)} #{account['account_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
