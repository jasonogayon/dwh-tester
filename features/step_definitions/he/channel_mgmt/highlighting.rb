# Rate Plan Highlighting
# ----------------------

When(/^hotel manager highlights a (.*?) rate plan$/) do |rateplan|
  if browser == :none
    @old_rateplan = get_rateplan_id(@property[:hotel_type], rateplan)
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_highlight_rateplan
    payload = build_enable_highlight_payload(@old_rateplan)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to highlight #{rateplan} rate plan, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^(?:hotel manager )?changes the highlight to a (.*?) rate plan$/) do |rateplan|
  if browser == :none
    @new_rateplan = get_rateplan_id(@property[:hotel_type], rateplan)
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_highlight_rateplan
    payload = build_update_highlight_payload(@new_rateplan, @old_rateplan)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to change the highlight to #{rateplan} rate plan, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^(?:hotel manager )?disables the highlighting(?: immediately afterwards)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_remove_highlight
    payload = build_disable_highlight_payload(@old_rateplan)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to remove highlighting, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end
