# Update Room Availability
# ------------------------

When(/^hotelier updates the availability of the room to (.*?) for check-in dates from (.*?) to (.*?)$/) do |availability, start_date, end_date|
  begin
    if browser == :none
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date)[0], end_date: get_date(end_date)[0]])
      url = FigNewton.url_he_set_availability
      payload = build_availability_payload(room[:id], room[:start_date], room[:end_date], availability)
      response = post(url, payload, @headers)
      expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to set room availability, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^hotelier cannot update the availability of the room to a value of (.*?)(?: for check-in dates from (.*?) to (.*?))?$/) do |availability, start_date, end_date|
  begin
    if browser == :none
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date ||= "TODAY")[0], end_date: get_date(end_date ||= "3 DAYS FROM NOW")[0]])
      url = FigNewton.url_he_set_availability
      payload = build_availability_payload(room[:id], room[:start_date], room[:end_date], availability)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(false), "Error: Hotelier is able to set room availability to a negative value, #{response}"
      expect(response["error_message"]).to eq("Please input value between 0 to 999")
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^hotelier sees that the room availability for those dates has been set to (.*?) after the update$/) do |expected_availability|
  begin
    if browser == :none
      room = @property[:rooms][0]
      url = FigNewton.url_he_get_availability
      payload = build_view_availability_payload(room[:start_date], room[:end_date])
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to get room availability, #{response}"
      for i in 0..(response["data"].length - 1)
        availability_data = response["data"][i] if response["data"][i]["room_id"].include? room[:id]
      end
      for i in 0..(Date.parse(room[:end_date]) - Date.parse(room[:start_date])).to_i
        date = (Date.parse(room[:start_date]) + i).to_s.delete("-")
        expect(availability_data[date]).to eq(expected_availability.to_i)
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end
