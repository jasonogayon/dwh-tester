# Update Room Rates
# -----------------

When(/^hotelier updates the rate plan room rates to (.*?) for check-in dates from (.*?) to (.*?)$/) do |room_rate, start_date, end_date|
  begin
    if browser == :none
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date)[0], end_date: get_date(end_date)[0], rate: room_rate])
      url = FigNewton.url_he_set_rates
      payload = build_rates_payload(@property[:public_rateplans][0][:id], room[:id], room[:start_date], room[:end_date], room[:rate])
      response = post(url, payload, @headers)
      expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to set room rate, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^hotelier cannot update the rate plan room rate to a value of (.*?)(?: for check-in dates from (.*?) to (.*?))?$/) do |room_rate, start_date, end_date|
  begin
    if browser == :none
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date ||= "TODAY")[0], end_date: get_date(end_date ||= "3 DAYS FROM NOW")[0], rate: room_rate])
      url = FigNewton.url_he_set_rates
      payload = build_rates_payload(@property[:public_rateplans][0][:id], room[:id], room[:start_date], room[:end_date], room[:rate])
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(false), "Error: Hotelier is able to set room rate to a value of #{room_rate}, #{response}"
      expect(response["error_message"]).to eq("Please enter rates that are greater than 0.") if room_rate.to_i.zero?
      expect(response["error_message"]).to eq("Failed to set Rates") if room_rate.to_f > 1_000_000_000_000
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^hotelier sees that the room rates for those dates has been set to (.*?) after the update$/) do |expected_rate|
  begin
    if browser == :none
      room = @property[:rooms][0]
      url = FigNewton.url_he_get_rates
      payload = build_view_rates_payload(room[:start_date], room[:end_date])
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to get room rates, #{response}"
      for i in 0..(response["data"].length - 1)
        rates_data = response["data"][i] if response["data"][i]["room_id"] == room[:id].to_i
      end
      for i in 0..(Date.parse(room[:end_date]) - Date.parse(room[:start_date])).to_i
        date = (Date.parse(room[:start_date]) + i).to_s.delete("-")
        expect(rates_data[date]).to eq(expected_rate)
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
  end
end


# Extract Room Rates
# -----------------

When(/^hotel manager extract room rates from a (.*?) rate plan(?: for check-in dates from (.*?) to (.*?))?$/) do |rateplan_type, start_date, end_date|
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    checkin_date = get_date(start_date ||= "TODAY")[0]
    checkout_date = get_date(end_date ||= "TOMORROW")[0]
    rateplan_id = get_rateplan_id(@property[:hotel_type], rateplan_type)

    url = FigNewton.url_he_extract_rates
    payload = build_extract_rates_payload(rateplan_id, checkin_date, checkout_date)
    puts response = post(url, payload, @headers)
    expect(response).to include("\"Hotel Name\",")
    expect(response).to include("\"Currency\",")
    expect(response).to include("\"Rate Plan Name\",")
    expect(response).to include("\"Rate Plan ID\",\"#{rateplan_id}\"")
    expect(response).to include("\"Rate Plan Type\",\"Nightly Rates\"")
    expect(response).to include("\"Date Range\",\"#{checkin_date} - #{checkout_date}\"")
    expect(response).to include("\"Room type\",\"#{checkin_date}\",\"#{checkout_date}\"")
    expect(response).to include("\"Accommodation-Based Room Rates\",\"1\",\"1\"")
    expect(response).to include("\"Occupancy-Based Room Rates (up to 1 person)\",\"5\",\"5\"")
    expect(response).to include("\"Occupancy-Based Room Rates (up to 2 persons)\",\"4\",\"4\"")
    expect(response).to include("\"Occupancy-Based Room Rates (up to 3 persons)\",\"3\",\"3\"")
    expect(response).to include("\"Occupancy-Based Room Rates (up to 4 persons)\",\"2\",\"2\"")
    expect(response).to include("\"Occupancy-Based Room Rates (up to 5 persons)\",\"1\",\"1\"")
  else
    raise "Not yet implemented using a browser"
  end
end
