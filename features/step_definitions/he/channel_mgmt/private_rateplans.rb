include DatesHelper

# Create a Private Rate Plan
# --------------------------

When(/^(?:guest support |hotel manager )?adds a (.*?)private rate plan(?: )?(?:into the existing property|too)?$/) do |rateplan_type|
  rateplan_alias = rateplan_type.gsub(Regexp.union(["Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only", "HTML"]), "").strip
  rateplan = @property[:private_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
  rateplan.update(Hash[name: "New #{rateplan[:name]} No. #{rand(1000)}"])
  @property[:private_rateplans] = Array[rateplan]
  step "guest support can add #{rateplan_type}private rate plans"
  step "guest support adds the private rate plan to a private account"
end

Then(/^guest support can add (.*?)private rate plans(?: )?(?:into the new property|too)?$/) do |rateplan_type|
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    step "hotel manager adds rooms if there aren't any"
    step "hotel manager adds private accounts if there aren't any"
    puts "\n----- Creating #{rateplan_type}Private Rate Plan(s) ------"
    @property[:private_rateplans].each do |rateplan|
      rateplan.update(Hash[details: get_rateplan_data(@property, rateplan)])
      step "hotel manager can add a private rate plan: #{rateplan}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a private rate plan: (.*?)$/) do |rateplan|
  if browser == :none
    rateplan = eval(rateplan)
    url = FigNewton.url_he_create_rateplan_private
    payload = build_private_rateplan_payload(rateplan[:details])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{rateplan[:name]} private rate plan into HE, #{response}"
    step "hotelier can view the private rate plan: #{rateplan[:name]}"
    @property[:private_rateplans].select { |rp| rp[:name] == rateplan[:name] }[0].update(Hash[id: @rateplan_id])
    @property[:private_rateplans].select { |rp| rp[:name] == rateplan[:name] }[0][:new_rateplan_details].update(Hash[id: @rateplan_id])
    step "hotel manager can update the newly added private rate plan" if rateplan[:to_edit]
    step "hotel manager can remove the newly added private rate plan" if rateplan[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the private rate plan: (.*?)$/) do |rateplan_name|
  if browser == :none
    url = FigNewton.url_he_view_private_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to get private rateplans from HE"
    rateplans["data"].each do |rateplan|
      @rateplan_id = rateplan["id"] if rateplan["rate_plan_name"].include? rateplan_name
    end
    puts "#{@rateplan_id.to_s.ljust(10)} #{rateplan_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added private rate plan$/) do
  if browser == :none
    # To-Do: rate plan update is divided into different categories, namely: details, booking conditions, rates, policies, inclusions, and private accounts
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added private rate plan$/) do
  if browser == :none
    url = FigNewton.url_he_delete_rateplan_private
    payload = build_delete_private_rateplan_payload(@rateplan_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete private rate plan from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's private rate plans(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_private_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to view private rate plans from HE, #{rateplans}"
    @property[:private_rateplans] = rateplans["data"]
    if !property_id.nil? && !@property[:private_rateplans].empty?
      puts "\n----- Retrieved Hotel Private Rate Plans ------"
      @property[:private_rateplans].each { |rateplan| puts "#{rateplan['id'].to_s.ljust(10)} #{rateplan['rate_plan_name']}" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^(?:guest support |hotel manager )?adds the private rate plan to a private account$/) do
  if browser == :none
    account = @property[:private_accounts].select { |act| act["account_name"] =~ /Privados/ }[0]
    url = FigNewton.url_he_add_rateplan_to_private_account
    payload = build_privateaccount_rateplan_payload(@rateplan_id, account["id"])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to bind rate plan to private account, #{response}"

    # Get Private Account Code
    url = FigNewton.url_he_view_private_accounts
    payload = build_get_privateaccount_info_payload(account["id"])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to get private account information, #{response}"
    account.update(Hash[code: response["data"]["account_code_lists"][0]["account_code"]])
  else
    raise "Not yet implemented using a browser"
  end
end

# Utility
# -------

Then(/^hotel manager can (?:also )?delete all unnecessary private rate plans from test hotel$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_private_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to get private rateplans from HE"
    rateplans["data"].each do |rateplan|
      @rateplan_id = rateplan["rate_plan_name"].include?("New") && rateplan["rate_plan_name"].include?("No.") ? rateplan["id"] : nil
      unless @rateplan_id.nil?
        step "hotel manager can remove the newly added private rate plan"
        puts "#{@rateplan_id.to_s.ljust(10)} #{rateplan['rate_plan_name']}"
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end
