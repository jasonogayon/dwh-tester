# Add Room to Rate Plan
# ---------------------

When(/^hotel manager can add rooms to the child rate plan via the Derived Rate Plans Calendar$/) do
  begin
    if browser == :none
      url = FigNewton.url_he_add_room_to_rateplan
      payload = build_add_room_to_child_rateplan_via_calendar_payload(@rateplan_id)
      response = post(url, payload, @headers)
      expect(response.to_s).not_to include("500 Internal Server Error"), "Error: 500 Internal Server Error"
      expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to get list of available rooms to add for child rate plan, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end
