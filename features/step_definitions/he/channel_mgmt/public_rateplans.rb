include DatesHelper

# Create a Public Rate Plan
# -------------------------

When(/^(?:guest support |hotel manager )?(?:also )?adds a (.*?)public rate plan(?: )?(?:with a promo code of "(.*?)" )?(?:into the existing property|too)?$/) do |rateplan_type, promocode|
  rateplan_alias = rateplan_type.gsub(Regexp.union(["Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only", "HTML"]), "").strip
  rateplan = @property[:public_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
  rateplan.update(Hash[name: "New #{rateplan[:name]} No. #{rand(1000)}"])
  rateplan.update(Hash[name: "I <3 DAD! No. #{rand(1000)}"]) if rateplan_type.include? "HTML"
  rateplan.update(Hash[promo_code: promocode]) unless promocode.nil?
  @property[:public_rateplans] = Array[rateplan]
  step "guest support can add #{rateplan_type}public rate plans"
end

When(/^(?:guest support |hotel manager )?adds a(?: "(.*?)" promo)? (.*?)child rate plan(?: )?(?:with (.*?) policy )?(?:with a promo code of "(.*?)" )?(?:into the existing property|too)?$/) do |promo, rateplan_type, policy, promocode|
  rateplan_alias = rateplan_type.gsub(Regexp.union(["Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only", "HTML"]), "").strip
  rateplan = @property[:public_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
  rateplan.update(Hash[name: "New #{rateplan[:name]} No. #{rand(1000)}"])
  rateplan.update(Hash[promo_code: promocode]) unless promocode.nil?
  rateplan.update(Hash[
    is_parent:          false,
    inherits_policy:    policy.nil? || policy.include?("custom") ? false : true,
    discount_type:      promo.nil? ? nil : promo.include?("%") ? "percent" : "fixed amount",
    discount_amount:    promo.nil? ? nil : promo.split("discount")[0].gsub(Regexp.union(["%", "USD", "PHP"]), "").strip,
    promo_type:         promo.nil? ? "Limited Offer" : promo.split("discount")[1].strip
  ])
  @property[:public_rateplans] = Array[rateplan]
  step "guest support can add #{rateplan_type}public rate plans"
end

Then(/^guest support can add (.*?)public rate plans(?: )?(?:into the new property|too)?$/) do |rateplan_type|
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    step "hotel manager adds rooms if there aren't any"
    puts "\n----- Creating #{rateplan_type}Public Rate Plan(s) ------"
    @property[:public_rateplans].each do |rateplan|
      rateplan.update(Hash[details: get_rateplan_data(@property, rateplan, rateplan_type ||= "Nightly")])
      step "hotel manager can add a public rate plan: #{rateplan}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a public rate plan: (.*?)$/) do |rateplan|
  if browser == :none
    rateplan = eval(rateplan)
    url = FigNewton.url_he_create_rateplan
    payload = rateplan[:is_parent] ? build_parent_rateplan_payload(rateplan[:details]) : build_child_rateplan_payload(rateplan[:details])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{rateplan[:name]} rate plan into HE, #{response}"
    step "hotelier can view the public rate plan: #{rateplan[:name]}"
    @property[:public_rateplans].select { |rp| rp[:name] == rateplan[:name] }[0].update(Hash[id: @rateplan_id])
    @property[:public_rateplans].select { |rp| rp[:name] == rateplan[:name] }[0][:new_rateplan_details].update(Hash[id: @rateplan_id])
    step "hotel manager can update the newly added public rate plan" if rateplan[:to_edit]
    step "hotel manager can remove the newly added public rate plan" if rateplan[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the public rate plan: (.*?)$/) do |rateplan_name|
  if browser == :none
    url = FigNewton.url_he_view_public_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to get rateplans from HE"
    rateplans["data"].each do |rateplan|
      @rateplan_id = rateplan["id"] if rateplan["rate_plan_name"].include? rateplan_name
    end
    puts "#{@rateplan_id.to_s.ljust(10)} #{rateplan_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added public rate plan$/) do
  if browser == :none
    # To-Do: rate plan update is divided into different categories, namely: details, photos, booking conditions, rates, extra beds, policies, and inclusions/add-ons
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added public rate plan$/) do
  if browser == :none
    url = FigNewton.url_he_delete_rateplan
    payload = build_delete_rateplan_payload(@rateplan_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete rate plan from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's public rate plans(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_public_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to view public rate plans from HE, #{rateplans}"
    @property[:public_rateplans] = rateplans["data"]
    if !property_id.nil? && !@property[:public_rateplans].empty?
      puts "\n----- Retrieved Hotel Public Rate Plans ------"
      @property[:public_rateplans].each do |rateplan|
        rateplan_info = ""
        rateplan_info += " Inactive" if rateplan["is_published"].zero?
        rateplan_info += " Derived" unless rateplan["parent_rate_plan_id"].nil?
        rateplan_info += " Promo Code: #{rateplan['promo_code']}" unless rateplan["promo_code"].nil?
        rateplan_info = "(" + rateplan_info.strip + ")" if rateplan_info != ""
        puts "#{rateplan['id'].to_s.ljust(10)} #{rateplan['rate_plan_name']} #{rateplan_info}"
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager adds a "(.*?)" room with availability of (.*?) into a (.*?) rate plan?$/) do |room_name, availability, rateplan|
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    @property[:public_rateplans] = [get_rateplan(@property, rateplan)]
    room = @property[:rooms].select { |rm| rm[:name].include?(room_name) }[0]
    room.update(Hash[inventory: availability, availability: availability])
    room.update(Hash[name: "New #{room[:name]} No. #{rand(1000)}"])
    step "hotel manager can add a room: #{room}"
    @property[:rooms] = [room]
    url = FigNewton.url_he_edit_rateplan_rooms
    payload = build_add_room_to_rateplan_payload(room[:id], get_rateplan_id(@property[:hotel_type], rateplan))
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add #{room_name} room to #{rateplan} rate plan, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager removes the room from rate plan$/) do
  if browser == :none
    url = FigNewton.url_he_delete_rateplan_rooms
    payload = build_remove_room_from_rateplan_payload(@property[:rooms][0][:id], @property[:public_rateplans][0][:id])
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to remove room from rate plan, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

# With Extra Beds
# ---------------

When(/^(?:guest support |hotel manager )?adds a (.*?)public rate plan(?: )?with extra beds (?:with a promo code of "(.*?)" )?(?:into the existing property|too)?$/) do |rateplan_type, promocode|
  rateplan_alias = rateplan_type.gsub(Regexp.union(["Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only", "HTML"]), "").strip
  rateplan = @property[:public_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
  rateplan.update(Hash[name: "New #{rateplan[:name]} No. #{rand(1000)}", with_extrabeds: true])
  rateplan.update(Hash[promo_code: promocode]) unless promocode.nil?
  @property[:public_rateplans] = Array[rateplan]
  step "guest support can add #{rateplan_type}public rate plans"
end

When(/^hotel manager can remove the extra bed setting from the rate plan$/) do
  begin
    if browser == :none
      url = FigNewton.url_he_edit_rateplan_extrabed
      payload = build_remove_extrabed_from_rateplan_payload(@property)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to remove extra bed setting from rate plan, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

# With Add-ons
# ------------

When(/^(?:guest support |hotel manager )?adds a (.*?)public rate plan(?: )?with add-ons (?:with a promo code of "(.*?)" )?(?:into the existing property|too)?$/) do |rateplan_type, promocode|
  rateplan_alias = rateplan_type.gsub(Regexp.union(["Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only", "HTML"]), "").strip
  rateplan = @property[:public_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
  rateplan.update(Hash[name: "New #{rateplan[:name]} No. #{rand(1000)}", with_addons: true])
  rateplan.update(Hash[promo_code: promocode]) unless promocode.nil?
  @property[:public_rateplans] = Array[rateplan]
  step "guest support can add #{rateplan_type}public rate plans"
end

When(/^hotel manager can remove the add-on setting from the rate plan$/) do
  begin
    if browser == :none
      url = FigNewton.url_he_edit_rateplan_addons
      payload = build_remove_addon_from_rateplan_payload(@property)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to remove add-ons from rate plan, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

# With Promo Code
# ---------------

Then(/^hotel manager cannot create a new rate plan with the same promo code$/) do
  rateplan = @property[:public_rateplans][0]
  new_rateplan_name = rateplan[:name].split("No.")[0] + "No. #{rand(1000)}"
  rateplan[:details].update(Hash[name: new_rateplan_name, rp_name: new_rateplan_name])
  begin
    if browser == :none
      url = FigNewton.url_he_create_rateplan
      payload = build_parent_rateplan_payload(rateplan[:details])
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(false), "Error: Hotelier is able to create a rate plan using an existing promo code, #{response}"
      expect(response["error_message"]).to eq("Promo Code already exist!")
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotelier can view the public rate plan: #{rateplan[:details][:name]}"
    step "hotel manager can remove the newly added public rate plan"
    rateplan[:details].update(Hash[name: rateplan[:name], rp_name: rateplan[:name]])
  end
end

Then(/^hotel manager can change the rate plan's promo code to "(.*?)"$/) do |promocode|
  rateplan = @property[:public_rateplans][0]
  begin
    if browser == :none
      step "hotelier can view the public rate plan: #{rateplan[:name]}"
      url = FigNewton.url_he_edit_bookingcondition
      payload = build_edit_promocode_payload(rateplan, promocode)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Unable to change rate plan promo code from #{rateplan[:promo_code]} to #{promocode}, #{response}"
      rateplan.update(Hash[promo_code: promocode])
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

Then(/^hotel manager can delete the rate plan's promo code$/) do
  rateplan = @property[:public_rateplans][0]
  if browser == :none
    url = FigNewton.url_he_edit_bookingcondition
    payload = build_edit_promocode_payload(rateplan, nil)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Unable to delete rate plan promo code, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

Then(/^hotelier disables the no-credit-card booking feature for the (.*?) rate plan$/) do |rateplan_type|
  if browser == :none
    rateplan = get_rateplan(@property, rateplan_type)
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_read_rateplan
    payload = "id=#{rateplan[:id]}"
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Unable to delete rate plan promo code, #{response}"
    rateplan.update(Hash[policies: response["data"]["policies"]])
    url = FigNewton.url_he_edit_rateplan_policies
    payload = build_edit_policies_payload(rateplan)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Hotelier is unable to disable the no-credit-card booking setting for the #{rateplan_type} rate plan, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

# Rate Mixing
# -----------

Then(/^stop sells the rate plan for dates from (.*?) to (.*?)$/) do |start_date, end_date|
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    rateplan = @property[:public_rateplans][0]
    url = FigNewton.url_he_set_rates
    payload = build_stopsell_payload(rateplan[:id], rateplan[:details][:room_id], get_date(start_date)[0], get_date(end_date)[0], 1)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Hotelier is unable to stop sell the rate plan for the desired date range, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

# Utility
# -------

Then(/^hotel manager can delete all unnecessary public rate plans from test hotel$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_public_rateplans
    rateplans = JSON.parse(get(url, @headers))
    expect(rateplans["success"]).to be(true), "Error: Failed to get rateplans from HE"
    rateplans["data"].each do |rateplan|
      @rateplan_id = rateplan["rate_plan_name"].include?("New") && rateplan["rate_plan_name"].include?("No.") ? rateplan["id"] : nil
      unless @rateplan_id.nil?
        step "hotel manager can remove the newly added public rate plan"
        puts "#{@rateplan_id.to_s.ljust(10)} #{rateplan['rate_plan_name']}"
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^admin can create public and private rate plans on a desired hotel property$/) do
  step "admin checks what hotel type the property is"
  @property[:public_rateplans].each do |rp|
    @property[:public_rateplans] = get_public_rateplans("Global Collect", true) if @property[:payment_scheme] == 1
    if @property[:payment_scheme] == 2
      @property[:public_rateplans] = filter_rateplans(@property, get_public_rateplans("HPP", false))
    end
    step "hotel manager adds a #{rp[:alias][1]} public rate plan into the existing property" unless rp[:name].include? "Test"
  end
  if @property[:privateaccount_enabled]
    @property[:private_rateplans].each do |rp|
      @property[:private_rateplans] = get_private_rateplans("Global Collect", true) if @property[:payment_scheme] == 1
      if @property[:payment_scheme] == 2
        @property[:private_rateplans] = filter_rateplans(@property, get_private_rateplans("HPP", false))
      end
      step "hotel manager adds a #{rp[:alias][1]} private rate plan into the existing property" unless rp[:name].include? "Test"
    end
  end
end

Then(/^admin can create child rate plans on a desired parent$/) do
  step "admin checks what hotel type the property is"
  @property[:public_rateplans].each do |rp|
    @property[:public_rateplans] = get_public_rateplans("Global Collect", true) if @property[:payment_scheme] == 1
    if @property[:payment_scheme] == 2
      @property[:public_rateplans] = filter_rateplans(@property, get_public_rateplans("HPP", false))
    end
    step "hotel manager adds a #{rp[:alias][1]} child rate plan into the existing property" unless rp[:name].include? "Test"
  end
end
