# Update Room Stop Sell Status
# ----------------------------

When(/^hotelier stops selling the room for check-in dates from (.*?) to (.*?)$/) do |start_date, end_date|
  begin
    if browser == :none
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date)[0], end_date: get_date(end_date)[0], stopsell: 1])
      url = FigNewton.url_he_set_rates
      payload = build_stopsell_payload(@property[:public_rateplans][0][:id], room[:id], room[:start_date], room[:end_date], room[:stopsell])
      response = post(url, payload, @headers)
      expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to stop sell the room, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

When(/^hotelier removes the room stops sell for check-in dates from (.*?) to (.*?)$/) do |start_date, end_date|
  begin
    if browser == :none
      step "hotelier stops selling the room for check-in dates from #{start_date} to #{end_date}"
      room = @property[:rooms][0]
      room.update(Hash[start_date: get_date(start_date)[0], end_date: get_date(end_date)[0], stopsell: 0])
      url = FigNewton.url_he_set_rates
      payload = build_stopsell_payload(@property[:public_rateplans][0][:id], room[:id], room[:start_date], room[:end_date], room[:stopsell])
      response = post(url, payload, @headers)
      expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to remove the room stop sell, #{response}"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^hotelier sees that the room is (?:not )?marked as stop sell after the update$/) do
  begin
    room = @property[:rooms][0]
    payload = build_view_stopsell_payload(room[:start_date], room[:end_date])
    response = JSON.parse(post(FigNewton.url_he_get_rates, payload, @headers))
    expect(response["success"]).to be(true)
    for i in 0..(response["data"].length - 1)
      rates_data = response["data"][i] if response["data"][i]["room_id"] == room[:id].to_i
    end
    for i in 0..(Date.parse(room[:end_date]) - Date.parse(room[:start_date])).to_i
      date = (Date.parse(room[:start_date]) + i).to_s.delete("-")
      expect(rates_data["#{date}stopsell"]).to eq(room[:stopsell])
    end
  rescue
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end
