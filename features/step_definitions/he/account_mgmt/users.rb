# Users
# -----

Then(/^guest support can add users(?: )?(?:into the new property|too)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    puts "\n----- Creating Hotel User Accounts ------"
    @property[:users].each do |user|
      user.update(Hash[property_id: @property[:id]])
      step "hotel manager can add a user: #{user}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can add a user: (.*?)$/) do |user|
  if browser == :none
    user = eval(user)
    url = FigNewton.url_he_add_user
    payload = build_user_payload(user)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to add a user into HE, #{response}"
    step "hotelier can view the user: #{user[:name]}"
    @property[:users].select { |us| us[:name] == user[:name] }[0].update(Hash[id: @user_id])
    @property[:users].select { |us| us[:name] == user[:name] }[0][:new_user_details].update(Hash[id: @user_id])
    step "hotel manager can update the newly added user" if user[:to_edit]
    step "hotel manager can remove the newly added user" if user[:to_delete]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotelier can view the user: (.*?)$/) do |user_name|
  if browser == :none
    url = FigNewton.url_he_view_users
    users = JSON.parse(get(url, @headers))
    expect(users["success"]).to be(true), "Error: Failed to get users from HE"
    users["data"].each do |user|
      @user_id = user["user_id"] if user["user_full_name"].include? user_name
    end
    puts "#{@user_id.ljust(10)} #{user_name}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can update the newly added user$/) do
  if browser == :none
    new_user_details = @property[:users].select { |us| us[:id] == @user_id }[0][:new_user_details]
    url = FigNewton.url_he_edit_user
    payload = build_user_payload(new_user_details)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to update user from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^hotel manager can remove the newly added user$/) do
  if browser == :none
    url = FigNewton.url_he_remove_user
    payload = build_remove_user_payload(@user_id)
    response = post(url, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to delete user from HE, #{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can get the hotel's users(?: from the hotelier extranet)?$/) do
  if browser == :none
    step "guest support logs into the Hotelier Extranet directly from CP"
    url = FigNewton.url_he_view_users
    users = JSON.parse(get(url, @headers))
    expect(users["success"]).to be(true), "Error: Failed to view users from HE, #{users}"
    @property[:users] = users["data"]
    if !property_id.nil? && !@property[:users].empty?
      puts "\n----- Retrieved Hotel Users ------"
      @property[:users].each { |user| puts "#{user['user_id'].ljust(10)} #{user['user_full_name']} (#{user['role_name']} | #{user['user_name']})" }
    end
  else
    raise "Not yet implemented using a browser"
  end
end
