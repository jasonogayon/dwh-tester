include RestHelper
include PayloadHelper
include DataLookup
include DatesHelper

# Hotel Extranet Log-in
# ---------------------

Then(/^hotelier logs into the Hotelier Extranet$/) do
  if browser == :none
    @session = []
    payload = build_login_payload(get_username(@property[:hotel_type]), get_password(@property[:hotel_type]))
    response = post(FigNewton.url_he_login, payload, @headers)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to log into Hotelier Extranet, #{response}"
    response.cookies.each_value { |v| @session.push(v) }
    @headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support logs into the Hotelier Extranet directly from CP$/) do
  if browser == :none
    url = FigNewton.url_he_login_from_cp
    payload = build_login_from_cp_payload(@property[:id])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to log into Hotelier Extranet directly via CP, #{response}"
    begin
      get(response["data"], @headers)
    rescue URI::InvalidURIError => err
      if FigNewton.environment == "local"
        get("http://local.hi.com/home", @headers)
      else
        raise err
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end
