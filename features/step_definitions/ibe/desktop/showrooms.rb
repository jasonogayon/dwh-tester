include ShowRoomsHelper

# Step 2: ShowRooms Page
# ----------------------

Then(/^guest sees that the (.*?) rate plan is (.*?) in the desktop IBE$/) do |rateplan, availability_status|
  is_private = rateplan.downcase.include? "private"
  begin
    if browser == :none
      @rsvsn_headers = @headers
      rateplan = is_private ? @property[:private_rateplans][0] : @property[:public_rateplans][0]
      arrival, departure = "10 DAYS FROM NOW", "12 DAYS FROM NOW" if @property[:nocc_alltime]
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      step "guest adds private promo code to IBE session" if is_private
      step "guest checks that the #{rateplan} rate plan is #{availability_status} in the desktop IBE"
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    # visibility = is_private ? "private" : "public"
    # step "hotel manager can remove the newly added #{visibility} rate plan" if custom_rateplan_id.nil?
  end
end

Then(/^guest checks that the (.*?) rate plan is (.*?) in the desktop IBE$/) do |rateplan, availability_status|
  if browser == :none
    notification = "Is your rate plan active? Or does it need a promo code?"
    @session = []
    url = FigNewton.url_ibe_showrooms % {
      hotel_id:       @property[:id],
      rateplan_id:    eval(rateplan)[:id],
      start_date:     @property[:reservation][:arrival][0],
      end_date:       @property[:reservation][:departure][0],
      rsvsn_id:       @property[:reservation][:reservation_id],
      rsvsn_hash:     @property[:reservation][:reservation_hash],
      user_id:        @property[:reservation][:user_id]
    }
    @property[:reservation].update(Hash[desktop_showrooms_url: url])
    request = Hash[method: :get, url: url, headers: @rsvsn_headers]
    if availability_status =~ /not available|unavailable/
      begin RestClient::Request.execute(request)
      rescue RestClient::ExceptionWithResponse => err
        expect(err.response.code).to eq(302), "Error: Rate plan is displayed in showRooms page when it shouldn't, #{err}"
      end
    else
      begin
        tries ||= 5
        response = RestClient::Request.execute(request)
        expect(response.code).to eq(200)
        response.cookies.each_value { |v| @session.push(v) }
        @rsvsn_headers ||= { "Cookie" => "PHPSESSID=#{@session[0]}" }
      rescue RestClient::Found => err
        raise "Error: Missing rate plan in the showRooms page: #{err}\n#{notification}\n#{url}"
      rescue RestClient::Exceptions::ReadTimeout => err
        (tries -= 1).zero? ? raise(err) : retry
      end
    end
    @property[:reservation].update(Hash[desktop_showrooms_url: url])
    if @property[:reservation][:is_custom] && custom_promocode.nil?
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], nil))
      room = doc.at("//*[contains(@class,'light roomName')][contains(@data-occupancy,'0')]")
      raise "Error: Unable to find an accommodation room type for the rate plan.\nOccupancy rooms not yet supported for custom reservations.\n#{notification}\n#{url}" if room.nil?
      @property[:reservation][:rooms] = [Hash(room_0_id: room["data-room-id"], room_0_qty: rand(1..3), room_0_occ: 0)]
    end
  else
    raise "Not yet implemented using a browser"
  end
end

# Child Rate Plans
# ----------------

Then(/^guest sees that the child rate plan is marked as a (.*?) promo on the desktop IBE$/) do |promo_type|
  begin
    if browser == :none
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], @rsvsn_headers))
      promo_banner = get_page_content(doc, "//*[@class='orange-text']").downcase
      expect(promo_banner).to eq("early booking promotion!") if promo_type.include? "Advanced Purchase"
      expect(promo_banner).to eq("last minute deal!") if promo_type.include? "Last Minute"
      expect(promo_banner).to eq("flash deal!") if promo_type.include? "Flash Deal"
      expect(promo_banner).to eq("limited offer!") if promo_type.include? "Limited Offer"
      rateplan = @property[:public_rateplans][0]
      unless rateplan[:discount_amount].nil?
        promo_banner_discount = get_page_content(doc, "//*[@class='orange-text']/following::p[1]")
        discount = rateplan[:discount_type].include?("percent") ? "#{rateplan[:discount_amount]}%" : "#{@property[:currency_code]} #{rateplan[:discount_amount]}"
        expect(promo_banner_discount).to eq("Get up to #{discount} discount if you reserve NOW") if rateplan[:discount_type].include?("percent")
        expect(promo_banner_discount).to eq("Get up to #{discount} discount per night if you reserve NOW") if rateplan[:discount_type].include?("fixed amount")
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Rate Plan Highlighting
# ----------------------

Then(/^guest sees that the (.*?) rate plan is highlighted on the desktop IBE$/) do |rateplan_type|
  if browser == :none
    rateplan = get_rateplan(@property, rateplan_type)
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    puts url = @property[:reservation][:desktop_showrooms_url]
    doc = Nokogiri::HTML(get(url, @rsvsn_headers))
    expect(get_page_content(doc, "//*[contains(@class,'valuedeal-text')]")).to eq("Value deal!")
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^(?:guest sees that )?the (.*?) rate plan is not highlighted on the desktop IBE$/) do |rateplan_type|
  if browser == :none
    rateplan = get_rateplan(@property, rateplan_type)
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    puts url = @property[:reservation][:desktop_showrooms_url]
    doc = Nokogiri::HTML(get(url, @rsvsn_headers))
    expect(get_page_content(doc, "//*[@class='valuedeal']")).to be(nil)
  else
    raise "Not yet implemented using a browser"
  end
end

# Extra Beds
# ----------

Then(/^guest sees that the extra beds dropdown is displayed on the desktop IBE rate plan page$/) do
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      step "guest sees that the public rate plan is available in the desktop IBE"
      puts url = FigNewton.url_ibe_showrooms_promo % {
        hotel_id:       @property[:id],
        start_date:     @property[:reservation][:arrival][0],
        end_date:       @property[:reservation][:departure][0],
        rateplan_id:    rateplan[:id],
        promo_code:     rateplan[:promo_code]
      }
      doc = Nokogiri::HTML(get(url, @rsvsn_headers))
      expect(get_page_content(doc, "//*[@class='extra-bed-dropdown']")).to eq("Need an extra bed?")
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

Then(/^guest sees that the extra beds dropdown is not displayed anymore on the desktop IBE rate plan page afterwards$/) do
  begin
    if browser == :none
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], @rsvsn_headers))
      expect(get_page_content(doc, "//*[@class='extra-bed-dropdown']")).to be(nil)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Rate Plan Promo Code
# --------------------

Then(/^guest sees that the rate plan is marked as an exclusive promo on both desktop and mobile IBEs$/) do
  step "guest sees that the rate plan is marked as an exclusive promo on the desktop IBE"
  step "guest sees that the rate plan is marked as an exclusive promo on the mobile IBE"
end

Then(/^guest sees that the rate plan is marked as an exclusive promo on the desktop IBE$/) do
  begin
    if browser == :none
      notification = "Is your rate plan active? Or does it need a promo code?"
      rateplan = @property[:public_rateplans][0]
      rateplan.update(Hash[id: custom_rateplan_id]) unless custom_rateplan_id.nil?
      rateplan.update(Hash[promo_code: custom_promocode])  unless custom_promocode.nil?
      step "guest sees that the public rate plan is available in the desktop IBE"
      puts url = FigNewton.url_ibe_showrooms_promo % {
        hotel_id:       @property[:id],
        start_date:     @property[:reservation][:arrival][0],
        end_date:       @property[:reservation][:departure][0],
        rateplan_id:    rateplan[:id],
        promo_code:     rateplan[:promo_code]
      }
      doc = Nokogiri::HTML(get(url, @rsvsn_headers))
      is_nocc = @property[:nocc_sameday] || @property[:nocc_nextday]
      promo_banner = is_nocc ? "//*[@class=' public_promo_code ']" : "//*[@class='public_promo_code_no_cc']"
      expected_banner = "Exclusive for promo code: #{rateplan[:promo_code].upcase}"
      expect(get_page_content(doc, promo_banner)).to eq(expected_banner)
      @property[:reservation].update(Hash[desktop_showrooms_promo_url: url])
      if @property[:reservation][:is_custom] && !custom_promocode.nil?
        doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_promo_url], nil))
        room = doc.at("//*[contains(@class,'light roomName')][contains(@data-occupancy,'0')]")
        raise "Error: Unable to find an accommodation room type for the rate plan.\nOccupancy rooms not yet supported for custom reservations.\n#{notification}\n#{url}" if room.nil?
        @property[:reservation][:rooms] = [Hash(room_0_id: room["data-room-id"], room_0_qty: rand(1..3), room_0_occ: 0)]
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

Then(/^guest cannot see the rate plan if promo code is not inputted when viewing the rate plan page$/) do
  begin
    if browser == :none
      puts url = @property[:reservation][:desktop_showrooms_url]
      doc = Nokogiri::HTML(get(url, nil))
      expect(get_page_content(doc, "//*[@class='public_promo_code']")).to be(nil)
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Room Availability
# -----------------

Then(/^guest sees that the room availability in the desktop IBE (?:also )?shows (.*?)$/) do |expected_availability|
  begin
    if browser == :none
      room = @property[:rooms][0]
      rateplan = @property[:public_rateplans][0]
      step "guest sees that the public rate plan is available in the desktop IBE"
      puts @property
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], nil))
      expect(doc.at("//select[@id='room_#{rateplan[:id]}_#{room[:id]}_0']")["data-available"]).to eq(expected_availability)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

Then(/^guest sees that the room is not available on the desktop IBE$/) do
  begin
    if browser == :none
      room = @property[:rooms][0]
      rateplan = @property[:public_rateplans][0]
      step "guest sees that the public rate plan is available in the desktop IBE"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], nil))
      expect(doc.at("//select[@id='room_#{rateplan[:id]}_#{room[:id]}_0']")).to be(nil)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

# Room Rates
# ----------

Then(/^guest sees that the room rate per night on the desktop IBE (?:also )shows (.*?)$/) do |expected_rate|
  begin
    if browser == :none
      room = @property[:rooms][0]
      rateplan = @property[:public_rateplans][0]
      step "guest sees that the public rate plan is available in the desktop IBE"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], nil))
      expect(get_page_content(doc, "//tr[contains(@data-rateplan-id,'#{rateplan[:id]}')][contains(@data-room-id,'#{room[:id]}')]//div[@class='price-per-night-incl']/span[@class='convert']").gsub(",", "").gsub(".00", "")).to eq(expected_rate)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager removes the room from rate plan"
    step "hotel manager can remove the newly added room"
  end
end

# Google CPC Test (Only for Production)
# -------------------------------------

Then(/^guest sees that the Montebello Villa Hotel rate plans are available on the desktop IBE$/) do
  if browser == :none
    if FigNewton.environment == "production"
      puts url = FigNewton.url_ibe_showrooms_googlecpc % {
        hotel_id:   14_674,
        start_date: get_date("TODAY")[0],
        end_date:   get_date("TOMORROW")[0]
      }
      request = Hash[method: :get, url: url, headers: @rsvsn_headers, max_redirects: 0]
      begin
        tries ||= 5
        response = RestClient::Request.execute(request)
        expect(response.code).to eq(200)
      rescue RestClient::Found => err
        raise "Error: Missing rate plans in the showRooms page: #{err}"
      rescue RestClient::Exceptions::ReadTimeout => err
        (tries -= 1).zero? ? raise(err) : retry
      end
    else
      raise "Google CPC test not applicable to test server"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

Then(/^guest sees that the rate plan is marked as a no-credit-card promo on the desktop IBE$/) do
  begin
    if browser == :none
      step "guest sees that the public rate plan is available in the desktop IBE"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_showrooms_url], nil))
      expect(get_page_content(doc, "//div[@class='nocc_block']")).to eq("Reserve without a credit card")
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
    step "guest support disables the no-credit-card booking feature for the property"
  end
end

# Rate Mixing
# -----------

Then(/^guest sees that a "(.*?)" mixed rate plan is displayed on the (.*?) IBE for stay dates from (.*?) to (.*?)$/) do |mixed_name, platform, start_date, end_date|
  begin
    if browser == :none
      rateplan = get_rateplan(@property, "Public Full Prepayment Non-Refundable")
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, start_date, end_date)])
      @property[:reservation].update(Hash[rateplan_id: "\"mixed_wob\""])
      @session = []
      puts url = FigNewton.url_ibe_showrooms % {
        rateplan_id:    0,
        hotel_id:       @property[:id],
        start_date:     @property[:reservation][:arrival][0],
        end_date:       @property[:reservation][:departure][0],
        rsvsn_id:       @property[:reservation][:reservation_id],
        rsvsn_hash:     @property[:reservation][:reservation_hash],
        user_id:        @property[:reservation][:user_id]
      }
      request = Hash[method: :get, url: url, headers: @rsvsn_headers, max_redirects: 0]
      begin
        tries ||= 5
        response = RestClient::Request.execute(request)
        expect(response.code).to eq(200)
        response.cookies.each_value { |v| @session.push(v) }
        @rsvsn_headers ||= { "Cookie" => "PHPSESSID=#{@session[0]}" }
      rescue RestClient::Found => err
        raise "Error: Mixed rate plan missing in the showRooms page: #{err}\n#{notification}\n#{url}"
      rescue RestClient::Exceptions::ReadTimeout => err
        (tries -= 1).zero? ? raise(err) : retry
      end
      @rsvsn_headers.update(Hash["User-Agent" => FigNewton.mobile_useragent]) if platform.include? "mobile"
      doc = Nokogiri::HTML(get(url, @rsvsn_headers))
      if platform.include? "mobile"
        room_id = doc.search("//div[contains(@class,'go-to-rp-page')]").attribute("id")
        @property[:reservation][:rooms] = [Hash(room_0_id: room_id, room_0_qty: 1, room_0_occ: 0, rp_name: mixed_name)]
      else
        expect(get_page_content(doc, "//a[@class='rpname']")).to eq(mixed_name)
        expect(get_page_content(doc, "//*[contains(@class,'valuedeal-text')]")).to eq("Recommended for you")
        room = doc.at("//*[contains(@class,'roomName')][contains(@data-occupancy,'0')]")
        @property[:reservation][:rooms] = [Hash(room_0_id: room["data-room-id"], room_0_qty: 1, room_0_occ: 0, rp_name: mixed_name)]
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "guest support can get the hotel's public rate plans"
    @property[:public_rateplans].each do |rp|
      @rateplan_id = rp["id"]
      step "hotel manager can remove the newly added public rate plan"
    end
  end
end

# Best Price Guarantee Copy
# -------------------------

When(/^guest checks the best price guarantee link in the rate plan page$/) do
  if browser == :none
    rateplan_type ||= @property[:hotel_type].include?("DWH") ? "Public Full Prepayment Non-Refundable" : "Public Pay upon Arrival with Lead Time Penalty"
    rateplan = get_rateplan(@property, rateplan_type, "1 Accommodation", "")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:reservation].update(Hash[payment_type: "credit card"])
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    response = get(@property[:reservation][:desktop_showrooms_url], nil)

    @copy = get_bpg_copies(response)
  else
    raise "Not yet implemented using a browser"
  end
end