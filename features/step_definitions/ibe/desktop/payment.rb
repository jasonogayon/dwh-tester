include BDOPaymentHelper
include AsiapayPaymentHelper
include PaypalPaymentHelper
include PaymentPageHelper

# Step 4: Payment Page
# --------------------

When(/^guest pays for the desktop IBE reservation$/) do
  if browser == :none
    reservation = @property[:reservation]
    if reservation[:is_paypal]
      response = step "guest pays for the Paypal reservation"
    else
      url = FigNewton.url_ibe_confirmreservation % { hotel_id: @property[:id], rsvsn_id: reservation[:reservation_id] }
      payload = build_payment_payload(@property)
      rateplan_name = @property[:reservation][:rooms][0][:rp_name]
      payload = build_base_payment_payload(@property) if !rateplan_name.nil? && rateplan_name.include?("Best Rates Without Breakfast")
      begin
        tries ||= 10
        begin
          tries ||= 3
          response = post(url, payload, @rsvsn_headers)
        rescue
          retry unless (tries -= 1).zero?
        end
        if @property[:property_name].include?("Webdriver -A")
          pay_comp = post(FigNewton.url_ibe_ap_paycomp, build_ap_payload(get_ap_payment_details(response)), @rsvsn_headers)
          page_redirect = post(FigNewton.url_ibe_ap_pageredirect, build_ap_redirect_payload(get_ap_redirect_details(pay_comp)), @rsvsn_headers)
          response = get_without_redirect(page_redirect[:location], @rsvsn_headers)
        end
        if (reservation[:is_dwh] || reservation[:is_hpp]) && !@property[:property_name].include?("Webdriver -A")
          payment_error = "Error: Reservation payment did not receive a response after several tries. Please retry later."
          begin
            raise payment_error if response[:location].nil?
          rescue TypeError
            raise payment_error
          end
          payment_retry = 0
          while response[:location].include?("/pay/")
            response = post(url, payload, @rsvsn_headers)
            payment_retry += 1
            sleep 5
            break if payment_retry == 10
          end
        end
      rescue RestClient::ServerBrokeConnection
        (tries -= 1).zero? ? raise("Error: Unable to proceed with reservation payment") : retry
      end
      if reservation[:is_bdo]
        vpcpay = post(FigNewton.url_ibe_bdo_vpcpay, build_vpcpay_payload(get_bdo_payment_details(response)), @rsvsn_headers)
        vpcpay_o = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + vpcpay[:location], "Cookie" => "#{vpcpay[:setcookie][0]}; #{vpcpay[:setcookie][1]}; #{vpcpay[:setcookie][2]}")
        ssl_headers = { "Cookie" => "#{vpcpay_o[:setcookie][0]}; #{vpcpay_o[:setcookie][1]}; #{vpcpay_o[:setcookie][2]}; #{vpcpay[:setcookie][0]}" }
        ssl_session = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + vpcpay_o[:location], ssl_headers)
        ssl_payment = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + ssl_session[:location], ssl_headers)
        response = get_without_redirect(ssl_payment[:location], @rsvsn_headers)
      end
    end
    expect(response[:location]).not_to be(nil), "Error: Desktop IBE reservation failed to push through"
    expect(response[:location]).not_to include("paymentError"), "Error: Unable to process reservation payment"
    reservation.update(Hash[desktop_confirmation_url: response[:location]])
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest pays for the Paypal reservation$/) do
  if browser == :none
    reservation = @property[:reservation]
    phpsessionid = @rsvsn_headers["Cookie"]

    # Get Paypal Express Checkout Token
    url = FigNewton.url_ibe_paypal_expresscheckout % { hotel_id: @property[:id], auth_key: reservation[:auth_key] }
    payload = build_payment_public_payload(@property)
    response = post(url, payload, @rsvsn_headers)
    token = response.split("&token=")[1].gsub("\"}", "")
    expect(token).to include("EC-"), "Error: Paypal Express Checkout token should start with EC-"

    # Go to Redirect Login Page
    url = FigNewton.url_ibe_paypal_hermes % { token: token }
    @rsvsn_headers = @rsvsn_headers.update(Hash["Cookie" => FigNewton.paypal_session_hermes % { token: token, email: FigNewton.paypal_email }])
    redirect_login = get(url, @rsvsn_headers)
    expect(get_page_content(Nokogiri::HTML(redirect_login), "//title")).to eq("Pay with a PayPal account - PayPal")
    cookies = get_paypal_redirect_login_cookies(phpsessionid, redirect_login)

    # Redirect to Merchant Web page
    paypal_details = get_paypal_payment_details(redirect_login)
    url = FigNewton.url_ibe_paypal_merchantweb % { session: paypal_details[:session], dispatch: paypal_details[:dispatch] }
    redirect_merchant = get(url, @rsvsn_headers.update(Hash["Cookie" => FigNewton.paypal_session_merchantweb % cookies]))
    expect(get_page_content(Nokogiri::HTML(redirect_merchant), "//title")).to eq("Pay with a PayPal account - PayPal")
    cookies = cookies.update(xpps: redirect_merchant.cookies["x-pp-s"], xppsilover: redirect_merchant.cookies["X-PP-SILOVER"])

    # Login to Paypal
    url = FigNewton.url_ibe_paypal_login % { dispatch: paypal_details[:dispatch] }
    payload = build_paypal_login_payload(paypal_details)
    account_login = post(url, payload, @rsvsn_headers.update(Hash["Cookie" => FigNewton.paypal_session_merchantweb % cookies]))
    expect(account_login).to include("Review your information"), "Error: Unable to log in to Paypal"
    cookies = get_paypal_account_login_cookies(phpsessionid, redirect_login, account_login)

    # Paypal Payment
    payload = build_paypal_payment_payload(paypal_details, account_login)
    begin
      tries ||= 10
      sleep 10
      response = post(url, payload, @rsvsn_headers.update(Hash["Cookie" => FigNewton.paypal_session_payment % cookies]))
      payment_retry = 0
      until response.include?("confirmPayPalPayment")
        response = post(url, payload, @rsvsn_headers.update(Hash["Cookie" => FigNewton.paypal_session_payment % cookies]))
        payment_retry += 1
        break if payment_retry == 10
      end
    rescue RestClient::ServerBrokeConnection
      (tries -= 1).zero? ? raise("Error: Unable to process Paypal payment") : retry
    end
    raise("Error: Unable to process Paypal payment") unless response.include?("confirmPayPalPayment")

    # Get Confirmation Page URL
    url = FigNewton.url_ibe_paypal_confirm % { hotel_id: @property[:id], token: token, payer_id: FigNewton.paypal_payer_id }
    get_without_redirect(url, "Cookie" => phpsessionid)
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest views the desktop IBE payment page(?: for a (.*?) rate plan)?(?: for (.*?) payment)?$/) do |rateplan_type, _payment_type|
  if browser == :none
    rateplan_type ||= @property[:hotel_type].include?("DWH") ? "Public Full Prepayment Non-Refundable" : "Public Pay upon Arrival with Lead Time Penalty"
    rateplan = get_rateplan(@property, rateplan_type, "1 Accommodation", "")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:reservation].update(Hash[payment_type: "credit card"])
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    step "guest goes to the desktop IBE payment information page"
    @property[:reservation].update(Hash[desktop_payment_url: FigNewton.url_ibe_pay % { hotel_id: @property[:id], rsvsn_id: @property[:reservation][:auth_key] }])
  else
    raise "Not yet implemented using a browser"
  end
end

# Copies and CTA
# --------------

When(/^guest reads the (.*?) copy(?: for a (.*?) rate plan)?(?: for (.*?) payment)?$/) do |copy_type, rateplan_type, payment_type|
  if browser == :none
    rateplan_type ||= @property[:hotel_type].include?("DWH") ? "Public Full Prepayment Non-Refundable" : "Public Pay upon Arrival with Lead Time Penalty"
    rateplan = get_rateplan(@property, rateplan_type, "1 Accommodation", "")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:reservation].update(Hash[payment_type: "credit card"])
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    step "guest goes to the desktop IBE payment information page"
    @property[:reservation].update(Hash[desktop_payment_url: FigNewton.url_ibe_pay % { hotel_id: @property[:id], rsvsn_id: @property[:reservation][:auth_key] }])
    pay = get_pay_details(@property)
    @copy = pay[:digicert] if copy_type.include? "digicert"
    @copy = pay[:no_prepay] if copy_type.include? "no prepay"
    @copy = pay[:preauth] if copy_type.include? "pre-auth"
    @copy = pay[:cc_fraud] if copy_type.include? "fraud"
    @copy = pay[:cc_cvv] if copy_type.include? "cvv"
    @copy = pay[:cc_security] if copy_type.include? "security"
    @copy = (rateplan[:is_private] && payment_type.include?("account") ? pay[:terms_account] : pay[:terms]) if copy_type.include? "terms"
    @copy = pay[:rsvsn_provider] if copy_type.include? "provider"
    @copy = pay[:request] if copy_type.include? "request"
    @copy = pay[:cookie] if copy_type.include? "cookie"

    # Check CTA while in the page
    if @property[:hotel_type].include? "Paypal"
      expect(pay[:cta_public]).to eq(nil), "Error: btnconfirmbooking CTA displayed when it shouldn't"
      expect(pay[:cta_paypal_cc]).not_to eq(nil), "Error: Missing btnCheckoutPaypalCC CTA"
      expect(pay[:cta_paypal]).not_to eq(nil), "Error: Missing btnCheckoutPaypal CTA"
    else
      expect(pay[:cta_public]).not_to eq(nil), "Error: Missing btnconfirmbooking CTA"
      expect(pay[:cta_paypal_cc]).to eq(nil), "Error: btnCheckoutPaypalCC CTA displayed when it shouldn't"
      expect(pay[:cta_paypal]).to eq(nil), "Error: btnCheckoutPaypal CTA displayed when it shouldn't"
    end
    expect(pay[:cta_account]).to eq(nil), "Error: pa-cc-btnconfirmbooking CTA displayed when it shouldn't"
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

When(/^guest sees that the credit card form is (?:not )?displayed$/) do
  if browser == :none
    doc = Nokogiri::HTML(get(@property[:reservation][:desktop_payment_url], @rsvsn_headers))
    if @property[:nocc_sameday] || @property[:nocc_nextday]
      begin
        nocc_form = get_page_content(doc, "//*[@class='no-cc-form']/div[@class='row-fluid pay-instruction']")
        expect(nocc_form).to include("Secure this booking without a credit card")
        expect(nocc_form).to include("Payment for your stay will be made at the hotel.")
        expect(doc.at("//div[@class='choose-payment-balloon']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_type']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_number']")).to be(nil)
        expect(doc.at("//*[@id='opt_yes']")).to be(nil)
        expect(doc.at("//*[@id='opt_no']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_name']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_exp_month']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_exp_year']")).to be(nil)
        expect(doc.at("//*[@id='payment_cc_cvv']")).to be(nil)
        expect(doc.at("//*[@id='sec_cc']")).to be(nil)
        expect(doc.at("//*[@id='fraud_cc']")).to be(nil)
        expect(doc.at("//*[@id='securecode']")).to be(nil)
      ensure
        step "guest support disables the no-credit-card booking feature for the property"
      end
    else
      withcc_form = get_page_content(doc, "//div[@class='choose-payment-balloon']")
      expect(withcc_form).to include("Choose your payment method")
    end
  else
    raise "Not yet implemented using a browser"
  end
end
