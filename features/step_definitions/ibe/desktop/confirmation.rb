# Step 5: Confirmation Page
# -------------------------

When(/^guest is redirected to the desktop IBE confirmation page$/) do
  if browser == :none
    reservation = @property[:reservation]
    puts confirmation_page = reservation[:desktop_confirmation_url]
    reservation.update(Hash[reservation_id: confirmation_page.split("/")[6], reservation_hash: confirmation_page.split("/")[7]])
    if reservation[:is_onhold]
      expect(confirmation_page).to include("/paymentUpload/"), "Error: On-hold reservation redirected somewhere else, #{confirmation_page}"
      reservation.update(Hash[desktop_confirmation_url: confirmation_page.gsub("paymentUpload", "details")])
      step "guest uploads credit card credentials on the desktop IBE for the on-hold reservation" if reservation[:images_sent]
    else
      expect(confirmation_page).to include("/details/"), "Error: Desktop IBE reservation redirected somewhere else, #{confirmation_page}"
    end
    reservation.update(Hash[confirmation_details: get_confirmation_details(@property)])
    puts "Confirmation Number: #{reservation[:confirmation_details][:confirmation_no]}" unless reservation[:confirmation_details][:confirmation_no].nil?
    puts "Transaction ID: #{reservation[:confirmation_details][:transaction_id]}" unless reservation[:confirmation_details][:transaction_id].nil?
    puts "Reservation ID: #{reservation[:reservation_id]}" unless reservation[:reservation_id].nil?
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest uploads credit card credentials on the desktop IBE for the on-hold reservation$/) do
  if browser == :none
    url = FigNewton.url_ibe_upload
    puts payload = build_credentials_payload("front", @property)
    puts post_multipart(url, payload, @rsvsn_headers)
    payload = build_credentials_payload("back", @property)
    post_multipart(url, payload, @rsvsn_headers)
    payload = build_credentials_payload("gov", @property)
    post_multipart(url, payload, @rsvsn_headers)
    puts payload = build_credentials_submit_payload(@property)
    puts post(FigNewton.url_ibe_upload_submit, payload, @rsvsn_headers)
  else
    raise "Not yet implemented using a browser"
  end
end

# Estimated Time of Arrival
# -------------------------

Then(/^the reservation's confirmed estimated time of arrival is the same as that which is set by the guest$/) do
  if browser == :none
    reservation = @property[:reservation]
    expect(reservation[:confirmation_details][:rsvsn_eta]).to include(get_eta(reservation[:eta]))
  else
    raise "Not yet implemented using a browser"
  end
end

# Add-ons
# -------

When(/^guest includes add-ons to the desktop IBE reservation$/) do
  if browser == :none
    url = FigNewton.url_ibe_cpage_addons % { hotel_id: @property[:id], rsvsn_id: @property[:reservation][:reservation_id] }
    payload = build_cpage_addons_payload(@property)
    response = post(url, payload, @rsvsn_headers)
    expect(response[:location]).to include("/details/"), "Error: Failed to add add-ons to reservation, #{response[:location]}"
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest sees that the reservation confirmation includes add-ons information$/) do
  if browser == :none
    doc = Nokogiri::HTML(get(@property[:reservation][:desktop_confirmation_url], nil))
    expect(get_page_content(doc, "//div[@class='add-ons-selection-container print-no-break']/h1")).to eq("Add-ons")
  else
    raise "Not yet implemented using a browser"
  end
end

# Promo Code
# ----------

Then(/^guest sees that the reservation confirmation includes promo code information$/) do
  if browser == :none
    doc = Nokogiri::HTML(get(@property[:reservation][:desktop_confirmation_url], nil))
    expect(get_page_content(doc, "//div[@id='reservation-details']//strong[contains(.,'Promo Code')]//following::text()[1]")).to eq("RESERVATION")
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

Then(/^credit card details are not displayed on the (?:desktop|mobile)? IBE confirmation page$/) do
  begin
    if browser == :none
      confirmation_page = @property[:reservation][:confirmation_details]
      expect(confirmation_page[:creditcard_owner]).to be(nil), "Error: Credit Card Owner name displayed in the confirmation page for a No-CC Booking"
      expect(confirmation_page[:creditcard_number]).to be(nil), "Error: Credit Card Number displayed in the confirmation page for a No-CC Booking"
      expect(confirmation_page[:creditcard_expiry]).to be(nil), "Error: Credit Card Expiry displayed in the confirmation page for a No-CC Booking"
      if @property[:reservation][:is_confirmed]
        expect(confirmation_page[:charge_prepayment]).to eq("0.00")
        expect(confirmation_page[:charge_balance_payable]).to eq(confirmation_page[:charge_total_reservation_cost])
      end
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "guest support disables the no-credit-card booking feature for the property"
  end
end

Then(/^the (.*?) copy displayed on the (?:desktop|mobile)? IBE confirmation page is "(.*?)"$/) do |copy_type, expected_copy|
  begin
    if browser == :none
      confirmation_page = @property[:reservation][:confirmation_details]
      actual_copy = confirmation_page[:policy_prepayment] if copy_type.include? "prepayment"
      actual_copy = confirmation_page[:policy_cancellation] if copy_type.include? "cancellation"
      actual_copy = actual_copy.delete("\n").delete("\t").gsub(/\s+/, " ").strip
      expect(actual_copy).to eq(expected_copy)
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "guest support disables the no-credit-card booking feature for the property"
  end
end

Then(/^the (.*?) copy is (?:also )?not displayed on the (?:desktop|mobile)? IBE confirmation page$/) do |copy_type|
  begin
    if browser == :none
      confirmation_page = @property[:reservation][:confirmation_details]
      actual_copy = confirmation_page[:policy_modification] if copy_type.include? "modification"
      actual_copy = confirmation_page[:policy_noshow] if copy_type.include? "no-show"
      actual_copy = confirmation_page[:policy_refund] if copy_type.include? "refund"
      actual_copy = confirmation_page[:policy_cardfraud] if copy_type.include? "card fraud"
      expect(actual_copy).to be(nil)
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "guest support disables the no-credit-card booking feature for the property"
  end
end
