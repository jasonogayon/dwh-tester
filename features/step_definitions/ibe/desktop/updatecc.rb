# Update Credit Card Details
# --------------------------

Then(/^guest can update the credit card details for the reservation$/) do
  if browser == :none
    url = FigNewton.url_ibe_updatecc % {
      hotel_id:   @property[:id],
      rsvsn_id:   @property[:reservation][:reservation_id],
      rsvsn_hash: @property[:reservation][:reservation_hash]
    }
    response = get(url, @rsvsn_headers)
    expect(response).to include("uniquekey"), "Error: Couldn't find the unique key needed to update the credit card details"
    unique_key = response.split("uniquekey:")[1].split("cp_user_id:")[0].to_s.scan(/\d+/).first
    @property[:reservation].update(Hash[auth_key: unique_key])

    url = FigNewton.url_ibe_process_updatecc % {
      hotel_id:   @property[:id],
      rsvsn_id:   @property[:reservation][:reservation_id],
      rsvsn_hash: @property[:reservation][:reservation_hash]
    }
    payload = build_update_cc_payload(@property)
    response = post(url, payload, @rsvsn_headers)
    expect(response[:location]).to include("/details/"), "Error: Failed to update credit card details for the reservation"
  else
    raise "Not yet implemented using a browser"
  end
end
