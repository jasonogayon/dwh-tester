# Privacy Policy Page
# --------------------

When(/^guest views the privacy policy page$/) do
  if browser == :none
    response = get(FigNewton.url_ibe_privacy % { hotel_id: @property[:id] }, @rsvsn_headers)

    doc = Nokogiri::HTML(response)
    @copy = get_page_content(doc, "//div[@class='row']")
  else
    raise "Not yet implemented using a browser"
  end
end
