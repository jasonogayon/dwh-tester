include ReservationChargesHelper
include ConfirmationPageHelper
include ReservationDetailsHelper

# Create Reservations
# -------------------

When(/^guest books a desktop reservation for (.*?) rooms on a (.*?) rate plan(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |rooms, rateplan_type, arrival, departure, _payment|
  if browser == :none
    begin
      rateplan = get_rateplan(@property, rateplan_type, rooms, "")
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
    ensure
      step "guest support disables the no-credit-card booking feature for the property" if @property[:nocc_sameday] || @property[:nocc_nextday]
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^modifies the reservation to (.*?) rooms on a (.*?) rate plan(?: for stay dates from (.*?) to (.*?))?$/) do |rooms, rateplan_type, arrival, departure|
  if browser == :none
    rateplan = get_rateplan(@property, rateplan_type, rooms, "")
    @property[:reservation].update(get_modified_rsvsn_info(rateplan, arrival, departure))
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    step "guest goes to the desktop IBE payment information page"
    step "guest pays for the desktop IBE reservation"
    step "guest is redirected to the desktop IBE confirmation page"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^cancels the reservation$/) do
  if browser == :none
    reservation = @property[:reservation]
    reservation.update(Hash[rsvsn_type: "cancelled", is_cancelled: true, is_confirmed: false, is_not_confirmed: true])
    url = FigNewton.url_ibe_cancelreservation % { hotel_id: @property[:id], confirmation_no: reservation[:confirmation_details][:confirmation_no] }
    get(url, @rsvsn_headers)
    unless assertion == :false
      reservation.update(Hash[confirmation_details: get_confirmation_details(@property)])
      reservation[:current_charges].update(Hash[prepayment: nil, balance_payable: nil])
      reservation.update(Hash[current_charges: update_reservation_charges(@property, reservation[:current_charges])])
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support marks the reservation as no-show(.*?)$/) do |noshow_type|
  if browser == :none
    reservation = @property[:reservation]
    reservation.update(Hash[rsvsn_type: "noshow", is_noshow: true, is_confirmed: false, is_not_confirmed: true])
    url = FigNewton.url_cp_noshow_request
    response = JSON.parse(post(url, build_request_noshow_payload(@property), @headers))
    expect(response["success"]).to be(true), "Error: Failed to request reservation for no-show"
    is_nocc = @property[:nocc_sameday] || @property[:nocc_nextday]
    step "hotelier searches for the reservation"
    reservation[:rateplan_details].update(Hash[is_refundable: reservation[:he_search][0]["refundable_flag"].zero? ? false : true])
    if reservation[:is_dwh] && (reservation[:rateplan_details][:is_refundable] || reservation[:rateplan_details][:is_partial_prepay]) && !is_nocc
      url = FigNewton.url_cp_noshow_mark
      response = JSON.parse(post(url, build_accept_noshow_payload(@property), @headers))
      expect(response["success"]).to be(true), "Error: Failed to accept no-show request for DWH reservation, #{response}"
      if noshow_type.include? "waived"
        payload = build_waived_noshow_payload(@property)
        reservation.update(Hash[is_noshow_waived: true])
      elsif noshow_type.include? "invalid"
        payload = build_invalid_noshow_payload(@property)
        reservation.update(Hash[is_noshow_invalid: true])
      end
      payload ||= build_mark_noshow_payload(@property)
      begin
        tries ||= 3
        response = JSON.parse(post(url, payload, @headers))
      rescue
        retry unless (tries -= 1).zero?
      end
      expect(response["success"]).to be(true), "Error: Failed to mark DWH reservation as no-show, #{response}"
    end
    unless assertion == :false
      reservation.update(Hash[confirmation_details: get_confirmation_details(@property)])
      reservation[:current_charges].update(Hash[prepayment: nil, balance_payable: "0.00"])
      reservation.update(Hash[current_charges: update_reservation_charges(@property, reservation[:current_charges])])
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest is notified that the (.*?) reservation has been (.*?)$/) do |platform, _rsvsn_type|
  if browser == :none
    reservation = @property[:reservation]
    rateplan = reservation[:rateplan_details]
    confirmation_page = platform =~ /mobile/ ? reservation[:mobile_confirmation_url] : reservation[:desktop_confirmation_url]
    if confirmation_page =~ /details|paymentUpload/ && assertion == :true
      payinfo = reservation[:current_charges]
      confirm = reservation[:confirmation_details]
      payinfo_charges = Hash[
        total_room_charge:      payinfo[:total_room_charge],
        total_tax:              payinfo[:total_tax],
        total_fee:              payinfo[:total_fee],
        total_reservation_cost: payinfo[:total_reservation_cost],
        refund:                 payinfo[:refund],
        penalty:                payinfo[:penalty],
        prepayment:             payinfo[:prepayment],
        balance_payable:        payinfo[:balance_payable]
      ]
      if reservation[:is_noshow] && reservation[:is_dwh] && rateplan[:is_nonrefundable]
        penalty = reservation[:is_noshow_waived] || reservation[:is_noshow_invalid] ? "0.00" : payinfo[:total_reservation_cost]
        payinfo_charges.update(Hash[penalty: penalty])
      end
      if reservation[:is_noshow] && reservation[:is_bdo] && rateplan[:is_nonrefundable]
        payinfo_charges.update(Hash[penalty: payinfo[:penalty_he]])
      end
      addon_cost = confirm[:charge_total_addon_cost]
      confirm_charges = Hash[
        total_room_charge:      confirm[:charge_room_charge],
        total_tax:              confirm[:charge_total_tax],
        total_fee:              confirm[:charge_total_fee],
        total_reservation_cost: sprintf("%.2f", confirm[:charge_total_reservation_cost].to_f - addon_cost.to_f),
        refund:                 confirm[:charge_refund],
        penalty:                confirm[:charge_penalty],
        prepayment:             confirm[:charge_prepayment],
        balance_payable:        reservation[:is_cancelled] ? nil : sprintf("%.2f", confirm[:charge_balance_payable].to_f - addon_cost.to_f)
      ]
      if platform =~ /mobile/
        payinfo_charges.update(Hash[prepayment: sprintf("%.1f", payinfo_charges[:prepayment])]) unless payinfo_charges[:prepayment].nil?
        payinfo_charges.update(Hash[balance_payable: sprintf("%.1f", payinfo_charges[:balance_payable])]) unless payinfo_charges[:balance_payable].nil?
        confirm_charges.update(Hash[prepayment: sprintf("%.1f", confirm_charges[:prepayment])]) unless confirm_charges[:prepayment].nil?
        confirm_charges.update(Hash[balance_payable: sprintf("%.1f", confirm_charges[:balance_payable])]) unless confirm_charges[:balance_payable].nil?
      end
      expect(payinfo_charges).to eq(confirm_charges), "Error: Discrepancy found in Confirmation Page reservation charges\nexpected:\n#{payinfo_charges}\nconfirmation:\n#{confirm_charges}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^the (.*?) reservation is logged in the Hotelier Extranet$/) do |platform|
  if browser == :none
    step "guest support logs into the Control Panel"
    step "guest support logs into the Hotelier Extranet directly from CP"
    step "hotelier views the reservation's details log"

    reservation = @property[:reservation]
    rateplan = reservation[:rateplan_details]
    payinfo = reservation[:current_charges]
    confirm = reservation[:confirmation_details]

    unless assertion == :false
      payinfo_charges = Hash[
        total_room_charge:      payinfo[:total_room_charge],
        total_tax:              payinfo[:total_tax],
        total_fee:              payinfo[:total_fee],
        total_addon_cost:       confirm[:charge_total_addon_cost],
        total_reservation_cost: payinfo[:total_reservation_cost],
        penalty:                payinfo[:penalty],
        prepayment:             payinfo[:prepayment_he_display],
        balance_payable:        payinfo[:balance_payable_he_display],
        dwh_revenue:            sprintf("%.1f", payinfo[:dwh_revenue]),
        hotel_revenue:          payinfo[:hotel_revenue]
      ]
      if ((reservation[:is_bdo] || reservation[:is_paypal]) && reservation[:is_not_confirmed]) || (reservation[:is_noshow] && reservation[:is_dwh] && rateplan[:is_refundable])
        payinfo_charges.update(Hash[penalty: payinfo[:penalty_he]])
      end
      is_nocc = @property[:nocc_sameday] || @property[:nocc_nextday]
      log_id = confirm[:charge_total_addon_cost].to_i > 0 ? (reservation[:is_confirmed] ? 1 : 2) : (reservation[:is_confirmed] ? 0 : 1)
      log_id = 3 if reservation[:is_dwh] && reservation[:is_noshow] && rateplan[:is_refundable] && !is_nocc
      details = get_he_rsvsn_details(reservation[:he_details][log_id])
      addon_cost = details[:charge_total_addon_cost]
      details_charges = Hash[
        total_room_charge:      details[:charge_room_charge_partial],
        total_tax:              details[:charge_total_tax],
        total_fee:              details[:charge_total_fee],
        total_addon_cost:       addon_cost,
        total_reservation_cost: sprintf("%.2f", details[:charge_total_reservation_cost].to_f - addon_cost.to_f),
        penalty:                details[:charge_penalty],
        prepayment:             details[:charge_prepayment],
        balance_payable:        sprintf("%.2f", details[:charge_balance_payable].to_f - addon_cost.to_f),
        dwh_revenue:            sprintf("%.1f", details[:charge_dwh_revenue]),
        hotel_revenue:          sprintf("%.2f", details[:charge_hotel_revenue].to_f - addon_cost.to_f)
      ]
      if platform =~ /mobile/
        payinfo_charges.update(Hash[prepayment: sprintf("%.1f", payinfo_charges[:prepayment])]) unless payinfo_charges[:prepayment].nil?
        payinfo_charges.update(Hash[balance_payable: sprintf("%.1f", payinfo_charges[:balance_payable])]) unless payinfo_charges[:balance_payable].nil?
        details_charges.update(Hash[prepayment: sprintf("%.1f", details_charges[:prepayment])]) unless details_charges[:prepayment].nil?
        details_charges.update(Hash[balance_payable: sprintf("%.1f", details_charges[:balance_payable])]) unless details_charges[:balance_payable].nil?
      end
      expect(payinfo_charges).to eq(details_charges), "Error: Discrepancy found in Hotelier Extranet reservation charges\nexpected:\n#{payinfo_charges}\nextranet:\n#{details_charges}"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

# With HTML Tags
# ------------------

When(/^guest can book a desktop IBE reservation for (.*?) rooms on the created rate plan(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |_rooms, arrival, departure, _payment|
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      @property[:reservation].update(Hash[is_custom: true])
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# With Extra Beds
# ---------------

When(/^guest books a desktop reservation for (.*?) rooms on a (.*?) rate plan with extra beds(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |rooms, rateplan_type, arrival, departure, _payment|
  begin
    if browser == :none
      step "hotel manager adds a Nightly #{rateplan_type.gsub('Public', '')} public rate plan with extra beds into the existing property"
      rateplan = get_rateplan(@property, rateplan_type, rooms, "")
      rateplan.update(Hash[id: @rateplan_id])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      @property[:public_rateplans] = [rateplan]
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# With Add-ons
# ------------

When(/^guest books a desktop reservation for (.*?) rooms on a (.*?) rate plan with add-ons(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |rooms, rateplan_type, arrival, departure, _payment|
  begin
    if browser == :none
      step "hotel manager adds a Nightly #{rateplan_type.gsub('Public', '')} public rate plan with add-ons into the existing property"
      rateplan = get_rateplan(@property, rateplan_type, rooms, "")
      rateplan.update(Hash[id: @rateplan_id])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      @property[:public_rateplans] = [rateplan]
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
      step "guest includes add-ons to the desktop IBE reservation"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# With Promo Code
# ---------------

When(/^guest books a desktop reservation for (.*?) rooms on a (.*?) rate plan with "(.*?)" Promo Code(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |rooms, rateplan_type, promocode, arrival, departure, _payment|
  begin
    if browser == :none
      step "hotel manager adds a Nightly #{rateplan_type.gsub('Public', '')} public rate plan with a promo code of \"#{promocode}\" into the existing property"
      rateplan = get_rateplan(@property, rateplan_type, rooms, "")
      rateplan.update(Hash[id: @rateplan_id])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      @property[:public_rateplans] = [rateplan]
      step "guest sees that the rate plan is marked as an exclusive promo on the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Rate Mixing
# -----------

When(/^guest can book rooms for the mixed rate plan on the desktop IBE$/) do
  begin
    if browser == :none
      step "guest goes to the desktop IBE payment information page"
      step "guest pays for the desktop IBE reservation"
      step "guest is redirected to the desktop IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "guest support can get the hotel's public rate plans"
    @property[:public_rateplans].each do |rateplan|
      @rateplan_id = rateplan["id"]
      step "hotel manager can remove the newly added public rate plan"
    end
  end
end

# Custom Reservation
# ------------------

When(/^guest can book a random number of rooms for a desired promo package and stay dates on the desktop IBE$/) do
  if browser == :none
    raise "Please provide a desired property ID for your custom reservation, i.e. PROP_ID=18442" if property_id.nil?
    raise "Please provide a desired rate plan ID for your custom reservation, i.e. RP_ID=20070" if custom_rateplan_id.nil?
    raise "Please provide a desired check-in date for your custom reservation, i.e. CHECKIN=2017-06-09" if custom_arrival.nil?
    raise "Please provide a desired number of stay nights for your custom reservation, i.e. NIGHTS=2" if custom_no_nights.nil?
    step "guest support enables the no-credit-card booking feature for the property" if no_creditcard == :true
    rateplan = get_rateplan(@property, nil)
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:public_rateplans] = [rateplan] unless custom_promocode.nil?
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE" if custom_promocode.nil?
    step "guest sees that the rate plan is marked as an exclusive promo on the desktop IBE" unless custom_promocode.nil?
    step "guest goes to the desktop IBE payment information page"
    step "guest pays for the desktop IBE reservation"
    step "guest is redirected to the desktop IBE confirmation page"
    step "guest support disables the no-credit-card booking feature for the property" if no_creditcard == :true
  else
    raise "Not yet implemented using a browser"
  end
end
