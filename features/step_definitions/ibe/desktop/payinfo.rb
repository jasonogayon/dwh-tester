# Step 3: PayInfo Page
# --------------------

When(/^guest goes to the desktop IBE payment information page$/) do
  if browser == :none
    reservation = @property[:reservation]
    url = FigNewton.url_ibe_newsession % { hotel_id: @property[:id] }
    payload = build_rooms_payload(@property)
    response = post(url, payload, @rsvsn_headers)
    raise "Error: Redirected to an error page, #{response[:location]}" if response[:location].include? "/errorPage/"
    reservation.update(Hash[auth_key: response[:location].split("/")[6], desktop_payinfo_url: response[:location]])
    unless assertion == :false
      if reservation[:is_modified]
        reservation.update(Hash[current_charges: get_payinfo_details(@property, reservation[:previous_charges])])
      else
        reservation.update(Hash[previous_charges: get_payinfo_details(@property)])
        reservation.update(Hash[current_charges: reservation[:previous_charges]])
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end

# Add-ons
# -------

When(/^guest includes add-ons on the desktop IBE payment information page$/) do
  if browser == :none
    url = FigNewton.url_ibe_cpage_addons % { hotel_id: @property[:id], rsvsn_id: @property[:reservation][:auth_key] }
    payload = build_payinfo_addons_payload(@property)
    post(url, payload, @rsvsn_headers)
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest sees that the add-ons section is displayed on the desktop IBE payment information page$/) do
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      rateplan.update(Hash[rooms: [Hash[room_0_id: rateplan[:details][:room_id], room_0_qty: 1, room_0_occ: 0]]])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
      step "guest sees that the public rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_payinfo_url], @rsvsn_headers))
      expect(get_page_content(doc, "//*[@id='add-ons-toggle-switch']/td[1]/h3")).to include("Enhance your stay with these Add-ons")
      expect(get_page_content(doc, "//*[@id='add-ons-toggle-switch']/td[1]/h3")).to include("(Click to view details)")
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end

Then(/^guest sees that the add-ons section is not displayed anymore on the desktop IBE payment information page afterwards$/) do
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      rateplan.update(Hash[rooms: [Hash[room_0_id: rateplan[:details][:room_id], room_0_qty: 1, room_0_occ: 0]]])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
      step "guest sees that the public rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_payinfo_url], @rsvsn_headers))
      expect(get_page_content(doc, "//*[@id='add-ons-toggle-switch']/td[1]/h3")).to be(nil)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# No Credit Card
# --------------

When(/^a copy of "(.*?)" is displayed on the desktop IBE payment information page$/) do |copy|
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      rateplan.update(Hash[rooms: [Hash[room_0_id: rateplan[:details][:room_id], room_0_qty: 1, room_0_occ: 0]]])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
      step "guest sees that the public rate plan is available in the desktop IBE"
      step "guest goes to the desktop IBE payment information page"
      doc = Nokogiri::HTML(get(@property[:reservation][:desktop_payinfo_url], @rsvsn_headers))
      expect(get_page_content(doc, "//ul[@class='list-check']")).to include(copy)
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
    step "guest support disables the no-credit-card booking feature for the property"
  end
end

# Best Price Guarantee Copy
# -------------------------

When(/^guest checks the best price guarantee link in the review page$/) do
  if browser == :none
    rateplan_type ||= @property[:hotel_type].include?("DWH") ? "Public Full Prepayment Non-Refundable" : "Public Pay upon Arrival with Lead Time Penalty"
    rateplan = get_rateplan(@property, rateplan_type, "1 Accommodation", "")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:reservation].update(Hash[payment_type: "credit card"])
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
    step "guest goes to the desktop IBE payment information page"
    response = get(@property[:reservation][:desktop_payinfo_url], @rsvsn_headers)

    @copy = get_bpg_copies(response)
  else
    raise "Not yet implemented using a browser"
  end
end