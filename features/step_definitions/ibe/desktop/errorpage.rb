# Unsubscribed Hotels
# -------------------

Then(/^guest is notified that the hotel's reservation system is no longer available on both the desktop and mobile booking engines$/) do
  step "guest is notified that the hotel's reservation system is no longer available on the desktop booking engine"
  step "guest is notified that the hotel's reservation system is no longer available on the mobile booking engine"
end

Then(/^guest is notified that the hotel's reservation system is no longer available on the (.*?) booking engine$/) do |platform|
  url = FigNewton.url_ibe_showrooms % {
    hotel_id:       @property[:id],
    rateplan_id:    FigNewton.unsubscribed_rp,
    start_date:     get_date("TOMORROW")[0],
    end_date:       get_date("2 DAYS FROM NOW")[0],
    rsvsn_id:       0,
    rsvsn_hash:     0,
    user_id:        0
  }
  @headers.update(Hash["User-Agent" => FigNewton.mobile_useragent]) if platform.include? "mobile"
  doc = Nokogiri::HTML(get(url, @headers))
  errorpage_copy = platform.include?("mobile") ? "//*[contains(@class,'error-message')]" : "//*[@id='instructions']"
  expect(get_page_content(doc, errorpage_copy)).to include("#{FigNewton.unsubscribed_hotel_name}'s reservation system is no longer available. Please contact us for any inquiries and concerns")
end
