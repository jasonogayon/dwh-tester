include DatesHelper

# Rate Plan Policy Copies
# -----------------------

When(/^guest reads the policies for a (.*?) rate plan on stay dates from (.*?) to (.*?)$/) do |rateplan, arrival, departure|
  raise "Error: Existing property info required for test to run properly, please add step.\ni.e. Given an existing Global Collect hotel property with public rate plans" if @property.nil?
  if use_data == :true
    rateplans = rateplan.include?("Private") ? @property[:private_rateplans] : @property[:public_rateplans]
    rateplan = rateplan.gsub(/DWH|HPP|BDO|Paypal|BDO-Paypal|Public|Private/, "").strip
    rateplan_id = rateplans.select { |rp| rp["rate_plan_name"].include?(rateplan) }[0]["id"]
  end
  rateplan_id ||= get_rateplan_id(@property[:hotel_type], rateplan)
  puts url = FigNewton.url_ibe_policies % { hotel_id: @property[:id], rateplan_id: rateplan_id, start_date: get_date(arrival ||= "TODAY")[0], end_date: get_date(departure ||= "TOMORROW")[0] }
  begin
    tries ||= 10
    @copy = get_policy_copies(RestClient.get(url, @headers))
  rescue RestClient::ServiceUnavailable
    (tries -= 1).zero? ? raise("Error: Unable to reach policies page") : retry
  end
  expect(@copy).not_to be(nil), "Error: Unable to retrieve policies information from page"
  step "guest support disables the no-credit-card booking feature for the property" if @property[:nocc_sameday] || @property[:nocc_nextday]
end

Then(/^a copy of "(.*?)" is (?:also )?displayed$/) do |expected_copy|
  expect(@copy).to include(expected_copy), "Error: Actual copy is #{@copy}."
end

Then(/^a copy of "(.*?)" is (?:also )?not displayed$/) do |expected_copy|
  expect(@copy).not_to include(expected_copy), "Error: Actual copy is #{@copy}."
end

Then(/^the (.*?) copy is not displayed$/) do |copy|
  expect(@copy).to be(nil), "Error: #{copy} is displayed on the page."
end
