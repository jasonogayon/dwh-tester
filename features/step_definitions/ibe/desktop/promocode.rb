# Promo Code
# ----------

When(/^guest adds public promo code to the desktop IBE session$/) do
  if browser == :none
    promo_code = "reservation"
    url = FigNewton.url_ibe_validate_public_promocode % { hotel_id: @property[:id] }
    payload = build_promocode_payload(promo_code)
    post(url, payload, @headers)
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest adds private promo code to the desktop IBE session$/) do
  if browser == :none
    # Create selectDates Session
    @session = []
    url = FigNewton.url_ibe_selectdates % { hotel_id: @property[:id] }
    get(url, nil).cookies.each_value { |v| @session.push(v) }
    @rsvsn_headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }

    # Validate Private Promo Code
    begin account_code = @property[:private_accounts].select { |act| act["account_name"] =~ /Privados/ }[0][:code]
    rescue NoMethodError
      account_code = FigNewton.promo_code
    end
    url = FigNewton.url_ibe_validate_private_promocode % { hotel_id: @property[:id] }
    payload = build_promocode_payload(account_code)
    response = post(url, payload, @rsvsn_headers)
    expect(eval(response)).to be(true), "Error: Private account code used seems to be invalid"

    # Process Dates
    url = FigNewton.url_ibe_processdates % { hotel_id: @property[:id] }
    payload = build_processdates_payload(account_code, @property)
    response = post(url, payload, @rsvsn_headers)
    expect(response[:location]).to include("/showRooms/"), "Error: Desktop process dates should redirect to showRooms page"
  else
    raise "Not yet implemented using a browser"
  end
end
