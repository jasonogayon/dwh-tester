# Dynamic Booking Engine Domain URLs
# ----------------------------------

Then(/^guest is redirected to the provided desktop subdomain when visiting the desktop booking engine$/) do
  if browser == :none
    begin
      sleep 120
      puts url = FigNewton.url_ibe_selectdates % { hotel_id: @property[:id] }
      response = get(url, @rsvsn_headers)
      puts response_urls = response.request.url
      expect(response_urls).to include(@property[:ibe_domain_url_desktop].delete('\'')), "Error: Guest not redirected to dynamic desktop booking engine domain\n{response_urls}"
    ensure
      step "an administrator adds a '' subdomain for the hotel's desktop booking engine"
    end
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest visits the desktop calendar page$/) do
  if browser == :none
    @session = []
    url = FigNewton.url_ibe_selectdates % { hotel_id: @property[:id] }
    begin
      get(url, nil).cookies.each_value { |v| @session.push(v) }
    rescue SocketError
      raise "Error: The site #{url} seems to be unreachable"
    end
    @rsvsn_headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }
  else
    raise "Not yet implemented using a browser"
  end
end
