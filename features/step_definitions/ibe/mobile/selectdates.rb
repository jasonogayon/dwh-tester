# Step 1: SelectDates Page
# ------------------------

When(/^guest visits the mobile IBE calendar page$/) do
  if browser == :none
    @session = []
    url = FigNewton.url_ibe_selectdates_mobile % { hotel_id: @property[:id] }
    get(url, nil).cookies.each_value { |v| @session.push(v) }
    @rsvsn_headers = { "Cookie" => "PHPSESSID=#{@session[0]}", "User-Agent" => FigNewton.mobile_useragent }
    @property[:reservation].update(Hash[mobile_selectdates_url: url])

    url = FigNewton.url_ibe_processdates_mobile % { hotel_id: @property[:id] }
    payload = build_processdates_payload(nil, @property)
    response = post(url, payload, @rsvsn_headers)
    expect(response[:location]).to include("/getRateplanDetails/"), "Error: Mobile process dates should redirect to getRateplanDetails page"
    @property[:reservation].update(Hash[mobile_processdates_url: url])
  else
    raise "Not yet implemented using a browser"
  end
end

# Dynamic Booking Engine Domain URLs
# ----------------------------------

Then(/^guest is redirected to the provided mobile subdomain when visiting the mobile booking engine$/) do
  if browser == :none
    begin
      sleep 120
      puts url = FigNewton.url_ibe_selectdates_mobile % { hotel_id: @property[:id] }
      @rsvsn_headers = { "User-Agent" => FigNewton.mobile_useragent }
      response = get(url, @rsvsn_headers)
      puts response_urls = response.request.url
      expect(response_urls).to include(@property[:ibe_domain_url_mobile].delete('\'')), "Error: Guest not redirected to dynamic mobile booking engine domain\n{response_urls}"
    ensure
      step "an administrator adds a '' subdomain for the hotel's mobile booking engine"
    end
  else
    raise "Not yet implemented using a browser"
  end
end
