include ShowRoomsHelper

# Step 2: ShowRooms Page
# ----------------------

When(/^guest goes to the mobile IBE rooms page$/) do
  if browser == :none
    url = FigNewton.url_ibe_showrooms_mobile % {
      hotel_id:   @property[:id],
      start_date: @property[:reservation][:arrival][0],
      end_date:   @property[:reservation][:departure][0],
      language:   @property[:reservation][:language],
      rsvsn_id:   @property[:reservation][:reservation_id]
    }
    get(url, @rsvsn_headers)
    @property[:reservation].update(Hash[mobile_showrooms_url: url])
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^(?:guest sees that )?the rate plan is (.*?) in the mobile IBE$/) do |availability_status|
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      rateplan.update(Hash[room_0_id: rateplan[:details][:room_id]])
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
      @property[:reservation].update(Hash[rooms: @property[:public_rateplans]])
      step "guest visits the mobile IBE calendar page"
      step "guest goes to the mobile IBE rooms page"
      step "guest goes to the mobile IBE rate plans page"
      step "guest goes to the mobile IBE payment page"
      puts url = @property[:reservation][:mobile_payinfo_url]
      request = Hash[method: :get, url: url, headers: @rsvsn_headers, max_redirects: 0]
      if availability_status =~ /not available|unavailable/
        begin RestClient::Request.execute(request)
        rescue RestClient::ExceptionWithResponse => err
          expect(err.response.code).to eq(302), "Error: Rate plan is displayed in mobile rooms page when it shouldn't"
        end
      else
        expect(RestClient::Request.execute(request).code).to eq(200), "Error: Missing rate plan in the mobile rooms page"
      end
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Rate Plan Promo Code
# --------------------

Then(/^guest sees that the rate plan is marked as an exclusive promo on the mobile IBE$/) do
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      puts url = FigNewton.url_ibe_showrooms_promo_mobile % {
        hotel_id:       @property[:id],
        start_date:     @property[:reservation][:arrival][0],
        end_date:       @property[:reservation][:departure][0],
        promo_code:     rateplan[:promo_code]
      }
      doc = Nokogiri::HTML(get(url, @rsvsn_headers.update(Hash["User-Agent" => FigNewton.mobile_useragent])))
      expect(get_page_content(doc, "//*[@class='promo-tag']")).to include(rateplan[:promo_code].downcase)
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
  end
end
