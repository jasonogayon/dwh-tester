# Step 4: Payment Page
# --------------------

When(/^guest goes to the mobile IBE payment page$/) do
  if browser == :none
    reservation = @property[:reservation]
    url = FigNewton.url_ibe_newsession_mobile % { hotel_id: @property[:id] }
    payload = build_rooms_payload_mobile(@property)
    response = post(url, payload, @rsvsn_headers)
    reservation.update(Hash[auth_key: response[:location].split("/")[6], mobile_payinfo_url: response[:location]])
    reservation.update(Hash[current_charges: get_payinfo_details_mobile(@property)]) unless assertion == :false || response[:location].include?("/noAvailPayment")
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

When(/^a copy of "(.*?)" is also displayed on the mobile IBE payment information page$/) do |copy|
  begin
    if browser == :none
      step "guest goes to the mobile IBE payment page"
      doc = Nokogiri::HTML(get(@property[:reservation][:mobile_payinfo_url], @rsvsn_headers))
      expect(get_page_content(doc, "//div[@class='col-xs-12 center no-cc-note-panel']")).to eq(copy)
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
    step "guest support disables the no-credit-card booking feature for the property"
  end
end
