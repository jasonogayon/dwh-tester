# Step 3: getRatePlanDetails Page
# -------------------------------

When(/^guest goes to the mobile IBE rate plans page$/) do
  if browser == :none
    url = FigNewton.url_ibe_getrateplandetails_mobile % {
      hotel_id:   @property[:id],
      start_date: @property[:reservation][:arrival][0],
      end_date:   @property[:reservation][:departure][0],
      room_id:    @property[:public_rateplans][0][:details][:room_id]
    }
    get(url, @rsvsn_headers)
    @property[:reservation].update(Hash[mobile_rateplans_url: url])
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest goes to the mobile IBE promo rate plan page$/) do
  if browser == :none
    puts url = FigNewton.url_ibe_getrateplandetails_promo_mobile % {
      hotel_id:       @property[:id],
      start_date:     @property[:reservation][:arrival][0],
      end_date:       @property[:reservation][:departure][0],
      room_id:        @property[:public_rateplans][0][:details][:room_id],
      rateplan_id:    @property[:public_rateplans][0][:id],
      promo_code:     custom_promocode
    }
    get(url, @rsvsn_headers)
    @property[:reservation].update(Hash[mobile_rateplans_url: url])
  else
    raise "Not yet implemented using a browser"
  end
end

# Rate Plan Mixing
# ----------------

When(/^guest goes to the mobile IBE mixed rate plans page$/) do
  if browser == :none
    step "guest goes to the mobile IBE rate plans page"
    doc = Nokogiri::HTML(get(@property[:reservation][:mobile_rateplans_url], @rsvsn_headers))
    expect(get_page_content(doc, "//div[@data-mixed='wob']")).to eq(@property[:reservation][:rooms][0][:rp_name])
    expect(get_page_content(doc, "//div[@class='ribbon center']")).to eq("Recommended for you")
  else
    raise "Not yet implemented using a browser"
  end
end

# No Credit Card
# --------------

When(/^guest sees that the rate plan is (?:also )?marked as a no-credit-card promo on the mobile IBE$/) do
  begin
    if browser == :none
      url = FigNewton.url_ibe_getrateplandetails_mobile % {
        hotel_id:   @property[:id],
        start_date: @property[:reservation][:arrival][0],
        end_date:   @property[:reservation][:departure][0],
        room_id:    @property[:public_rateplans][0][:details][:room_id]
      }
      doc = Nokogiri::HTML(get(url, @rsvsn_headers.update(Hash["User-Agent" => FigNewton.mobile_useragent])))
      expect(get_page_content(doc, "//*[@class='no-credit-card-blurb right']")).to include("Reserve without a credit card")
      @property[:reservation].update(Hash[mobile_rateplans_url: url])
    else
      raise "Not yet implemented using a browser"
    end
  rescue
    step "hotel manager can remove the newly added public rate plan"
    step "guest support disables the no-credit-card booking feature for the property"
  end
end
