include BDOPaymentHelper

# Step 4: Payment Page
# --------------------

When(/^guest pays for the mobile IBE reservation$/) do
  if browser == :none
    reservation = @property[:reservation]
    get(reservation[:mobile_payinfo_url], @rsvsn_headers)
    puts url = FigNewton.url_ibe_confirmreservation_mobile % { hotel_id: @property[:id], rsvsn_id: reservation[:reservation_id] }
    puts payload = build_payment_payload_mobile(@property)
    begin
      tries ||= 10
      puts response = post(url, payload, @rsvsn_headers)
      if @property[:property_name].include?("Webdriver -A")
        pay_comp = post(FigNewton.url_ibe_ap_paycomp, build_ap_payload(get_ap_payment_details(response)), @rsvsn_headers)
        page_redirect = post(FigNewton.url_ibe_ap_pageredirect, build_ap_redirect_payload(get_ap_redirect_details(pay_comp)), @rsvsn_headers)
        response = get_without_redirect(page_redirect[:location], @rsvsn_headers)
      end
      if (reservation[:is_dwh] || reservation[:is_hpp]) && !@property[:property_name].include?("Webdriver -A")
        raise "Error: Reservation payment did not receive a response after several tries. Please retry later." if response[:location].nil?
        payment_retry = 0
        while response[:location].include?("/pay/")
          response = post(url, payload, @rsvsn_headers)
          payment_retry += 1
          break if payment_retry == 10
        end
      end
    rescue RestClient::ServerBrokeConnection
      (tries -= 1).zero? ? raise("Unable to proceed with reservation payment") : retry
    end
    if reservation[:is_bdo]
      vpcpay = post(FigNewton.url_ibe_bdo_vpcpay, build_vpcpay_payload(get_bdo_payment_details(response)), @rsvsn_headers)
      vpcpay_o = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + vpcpay[:location], "Cookie" => "#{vpcpay[:setcookie][0]}; #{vpcpay[:setcookie][1]}; #{vpcpay[:setcookie][2]}")
      ssl_headers = { "Cookie" => "#{vpcpay_o[:setcookie][0]}; #{vpcpay_o[:setcookie][1]}; #{vpcpay_o[:setcookie][2]}; #{vpcpay[:setcookie][0]}" }
      ssl_session = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + vpcpay_o[:location], ssl_headers)
      ssl_payment = get_without_redirect(FigNewton.url_ibe_bdo_mastercard + ssl_session[:location], ssl_headers)
      response = get_without_redirect(ssl_payment[:location], @rsvsn_headers)
    end
    expect(response[:location]).not_to be(nil), "Error: Mobile IBE reservation failed to push through"
    reservation.update(Hash[mobile_confirmation_url: response[:location]])
  else
    raise "Not yet implemented using a browser"
  end
end
