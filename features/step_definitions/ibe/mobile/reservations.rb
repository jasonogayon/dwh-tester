include ReservationChargesHelper
include ConfirmationPageHelper
include ReservationDetailsHelper

# Create Reservations via Mobile
# ------------------------------

When(/^guest books a mobile reservation for (.*?) rooms on a (.*?) rate plan(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |rooms, rateplan_type, arrival, departure, payment|
  if browser == :none
    rateplan = get_rateplan(@property, rateplan_type, rooms, payment ||= "")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
    step "guest visits the mobile IBE calendar page"
    step "guest goes to the mobile IBE rooms page"
    step "guest goes to the mobile IBE rate plans page"
    step "guest goes to the mobile IBE payment page"
    step "guest pays for the mobile IBE reservation"
    step "guest is redirected to the mobile IBE confirmation page"
  else
    raise "Not yet implemented using a browser"
  end
end

# With HTML Tags
# ------------------

When(/^guest can book a mobile IBE reservation for (.*?) rooms on the created rate plan(?: for stay dates from (.*?) to (.*?))?(?: using a (.*?))?$/) do |_rooms, arrival, departure, _payment|
  begin
    if browser == :none
      rateplan = @property[:public_rateplans][0]
      @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan, arrival, departure)])
      @property[:reservation].update(Hash[is_custom: true])
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest visits the mobile IBE calendar page"
      step "guest goes to the mobile IBE rooms page"
      step "guest goes to the mobile IBE rate plans page"
      step "guest goes to the mobile IBE payment page"
      step "guest pays for the mobile IBE reservation"
      step "guest is redirected to the mobile IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "hotel manager can remove the newly added public rate plan"
  end
end

# Rate Mixing
# -----------

When(/^guest can book rooms for the mixed rate plan on the mobile IBE$/) do
  begin
    if browser == :none
      step "guest goes to the mobile IBE mixed rate plans page"
      step "guest goes to the mobile IBE payment page"
      step "guest pays for the mobile IBE reservation"
      step "guest is redirected to the mobile IBE confirmation page"
    else
      raise "Not yet implemented using a browser"
    end
  ensure
    step "guest support can get the hotel's public rate plans"
    @property[:public_rateplans].each do |rateplan|
      @rateplan_id = rateplan["id"]
      step "hotel manager can remove the newly added public rate plan"
    end
  end
end

# Custom Reservation
# ------------------

When(/^guest can book a random number of rooms for a desired promo package and stay dates on the mobile IBE$/) do
  if browser == :none
    raise "Please provide a desired property ID for your custom reservation, i.e. PROP_ID=18442" if property_id.nil?
    raise "Please provide a desired rate plan ID for your custom reservation, i.e. RP_ID=20070" if custom_rateplan_id.nil?
    raise "Please provide a desired check-in date for your custom reservation, i.e. CHECKIN=2017-06-09" if custom_arrival.nil?
    raise "Please provide a desired number of stay nights for your custom reservation, i.e. NIGHTS=2" if custom_no_nights.nil?
    step "guest support enables the no-credit-card booking feature for the property" if no_creditcard == :true
    rateplan = get_rateplan(@property, "Public Full Prepayment Non-Refundable")
    @property.update(Hash[reservation: get_default_rsvsn_info(@property[:hotel_type], rateplan)])
    @property[:public_rateplans] = [rateplan] unless custom_promocode.nil?
    step "guest adds private promo code to the desktop IBE session" if rateplan[:is_private]
    if custom_promocode.nil?
      step "guest checks that the #{rateplan} rate plan is available in the desktop IBE"
      step "guest visits the mobile IBE calendar page"
      step "guest goes to the mobile IBE rooms page"
      step "guest goes to the mobile IBE rate plans page"
    else
      step "guest sees that the rate plan is marked as an exclusive promo on the desktop IBE"
      step "guest sees that the rate plan is marked as an exclusive promo on the mobile IBE"
      step "guest goes to the mobile IBE promo rate plan page"
    end
    step "guest goes to the mobile IBE payment page"
    step "guest pays for the mobile IBE reservation"
    step "guest is redirected to the mobile IBE confirmation page"
    step "guest support disables the no-credit-card booking feature for the property" if no_creditcard == :true
  else
    raise "Not yet implemented using a browser"
  end
end
