# Step 5: Confirmation Page
# -------------------------

When(/^guest is redirected to the mobile IBE confirmation page$/) do
  if browser == :none
    reservation = @property[:reservation]
    puts confirmation_page = reservation[:mobile_confirmation_url]
    reservation.update(Hash[reservation_id: confirmation_page.split("/")[6], reservation_hash: confirmation_page.split("/")[7]])
    if reservation[:is_onhold]
      expect(confirmation_page).to include("/paymentUpload/"), "Error: On-hold reservation redirected somewhere else, #{confirmation_page}"
      reservation.update(Hash[mobile_confirmation_url: confirmation_page.gsub("paymentUpload", "details")])
      step "guest uploads credit card credentials on the mobile IBE for the on-hold reservation" if reservation[:images_sent]
    else
      expect(confirmation_page).to include("/details/"), "Error: Mobile IBE reservation redirected somewhere else, #{confirmation_page}"
    end
    reservation.update(Hash[confirmation_details: get_confirmation_details_mobile(@property)])
    puts "Confirmation Number: #{reservation[:confirmation_details][:confirmation_no]}" unless reservation[:confirmation_details][:confirmation_no].nil?
    puts "Transaction ID: #{reservation[:confirmation_details][:transaction_id]}" unless reservation[:confirmation_details][:transaction_id].nil?
    puts "Reservation ID: #{reservation[:reservation_id]}" unless reservation[:reservation_id].nil?
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest uploads credit card credentials on the mobile IBE for the on-hold reservation$/) do
  if browser == :none
    url = FigNewton.url_ibe_upload_mobile
    puts payload = build_credentials_payload("front", @property)
    puts post_multipart(url, payload, @rsvsn_headers)
    payload = build_credentials_payload("back", @property)
    post_multipart(url, payload, @rsvsn_headers)
    payload = build_credentials_payload("gov", @property)
    post_multipart(url, payload, @rsvsn_headers)
    puts payload = build_credentials_submit_payload(@property)
    puts post(FigNewton.url_ibe_upload_submit_mobile, payload, @rsvsn_headers)
  else
    raise "Not yet implemented using a browser"
  end
end
