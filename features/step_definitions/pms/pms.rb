# Property Management System
# --------------------------

When(/^hotelier sends a(?:n)? (.*?) PMS request(?: with dates from (.*?) to (.*?))?$/) do |request_type, start_date, end_date|
  @pms = Hash[type: request_type.downcase, start_date: start_date ||= "TODAY", end_date: end_date ||= "TOMORROW"]
  request = build_pms_request(@pms[:type], get_date(@pms[:start_date])[8], get_date(@pms[:end_date])[8])
  response = RestClient.post FigNewton.pms_url, xml: request
  @pms.update(Hash[request: request, response: response])
  puts "REQUEST:\n#{Nokogiri::XML(request)}\nRESPONSE:\n#{Nokogiri::XML(response)}"
end

Then(/^(?:hotelier receives a PMS response containing|the PMS response also contains)? a(?:n)? (.*?) element(?: with the (.*?) attribute)?(?: value of "(.*?)")?$/) do |element, attribute, value|
  response = Nokogiri::XML(@pms[:response]).to_s
  expect(response).to include(element), "Error: Missing #{element} element in XML response"
  expect(response).to include(attribute), "Error: Missing #{attribute} element in XML response" unless attribute.nil?
  expect(response).to include(value), "Error: Actual #{attribute} value is #{value}" unless value.nil?
end
