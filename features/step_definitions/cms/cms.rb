# Channel Management System
# -------------------------

When(/^hotelier sends a(?:n)? (.*?) CMS (.*?) request(?: with dates from (.*?) to (.*?))?$/) do |request_type, channel, start_date, end_date|
  @headers = { "Content-Type" => "text/xml" }
  @cms = Hash[
    channel:        channel.downcase,
    type:           request_type.downcase,
    start_date:     start_date ||= "TODAY",
    end_date:       end_date ||= "TOMORROW",
    days_of_week:   get_days_of_week(request_type, get_date(start_date)[10], get_date(end_date)[10]),
    api_key:        FigNewton.cms_agoda_api_key
  ]
  @cms.update(Hash[api_key: @cms[:api_key].chop]) if request_type.downcase.include?("invalid key")
  payload = build_cms_request(@cms[:channel], @cms[:type], get_date(@cms[:start_date])[8], get_date(@cms[:end_date])[8], @cms[:days_of_week])
  begin
    if @cms[:channel].include? "agoda"
      url = FigNewton.cms_agoda_url_getbookinglist % { api_key: @cms[:api_key] } if @cms[:type].include? "getbookinglist"
      url = FigNewton.cms_agoda_url_getbookingdetails % { api_key: @cms[:api_key] } if @cms[:type].include? "getbookingdetails"
      url = FigNewton.cms_agoda_url_setari % { api_key: @cms[:api_key] } if @cms[:type].include? "setari"
      response = post(url, payload, @headers)
    elsif @cms[:channel].include?("booking[dot]com") || @cms[:channel].include?("bookingdotcom")
      url = FigNewton.cms_bookingdotcom_url_availnotif if @cms[:type].include? "availnotif"
      url = FigNewton.cms_bookingdotcom_url_rateamountnotif if @cms[:type].include? "rateamountnotif"
      url = FigNewton.cms_bookingdotcom_url_resmodifynotif if @cms[:type].include? "resmodifynotif"
      url = FigNewton.cms_bookingdotcom_url_resnotif if @cms[:type].include? "resnotif"
      begin
        response = post(url, payload, @headers)
        response = (RestClient::Resource.new url, FigNewton.cms_bookingdotcom_auth_username, FigNewton.cms_bookingdotcom_auth_password).post xml: payload
      rescue RestClient::Unauthorized
        puts "REQUEST:\n#{Nokogiri::XML(payload)}"
      end
    end
    begin
      response_body = response[:body]
    rescue TypeError
      response_body = response.body
    end
    @cms.update(Hash[request: payload, response: response_body])
    puts "REQUEST:\n#{Nokogiri::XML(payload)}\nRESPONSE:\n#{response_body}"
  rescue RestClient::BadRequest
    puts "REQUEST:\n#{Nokogiri::XML(payload)}"
  end
end

Then(/^(?:hotelier receives a CMS response containing|the CMS response also contains)? a(?:n)? (.*?) element(?: with the (.*?) attribute)?(?: value of "(.*?)")?$/) do |element, attribute, value|
  if @cms[:channel].include?("booking[dot]com") || @cms[:channel].include?("bookingdotcom")
    response = Nokogiri::XML(@cms[:response])
    expect(response).to include(element), "Error: Missing #{element} element in XML response"
    expect(response).to include(attribute), "Error: Missing #{attribute} element in XML response" unless attribute.nil?
    expect(response).to include(value), "Error: Actual #{attribute} value is #{value}" unless value.nil?
  else
    if (@cms[:type].include?("invalid") || @cms[:type].include?("missing")) && !(@cms[:type].include?("getbookingdetails") && @cms[:type].include?("invalid booking"))
      response = @cms[:response]
      expect(response.to_s).to include(value), "Error: Actual #{attribute} value is #{value}"
    else
      response = Nokogiri::XML(@cms[:response])
      actual = response.xpath(element).attr(attribute).to_s
      if @cms[:type].include? "setari"
        expect(actual).not_to be_empty, "Error: Missing #{attribute} value for #{element} element in XML response"
      else
        expect(actual).to eq(value), "Error: Actual #{attribute} value is #{value}"
      end
    end
  end
end
