# Control Panel Admin
# -------------------

When(/^query from terminal$/) do
  step "guest support logs into the Control Panel"
  url = FigNewton.url_cp_dbquery
  payload = "db=#{db}&query=#{query}"
  @query_response = post(url, payload, @headers)
  begin
    puts "Database: #{db}"
    puts "Query: #{query}"
    if query.downcase.include? "select"
      JSON.parse(@query_response)["data"].each do |data|
        puts "\n"
        for i in 0..data.size - 1
          puts JSON.parse(@query_response)["columns"][i].to_s + ": " + data[i - 1].to_s
        end
      end
    else
      JSON.pretty_generate(JSON.parse(@query_response))
    end
  rescue TypeError
    puts JSON.pretty_generate(@query_response)
  end
end

When(/^create an excel file report$/) do
  today = DateTime.now
  date = "#{today.year}-#{today.month}-#{today.day}-#{today.hour}-#{today.min}-#{today.sec}"

  begin
    columns = JSON.parse(@query_response)["columns"]
    data = JSON.parse(@query_response)["data"]
    workbook = WriteXLSX.new("reports/query_#{date}.xlsx")
    worksheet = workbook.add_worksheet
    if query.downcase.include? "select"
      for j in 0..columns.size
        worksheet.write(0, j, columns[j])
      end
      for i in 1..data.size
        for j in 0..columns.size
          worksheet.write(i, j, data[i - 1][j])
        end
      end
    end
    workbook.close
  rescue TypeError
    raise "Error: Unable to create Excel report for query. Please check query response."
  end
end
