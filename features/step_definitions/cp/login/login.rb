include RestHelper
include PayloadHelper
include DataLookup

# Control Panel Log-in
# --------------------

When(/^guest support logs into the Control Panel$/) do
  if browser == :none
    @session = []
    payload = build_login_payload(FigNewton.username_cp, FigNewton.password_cp)
    response = post(FigNewton.url_cp_login, payload, nil)
    expect(JSON.parse(response)["success"]).to be(true), "Error: Failed to log into Control Panel"
    response.cookies.each_value { |v| @session.push(v) }
    @headers = { "Cookie" => "PHPSESSID=#{@session[0]}" }
  else
    raise "Not yet implemented using a browser"
  end
end
