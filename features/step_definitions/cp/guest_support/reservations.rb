# Control Panel Reservations
# --------------------------

When(/^guest support gets total number of (.*?) confirmed reservations for (.*?)$/) do |payment_scheme, date|
  if browser == :none
    url = FigNewton.url_cp_read_reservations
    payload = build_read_reservations(date, date, "confirmed", payment_scheme, @start ||= 0, @limit ||= 1)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Unable to get the total number of #{payment_scheme} confirmed reservations for #{date}"
    response
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support can get the total number of (.*?) reservations(?: (.*?))?$/) do |rsvsn_type, date|
  if browser == :none
    step "guest support logs into the Control Panel"
    start_date = custom_start.nil? ? (get_date(date)[0] ||= "2017-06-01") : custom_start
    end_date = custom_end.nil? ? (get_date(date)[0] ||= "2017-06-10") : custom_end
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      date = (Date.parse(start_date) + i).to_s.split("-")
      date = "#{date[1]}/#{date[2]}/#{date[0]}"
      dwh_full = step "guest support gets total number of DWH-Full #{rsvsn_type} reservations for #{date}"
      dwh_partial = step "guest support gets total number of DWH-Partial #{rsvsn_type} reservations for #{date}"
      hpp = step "guest support gets total number of HPP #{rsvsn_type} reservations for #{date}"
      bdo = step "guest support gets total number of BDO #{rsvsn_type} reservations for #{date}"
      paypal = step "guest support gets total number of PAYPAL #{rsvsn_type} reservations for #{date}"

      # Create JSON Report
      filename = "reports/Number of Confirmed Reservations (#{FigNewton.environment}).json"
      data = {}
      data = JSON.parse(File.read(filename)) if File.file?(filename)
      data.update(Hash[date => Hash[
        "DWH-Full"      => dwh_full["total"],
        "DWH-Partial"   => dwh_partial["total"],
        "HPP"           => hpp["total"],
        "BDO"           => bdo["total"],
        "Paypal"        => paypal["total"],
        "Total"         => dwh_full["total"].to_i + dwh_partial["total"].to_i + hpp["total"].to_i + bdo["total"].to_i + paypal["total"].to_i
      ]])
      data = Hash[data.sort.reverse]
      File.open(filename, "w") { |f| f.write(data.to_json) }
      puts JSON.pretty_generate(data[date])
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support can get the number of confirmed reservations(?: (.*?))? with extra bed, add-on, or are private$/) do |date|
  if browser == :none
    step "guest support logs into the Control Panel"
    start_date = custom_start.nil? ? (get_date(date)[0] ||= "2017-06-01") : custom_start
    end_date = custom_end.nil? ? (get_date(date)[0] ||= "2017-06-10") : custom_end
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      date = (Date.parse(start_date) + i).to_s.split("-")
      date = "#{date[1]}/#{date[2]}/#{date[0]}"
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      @limit = response["total"]
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      data = response["data"]
      count_extrabeds = count_addons = count_private = 0
      for j in 0..(response["total"] - 1)
        next if data[j].nil?
        count_extrabeds += 1 unless data[j]["extra_bed_details"].nil? || data[j]["extra_bed_details"].include?("[]")
        count_addons += 1 unless data[j]["add_on_details"].nil? || data[j]["add_on_details"].include?("null")
        count_private += 1 unless data[j]["private_account_id"].nil?
      end

      # Create JSON Report
      filename = "reports/Reservations With Extra Beds or Add-ons or Are Private (#{FigNewton.environment}).json"
      data = {}
      data = JSON.parse(File.read(filename)) if File.file?(filename)
      data.update(Hash[date => Hash[
        "Total Reservations"  => response["total"],
        "With Extra Beds"     => count_extrabeds,
        "With Add-ons"        => count_addons,
        "Are Private"         => count_private
      ]])
      data = Hash[data.sort.reverse]
      File.open(filename, "w") { |f| f.write(data.to_json) }
      puts JSON.pretty_generate(data[date])
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support can get information from confirmed reservations(?: (.*?))?$/) do |date|
  if browser == :none
    step "guest support logs into the Control Panel"
    start_date = custom_start.nil? ? (get_date(date)[0] ||= "2017-06-01") : custom_start
    end_date = custom_end.nil? ? (get_date(date)[0] ||= "2017-06-10") : custom_end
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      date = (Date.parse(start_date) + i).to_s.split("-")
      date = "#{date[1]}/#{date[2]}/#{date[0]}"
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      @limit = response["total"]
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      data = response["data"]
      total_dwh_revenue_usd = total_amount_usd = 0
      rsvsn_by_pgw_method = Hash.new(0)
      rsvsn_by_property = Hash.new(0)
      rsvsn_by_country = Hash.new(0)
      rsvsn_by_currency = Hash.new(0)
      rsvsn_by_commission = Hash.new(0)
      rsvsn_by_adults_children = Hash.new(0)
      rsvsn_by_nights = Hash.new(0)
      rsvsn_by_payment_type = Hash.new(0)
      rsvsn_ratemixed = Hash[
        total:              0,
        confirmation_nos:   []
      ]
      rsvsn_nocreditcard = Hash[
        total:              0,
        confirmation_nos:   []
      ]
      for i in 0..(response["total"] - 1)
        rsvsn_by_pgw_method[data[i]["pgw_method"]] += 1
        rsvsn_by_property[data[i]["property_name"]] += 1
        rsvsn_by_country[data[i]["country_name"]] += 1
        rsvsn_by_currency[data[i]["currency_name"]] += 1
        rsvsn_by_commission[data[i]["pct_commission"]] += 1
        rsvsn_by_adults_children[data[i]["no_of_adults_children"]] += 1
        rsvsn_by_nights[data[i]["no_of_nights"]] += 1
        rsvsn_by_payment_type[data[i]["payment_type"]] += 1
        total_dwh_revenue_usd += data[i]["dwh_revenue_usd"].to_f
        total_amount_usd += data[i]["total_amount_usd"].to_f
        unless data[i]["mixed_info"].nil?
          rsvsn_ratemixed[:total] += 1
          rsvsn_ratemixed[:confirmation_nos].push(data[i]["confirmation_no"])
        end
        if data[i]["ccard_no"] == "" && data[i]["ccard_name"].nil? && data[i]["expiry"].nil?
          rsvsn_nocreditcard[:total] += 1
          rsvsn_nocreditcard[:confirmation_nos].push(data[i]["confirmation_no"])
        end
      end

      # Create JSON Report
      filename = "reports/Reservations Information (#{FigNewton.environment}).json"
      data = {}
      data = JSON.parse(File.read(filename)) if File.file?(filename)
      data.update(Hash[date => Hash[
        "Total Reservations"                => response["total"],
        "Total DWH Revenue (USD)"           => total_dwh_revenue_usd.round(2),
        "Total Amount (USD)"                => total_amount_usd.round(2),
        "No. of Rate-Mixed Reservations"    => rsvsn_ratemixed,
        "No. of No-Credit-Card Bookings"    => rsvsn_nocreditcard,
        "PGW_Method"                        => Hash[rsvsn_by_pgw_method.sort_by { |_k, v| v }.reverse],
        "Payment Type"                      => Hash[rsvsn_by_payment_type.sort_by { |_k, v| v }.reverse],
        "Country"                           => Hash[rsvsn_by_country.sort_by { |_k, v| v }.reverse],
        "Currency"                          => Hash[rsvsn_by_currency.sort_by { |_k, v| v }.reverse],
        "Commission"                        => Hash[rsvsn_by_commission.sort_by { |_k, v| v }.reverse],
        "No. of Nights"                     => Hash[rsvsn_by_nights.sort_by { |_k, v| v }.reverse],
        "No. of Adults & Children"          => Hash[rsvsn_by_adults_children.sort_by { |_k, v| v }.reverse],
        "Property"                          => Hash[rsvsn_by_property.sort_by { |_k, v| v }.reverse]
      ]])
      data = Hash[data.sort.reverse]
      File.open(filename, "w") { |f| f.write(data.to_json) }
      puts JSON.pretty_generate(data[date])
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support can get the number of mobile reservations (.*?)$/) do |date|
  if browser == :none
    step "guest support logs into the Control Panel"
    start_date = custom_start.nil? ? (get_date(date)[0] ||= "2017-06-01") : custom_start
    end_date = custom_end.nil? ? (get_date(date)[0] ||= "2017-06-10") : custom_end
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      date = (Date.parse(start_date) + i).to_s.split("-")
      date = "#{date[1]}/#{date[2]}/#{date[0]}"
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      @limit = response["total"]
      response = step "guest support gets total number of all confirmed reservations for #{date}"
      data = response["data"]
      reservation_ids = []
      for i in 0..(response["total"] - 1)
        reservation_ids[i] = data[i]["id"] unless data[i].nil?
      end
      count_mobilev7 = count_mobilev6 = 0
      reservation_ids.each do |reservation_id|
        url = FigNewton.url_cp_audit_log % { rsvsn_id: reservation_id }
        log = JSON.parse(get(url, @headers))["data"][0]
        count_mobilev7 += 1 if log["gi_version"].include? "mobilev7"
        count_mobilev6 += 1 if log["gi_version"].include? "mobile "
      end

      # Create JSON Report
      filename = "reports/Reservations Made Via Mobile (#{FigNewton.environment}).json"
      data = {}
      data = JSON.parse(File.read(filename)) if File.file?(filename)
      data.update(Hash[date => Hash[
        "Total Reservations" => response["total"],
        "Mobile v7" => count_mobilev7,
        "Mobile v6" => count_mobilev6
      ]])
      data = Hash[data.sort.reverse]
      File.open(filename, "w") { |f| f.write(data.to_json) }
      puts JSON.pretty_generate(data[date])
    end
  else
    raise "Not yet implemented using a browser"
  end
end
