# Control Panel Reservation Details
# ---------------------------------

Then(/^guest support sees that the number of occupants for those reservations is the same as the total number of rooms booked$/) do
  if browser == :none
    @reservations[:ids].each do |reservation_id|
      url = FigNewton.url_cp_audit_log % { rsvsn_id: reservation_id }
      logs = JSON.parse(get(url, @headers))
      log = logs["data"][logs["data"].size.to_i - 1]
      raise "Error: PHP Error found in Reservation Details window" if log.include?("PHP Error")
      confirmation_no = @reservations[:confirmation_nos][Hash[@reservations[:ids].map.with_index.to_a][reservation_id]]
      if log["status_id"] == 2
        raise "Error: Missing Room Details for #{FigNewton.environment} Confirmation No. #{confirmation_no}" if log["room_occupants"].empty?
        expect(log["total_no_of_rooms"]).to eq(log["room_occupants"].size), "Total Number of Rooms != Room Occupants, for #{FigNewton.environment} Confirmation No. #{confirmation_no}"
      end
    end
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^(.*?) most recent reservations in the Control Panel$/) do |limit|
  if browser == :none
    step "guest support logs into the Control Panel"
    url = FigNewton.url_cp_read_reservations
    payload = build_get_recent_reservations_payload(limit)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Unable to get the #{limit} most recent reservations"
    @reservations = Hash[ids: [], confirmation_nos: []]
    for i in 0..(limit.to_i - 1)
      @reservations[:ids][i] = response["data"][i]["id"]
      @reservations[:confirmation_nos][i] = response["data"][i]["confirmation_no"]
    end
  else
    raise "Not yet implemented using a browser"
  end
end
