# Search Reservation
# ------------------

Then(/^guest support searches for the reservation$/) do
  if browser == :none
    url = FigNewton.url_cp_read_reservations
    payload = build_search_reservation_payload(custom_confirmation_no)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to find reservation via search, #{response}"
    @reservation = response["data"]
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^guest support can view information about an existing reservation$/) do
  if browser == :none
    raise "Please provide the confirmation number of the reservation, i.e. CONFIRMATION_NO=4318902" if custom_confirmation_no.nil?
    step "guest support logs into the Control Panel"
    step "guest support searches for the reservation"
    puts JSON.pretty_generate(@reservation[0])
  else
    raise "Not yet implemented using a browser"
  end
end
