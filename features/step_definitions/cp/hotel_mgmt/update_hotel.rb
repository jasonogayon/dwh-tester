# Update Hotel Property
# ---------------------

When(/^guest support updates the property to GI Activated status$/) do
  step "guest support updates the property status to Submission Accepted"
  step "guest support updates the property information so that the property can be set to GI Activated"
  step "guest support updates the property status to Web Services Received"
  step "guest support updates the property status to Web Services Accepted"
  step "guest support updates the property status to Web Services Website Launched"
  step "guest support updates the property status to Go Live Received"
  step "guest support updates the property status to Go Live Accepted"
  step "guest support updates the property status to Go Live HI Training"
  step "guest support updates the property status to Go Live GI Activated"
  step "guest support updates the property category to Demo"
  step "admin enables BDO reservations for the property" if @property[:hotel_type].include? "BDO"
end

When(/^guest support updates the property status to (.*?)$/) do |status|
  if browser == :none
    url = FigNewton.url_cp_edit_property_status
    payload = build_update_hotel_status_payload(@property[:id], get_property_status(status))
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to update property status to #{status}"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support updates the property information so that the property can be set to GI Activated$/) do
  if browser == :none
    step "guest support updates the property logo"
    url = FigNewton.url_cp_edit_property_info
    payload = build_update_hotel_info_payload(@property)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to update property information\n#{response}"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support updates the property logo$/) do
  if browser == :none
    url = FigNewton.url_cp_upload_property_logo
    payload = build_update_hotel_image_payload(@property[:id])
    response = JSON.parse(post_multipart(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Failed to update property logo"
    @property.update(Hash[logo: response["data"]["realfilename"]])
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support updates the property category to Demo$/) do
  if browser == :none
    url = FigNewton.url_cp_change_category
    payload = build_update_hotel_category_payload(@property[:id])
    expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to update property category to Demo"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^admin enables BDO reservations for the property$/) do
  db = "dwhdb"
  query = "UPDATE core.m_organizations SET bdo_merchant_id = '#{FigNewton.bdo_merchant_id}', bdo_merchant_access_code = '#{FigNewton.bdo_merchant_access_code}', bdo_merchant_hash_key = '#{FigNewton.bdo_merchant_hash_key}' WHERE id = #{@property[:id]};"
  url = FigNewton.url_cp_dbquery
  payload = "db=#{db}&query=#{query}"
  expect(JSON.parse(post(url, payload, @headers))["success"]).to be(true), "Error: Failed to enable BDO reservations for the property"
end

# Dynamic Booking Engine Domain URLs
# ----------------------------------

When(/^an administrator adds a '(.*?)' subdomain for the hotel's (.*?) booking engine$/) do |subdomain, platform|
  if browser == :none
    begin
      url = FigNewton.url_cp_dbquery
      @property.update(Hash[ibe_domain_url_desktop: subdomain.nil? ? "null" : "\'#{subdomain}\'"]) if platform.include? "desktop"
      @property.update(Hash[ibe_domain_url_mobile: subdomain.nil? ? "null" : "\'#{subdomain}\'"]) if platform.include? "mobile"
      payload = build_update_hotel_ibe_domain_payload(@property)
      response = JSON.parse(post(url, payload, @headers))
      expect(response["success"]).to be(true), "Error: Failed to update hotel's dynamic domain URLs\n#{response}"
      expect(response["affected_rows"]).to be(1), "Error: More than 1 rows were affected by the update query\n#{response}"
    rescue
      step "an administrator adds a '' subdomain for the hotel's desktop booking engine"
      step "an administrator adds a '' subdomain for the hotel's mobile booking engine"
    end
  else
    raise "Not yet implemented using a browser"
  end
end
