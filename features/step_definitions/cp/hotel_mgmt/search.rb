# Search Hotel By Name
# --------------------

When(/^guest support searches for the newly created hotel property$/) do
  if browser == :none
    step "guest support logs into the Control Panel"
    url = FigNewton.url_cp_read_property
    payload = build_read_property_payload(@property[:property_name])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Couldn't find newly created hotel property in CP"
    @property.update(Hash[id: response["data"][0]["id"]])
    @property.update(Hash[property_name: "#{@property[:property_name]} No. #{@property[:spi_id]}"]) if property_name.nil? && !@property[:spi_id].nil?
    puts "#{@property[:id].to_s.ljust(10)} #{@property[:property_name]}"
  else
    raise "Not yet implemented using a browser"
  end
end

When(/^guest support searches for the existing hotel property$/) do
  if browser == :none
    step "guest support logs into the Control Panel"
    url = FigNewton.url_cp_read_property
    payload = build_read_property_by_id_payload(@property[:id])
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Couldn't find existing hotel property in CP"
    expect(response["total"]).to eq(1), "Error: Couldn't find existing hotel property in CP"
    @property.update(Hash[
      property_name:            response["data"][0]["org_name"],
      currency_code:            response["data"][0]["currency_code"],
      public_commission:        response["data"][0]["pct_commission"].to_i,
      private_commission:       response["data"][0]["private_account_rate_commision"].to_i,
      payment_scheme:           response["data"][0]["payment_scheme_id"].to_i,
      bdo_enabled:              !response["data"][0]["bdo_merchant_id"].nil?,
      pp_enabled:               response["data"][0]["pp_enabled"] == "1" ? true : false,
      privateaccount_enabled:   response["data"][0]["private_account_enabled"] == "t" ? true : false,
    ])
    is_dwh = response["data"][0]["payment_scheme_id"].to_i == 1
    @property.update(Hash[is_dwh: is_dwh, hotel_type: is_dwh ? "DWH" : "Hotel"])
    @property.update(Hash[public_rateplans: get_public_rateplans(@property[:hotel_type], @property[:is_dwh])])
    @property.update(Hash[private_rateplans: get_private_rateplans(@property[:hotel_type], @property[:is_dwh])])
    puts "#{@property[:id].to_s.ljust(10)} #{@property[:property_name]}"
    puts "(This is a custom hotel property as set by tester for this particular test)" unless property_id.nil?
  else
    raise "Not yet implemented using a browser"
  end
end

Then(/^admin checks what hotel type the property is$/) do
  raise "Please provide a desired property ID in order to create rate plans, i.e. PROP_ID=18442" if property_id.nil?
  hotel = "Global Collect" if hotel.nil?
  @property = build_default_hotelinfo(hotel)
  @property.update(Hash[id: property_id.nil? ? get_hotel_id(@property[:hotel_type], @property[:is_asiapay]) : property_id.to_i])
  step "guest support searches for the existing hotel property"
  unless @property[:payment_scheme] == 1
    if @property[:bdo_enabled] || @property[:pp_enabled]
      @property.update(build_default_hotelinfo("BDO")) if @property[:bdo_enabled]
      @property.update(build_default_hotelinfo("Paypal-Only")) if @property[:pp_enabled]
    else
      @property.update(build_default_hotelinfo("HPP"))
    end
    @property[:public_rateplans] = filter_rateplans(@property, get_public_rateplans("HPP", false))
    @property[:private_rateplans] = filter_rateplans(@property, get_private_rateplans("HPP", false))
  end
end

When(/^guest support can get the total number of active hotels$/) do
  if browser == :none
    start = has_privateaccounts = is_dwh = is_hpp = is_bdo = is_paypal = has_ratemixing = has_fullpayment = has_nocreditcard = has_dynamic_dibe_domain = has_dynamic_mibe_domain = 0
    step "guest support logs into the Control Panel"
    url = FigNewton.url_cp_get_properties
    payload = build_get_hotels_by_country_payload(start, 1)
    response = JSON.parse(post(url, payload, @headers))
    expect(response["success"]).to be(true), "Error: Couldn't find all active hotels in CP"
    remaining = response["total"]

    active_hotels = {}
    ratemixed_hotels = []
    nocc_hotels = []
    hotels_with_dynamic_ibe_domains_desktop = []
    hotels_with_dynamic_ibe_domains_mobile = []
    hotels_by_country = Hash.new(0)
    hotels_by_currency = Hash.new(0)
    hotels_by_commission = Hash.new(0)
    hotels_by_market = Hash.new(0)
    while remaining > 0
      limit = 500
      payload = build_get_hotels_by_country_payload(start, limit)
      response = JSON.parse(post(url, payload, @headers))
      response["data"].each do |hotel|
        hotels_by_country[hotel["country_name"]] += 1
        hotels_by_currency[hotel["currency_name"]] += 1
        hotels_by_commission[hotel["pct_commission"]] += 1
        hotels_by_market[hotel["market_name"]] += 1
        has_privateaccounts += 1 if hotel["private_account_enabled"] == "t"
        if hotel["rate_mixing_flag"] == "1"
          has_ratemixing += 1
          ratemixed_hotels.push(hotel["org_name"])
        end
        if hotel["cp_no_cc_booking_flag"] == "t"
          has_nocreditcard += 1
          nocc_hotels.push(hotel["org_name"])
        end
        unless hotel["ibe_desktop_subdomain"].nil?
          has_dynamic_dibe_domain += 1
          hotels_with_dynamic_ibe_domains_desktop.push("#{hotel['org_name']}: http://#{hotel['ibe_desktop_subdomain']}/reservation/selectDates/#{hotel['id']}")
        end
        unless hotel["ibe_mobile_subdomain"].nil?
          has_dynamic_mibe_domain += 1
          hotels_with_dynamic_ibe_domains_mobile.push("#{hotel['org_name']}: http://#{hotel['ibe_mobile_subdomain']}/reservation/selectDates/#{hotel['id']}")
        end
        has_fullpayment += 1 if hotel["full_payment_flag"] == "1"
        if hotel["payment_scheme_id"] == "2"
          if hotel["payment_processor"] == "BD"
            is_bdo += 1
          elsif hotel["payment_processor"] == "PP"
            is_paypal += 1
          else
            is_hpp += 1
          end
        else
          is_dwh += 1
        end
      end
      remaining -= limit
      start += limit
    end
    active_hotels.update(Hash[
      Total:                      response["total"],
      Country:                    Hash[hotels_by_country.sort_by { |_k, v| v }.reverse],
      Payment_Processor:          Hash[DWH: is_dwh, HPP: is_hpp, BDO: is_bdo, Paypal: is_paypal],
      Currency:                   Hash[hotels_by_currency.sort_by { |_k, v| v }.reverse],
      Percent_Commission:         Hash[hotels_by_commission.sort_by { |_k, v| v }.reverse],
      FullPayment_Enabled:        has_fullpayment,
      PrivateAccounts_Enabled:    has_privateaccounts,
      RateMixing_Enabled:         Hash[total: has_ratemixing, hotels: ratemixed_hotels.sort],
      NoCreditCard_Enabled:       Hash[total: has_nocreditcard, hotels: nocc_hotels.sort],
      Dynamic_Domains_Enabled:    Hash[total_desktop: has_dynamic_dibe_domain, total_mobile: has_dynamic_mibe_domain, hotels_desktop: hotels_with_dynamic_ibe_domains_desktop.sort]
    ])

    # Create JSON Report
    date = Date.parse(get_date("TODAY")[0]).to_s.split("-")
    date = "#{date[1]}/#{date[2]}/#{date[0]}"
    filename = "reports/Number of Active Hotels (#{FigNewton.environment}).json"
    data = {}
    data = JSON.parse(File.read(filename)) if File.file?(filename)
    data.update(Hash[date => active_hotels])
    data = Hash[data.sort.reverse]
    File.open(filename, "w") { |f| f.write(data.to_json) }
    puts JSON.pretty_generate(data[date])
  else
    raise "Not yet implemented using a browser"
  end
end
