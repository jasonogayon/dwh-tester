Feature: Create Custom Reservations on the Desktop or Mobile IBE

    Guests should be able to book a hotel room on a desired property and promo rate plan package via the desktop or mobile booking engine.

    Background:
        Given an existing hotel property with rooms


    @custom_rsvsn @desktop @confirm
    Scenario: Create Custom Desktop Public Confirmed Reservation
        Then guest can book a random number of rooms for a desired promo package and stay dates on the desktop IBE

    @custom_rsvsn @desktop @cancel
    Scenario: Create Custom Desktop Public Cancelled Reservation
        Then guest can book a random number of rooms for a desired promo package and stay dates on the desktop IBE
        And cancels the reservation

    @custom_rsvsn @desktop @noshow
    Scenario: Create Custom Desktop Public No-Show Reservation
        Then guest can book a random number of rooms for a desired promo package and stay dates on the desktop IBE
        And guest support marks the reservation as no-show

    @custom_rsvsn @mobile @confirm
    Scenario: Create Custom Mobile Public Confirmed Reservation
        Then guest can book a random number of rooms for a desired promo package and stay dates on the mobile IBE
