Feature: View Hotel Information

    Hotelier should be able to view hotel information from a desired property


    @view_rooms
    Scenario: View Information From a Desired Hotel, Rooms
        Given an existing hotel property with rooms

    @view_public_rateplans
    Scenario: View Information From a Desired Hotel, Public Rate Plans
        Given an existing hotel property with public rate plans

    @view_private_rateplans
    Scenario: View Information From a Desired Hotel, Private Rate Plans
        Given an existing hotel property with private rate plans

    @view_private_accounts
    Scenario: View Information From a Desired Hotel, Private Accounts
        Given an existing hotel property with private accounts

    @view_taxes
    Scenario: View Information From a Desired Hotel, Taxes
        Given an existing hotel property with taxes

    @view_users
    Scenario: View Information From a Desired Hotel, Users
        Given an existing hotel property with users

    @view_all
    Scenario: View Information From a Desired Hotel, All
        Given an existing hotel property with property information, contacts, facilities, amenities, rooms, taxes, advisories, add-ons, users, public rate plans, private accounts, and private rate plans
