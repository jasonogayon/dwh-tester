Feature: Create Private Rate Plans, Nightly

    Hotelier should be able to create a nightly private rate plan on the extranet.


    @custom_rateplan @private @dwh @full @nonrefundable
    Scenario: Create a Private Rate Plan, DWH Full Non-Refundable
        Given an existing Global Collect hotel property
        Then hotel manager adds a Full Non-Refundable private rate plan into the existing property

    @custom_rateplan @private @dwh @full @refundable @leadtime
    Scenario: Create a Private Rate Plan, DWH Full Refundable, Lead Time Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Full Refundable with Lead Time Penalty private rate plan into the existing property

    @custom_rateplan @private @dwh @full @refundable @firstnight
    Scenario: Create a Private Rate Plan, DWH Full Refundable, First Night Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Full Refundable with First Night Penalty private rate plan into the existing property

    @custom_rateplan @private @dwh @full @refundable @fullcharge
    Scenario: Create a Private Rate Plan, DWH Full Refundable, Full Charge Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Full Refundable with Full Charge Penalty private rate plan into the existing property

    @custom_rateplan @private @dwh @full @refundable @none
    Scenario: Create a Private Rate Plan, DWH Full Refundable, No Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Full Refundable with No Penalty private rate plan into the existing property



    @custom_rateplan @private @hpp @arrival @leadtime
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Arrival with Lead Time Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @arrival @firstnight
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, First Night Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Arrival with First Night Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @arrival @fullcharge
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, Full Charge Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Arrival with Full Charge Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @arrival @none
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, No Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Arrival with No Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Non-Refundable, Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @refundable @leadtime
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @refundable @firstnight
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Refundable with First Night Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @refundable @fullcharge
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty private rate plan into the existing property

    @custom_rateplan @private @hpp @partial @refundable @none
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Refundable with No Penalty private rate plan into the existing property



    @custom_rateplan @private @bdo @partial @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Non-Refundable, Immediate Penalty
        Given an existing BDO hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable private rate plan into the existing property

    @custom_rateplan @private @bdo @partial @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing BDO hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property

    @custom_rateplan @private @bdo @partial @refundable @leadtime
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing BDO hotel property
        Then hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty private rate plan into the existing property



    @custom_rateplan @private @pp @full @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Non-Refundable, Immediate Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Non-Refundable private rate plan into the existing property

    @custom_rateplan @private @pp @full @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property

    @custom_rateplan @private @pp @full @refundable @leadtime
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, Lead Time Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Refundable with Lead Time Penalty private rate plan into the existing property

    @custom_rateplan @private @pp @full @refundable @firstnight
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, First Night Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Refundable with First Night Penalty private rate plan into the existing property

    @custom_rateplan @private @pp @full @refundable @fullcharge
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, Full Charge Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Refundable with Full Charge Penalty private rate plan into the existing property

    @custom_rateplan @private @pp @full @refundable @none
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, No Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Pay upon Booking Full Prepayment Refundable with No Penalty private rate plan into the existing property
