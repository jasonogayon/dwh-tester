Feature: Delete Unnecessary Rate Plans

    @delete_unnecessary_rateplans @public @dwh
    Scenario: Delete Unnecessary Public Rate Plans from Test Hotel, Global Collect
        Given an existing Global Collect hotel property
        Then hotel manager can delete all unnecessary public rate plans from test hotel
        And hotel manager can also delete all unnecessary private rate plans from test hotel

    @delete_unnecessary_rateplans @public @hpp
    Scenario: Delete Unnecessary Public Rate Plans from Test Hotel, HPP
        Given an existing HPP hotel property
        Then hotel manager can delete all unnecessary public rate plans from test hotel
        And hotel manager can also delete all unnecessary private rate plans from test hotel

    @delete_unnecessary_rateplans @public @bdo
    Scenario: Delete Unnecessary Public Rate Plans from Test Hotel, BDO
        Given an existing BDO hotel property
        Then hotel manager can delete all unnecessary public rate plans from test hotel
        And hotel manager can also delete all unnecessary private rate plans from test hotel

    @disable_nocc
    Scenario: Disable No-Credit-Card Feature
        Given an existing Global Collect hotel property
        When guest support enables the no-credit-card booking feature for the property
        Then guest support disables the no-credit-card booking feature for the property
