Feature: Create Custom Hotel Property

    Sales partner can create a new hotel property via the SPI.
    Guest support can update the newly created hotel into GI Activated status.
    Guest support can add hotel information to the newly created hotel since they have access to the Hotelier Extranet.


    @gc_hotel
    Scenario: Create a new Hotel, Global Collect
        Given a new Global Collect hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @ap_hotel
    Scenario: Create a new Hotel, Asiapay
        Given a new Asiapay hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @hpp_hotel
    Scenario: Create a new Hotel, HPP
        Given a new HPP hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @bdo_hotel
    Scenario: Create a new Hotel, BDO
        Given a new BDO hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @bp_hotel
    Scenario: Create a new Hotel, BDO-Paypal in PHP
        Given a new BDO-Paypal in PHP hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @pp_hotel
    Scenario: Create a new Hotel, Paypal Only
        Given a new Paypal Only hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories

    @bn_hotel
    Scenario: Create a new Hotel, Bancnet with Rate Mixing
        Given a new Bancnet with Rate Mixing hotel property with property information, amenities, facilities, rooms, extra bed information, users, taxes and surcharges, nightly public rate plans, private rate plans, contacts, add-ons, and advisories
