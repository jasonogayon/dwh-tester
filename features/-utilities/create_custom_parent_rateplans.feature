Feature: Create Parent Rate Plans, Nightly

    Hotelier should be able to create a nightly rates rate plan on the extranet.


    @new_rateplans
    Scenario: Create Generic Parent Rate Plans for a Desired Property
        * admin can create public and private rate plans on a desired hotel property

    @new_child_rateplans
    Scenario: Create Generic Child Rate Plans for a Desired Parent
        * admin can create child rate plans on a desired parent



    @custom_rateplan @parent @public @nightly @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Lead Time Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Partial Prepayment with Lead Time Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @partial @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, First Night Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Partial Prepayment with First Night Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @partial @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Full Charge Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Partial Prepayment with Full Charge Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @partial @none
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, No Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Partial Prepayment with No Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @full @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, Lead Time Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Full Refundable with Lead Time Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @full @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, First Night Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Full Refundable with First Night Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @full @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, Full Charge Immediate Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Full Refundable with Full Charge Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @dwh @full @refundable @none
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, No Penalty
        Given an existing Global Collect hotel property
        Then hotel manager adds a Nightly Full Refundable with No Penalty public rate plan into the existing property



    @custom_rateplan @parent @public @nightly @hpp @arrival @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Arrival, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Arrival with Lead Time Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @arrival @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Arrival, First Night Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Arrival with First Night Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @arrival @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Arrival, Full Charge Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Arrival with Full Charge Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @arrival @none
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Arrival, No Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Arrival with No Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Non-Refundable, Immediate Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Non-Refundable public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Refundable with First Night Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @hpp @partial @refundable @none
    Scenario: Create a Public Parent Rate Plan, Nightly, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing HPP hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Refundable with No Penalty public rate plan into the existing property



    @custom_rateplan @parent @public @nightly @bdo @partial @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Nightly, BDO Pay upon Booking Partial Non-Refundable, Immediate Penalty
        Given an existing BDO hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Non-Refundable public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @bdo @partial @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Nightly, BDO Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing BDO hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @bdo @partial @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, BDO Pay upon Booking Partial Refundable, Lead Time Penalty
        Given an existing BDO hotel property
        Then hotel manager adds a Nightly Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty public rate plan into the existing property



    @custom_rateplan @parent @public @nightly @pp @full @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Non-Refundable, Immediate Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Non-Refundable public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @pp @full @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Non-Refundable, Modification/Cancellation Not Allowed
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @pp @full @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Refundable, Lead Time Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Refundable with Lead Time Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @pp @full @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Refundable, First Night Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Refundable with First Night Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @pp @full @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Refundable, Full Charge Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Refundable with Full Charge Penalty public rate plan into the existing property

    @custom_rateplan @parent @public @nightly @pp @full @refundable @none
    Scenario: Create a Public Parent Rate Plan, Nightly, Paypal-Only Pay upon Booking Full Refundable, No Penalty
        Given an existing Paypal-Only hotel property
        Then hotel manager adds a Nightly Pay upon Booking Full Prepayment Refundable with No Penalty public rate plan into the existing property
