Feature: Highlight a Rate Plan

    Hotelier should be able to highlight a rate plan, but only one rate plan at a time.

    Background:
        Given an existing Global Collect hotel property


    @highlight @add
    Scenario: Highlight a Rate Plan
        When hotel manager highlights a Public Full Prepayment Non-Refundable rate plan
        Then guest sees that the Public Full Prepayment Non-Refundable rate plan is highlighted on the desktop IBE

    @highlight @edit
    Scenario: Choose a New Rate Plan to Highlight
        When hotel manager highlights a Public Full Prepayment Non-Refundable rate plan
        But changes the highlight to a Public Partial Prepayment with Lead Time Penalty rate plan
        Then guest sees that the Public Partial Prepayment with Lead Time Penalty rate plan is highlighted on the desktop IBE
        And the Public Full Prepayment Non-Refundable rate plan is not highlighted on the desktop IBE

    @highlight @delete
    Scenario: Remove Rate Plan Highlighting
        When hotel manager highlights a Public Full Prepayment Refundable with Lead Time Penalty rate plan
        But disables the highlighting immediately afterwards
        Then guest sees that the Public Full Prepayment Refundable with Lead Time Penalty rate plan is not highlighted on the desktop IBE
