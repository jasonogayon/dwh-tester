Feature: Create Parent Rate Plans, DWH Nightly

    Hotelier should be able to create a nightly rates rate plan on the extranet. DWH rate plans has its own set of policies.

    Notes:
    - A rate plan is still created even when the promo code used for building the rate plan is a duplicate, unlike for duplicate rate plan names
    - It is possible to create rate plans with promo code that has less than 3 characters and has spaces, which is invalid in the user interface

    Background:
        Given an existing Global Collect hotel property


    @create_rateplan @parent @active @public @nightly @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Lead Time Penalty
        When hotel manager adds a Nightly Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @partial @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, First Night Immediate Penalty
        When hotel manager adds a Nightly Partial Prepayment with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @partial @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Full Charge Immediate Penalty
        When hotel manager adds a Nightly Partial Prepayment with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @partial @none
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, No Penalty
        When hotel manager adds a Nightly Partial Prepayment with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable
        When hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @full @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, Lead Time Penalty
        When hotel manager adds a Nightly Full Refundable with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @full @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, First Night Immediate Penalty
        When hotel manager adds a Nightly Full Refundable with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @full @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, Full Charge Immediate Penalty
        When hotel manager adds a Nightly Full Refundable with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @nightly @dwh @full @refundable @none
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, No Penalty
        When hotel manager adds a Nightly Full Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE


    # Inactive Rate Plan
    # ------------------

    @create_rateplan @parent @inactive @public @nightly @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Lead Time Penalty, Inactive
        When hotel manager adds a Nightly Inactive Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        And the rate plan is also not available in the mobile IBE


    # Mobile-Only Rate Plan
    # ---------------------

    @create_rateplan @parent @mobile @public @nightly @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable, Mobile-Only
        When hotel manager adds a Nightly Mobile-Only Full Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        But the rate plan is available in the mobile IBE


    # Special Rate Plan
    # -----------------

    @create_rateplan @parent @special @public @nightly @dwh @full @refundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, No Penalty, Special
        When hotel manager adds a Nightly Special Full Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE


    # With Extra Beds
    # ---------------

    @create_rateplan @parent @extrabed @public @nightly @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Partial, Lead Time Penalty, With Extra Beds
        When hotel manager adds a Nightly Partial Prepayment with Lead Time Penalty public rate plan with extra beds into the existing property
        Then guest sees that the extra beds dropdown is displayed on the desktop IBE rate plan page
        And hotel manager can remove the extra bed setting from the rate plan
        And guest sees that the extra beds dropdown is not displayed anymore on the desktop IBE rate plan page afterwards


    # With Add-ons
    # ------------

    @create_rateplan @parent @addon @public @nightly @dwh @full @nonrefundable @flaky
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable, With Add-ons
        When hotel manager adds a Nightly Full Non-Refundable public rate plan with add-ons into the existing property
        Then guest sees that the add-ons section is displayed on the desktop IBE payment information page
        And hotel manager can remove the add-on setting from the rate plan
        Then guest sees that the add-ons section is not displayed anymore on the desktop IBE payment information page afterwards


    # With Promo Code
    # ---------------

    @create_rateplan @parent @promocode @public @nightly @dwh @full @refundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Refundable, No Penalty, With Promo Code
        When hotel manager adds a Nightly Special Full Refundable with No Penalty public rate plan with a promo code of "LIMITEDOFFER" into the existing property
        Then hotel manager cannot create a new rate plan with the same promo code
        And hotel manager can change the rate plan's promo code to "NEWPROMOCODES"
        And guest sees that the rate plan is marked as an exclusive promo on both desktop and mobile IBEs
        And guest cannot see the rate plan if promo code is not inputted when viewing the rate plan page
        And hotel manager can delete the rate plan's promo code


    # No Credit Card
    # --------------

    @create_rateplan @parent @nocc @public @nightly @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Nightly, DWH Full Non-Refundable, No Credit Card
        When hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property
        And guest support enables the no-credit-card booking feature for the property
        And guest sees that the rate plan is marked as a no-credit-card promo on the desktop IBE
        And a copy of "NO CREDIT CARD NEEDED" is displayed on the desktop IBE payment information page
        And guest sees that the rate plan is also marked as a no-credit-card promo on the mobile IBE
        And a copy of "Secure this booking without a credit card.Payment for your stay will be made at the hotel." is also displayed on the mobile IBE payment information page

    @create_rateplan @parent @nocc @alltime
    Scenario: Create a Public Parent Rate Plan, All-Time No Credit Card
        When hotel manager adds a Nightly Full Non-Refundable public rate plan into the existing property
        And guest support enables the all-time no-credit-card booking feature for the property
        And guest sees that the rate plan is marked as a no-credit-card promo on the desktop IBE
        And a copy of "NO CREDIT CARD NEEDED" is displayed on the desktop IBE payment information page
        And guest sees that the rate plan is also marked as a no-credit-card promo on the mobile IBE
        And a copy of "Secure this booking without a credit card.Payment for your stay will be made at the hotel." is also displayed on the mobile IBE payment information page
