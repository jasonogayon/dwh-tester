Feature: Create Child Rate Plans, DWH Nightly

    Hotelier should be able to create a nightly rates child rate plan on the extranet. DWH rate plans has its own set of policies.

    Background:
        Given an existing Global Collect hotel property


    @create_rateplan @child @active @public @nightly @dwh @partial @leadtime
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Partial, Lead Time Penalty
        When hotel manager adds a Nightly Partial Prepayment with Lead Time Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @partial @firstnight @advanced_purchase
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Partial, First Night Immediate Penalty
        When hotel manager adds a "10.98% discount Advanced Purchase" promo Nightly Partial Prepayment with First Night Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And guest sees that the child rate plan is marked as a Advanced Purchase promo on the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @partial @fullcharge @last_minute
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Partial, Full Charge Immediate Penalty
        When hotel manager adds a "USD 5.34 discount Last Minute" promo Nightly Partial Prepayment with Full Charge Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And guest sees that the child rate plan is marked as a Last Minute promo on the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @partial @none @flash_deal
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Partial, No Penalty
        When hotel manager adds a "10% discount Flash Deal" promo Nightly Partial Prepayment with No Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And guest sees that the child rate plan is marked as a Flash Deal promo on the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @full @nonrefundable @minimum_night
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Full Non-Refundable
        When hotel manager adds a "USD 15 discount Minimum Night" promo Nightly Full Non-Refundable child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @full @refundable @leadtime @limited_offer
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Full Refundable, Lead Time Penalty
        When hotel manager adds a "91% discount Limited Offer" promo Nightly Full Refundable with Lead Time Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And guest sees that the child rate plan is marked as a Limited Offer promo on the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @full @refundable @firstnight
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Full Refundable, First Night Immediate Penalty
        When hotel manager adds a Nightly Full Refundable with First Night Penalty child rate plan with inherited policy into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @full @refundable @fullcharge
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Full Refundable, Full Charge Immediate Penalty
        When hotel manager adds a Nightly Full Refundable with Full Charge Penalty child rate plan with custom policy into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @child @active @public @nightly @dwh @full @refundable @none
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Full Refundable, No Penalty
        When hotel manager adds a Nightly Full Refundable with No Penalty child rate plan into the existing property
        Then guest sees that the child rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE


    # Add Rooms Via Derived Rate Plans Calendar
    # -----------------------------------------

    @create_rateplan @child @add_rooms
    Scenario: Create a Public Child Rate Plan, Nightly, DWH Partial, Lead Time Penalty
        When hotel manager adds a Nightly Partial Prepayment with Lead Time Penalty child rate plan into the existing property
        Then hotel manager can add rooms to the child rate plan via the Derived Rate Plans Calendar
