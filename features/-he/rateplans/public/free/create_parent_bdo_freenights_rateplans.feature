Feature: Create Parent Rate Plans, BDO Free Nights

    Hotelier should be able to create a free nights package rate plan on the extranet. BDO rate plans has its own set of policies.

    Background:
        Given an existing BDO hotel property


    @create_rateplan @parent @active @public @free @bdo @partial @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Free Nights, BDO Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Free Nights Pay upon Booking Partial Prepayment Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @bdo @partial @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Free Nights, BDO Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Free Nights Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @bdo @partial @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Free Nights, BDO Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Free Nights Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE
