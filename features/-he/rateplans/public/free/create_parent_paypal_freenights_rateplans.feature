Feature: Create Parent Rate Plans, Paypal-Only Free Nights

    Hotelier should be able to create a free nights package rate plan on the extranet. Paypal-Only rate plans has its own set of policies.

    Background:
        Given an existing Paypal-Only hotel property


    @create_rateplan @parent @active @public @free @pp @full @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Non-Refundable, Immediate Penalty
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @full @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @full @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Refundable, Lead Time Penalty
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Refundable with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @full @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Refundable, First Night Penalty
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Refundable with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @full @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Refundable, Full Charge Penalty
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Refundable with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @full @refundable @none
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Full Refundable, No Penalty
        When hotel manager adds a Free Nights Pay upon Booking Full Prepayment Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE


    # Partial Rate Plan
    # -----------------

    @create_rateplan @parent @active @public @free @pp @partial @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Free Nights Pay upon Booking Partial Prepayment Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        And the rate plan is also not available in the mobile IBE

    @create_rateplan @parent @active @public @free @pp @firstnight @refundable @none
    Scenario: Create a Public Parent Rate Plan, Free Nights, Paypal-Only Pay upon Booking First Night Refundable, No Penalty
        When hotel manager adds a Free Nights Pay upon Booking First Night Prepayment Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        And the rate plan is also not available in the mobile IBE
