Feature: Create Parent Rate Plans, DWH Fixed Nights

    Hotelier should be able to create a fixed nights package rate plan on the extranet. DWH rate plans has its own set of policies.

    Background:
        Given an existing Global Collect hotel property


    @create_rateplan @parent @active @public @fixed @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Partial, Lead Time Penalty
        When hotel manager adds a Fixed Nights Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @partial @firstnight
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Partial, First Night Immediate Penalty
        When hotel manager adds a Fixed Nights Partial Prepayment with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @partial @fullcharge
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Partial, Full Charge Immediate Penalty
        When hotel manager adds a Fixed Nights Partial Prepayment with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @partial @none
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Partial, No Penalty
        When hotel manager adds a Fixed Nights Partial Prepayment with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Non-Refundable
        When hotel manager adds a Fixed Nights Full Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @full @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Refundable, Lead Time Penalty
        When hotel manager adds a Fixed Nights Full Refundable with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @full @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Refundable, First Night Immediate Penalty
        When hotel manager adds a Fixed Nights Full Refundable with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @full @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Refundable, Full Charge Immediate Penalty
        When hotel manager adds a Fixed Nights Full Refundable with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @dwh @full @refundable @none
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Refundable, No Penalty
        When hotel manager adds a Fixed Nights Full Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE


    # Inactive Rate Plan
    # ------------------

    @create_rateplan @parent @inactive @public @fixed @dwh @partial @leadtime
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Partial, Lead Time Penalty, Inactive
        When hotel manager adds a Fixed Nights Inactive Partial Prepayment with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        And the rate plan is also not available in the mobile IBE


    # Mobile-Only Rate Plan
    # ---------------------

    @create_rateplan @parent @mobile @public @fixed @dwh @full @nonrefundable
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Non-Refundable, Mobile-Only
        When hotel manager adds a Fixed Nights Mobile-Only Full Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is not available in the desktop IBE
        But the rate plan is available in the mobile IBE


    # Special Rate Plan
    # -----------------

    @create_rateplan @parent @special @public @fixed @dwh @full @refundable
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, DWH Full Refundable, No Penalty, Special
        When hotel manager adds a Fixed Nights Special Full Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE
