Feature: Create Parent Rate Plans, HPP Fixed Nights

    Hotelier should be able to create a fixed nights package rate plan on the extranet. HPP rate plans has its own set of policies.

    Background:
        Given an existing HPP hotel property


    @create_rateplan @parent @active @public @fixed @hpp @arrival @leadtime
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Arrival, Lead Time Penalty
        When hotel manager adds a Fixed Nights Pay upon Arrival with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @arrival @firstnight
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Arrival, First Night Immediate Penalty
        When hotel manager adds a Fixed Nights Pay upon Arrival with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @arrival @fullcharge
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Arrival, Full Charge Immediate Penalty
        When hotel manager adds a Fixed Nights Pay upon Arrival with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @arrival @none
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Arrival, No Penalty
        When hotel manager adds a Fixed Nights Pay upon Arrival with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @nonrefundable @immediate
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Non-Refundable public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @nonrefundable @notallowed
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @refundable @leadtime
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @refundable @firstnight
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Refundable with First Night Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @refundable @fullcharge
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE

    @create_rateplan @parent @active @public @fixed @hpp @partial @refundable @none
    Scenario: Create a Public Parent Rate Plan, Fixed Nights, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Fixed Nights Pay upon Booking Partial Prepayment Refundable with No Penalty public rate plan into the existing property
        Then guest sees that the public rate plan is available in the desktop IBE
        And the rate plan is also available in the mobile IBE
