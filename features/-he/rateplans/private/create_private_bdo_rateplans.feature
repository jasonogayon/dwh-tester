Feature: Create Private Rate Plans, BDO Nightly

    Hotelier should be able to create a nightly private rate plan on the extranet. BDO rate plans has its own set of policies.

    Background:
        Given an existing BDO hotel property


    @create_rateplan @private @bdo @partial @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @bdo @partial @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @bdo @partial @refundable @leadtime
    Scenario: Create a Private Rate Plan, BDO Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE
