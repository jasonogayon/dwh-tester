Feature: Create Private Rate Plans, Paypal-Only Nightly

    Hotelier should be able to create a nightly private rate plan on the extranet. Paypal rate plans has its own set of policies.

    Background:
        Given an existing Paypal-Only hotel property


    @create_rateplan @private @pp @full @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Non-Refundable, Immediate Penalty
        When hotel manager adds a Pay upon Booking Full Prepayment Non-Refundable private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @pp @full @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Pay upon Booking Full Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @pp @full @refundable @leadtime
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Full Prepayment Refundable with Lead Time Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @pp @full @refundable @firstnight
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, First Night Penalty
        When hotel manager adds a Pay upon Booking Full Prepayment Refundable with First Night Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @pp @full @refundable @fullcharge
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, Full Charge Penalty
        When hotel manager adds a Pay upon Booking Full Prepayment Refundable with Full Charge Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @pp @full @refundable @none
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Full Refundable, No Penalty
        When hotel manager adds a Pay upon Booking Full Prepayment Refundable with No Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE


    # Partial Rate Plan
    # -----------------

    @create_rateplan @private @pp @partial @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable private rate plan into the existing property
        Then guest sees that the private rate plan is not available in the desktop IBE

    @create_rateplan @private @pp @firstnight @refundable @none
    Scenario: Create a Private Rate Plan, Paypal-Only Pay upon Booking First Night Refundable, No Penalty
        When hotel manager adds a Pay upon Booking First Night Prepayment Refundable with No Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is not available in the desktop IBE