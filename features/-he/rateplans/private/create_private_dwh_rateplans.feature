Feature: Create Private Rate Plans, DWH Nightly

    Hotelier should be able to create a nightly private rate plan on the extranet. DWH rate plans has its own set of policies.

    Background:
        Given an existing Global Collect hotel property


    @create_rateplan @private @dwh @full @nonrefundable
    Scenario: Create a Private Rate Plan, DWH Full Non-Refundable
        When hotel manager adds a Full Non-Refundable private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @dwh @full @refundable @leadtime
    Scenario: Create a Private Rate Plan, DWH Full Refundable, Lead Time Penalty
        When hotel manager adds a Full Refundable with Lead Time Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @dwh @full @refundable @firstnight
    Scenario: Create a Private Rate Plan, DWH Full Refundable, First Night Immediate Penalty
        When hotel manager adds a Full Refundable with First Night Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @dwh @full @refundable @fullcharge
    Scenario: Create a Private Rate Plan, DWH Full Refundable, Full Charge Immediate Penalty
        When hotel manager adds a Full Refundable with Full Charge Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @dwh @full @refundable @none
    Scenario: Create a Private Rate Plan, DWH Full Refundable, No Penalty
        When hotel manager adds a Full Refundable with No Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE
