Feature: Create Private Rate Plans, HPP Nightly

    Hotelier should be able to create a nightly private rate plan on the extranet. HPP rate plans has its own set of policies.

    Background:
        Given an existing HPP hotel property


    @create_rateplan @private @hpp @arrival @leadtime
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, Lead Time Penalty
        When hotel manager adds a Pay upon Arrival with Lead Time Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @arrival @firstnight
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, First Night Immediate Penalty
        When hotel manager adds a Pay upon Arrival with First Night Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @arrival @fullcharge
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, Full Charge Immediate Penalty
        When hotel manager adds a Pay upon Arrival with Full Charge Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @arrival @none
    Scenario: Create a Private Rate Plan, HPP Pay upon Arrival, No Penalty
        When hotel manager adds a Pay upon Arrival with No Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @nonrefundable @immediate
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Non-Refundable, Immediate Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @nonrefundable @notallowed
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Non-Refundable, Modification/Cancellation Not Allowed
        When hotel manager adds a Pay upon Booking Partial Prepayment Non-Refundable with Modification/Cancellation Not Allowed private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @refundable @leadtime
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @refundable @firstnight
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Refundable with First Night Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @refundable @fullcharge
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Refundable with Full Charge Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE

    @create_rateplan @private @hpp @partial @refundable @none
    Scenario: Create a Private Rate Plan, HPP Pay upon Booking Partial Refundable, Lead Time Penalty
        When hotel manager adds a Pay upon Booking Partial Prepayment Refundable with No Penalty private rate plan into the existing property
        Then guest sees that the private rate plan is available in the desktop IBE
