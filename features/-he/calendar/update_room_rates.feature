Feature: Room Rates

    Hotelier should be able to update room rates for a desired rate plan, unless for a zero, negative, or over the maximum rate limit input.

    Tests Marked as Not Ready:
    - Hotelier is able to set a rate plan room rate to zero through the HTTP layer

    Background:
        Given an existing Global Collect hotel property
        And hotel manager adds a "Standard" room with availability of 100 into a Public Full Prepayment Non-Refundable rate plan


    @room_rates @positive
    Scenario: Set Room Rates to 1234.56
        When hotelier updates the rate plan room rates to 1234.56 for check-in dates from TOMORROW to 3 DAYS FROM NOW
        Then hotelier sees that the room rates for those dates has been set to 1234.56 after the update
        And guest sees that the room rate per night on the desktop IBE also shows 1234.56

    @room_rates @zero @flaky
    Scenario: Set Room Rates to Zero (Below Minimum Rate)
        Then hotelier cannot update the rate plan room rate to a value of 0

    @room_rates @negative
    Scenario: Set Room Rates to -1234.56 (Below Minimum Rate)
        Then hotelier cannot update the rate plan room rate to a value of -1234.56

    @room_rates @limit
    Scenario: Set Room Rates to 1000000000000.1 (Over Maximum Rate Limit)
        Then hotelier cannot update the rate plan room rate to a value of 1000000000000.1
