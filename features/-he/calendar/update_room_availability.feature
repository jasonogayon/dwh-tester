Feature: Room Availability

    Hotelier should be able to update room availability, unless for a negative input.
    Room availability is also automatically updated when reservations are made or are cancelled.

    Background:
        Given an existing Global Collect hotel property
        And hotel manager adds a "Standard" room with availability of 100 into a Public Full Prepayment Non-Refundable rate plan


    @room_availability @positive
    Scenario: Set Room Availability to 8
        When hotelier updates the availability of the room to 8 for check-in dates from TODAY to 3 DAYS FROM NOW
        Then hotelier sees that the room availability for those dates has been set to 8 after the update
        And guest sees that the room availability in the desktop IBE also shows 8

    @room_availability @zero
    Scenario: Set Room Availability to Zero
        When hotelier updates the availability of the room to 0 for check-in dates from TOMORROW to 3 DAYS FROM NOW
        Then hotelier sees that the room availability for those dates has been set to 0 after the update
        And guest sees that the room is not available on the desktop IBE

    @room_availability @negative
    Scenario: Set Room Availability to -5
        Then hotelier cannot update the availability of the room to a value of -5
