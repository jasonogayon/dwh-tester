Feature: Extract Room Rates

    Hotelier should be able to extract room rates of a given rate plan for a desired range of dates.

    Background:
        Given an existing Global Collect hotel property


    @ratesextract
    Scenario: Extract Room Rates
        And hotel manager extract room rates from a Public Full Prepayment Non-Refundable rate plan
        # Then hotelier sees that the room availability for those dates has been set to 8 after the update
