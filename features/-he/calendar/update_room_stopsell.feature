Feature: Room Stop Sell

    Hotelier should be able to update room stop sell status for a desired rate plan

    Background:
        Given an existing Global Collect hotel property
        And hotel manager adds a "Standard" room with availability of 100 into a Public Full Prepayment Non-Refundable rate plan


    @room_stopsell @add
    Scenario: Set Stop Sell
        When hotelier stops selling the room for check-in dates from TOMORROW to 3 DAYS FROM NOW
        Then hotelier sees that the room is marked as stop sell after the update
        And guest sees that the room is not available on the desktop IBE

    @room_stopsell @remove
    Scenario: Remove Stop Sell
        When hotelier removes the room stops sell for check-in dates from TOMORROW to 3 DAYS FROM NOW
        Then hotelier sees that the room is not marked as stop sell after the update
        And guest sees that the room availability in the desktop IBE shows 100