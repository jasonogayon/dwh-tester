Feature: Request a Reservation for Cancellation

    Hotelier should be able to request for a reservation to be cancelled, except when the reservation is DWH-processed.


    @request_to_cancel @dwh
    Scenario: Request a Confirmed Reservation for Cancellation, DWH Partial Rate Plan
        Given an existing Global Collect hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then hotelier cannot request for the reservation to be cancelled

    @request_to_cancel @hpp
    Scenario: Request a Confirmed Reservation for Cancellation, HPP Pay Upon Arrival Rate Plan
        Given an existing HPP hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then hotelier can request for the reservation to be cancelled

    @request_to_cancel @bdo
    Scenario: Request a Confirmed Reservation for Cancellation, BDO Pay Upon Booking Refundable Rate Plan
        Given an existing BDO hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then hotelier can request for the reservation to be cancelled
