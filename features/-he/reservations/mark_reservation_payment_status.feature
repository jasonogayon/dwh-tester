Feature: Update Reservation Payment Status

    Hotelier should be able to mark a reservation as pre-authorized or charged, but only for HPP reservations.


    @payment_status @dwh
    Scenario: Mark a Confirmed Reservation as Charged or Pre-Authorized, Confirmed Reservation, DWH Partial Rate Plan
        Given an existing Global Collect hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then hotelier sees that the reservation is already marked as charged

    @payment_status @hpp
    Scenario: Mark a Confirmed Reservation as Charged or Pre-Authorized, Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        Given an existing HPP hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then hotelier can mark the reservation as pre-authorized

    @payment_status @hpp
    Scenario: Mark a Confirmed Reservation as Charged or Pre-Authorized, Confirmed Reservation, HPP Pay Upon Booking Refundable Rate Plan
        Given an existing HPP hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then hotelier can mark the reservation as charged

    @payment_status @bdo
    Scenario: Mark a Confirmed Reservation as Charged or Pre-Authorized, Confirmed Reservation, BDO Pay Upon Booking Refundable Rate Plan
        Given an existing BDO hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then hotelier sees that the reservation is already marked as charged
