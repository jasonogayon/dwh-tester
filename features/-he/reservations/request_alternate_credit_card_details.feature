Feature: Request for Alternate Credit Card Details

    Hotelier should be able to request alternate credit card details from guest, but only for a HPP reservation.

    Tests Marked as Not Ready:
    - It is possible to request for alternate credit card details for BDO reservations through the HTPP layer.


    @request_for_cc @dwh
    Scenario: Request for Alternate CC Details, Confirmed Reservation, DWH Partial Rate Plan
        Given an existing Global Collect hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then hotelier cannot request for alternate credit card details from the guest

    @request_for_cc @hpp
    Scenario: Request for Alternate CC Details, Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        Given an existing HPP hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then hotelier can request for alternate credit card details from the guest
        And guest can update the credit card details for the reservation

    @request_for_cc @bdo @flaky
    Scenario: Request for Alternate CC Details, Confirmed Reservation, BDO Pay Upon Booking Refundable Rate Plan
        Given an existing BDO hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then hotelier cannot request for alternate credit card details from the guest
