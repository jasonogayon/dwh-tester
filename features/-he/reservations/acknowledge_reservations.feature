Feature: Acknowledge a Reservation

    Hotelier should be able to acknowledge a reservation.


    @acknowledgment @dwh
    Scenario: Acknowledge a Confirmed Reservation, DWH Partial Rate Plan
        Given an existing Global Collect hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Partial Prepayment with Lead Time Penalty rate plan
        Then hotelier can acknowledge the reservation

    @acknowledgment @hpp
    Scenario: Acknowledge a Confirmed Reservation, HPP Pay Upon Arrival Rate Plan
        Given an existing HPP hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Arrival with Lead Time Penalty rate plan
        Then hotelier can acknowledge the reservation

    @acknowledgment @bdo
    Scenario: Acknowledge a Confirmed Reservation, BDO Pay Upon Booking Refundable Rate Plan
        Given an existing BDO hotel property
        When guest books a desktop reservation for 2 Accommodation rooms on a Public Pay upon Booking Partial Prepayment Refundable with Lead Time Penalty rate plan
        Then hotelier can acknowledge the reservation
