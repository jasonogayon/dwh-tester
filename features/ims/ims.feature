Feature: Impact Marketing Solutions API

    # Existing Accounts:
    # Platinum:  6303000000000907 | RZAJ7958
    # Gold:      6303000000000918 | SRLK3718
    # New Testing Segment (Member Prefix is 6399): DJ182XJL

    # Wordpress Account: jasonogayon | JUkOlMlODlK8w#HuC^Ci!b6v
    # Rewards Logs Query: dwh_log | SELECT * FROM rewards_provider_logs WHERE provider_action = 'SIGNUP' ORDER BY id DESC;


    @ims @valid @login
    Scenario: IMS Valid Login Request
        When rewards member sends an IMS valid login request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "login" value
        And the IMS response also contains a non-null "key" field

    @ims @valid @logout
    Scenario: IMS Valid Logout Request
        When rewards member logs in and sends an IMS valid logout request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "logoff" value

    @ims @valid @resetpassword
    Scenario: IMS Valid Password Reset Request
        When rewards member sends an IMS valid password reset request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "resetpw" value

    @ims @valid @session
    Scenario: IMS Valid Retrieve Session Request
        When rewards member logs in and sends an IMS valid retrieve session request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "validsession" value
        And the IMS response also contains a non-null "membership_ucode" field inside "member"
        And the IMS response also contains a non-null "member_level" field inside "member"
        And the IMS response also contains a non-null "member_points" field inside "member"
        And the IMS response also contains a non-null "member_expiry" field inside "member"
        And the IMS response also contains a non-null "member_blocked" field inside "member"
        And the IMS response also contains a non-null "birthday" field inside "member"
        And the IMS response also contains a non-null "name" field inside "member"
        And the IMS response also contains a non-null "gender" field inside "member"
        And the IMS response also contains a non-null "mobile_phone" field inside "member"
        And the IMS response also contains a non-null "email" field inside "member"
        And the IMS response also contains a non-null "address" field inside "member"
        And the IMS response also contains a non-null "wedding_anniversary" field inside "member"

    @ims @valid @voucher
    Scenario: IMS Valid Retrieve Voucher Request
        When rewards member logs in and sends an IMS valid retrieve voucher request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "membervoucherlist" value
        And the IMS response also contains a non-null "list" field

    @ims @valid @linksocial @facebook
    Scenario: IMS Valid Link Facebook Request
        When rewards member sends an IMS valid link facebook request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "linkmember" value
        And the rewards member can log in via the facebook account
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "signin" value

    @ims @valid @linksocial @linkedin
    Scenario: IMS Valid Link LinkedIn Request
        When rewards member sends an IMS valid link linkedin request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "linkmember" value
        And the rewards member can log in via the linkedin account
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "signin" value

    @ims @valid @signup
    Scenario: IMS Valid Signup New Member Request
        When rewards member sends an IMS valid signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "signup" value

    @ims @valid @pointshistory
    Scenario: IMS Valid Points History Request
        When rewards member logs in and sends an IMS valid points history request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "pointshistory_member" value

    @ims @valid @memberdetails
    Scenario: IMS Valid Update Member Details Request
        When rewards member logs in and sends an IMS valid update member details request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "update_mem_details" value

    @ims @valid @changepassword
    Scenario: IMS Valid Change Password Request
        When rewards member logs in and sends an IMS valid change password request
        Then rewards member receives an IMS response containing a "success" key with a "Y" value
        And the IMS response also contains a "action" key with a "changepw" value



    @ims @invalid @login
    Scenario: IMS Invalid Login Request, Missing Member Number
        When rewards member sends an IMS missing member number login request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1401" value

    @ims @invalid @login
    Scenario: IMS Invalid Login Request, Password Too Short
        When rewards member sends an IMS password too short login request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1402" value

    @ims @invalid @login
    Scenario: IMS Invalid Login Request, Incorrect Member Number
        When rewards member sends an IMS incorrect member number login request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1403" value

    @ims @invalid @login
    Scenario: IMS Invalid Login Request, Incorrect Password
        When rewards member sends an IMS incorrect password login request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1404" value



    @ims @invalid @logout
    Scenario: IMS Invalid Logout Request, Incorrect Session Key
        When rewards member logs in and sends an IMS incorrect session key logout request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1501" value



    @ims @invalid @resetpassword
    Scenario: IMS Invalid Password Reset Request, Incorrect Birthday
        When rewards member sends an IMS incorrect birthday password reset request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1303" value

    @ims @invalid @resetpassword
    Scenario: IMS Invalid Password Reset Request, Incorrect Member Number
        When rewards member sends an IMS incorrect member number password reset request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1304" value



    @ims @invalid @session
    Scenario: IMS Invalid Retrieve Session Request, Incorrect Session Key
        When rewards member logs in and sends an IMS incorrect session key retrieve session request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1501" value



    @ims @invalid @voucher
    Scenario: IMS Invalid Retrieve Voucher Request, Incorrect Session Key
        When rewards member logs in and sends an IMS incorrect session key retrieve voucher request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "1501" value



    @ims @invalid @linksocial @facebook
    Scenario: IMS Invalid Link Facebook Request, Incorrect EDM Code
        When rewards member sends an IMS incorrect edm code link facebook request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "6001" value

    @ims @invalid @linksocial @facebook
    Scenario: IMS Invalid Link Facebook Request, Incorrect Member UCode
        When rewards member sends an IMS incorrect member ucode link facebook request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "6002" value

    @ims @invalid @linksocial @facebook
    Scenario: IMS Invalid Link Facebook Request, Missing Login Type
        When rewards member sends an IMS missing type link facebook request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "6003" value

    @ims @invalid @linksocial @facebook
    Scenario: IMS Invalid Link Facebook Request, Missing Login ID
        When rewards member sends an IMS missing id link facebook request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "6004" value



    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Missing Name
        When rewards member sends an IMS missing name signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4001" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Missing Email
        When rewards member sends an IMS missing email signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4003" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Incorrect Email
        When rewards member sends an IMS incorrect email signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4005" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Invalid Birthday
        When rewards member sends an IMS invalid birthday signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4006" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Invalid Wedding Anniversary
        When rewards member sends an IMS invalid anniversary signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4007" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Incorrect Membership Type
        When rewards member sends an IMS incorrect membership type signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4008" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Existing Mobile
        When rewards member sends an IMS existing mobile signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4009" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Existing Email
        When rewards member sends an IMS existing email signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4010" value

    @ims @invalid @signup
    Scenario: IMS Invalid Signup New Member Request, Incorrect Type
        When rewards member sends an IMS incorrect type signup new member request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "4011" value



    @ims @invalid @changepassword
    Scenario: IMS Invalid Change Password Request, Incorrect Password
        When rewards member logs in and sends an IMS incorrect change password request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "2102" value
        And the IMS response also contains a "reason_text" key with a "Password must contain at least one uppercase alpha character, one lowercase alpha character and one numerical character" value

    @ims @invalid @changepassword
    Scenario: IMS Invalid Change Password Request, Short Password
        When rewards member logs in and sends an IMS short change password request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "2101" value
        And the IMS response also contains a "reason_text" key with a "Password length must be no shorter than 8 characters and no longer than 16 characters" value

    @ims @invalid @changepassword
    Scenario: IMS Invalid Change Password Request, Too Long Password
        When rewards member logs in and sends an IMS too long change password request
        Then rewards member receives an IMS response containing a "success" key with a "N" value
        And the IMS response also contains a "reasoncode" key with a "2101" value
        And the IMS response also contains a "reason_text" key with a "Password length must be no shorter than 8 characters and no longer than 16 characters" value
