Feature: Channel Management System, Booking[Dot]Com, OTA_HotelRateAmountNotifRQ

    Hotelier should be able to send an OTA_HotelRateAmountNotifRQ CMS request to Booking[Dot]Com

    Tests Marked as Not Ready:
    - Requests to Booking[Dot]Com in Production returns a 401 Unauthorized error


    @cms @bookingdotcom @rateamountnotif @valid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Valid Request
        When hotelier sends an OTA_HotelRateAmountNotifRQ CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @rateamountnotif @invalid @rates @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Rates = 1
        When hotelier sends an OTA_HotelRateAmountNotifRQ with low rates CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "Room Rate too Low for Room 'Standard Single Room' and Rate Category 'Standard Rate'"

    @cms @bookingdotcom @rateamountnotif @valid @zerorate @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Rates = 0
        When hotelier sends an OTA_HotelRateAmountNotifRQ with zero rates CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains an Warning element with the Code attribute value of "367"
        And the CMS response also contains an Warning element with the ShortText attribute value of "with price '0' might be too low, please check"

    @cms @bookingdotcom @rateamountnotif @invalid @rates @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Rates = -1
        When hotelier sends an OTA_HotelRateAmountNotifRQ with negative rates CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "BaseByGuestAmt: AmountAfterTax attribute has wrong format: value '-100.0' is not a a non-negative real number"

    @cms @bookingdotcom @rateamountnotif @valid @locatorid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Locator ID = 0
        When hotelier sends an OTA_HotelRateAmountNotifRQ with zero locator ID CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @rateamountnotif @invalid @locatorid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Locator ID = -1
        When hotelier sends an OTA_HotelRateAmountNotifRQ with negative locator ID CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "RateAmountMessage: LocatorID attribute has wrong format: value '-1' is not a an alphanumeric string up to 64 characters in length"

    @cms @bookingdotcom @rateamountnotif @invalid @rateplan @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Rate Plan ID = 0
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid rateplan CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "402"
        And the CMS response also contains an Error element with the ShortText attribute value of "RatePlanCode '0' doesn't exist"

    @cms @bookingdotcom @rateamountnotif @invalid @room @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Room ID = 0
        When hotelier sends an OTA_HotelRateAmountNotifRQ with invalid room CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "402"
        And the CMS response also contains an Error element with the ShortText attribute value of "room with InvTypeCode '0' doesn't exist"

    @cms @bookingdotcom @rateamountnotif @invalid @startdate @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Invalid Start Date
        When hotelier sends an OTA_HotelRateAmountNotifRQ CMS Booking[Dot]Com request with dates from 3 DAYS BEFORE to TODAY
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "is in the past"

    @cms @bookingdotcom @rateamountnotif @invalid @enddate @flaky
    Scenario: Booking[Dot]Com, OTA_HotelRateAmountNotifRQ, Request with Invalid End Date
        When hotelier sends an OTA_HotelRateAmountNotifRQ CMS Booking[Dot]Com request with dates from TODAY to 3 DAYS BEFORE
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element
