Feature: Channel Management System, Booking[Dot]Com, OTA_HotelAvailNotifRQ

    Hotelier should be able to send an OTA_HotelAvailNotifRQ CMS request to Booking[Dot]Com

    Tests Marked as Not Ready:
    - Requests to Booking[Dot]Com in Production returns a 401 Unauthorized error


    @cms @bookingdotcom @availnotif @valid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Valid Request
        When hotelier sends an OTA_HotelAvailNotifRQ Open CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @availnotif @valid @stopsell @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Valid Request (Stop Sell)
        When hotelier sends an OTA_HotelAvailNotifRQ Closed CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @availnotif @valid @bookinglimit @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Booking Limit = 0
        When hotelier sends an OTA_HotelAvailNotifRQ with zero booking limit CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @availnotif @invalid @bookinglimit @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Booking Limit = -1
        When hotelier sends an OTA_HotelAvailNotifRQ with negative booking limit CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "AvailStatusMessage: BookingLimit attribute has wrong format: value '-1' is not a a non-negative integer"

    @cms @bookingdotcom @availnotif @valid @locatorid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Locator ID = 0
        When hotelier sends an OTA_HotelAvailNotifRQ with zero locator ID CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @availnotif @invalid @locatorid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Locator ID = -1
        When hotelier sends an OTA_HotelAvailNotifRQ with negative locator ID CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "AvailStatusMessage: LocatorID attribute has wrong format: value '-1' is not a an alphanumeric string up to 64 characters in length"

    @cms @bookingdotcom @availnotif @invalid @rateplan @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Rate Plan ID = 0
        When hotelier sends an OTA_HotelAvailNotifRQ with invalid rateplan CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "Rate '0' does not belong to hotel"

    @cms @bookingdotcom @availnotif @invalid @room @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Room ID = 0
        When hotelier sends an OTA_HotelAvailNotifRQ with invalid room CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "321"
        And the CMS response also contains an Error element with the ShortText attribute value of "StatusApplicationControl: missing InvTypeCode or HotelCode"

    @cms @bookingdotcom @availnotif @invalid @startdate @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Invalid Start Date
        When hotelier sends an OTA_HotelAvailNotifRQ CMS Booking[Dot]Com request with dates from 3 DAYS BEFORE to TODAY
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "is in the past"

    @cms @bookingdotcom @availnotif @invalid @enddate @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Invalid End Date
        When hotelier sends an OTA_HotelAvailNotifRQ CMS Booking[Dot]Com request with dates from TODAY to 3 DAYS BEFORE
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "321"
        And the CMS response also contains an Error element with the ShortText attribute value of "nothing to do - no dates selected"

    @cms @bookingdotcom @availnotif @invalid @los @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Over The Max LOS (194758947896789476948764897694876023840759037895648956)
        When hotelier sends an OTA_HotelAvailNotifRQ with over max LOS CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a <Success/> element
        And the CMS response also contains a RUID element

    @cms @bookingdotcom @availnotif @invalid @los @flaky
    Scenario: Booking[Dot]Com, OTA_HotelAvailNotifRQ, Request with Negative LOS (-5)
        When hotelier sends an OTA_HotelAvailNotifRQ with invalid minimum LOS CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "LengthOfStay: Time attribute has wrong format: value '-5' is not a a non-negative integer"
