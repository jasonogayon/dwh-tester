Feature: Channel Management System, Booking[Dot]Com, OTA_HotelResNotifRS

    Hotelier should be able to send an OTA_HotelResNotifRS CMS request to Booking[Dot]Com

    Tests Marked as Not Ready:
    - Requests to Booking[Dot]Com in Production returns a 401 Unauthorized error


    @cms @bookingdotcom @resnotif @valid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelResNotifRS, Valid Request
        When hotelier sends an OTA_HotelResNotifRS CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a RUID element

    @cms @bookingdotcom @resnotif @invalid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelResNotifRS, Request with Invalid Booking
        When hotelier sends an OTA_HotelResNotifRS with invalid booking CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing a RUID element

    @cms @bookingdotcom @resnotif @invalid @flaky
    Scenario: Booking[Dot]Com, OTA_HotelResNotifRS, Request with Invalid Booking Source
        When hotelier sends an OTA_HotelResNotifRS with invalid booking source CMS Booking[Dot]Com request
        Then hotelier receives a CMS response containing an Error element with the Code attribute value of "367"
        And the CMS response also contains an Error element with the ShortText attribute value of "HotelReservationID: ResID_Source attribute has wrong format: value 'BOOKING.CO' does not look like regular expression /^(BOOKING.COM|RT)$/"
        And the CMS response also contains an Error element with the ShortText attribute value of "HotelReservationID: incorrect sequence (first HotelReservationID should have ResID_Source 'BOOKING.COM' followed by second with ResID_Source 'RT' and so repeated)"
