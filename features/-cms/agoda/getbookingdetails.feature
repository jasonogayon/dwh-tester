Feature: Channel Management System, Agoda YCS 5, GetBookingDetails

    Hotelier should be able to send a GetBookingDetails CMS request to Agoda

    @cms @agoda @getbookingdetails @valid @flaky
    Scenario: Agoda YCS5, GetBookingDetails, Valid Request
        When hotelier sends a GetBookingDetails CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //bookings element with the count attribute value of "0"

    @cms @agoda @getbookingdetails @invalid @booking @flaky
    Scenario: Agoda YCS5, GetBookingDetails, Request with Booking ID = 0
        When hotelier sends a GetBookingDetails with invalid booking CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //bookings element with the count attribute value of "0"

    @cms @agoda @getbookingdetails @invalid @hotel
    Scenario: Agoda YCS5, GetBookingDetails, Request with Hotel ID = 0
        When hotelier sends a GetBookingDetails with invalid hotel CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1201"
        And the CMS response also contains an //error element with the description attribute value of "API key is not authorised for Property id"

    @cms @agoda @getbookingdetails @invalid @language
    Scenario: Agoda YCS5, GetBookingDetails, Request with Language = 0
        When hotelier sends a GetBookingDetails with invalid language CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "is not facet-valid with respect to pattern '[a-zA-Z]{2}' for type 'languageType'"

    @cms @agoda @getbookingdetails @invalid @key
    Scenario: Agoda YCS5, GetBookingDetails, Request with Invalid Key
        When hotelier sends a GetBookingDetails with invalid key CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1503"
        And the CMS response also contains an //error element with the description attribute value of "Authentication failed: The resource requires authentication, which was not supplied with the request"

    @cms @agoda @getbookingdetails @missing @language
    Scenario: Agoda YCS5, GetBookingDetails, Request without a Language element
        When hotelier sends a GetBookingDetails with missing language CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1501"
        And the CMS response also contains an //error element with the description attribute value of "The request is invalid: The request content was malformed"

    @cms @agoda @getbookingdetails @missing @property
    Scenario: Agoda YCS5, GetBookingDetails, Request without a Property element
        When hotelier sends a GetBookingDetails with missing property CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1511"
        And the CMS response also contains an //error element with the description attribute value of "Invalid content was found starting with element 'booking'. One of '{property}' is expected"

    @cms @agoda @getbookingdetails @missing @booking
    Scenario: Agoda YCS5, GetBookingDetails, Request without a Booking element
        When hotelier sends a GetBookingDetails with missing booking CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1511"
        And the CMS response also contains an //error element with the description attribute value of "The content of element 'property' is not complete. One of '{booking}' is expected"
