Feature: Channel Management System, Agoda YCS 5, SetAri


    @cms @agoda @setari @valid
    Scenario: Agoda YCS5, SetAri All, Valid Request without Extra Bed
        When hotelier sends a SetAri All CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @valid @extrabed
    Scenario: Agoda YCS5, SetAri All, Valid Request with Extra Bed
        When hotelier sends a SetAri All with extra bed CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2208"
        And the CMS response also contains an //error element with the description attribute value of "Max extrabed has not been set, You cannot set the extrabed rate"

    @cms @agoda @setari @invalid @daysofweek
    Scenario: Agoda YCS5, SetAri All, Request with Days of Week = 0
        When hotelier sends a SetAri All with invalid days of week CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '0' is not facet-valid with respect to minInclusive '1' for type 'dow'"

    @cms @agoda @setari @invalid @hotel
    Scenario: Agoda YCS5, SetAri All, Request with Hotel ID = 0
        When hotelier sends a SetAri All with invalid hotel CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1301"
        And the CMS response also contains an //error element with the description attribute value of "Invalid Property id: 0"

    @cms @agoda @setari @invalid @rateplan
    Scenario: Agoda YCS5, SetAri All, Request with Rate Plan ID = 0
        When hotelier sends a SetAri All with invalid rateplan CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2209"
        And the CMS response also contains an //error element with the description attribute value of "Rate plan: 0 does not exist for Property id"

    @cms @agoda @setari @invalid @room
    Scenario: Agoda YCS5, SetAri All, Request with Room ID = 0
        When hotelier sends a SetAri All with invalid room CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2205"
        And the CMS response also contains an //error element with the description attribute value of "Room: 0 does not exist for Property id"

    @cms @agoda @setari @invalid @currency
    Scenario: Agoda YCS5, SetAri All, Request with Currency = 0
        When hotelier sends a SetAri All with invalid currency CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '0' with length = '1' is not facet-valid with respect to minLength '3' for type 'currency'"

    @cms @agoda @setari @invalid @key
    Scenario: Agoda YCS5, SetAri All, Request with Invalid Key
        When hotelier sends a SetAri All with invalid key CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1503"
        And the CMS response also contains an //error element with the description attribute value of "Authentication failed: The resource requires authentication, which was not supplied with the request"

    @cms @agoda @setari @rates @valid
    Scenario: Agoda YCS5, SetAri Rates Only, Valid Request without Extra Bed
        When hotelier sends a SetAri Rates Only CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @rates @invalid @zero
    Scenario: Agoda YCS5, SetAri All, Request with Rates = 0
        When hotelier sends a SetAri All with zero rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '0' is not facet-valid with respect to minExclusive '0.0' for type 'positive-decimal'"

    @cms @agoda @setari @rates @invalid @negative
    Scenario: Agoda YCS5, SetAri All, Request with Negative Rates (-230.45)
        When hotelier sends a SetAri All with negative rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '-230.45' is not facet-valid with respect to minExclusive '0.0' for type 'positive-decimal'"

    @cms @agoda @setari @rates @valid @stopsell
    Scenario: Agoda YCS5, SetAri All, Request with Closed Rates (Stop Sell)
        When hotelier sends a SetAri All with closed rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @rates @invalid @max
    Scenario: Agoda YCS5, SetAri All, Request with Over The Max Rates (1.0e+15)
        When hotelier sends a SetAri All with over max rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "'1.0e+15' is not a valid value for 'decimal'"

    @cms @agoda @setari @rates @invalid @blank
    Scenario: Agoda YCS5, SetAri All, Request with Blank Rates
        When hotelier sends a SetAri All with blank rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "'' is not a valid value for 'decimal'"

    @cms @agoda @setari @rates @invalid
    Scenario: Agoda YCS5, SetAri All, Request with Single Rates Bigger Than Other Rates
        When hotelier sends a SetAri All with bigger single rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2102"
        And the CMS response also contains an //error element with the description attribute value of "Single rate cannot be greater than Double rate"

    @cms @agoda @setari @rates @invalid
    Scenario: Agoda YCS5, SetAri All, Request with Double Rates Bigger Than Other Rates
        When hotelier sends a SetAri All with bigger double rates CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2103"
        And the CMS response also contains an //error element with the description attribute value of "Double rate cannot be greater than Full rate"

    @cms @agoda @setari @inventory @valid
    Scenario: Agoda YCS5, SetAri Inventory Only, Valid Request without Extra Bed
        When hotelier sends a SetAri Inventory Only CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @inventory @valid @stopsell
    Scenario: Agoda YCS5, SetAri All, Request with Closed Allotment
        When hotelier sends a SetAri All with closed allotment CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @inventory @invalid max
    Scenario: Agoda YCS5, SetAri All, Request with Over The Max Allotment
        When hotelier sends a SetAri All with over max allotment CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2104"
        And the CMS response also contains an //error element with the description attribute value of "Allotment: 1000 cannot be greater than 999"

    @cms @agoda @setari @inventory @invalid @negative
    Scenario: Agoda YCS5, SetAri All, Request with Negative Allotment
        When hotelier sends a SetAri All with invalid allotment CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '-9784628746284628' is not facet-valid with respect to minInclusive '0' for type 'allotment'"

    @cms @agoda @setari @los @valid
    Scenario: Agoda YCS5, SetAri LOS Only, Valid Request without Extra Bed
        When hotelier sends a SetAri LOS Only CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //result element with the TUID attribute

    @cms @agoda @setari @los @invalid @max
    Scenario: Agoda YCS5, SetAri All, Request with Over The Max LOS (1004)
        When hotelier sends a SetAri All with over max LOS CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "2106"
        And the CMS response also contains an //error element with the description attribute value of "Max LOS: 1004 cannot be less than 0 or greater than 99"

    @cms @agoda @setari @los @invalid @negative
    Scenario: Agoda YCS5, SetAri All, Request with Negative LOS (-5)
        When hotelier sends a SetAri All with invalid minimum LOS CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "Value '-5' is not facet-valid with respect to minInclusive '1' for type 'positiveInteger'"
