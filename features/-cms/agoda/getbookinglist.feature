Feature: Channel Management System, Agoda YCS 5, GetBookingList

    Hotelier should be able to send a GetBookingList CMS request to Agoda

    @cms @agoda @getbookinglist @valid @flaky
    Scenario: Agoda YCS5, GetBookingList, Valid Request
        When hotelier sends a GetBookingList CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing a //properties element with the count attribute value of "1"

    @cms @agoda @getbookinglist @invalid @hotel
    Scenario: Agoda YCS5, GetBookingList, Request with Hotel ID = 0
        When hotelier sends a GetBookingList with invalid hotel CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1201"
        And the CMS response also contains an //error element with the description attribute value of "API key is not authorised for Property id"

    @cms @agoda @getbookinglist @invalid @timezone
    Scenario: Agoda YCS5, GetBookingList, Request without Timezone
        When hotelier sends a GetBookingList with invalid timezone CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "0"
        And the CMS response also contains an //error element with the description attribute value of "is not facet-valid with respect to pattern '.+T.+(Z|[+-].+)' for type 'dateTimeWithTimezone'"

    @cms @agoda @getbookinglist @invalid @key
    Scenario: Agoda YCS5, GetBookingList, Request with Invalid Key
        When hotelier sends a GetBookingList with invalid key CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1503"
        And the CMS response also contains an //error element with the description attribute value of "Authentication failed: The resource requires authentication, which was not supplied with the request"

    @cms @agoda @getbookinglist @invalid @datetime
    Scenario: Agoda YCS5, GetBookingList, Request without a DateTime element
        When hotelier sends a GetBookingList with missing date CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1501"
        And the CMS response also contains an //error element with the description attribute value of "The request is invalid: The request content was malformed"

    @cms @agoda @getbookinglist @invalid @property
    Scenario: Agoda YCS5, GetBookingList, Request without a Property element
        When hotelier sends a GetBookingList with missing property CMS Agoda YCS5 request
        Then hotelier receives a CMS response containing an //error element with the code attribute value of "1511"
        And the CMS response also contains an //error element with the description attribute value of "The content of element 'criteria' is not complete. One of '{property}' is expected."
