require "date"
require "fig_newton"
require "gmail"
require "json"
require "net/http"
require "nokogiri"
require "open-uri"
require "page-object"
require "parallel_tests"
require "require_all"
require "rest-client"
require "roo"
require "rspec"
require "selenium-webdriver"
require "uri"
require "watir"
require "write_xlsx"

World(PageObject::PageFactory)

def browser
  (ENV["BROWSER"] ||= "none").downcase.to_sym
end

def machine
  (ENV["MACHINE"] ||= "local").downcase.to_sym
end

def mobile
  (ENV["MOBILE"] ||= "false").downcase.to_sym
end

def mac
  (ENV["MAC"] ||= "false").downcase.to_sym
end

def view_data
  (ENV["VIEW_DATA"] ||= "false").downcase.to_sym
end

def use_data
  (ENV["USE_DATA"] ||= "false").downcase.to_sym
end

def assertion
  (ENV["ASSERT"] ||= "true").downcase.to_sym
end

def no_creditcard
  (ENV["NOCC"] ||= "false").downcase.to_sym
end

def debug
  ENV["DEBUG"] ||= nil
end

def property_id
  ENV["PROP_ID"] ||= nil
end

def property_name
  ENV["PROP_NAME"] ||= nil
end

def custom_rateplan_id
  ENV["RP_ID"] ||= nil
end

def custom_arrival
  ENV["CHECKIN"] ||= nil
end

def custom_no_nights
  ENV["NIGHTS"] ||= nil
end

def custom_promocode
  ENV["PROMOCODE"] ||= nil
end

def custom_confirmation_no
  ENV["CONFIRMATION_NO"] ||= nil
end

def custom_start
  ENV["START"] ||= nil
end

def custom_end
  ENV["END"] ||= nil
end

def custom_email
  ENV["EMAIL"] ||= nil
end

def custom_email_password
  ENV["EMAIL_PASS"] ||= nil
end

def custom_cc
  ENV["CC"] ||= nil
end

def db
  ENV["DB"] ||= nil
end

def query
  ENV["QUERY"] ||= nil
end
