# Control Panel Helper
module ControlPanelHelper
  include PageObject

  # Element Locators
  h2(:rsvsn_header, xpath: "//h2")
  div(:feedback, xpath: "*[contains(@class, 'feedbackmsg')]")
  div(:loading, xpath: "//div[contains(., 'Loading')]")
  div(:please_wait, xpath: "//div[contains(., 'Please wait')]")
  div(:one_moment_please, xpath: "//div[contains(., 'One moment')]")
  div(:page_loading, id: "pageloading")
  div(:loading_mask, id: "loading-mask")
  link(:hotel_mgmt, xpath: "//li[@id='mainBody__propertyPanel']/a[2]")
  link(:online_paid, xpath: "//li[@id='mainBody__onlinepaidPanel']/a[2]")
  link(:guest_support, xpath: "//li[@id='mainBody__inboundPanel']/a[2]")
  link(:user_mgmt, xpath: "//li[@id='mainBody__userPanel']/a[2]")
  link(:reports, xpath: "//li[@id='mainBody__userPanel']/following::li[1]/a[2]")
  link(:sp_admin, xpath: "//li[@id='mainBody__userPanel']/following::li[2]/a[2]")
  link(:cp_admin, xpath: "//li[@id='mainBody__userPanel']/following::li[3]/a[2]")

  # Methods
  def goto(tab_name)
    one_moment_please_element.wait_until_present(timeout: 120)
    one_moment_please_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    if tab_name.include? "hotel"
      hotel_mgmt
    elsif tab_name.include? "online"
      online_paid
    elsif tab_name.include?("guest") || tab_name.include?("support")
      guest_support
    elsif tab_name.include? "user"
      user_mgmt
    elsif tab_name.include? "report"
      reports
    elsif tab_name.include?("sale") || tab_name.include?("partner")
      sp_admin
    elsif tab_name.include? "control"
      cp_admin
    end
  end
end
