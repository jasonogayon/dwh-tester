# Booking Engine Helper
module BookingEngineHelper
  include PageObject

  # Element Locators
  h2(:rsvsn_header, xpath: "//h2")
  div(:los_dialog, id: "cDialog")
  div(:preloader, id: "preloader")
  div(:loading, xpath: "//div[contains(., 'Loading')]")

  # Methods
  def handle_alert
    @browser.alert.ok if @browser.alert.exists?
  end

  def choose_staydates(arrival_date, departure_date)
    self.class.link(:arrival, text: arrival_date)
    arrival_element.wait_until_present(timeout: 120)
    arrival
    self.class.link(:departure, text: departure_date)
    departure_element.wait_until_present(timeout: 120)
    departure
  end

  def get_los_dialog_copy
    los_dialog_element.wait_until_present(timeout: 120)
    los_dialog
  end
end
