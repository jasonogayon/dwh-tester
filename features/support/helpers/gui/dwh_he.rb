# Hotel Extranet Helper
module HotelExtranetHelper
  include PageObject

  page_url FigNewton.login_he_url

  # Element Locators
  h2(:rsvsn_header, xpath: "//h2")
  div(:logo, id: "application_logo")
  div(:feedback, xpath: "*[contains(@class, 'feedbackmsg')]")
  div(:loading, xpath: "//div[contains(., 'Loading')]")
  div(:saving, xpath: "//div[contains(., 'Saving')]")
  div(:please_wait, xpath: "//div[contains(., 'Please wait')]")
  div(:one_moment_please, xpath: "//div[contains(., 'One moment')]")
  div(:page_loading, id: "pageloading")
  div(:loading_mask, id: "loading-mask")
  div(:mask, xpath: "//div[contains(@class, 'ext-el-mask')]")
  div(:reports_panel, id: "panelReports")
  div(:help_links_panel, id: "helplinkspanel")
  div(:quick_links_panel, id: "quicklinkspanel")
  div(:reservation, xpath: "//*[@id='gridReservations']/div/div/div/div/div[2]/div/div[1]/table/tbody/tr/td[2]/div")
  link(:my_account, id: "lnkyouraccount")
  link(:logout, id: "lnklogout")
  link(:dashboard, xpath: "//li[@id='tabpanelAppBody__portalHome']/a[2]")
  link(:hotel_info, xpath: "//li[@id='tabpanelAppBody__panelHotel']/a[2]")
  link(:channel_mgmt, xpath: "//li[@id='tabpanelAppBody__panelAvailability']/a[2]")
  link(:reservations, xpath: "//li[@id='tabpanelAppBody__panelReservations']/a[2]")
  link(:account_mgmt, xpath: "//li[@id='tabpanelAppBody__panelAcctMan']/a[2]")
  span(:logged_user, id: "logged_in_username")
  button(:click_confirm, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/em/button")
  button(:click_cancel, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//table/tbody/tr/td/table/tbody/tr/td[3]/table/tbody/tr[2]/td[2]/em/button")
  button(:authentication_server_failure, xpath: "//div[contains(@class,'x-window-dlg')]//button[contains(.,'OK')]")

  # Methods
  def goto(tab_name)
    page_loading_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    logo_element.wait_until_present(timeout: 120)
    quick_links_panel_element.wait_until_present(timeout: 120)
    loading_mask_element.wait_while_present(timeout: 300)
    if tab_name.include? "dashboard"
      dashboard
    elsif tab_name.include? "hotel"
      hotel_info
    elsif tab_name.include? "channel"
      channel_mgmt
    elsif tab_name.include? "reservation"
      reservations
    elsif tab_name.include? "account"
      account_mgmt
    end
  end

  def select_option(option)
    self.class.div(:option, xpath: "//div[contains(@class,'x-combo-list-item')][contains(., '#{option}')]")
    option_element.wait_until_present(timeout: 120)
    option_element.click
    option_element.wait_while_present(timeout: 300)
  end

  def delete_item(item_type, item_name)
    key = 2 if item_type.include? "facility"
    key = 4 if item_type.include? "advisory"
    key = 5 if item_type.include? "tax"
    key = 7 if item_type.include? "contact"
    key ||= 1
    self.class.image(:option, xpath: "//td[contains(., '#{item_name}')]/following::td[#{key}]/div/img")
    option_element.wait_until_present(timeout: 120)
    option_element.click
    click_confirm_element.wait_until_present(timeout: 120)
    click_confirm_element.click
    feedback_element.wait_until_present(timeout: 120)
    option_element.wait_while_present(timeout: 300)
  end
end
