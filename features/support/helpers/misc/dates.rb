# Dates Helper
module DatesHelper
  def get_date(date)
    time = convert_date(date)
    difference = time - DateTime.now
    day = time.day.to_s
    wday = time.wday
    day = "0" + day if time.day < 10
    month = time.month.to_s
    month = "0" + month if time.month < 10
    wday = 7 if wday == 0

    # Format Dates
    date_showrooms = "#{time.year}-#{month}-#{day}"
    date_policies = "#{time.strftime('%B')} #{day}, #{time.year}"
    date_rateplan = "#{time.month}/#{time.day}/#{time.year}"
    date_validity = "#{day} #{time.strftime('%B')} #{time.year}"
    date_los = "#{time.strftime('%A')}, #{date_validity}"
    date_pms = "#{time.year}-#{month}-#{day}"
    date_image = "#{date_pms}-#{time.hour}#{time.min}#{time.sec}"
    date_rateplan_published_date = "#{day} #{time.strftime('%b')} #{time.year}"
    date_cp_date_filter = "#{time.strftime('%A')}, #{time.strftime('%B')} #{day} #{time.year}"
    date_http = "#{time.strftime('%A')}, #{date_policies}".gsub(" ", "+")
    return date_showrooms, time.day.to_s, difference, date_policies, date_rateplan, date_validity, date_los, time, date_pms, date_image, wday, date_rateplan_published_date, date_cp_date_filter, date_http
  end

  def get_difference_of(departure_date, arrival_date)
    "#{(convert_date(departure_date) - convert_date(arrival_date)).to_i} night/s"
  end

  def get_days_of_week(request_type, start_dow, end_dow)
    if request_type.include? "incorrect days of week"
      start_dow = 1
      end_dow = 7
    end
    for i in start_dow..end_dow do
      dow = "#{dow}<dow>#{i}</dow>"
    end
    "<dow>0</dow>" if request_type.include? "invalid days of week"
  end

  private
  def convert_date(date)
    time = DateTime.now
    time += 1 if date.include? "TOMORROW"
    time += date.split(" DAYS FROM NOW")[0].to_i if date.include? "DAYS FROM NOW"
    time += (date.split(" WEEKS FROM NOW")[0].to_i * 7) if date.include? "WEEKS FROM NOW"
    time -= date.split(" DAYS BEFORE")[0].to_i if date.include? "DAYS BEFORE"
    time -= 1 if date.include? "YESTERDAY"
    return time
  end
end
