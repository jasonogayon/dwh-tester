# Email Helper
module EmailHelper
  CONFIRMATION_NO = "//td[contains(., 'Confirmation Number')]/span"
  TRANSACTION_ID = "//td[contains(., 'Transaction ID')]/span"
  GUEST_NAME = "//td[contains(., 'Guest Name')]/span"
  GUEST_EMAIL = "//td[contains(., 'Email Address')]/span"
  GUEST_MOBILE = "//td[contains(., 'Mobile Number')]/span"
  GUEST_COUNTRY = "//td[contains(., 'Country')]/span"
  PAYPAL_OWNER = "//td[contains(., 'Paypal Account Owner')]/span"
  PAYPAL_EMAIL = "//td[contains(., 'Paypal Account Email')]/span"
  PAYPAL_MOBILE = "//td[contains(., 'Paypal Account Mobile')]/span"
  PRIVATE_ACCOUNT = "//td[contains(., 'Private Account')]/span"
  PRIVATE_PROMOCODE = "//td[contains(., 'Promo Code')]/span"
  CREDITCARD_OWNER = "//td[contains(., 'Card Owner')]/span"
  CREDITCARD_NUMBER = "//td[contains(., 'Card Number')]/span"
  CREDITCARD_EXPIRY = "//td[contains(., 'Card Expiry')]/span"
  RSVSN_HEADER = "//td[contains(., 'Webdriver')]/following::td[1]/h4"
  RSVSN_MADEON = "//td[contains(., 'Reservation Made On')]/span"
  RSVSN_MODIFIEDON = "//td[contains(., 'Reservation Modified On')]/span"
  RSVSN_CANCELLEDON = "//td[contains(., 'Reservation Cancelled On')]/span"
  RSVSN_NIGHTS = "//td[contains(., 'Number of night')]/span"
  RSVSN_ROOMS = "//td[contains(., 'Room')]/span"
  RSVSN_ETA = "//td[contains(., 'Estimated Time of Arrival')]/span"
  RSVSN_OCCUPANTS = "//div[contains(., 'Number of Guests')]/following::div[1]"
  RSVSN_CHECKIN = "//td[contains(., 'Check In')]/span"
  RSVSN_CHECKOUT = "//td[contains(., 'Check Out')]/span"
  RSVSN_SPECIALREQUEST = "//h4[contains(., 'Special Request')]/following::span[1]"
  RSVSN_PUBLIC_RATEPLAN ="//div[contains(., 'Rate Plan')]"
  RSVSN_PRIVATE_RATEPLAN = "//div[contains(., 'Package')]"
  RSVSN_MEAL_NAME = "//div[contains(., 'Meal:')]"
  RSVSN_MEAL_DESCRIPTION = "//div[contains(., 'Meal Description')]/following::div[1]"
  CHARGE_ROOM_CHARGE = "//td[contains(., 'Room Cost')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_TOTAL_TAX = "//td[contains(., 'Total Taxes')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_TOTAL_FEE = "//td[contains(., 'Total Fees')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_TOTAL_ADDON_COST = "//td[contains(., 'Total Add-ons Charges')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_TOTAL_RESERVATION_COST = "//td[contains(., 'TOTAL RESERVATION COST')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_PREPAYMENT = "//td[contains(., 'PREPAYMENT')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_BALANCE_PAYABLE = "//td[contains(., 'PAYABLE AT THE HOTEL')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_REFUND_CANCELLATION = "//div[contains(., 'Refund')]/div[contains(., 'Refund')]"
  CHARGE_REFUND_MODIFICATION = "//td[contains(., 'REFUND')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_MODIFICATION = "//td[contains(., 'MODIFICATION CHARGE')]/following::td[1][contains(@style, 'text-align:right')]"
  CHARGE_CANCELLATION = "//div[contains(., 'Cancellation Charge')]/div[contains(., 'Cancellation Charge')]"

  def get_email(reservation_type, hotelname, confirmation_no)
    email = access_email(reservation_type, hotelname, confirmation_no)
    if !email.nil?
      ce = email.message.body.decoded
      email.delete!
      return ce
    else
      raise "Email data not properly retrieved."
    end
  end

  def delete_email(reservation_type, hotelname, confirmation_no)
    email = access_email(reservation_type, hotelname, confirmation_no)
    email.delete!
  end

  def delete_emails
    gmail = Gmail.new(custom_email.nil? ? FigNewton.email : custom_email, custom_email_password.nil? ? FigNewton.email_password : custom_email_password)
    gmail.inbox.find(from: "noreply@directwithhotels.com").each do |email|
      email.delete!
    end
  end

  def get_reservationdetails(page, reservation_type)
    Hash[
      rsvsn_header:                   page.xpath(RSVSN_HEADER).text.strip!,
      confirmation_no:                page.xpath(CONFIRMATION_NO).text,
      transaction_id:                 page.xpath(TRANSACTION_ID).text,
      guest_name:                     page.xpath(GUEST_NAME).text,
      guest_email:                    page.xpath(GUEST_EMAIL).text,
      guest_mobile:                   page.xpath(GUEST_MOBILE).text,
      guest_country:                  page.xpath(GUEST_COUNTRY).text,
      paypal_owner:                   page.xpath(PAYPAL_OWNER).text,
      paypal_email:                   page.xpath(PAYPAL_EMAIL).text,
      paypal_mobile:                  page.xpath(PAYPAL_MOBILE).text,
      private_account:                page.xpath(PRIVATE_ACCOUNT).text,
      private_promocode:              page.xpath(PRIVATE_PROMOCODE).text,
      creditcard_owner:               page.xpath(CREDITCARD_OWNER).text,
      creditcard_number:              page.xpath(CREDITCARD_NUMBER).text,
      creditcard_expiry:              page.xpath(CREDITCARD_EXPIRY).text,
      rsvsn_madeon:                   page.xpath(RSVSN_MADEON).text,
      rsvsn_modifiedon:               page.xpath(RSVSN_MODIFIEDON).text,
      rsvsn_cancelledon:              page.xpath(RSVSN_CANCELLEDON).text,
      rsvsn_nights:                   page.xpath(RSVSN_NIGHTS).text,
      rsvsn_rooms:                    page.xpath(RSVSN_ROOMS).text.strip!,
      rsvsn_eta:                      page.xpath(RSVSN_ETA).text,
      rsvsn_occupants:                page.xpath(RSVSN_OCCUPANTS).text.split("Note")[0].strip!,
      rsvsn_rateplan:                 get_rateplan_info(page, reservation_type),
      rsvsn_checkin:                  page.xpath(RSVSN_CHECKIN).text,
      rsvsn_checkout:                 page.xpath(RSVSN_CHECKOUT).text,
      rsvsn_meal_name:                page.xpath(RSVSN_MEAL_NAME).text,
      rsvsn_meal_description:         page.xpath(RSVSN_MEAL_DESCRIPTION).text,
      rsvsn_specialrequest:           page.xpath(RSVSN_SPECIALREQUEST).text,
      charge_room_charge:             convert_charges("room_charge", reservation_type, trim(page.xpath(CHARGE_ROOM_CHARGE).text)),
      charge_total_tax:               convert_charges("total_tax", reservation_type, trim(page.xpath(CHARGE_TOTAL_TAX).text)),
      charge_total_fee:               convert_charges("total_fee", reservation_type, trim(page.xpath(CHARGE_TOTAL_FEE).text)),
      charge_total_addon_cost:        convert_charges("total_addon_cost", reservation_type, trim(page.xpath(CHARGE_TOTAL_ADDON_COST).text)),
      charge_total_reservation_cost:  convert_charges("total_reservation_cost", reservation_type, trim(page.xpath(CHARGE_TOTAL_RESERVATION_COST).text)),
      charge_prepayment:              convert_charges("prepayment", reservation_type, trim(page.xpath(CHARGE_PREPAYMENT).text)),
      charge_balance_payable:         convert_charges("balance_payable", reservation_type, trim(page.xpath(CHARGE_BALANCE_PAYABLE).text)),
      charge_refund:                  convert_charges("refund", reservation_type, get_refund(page, reservation_type)),
      charge_penalty:                 convert_charges("penalty", reservation_type, get_penalty(page, reservation_type)),
    ]
  end

  private
  def get_penalty(page, reservation_type)
    trim(page.xpath(CHARGE_CANCELLATION).text) if reservation_type.include?("cancel") || reservation_type.include?("noshow")
    trim(page.xpath(CHARGE_MODIFICATION).text) if reservation_type.include? "modif"
  end

  def get_refund(page, reservation_type)
    trim(page.xpath(CHARGE_REFUND_CANCELLATION).text) if reservation_type.include?("cancel") || reservation_type.include?("noshow")
    trim(page.xpath(CHARGE_REFUND_MODIFICATION).text) if reservation_type.include? "modif"
  end

  def get_rateplan_info(page, reservation_type)
    reservation_type.include?("public") ? page.xpath(RSVSN_PUBLIC_RATEPLAN).text : page.xpath(RSVSN_PRIVATE_RATEPLAN).text
  end

  def convert_charges(data_type, reservation_type, data)
    return nil if ((data_type.include?("addon_cost") || data_type.include?("refund") || data_type.include?("penalty")) && data == "0.00") || ((data_type.include?("balance_payable") || data_type.include?("prepayment")) && reservation_type.include?("cancel"))
  end

  def access_email(reservation_type, hotelname, confirmation_no)
    begin
      gmail = Gmail.new(FigNewton.email, FigNewton.email_password)
      try_email(20) do
        @email = gmail.inbox.emails(:unread, from: "noreply@directwithhotels.com", subject: get_emailsubject(reservation_type, hotelname, confirmation_no)).last
      end
      return @email
    rescue
      raise "Cannot access Gmail using the gmail gem at this time."
    end
  end

  def try_email(number_of_times)
    count = 0
    item_of_interest = nil
    until !item_of_interest.nil? || count == number_of_times
      item_of_interest = yield
      sleep 10
      count += 1
    end
  end

  def get_emailsubject(reservation_type, hotelname, confirmation_no)
    email_subject = "Cancelled" if reservation_type.include? "cancel"
    email_subject = "Modified" if reservation_type.include? "modif"
    email_subject = "No-Show" if reservation_type.include? "noshow"
    email_subject = "Pending" if reservation_type.include? "onhold"
    email_subject ||= "New"
    reservation_type.include?("alternate credit card") ? "Request for Alternate Credit Card Details" : "#{email_subject} Reservation with #{hotelname} (CN: #{confirmation_no})"
  end

  def trim(data)
    # Remove non-numeric characters, except for period
    data.gsub!(/[^\d.]+/, "") unless data.nil?
  end
end
