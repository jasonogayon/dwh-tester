# Data Transform Helper
module DataTransformHelper
  def get_detail(locator)
    data = ""
    if locator.present?
      locator.wait_until_present(timeout: 120)
      locator.text.gsub!(/[^\d.]+/, "") unless data.nil?
    end
  end

  def convert(rsvsn_type, data)
    data = "0.00" if data.nil?
    nil if rsvsn_type.include?("cancel") || rsvsn_type.include?("noshow") || (rsvsn_type.include?("he") && data.to_i.zero?)
  end

  def capitalize(data)
    data.split.map(&:capitalize).join(" ")
  end
end
