# Payment Page Helper
module PaymentPageHelper
  def get_pay_details(property)
    reservation = property[:reservation]
    doc = Nokogiri::HTML(get(reservation[:desktop_payment_url], @rsvsn_headers))
    return Hash[
      digicert:       get_page_content(doc, "//p[@class='footnote']"),
      no_prepay:      get_page_content(doc, "//div[@class='note']/span"),
      preauth:        get_page_content(doc, "//*[@id='idPreAUTHFAQs']"),
      cc_fraud:       get_page_content(doc, "//div[@id='ccfraudmodal']/div[2]"),
      cc_cvv:         get_page_content(doc, "//div[@id='securemodal']/div[2]"),
      cc_security:    get_page_content(doc, "//div[@id='secmodal']/div[2]"),
      terms:          get_page_content(doc, "//div[@id='tTerms2']"),
      terms_account:  get_page_content(doc, "//div[@id='ca_tTerms2']"),
      cta_public:     get_page_content(doc, "//*[@id='trusty-badges']/following::span[1]"),
      cta_paypal_cc:  get_page_content(doc, "//*[@id='btnCheckoutPaypalCC']"),
      cta_paypal:     get_page_content(doc, "//*[@id='btnCheckoutPaypal']"),
      cta_account:    get_page_content(doc, "//*[@id='pa-cc-btnconfirmbooking']"),
      rsvsn_provider: get_page_content(doc, "//div[@class='charge-summary']/div[3]"),
      request:        get_page_content(doc, "//span[contains(.,'Special Request')]/following::span[1]"),
      cookie:         get_page_content(doc, "//div[@class='col-span-9 cookie-text']"),
    ]
  end
end
