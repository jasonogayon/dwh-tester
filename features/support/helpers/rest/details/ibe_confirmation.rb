# Confirmation Page Helper
module ConfirmationPageHelper
  def get_confirmation_details(property)
    reservation = property[:reservation]
    doc = Nokogiri::HTML(get(reservation[:desktop_confirmation_url], @rsvsn_headers))
    return Hash[
      rsvsn_header:                   get_page_content(doc, "//h2"),
      confirmation_no:                get_page_content(doc, "//td[contains(., 'Confirmation Number')]/following::td[1]"),
      transaction_id:                 get_page_content(doc, "//td[contains(., 'Transaction ID')]/following::td[1]"),
      guest_name:                     get_page_content(doc, "//td[contains(., 'Guest Name')]/following::td[1]"),
      guest_email:                    get_page_content(doc, "//td[contains(., 'Email Address')]/following::td[1]"),
      guest_mobile:                   get_page_content(doc, "//td[contains(., 'Mobile Number')]/following::td[1]"),
      guest_country:                  get_page_content(doc, "//td[contains(., 'Country')]/following::td[1]"),
      paypal_owner:                   get_page_content(doc, "//td[contains(., 'PayPal Account Owner')]/following::td[1]"),
      paypal_email:                   get_page_content(doc, "//td[contains(., 'PayPal Account Email')]/following::td[1]"),
      paypal_mobile:                  get_page_content(doc, "//td[contains(., 'PayPal Account Mobile')]/following::td[1]"),
      private_account:                get_page_content(doc, "//td[contains(., 'Private Account')]/following::td[1]"),
      private_promocode:              get_page_content(doc, "//td[contains(., 'Promo Code')]/following::td[1]"),
      creditcard_owner:               get_page_content(doc, "//td[contains(., 'Name of Card Owner')]/following::td[1]"),
      creditcard_number:              get_page_content(doc, "//td[contains(., 'Card Number')]/following::td[1]"),
      creditcard_expiry:              get_page_content(doc, "//td[contains(., 'Credit Card Expiry')]/following::td[1]"),
      rsvsn_madeon:                   get_page_content(doc, "//td[contains(., 'Reservation Made On')]/following::td[1]"),
      rsvsn_modifiedon:               get_page_content(doc, "//td[contains(., 'Reservation Modified On')]/following::td[1]"),
      rsvsn_cancelledon:              get_page_content(doc, "//td[contains(., 'Reservation Cancelled On')]/following::td[1]"),
      rsvsn_details:                  get_page_content(doc, "//td[contains(., 'Reservation Details')]/following::td[1]").gsub("\n", "").split.join(" "),
      rsvsn_eta:                      get_page_content(doc, "//td[contains(., 'Estimated Time of Arrival')]/following::td[1]"),
      rsvsn_adults:                   get_page_content(doc, "//*[@id='reservation-details']//ul[contains(., 'Adult')][1]/li[1]"),
      rsvsn_children:                 get_page_content(doc, "//*[@id='reservation-details']//ul[contains(., 'Adult')][1]/li[2]"),
      rsvsn_checkin:                  get_page_content(doc, "//td[contains(., 'Check In')]/following::td[1]"),
      rsvsn_checkout:                 get_page_content(doc, "//td[contains(., 'Check Out')]/following::td[1]"),
      rsvsn_meal_name:                get_page_content(doc, "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Meal')][1]"),
      rsvsn_meal_description:         get_page_content(doc, "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Meal Description')]/following::div[1]"),
      rsvsn_specialrequest:           get_page_content(doc, "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Special Request')][1]"),
      rsvsn_promocode:                get_page_content(doc, "//div[@id='reservation-details']//strong[contains(.,'Promo Code')]//following::text()[1]"),
      charge_room_charge:             get_page_content(doc, "//td[contains(., 'Room Cost')]/following::td[1]", 2),
      charge_total_tax:               get_page_content(doc, "//td[contains(., 'Taxes')]/following::td[1]", 2),
      charge_total_fee:               get_page_content(doc, "//td[contains(., 'Fees')]/following::td[1]", 2),
      charge_total_addon_cost:        get_page_content(doc, "//td[contains(., 'Total Add-ons Charges')]/following::td[1]", 2),
      charge_total_reservation_cost:  get_page_content(doc, "//td[contains(., 'Total Reservation Cost')]/following::td[1]", 2),
      charge_prepayment:              get_page_content(doc, "//td[contains(., 'Prepayment')]/following::td[1]", 2),
      charge_balance_payable:         get_page_content(doc, "//td[contains(., 'Payable at the Hotel')]/following::td[1]", 2),
      charge_refund:                  get_page_content(doc, "//td[contains(., 'Refund:')]/following::td[1]", 2),
      charge_penalty:                 get_penalty_value(doc, reservation),
      policy_arrival:                 get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Arrival')][1]"),
      policy_departure:               get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Departure')][1]"),
      policy_children:                get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Children')][1]"),
      policy_prepayment:              get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Prepayment')][1]"),
      policy_modification:            get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Modification')][1]"),
      policy_cancellation:            get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Cancellation')][1]"),
      policy_noshow:                  get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'No-show')][1]"),
      policy_refund:                  get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'Refund')][1]"),
      policy_paypal:                  get_page_content(doc, "//*[@id='reservation-hotel-policies']//li[contains(., 'PayPal')][1]"),
      policy_cardfraud:               get_page_content(doc, "//*[@id='reservation-hotel-policies']//p[contains(., 'Fraud')][1]/following::p[1]")
    ]
  end

  def get_confirmation_details_mobile(property)
    reservation = property[:reservation]
    doc = Nokogiri::HTML(get(reservation[:mobile_confirmation_url], @rsvsn_headers))
    rsvsn_details = Hash[
        rsvsn_header:                   get_page_content(doc, "//span[@class='msg']"),
        confirmation_no:                get_page_content(doc, "//p[contains(., 'Confirmation Number')]").gsub("Confirmation Number: ", ""),
        transaction_id:                 get_page_content(doc, "//p[contains(., 'Transaction ID')]").gsub("Transaction ID: ", ""),
        guest_name:                     get_page_content(doc, "//p[contains(.,'Guest Name')]"),
        guest_email:                    get_page_content(doc, "//li[contains(.,'Email')]"),
        guest_mobile:                   get_page_content(doc, "//li[contains(.,'Mobile')]"),
        guest_country:                  get_page_content(doc, "//li[contains(.,'Country')]"),
        paypal_owner:                   get_page_content(doc, "//li[contains(.,'PayPal Account Owner')]"),
        paypal_email:                   get_page_content(doc, "//li[contains(.,'PayPal Account Email')]"),
        paypal_mobile:                  get_page_content(doc, "//li[contains(.,'PayPal Account Mobile')]"),
        private_account:                get_page_content(doc, "//li[contains(.,'Private Account')]"),
        private_promocode:              get_page_content(doc, "//li[contains(.,'Promo Code')]"),
        creditcard_owner:               get_page_content(doc, "//div[contains(@class,'col-xs-6')][contains(.,'Name of Card Owner')]/span[2]"),
        creditcard_number:              get_page_content(doc, "//div[contains(@class,'col-xs-6')][contains(.,'Card Number')]/span[2]"),
        creditcard_expiry:              get_page_content(doc, "//div[contains(@class,'col-xs-6')][contains(.,'Credit Card Expiry')]/span[2]"),
        rsvsn_madeon:                   get_page_content(doc, "//p[contains(.,'Reservation Date')]"),
        rsvsn_modifiedon:               get_page_content(doc, "//p[contains(.,'Reservation Modified On')]"),
        rsvsn_cancelledon:              get_page_content(doc, "//p[contains(.,'Reservation Cancelled On')]"),
        rsvsn_details:                  get_page_content(doc, "//ul[@class='conf-res-details']/li[1]"),
        rsvsn_checkin:                  get_page_content(doc, "//p[contains(.,'Check In Date')]"),
        rsvsn_checkout:                 get_page_content(doc, "//p[contains(.,'Check Out Date')]"),
        rsvsn_meal_name:                get_page_content(doc, "//a[@id='accordion']/div[2]/div[2]//p[2]"),
        rsvsn_specialrequest:           get_page_content(doc, "//li[contains(.,'Special Requests')]"),
        rsvsn_eta:                      get_page_content(doc, "//li[contains(.,'Estimated Time of Arrival')]"),
        charge_room_charge:             get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Room Cost')]/following::div[1]", 2),
        charge_total_tax:               get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Taxes')]/following::div[1]", 2),
        charge_total_fee:               get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Fees')]/following::div[1]", 2),
        charge_total_addon_cost:        get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Total Add-on Charges')]/following::div[1]", 2),
        charge_total_reservation_cost:  get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Total reservation cost')]/following::div[1]", 2),
        charge_prepayment:              get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Prepayment')]/following::div[1]", 2),
        charge_balance_payable:         get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Payable')]/following::div[1]", 2),
        charge_refund:                  get_page_content(doc, "//div[contains(@class,'col-xs-6 no-padding right')][contains(.,'Refund')]/following::div[1]", 2),
        charge_penalty:                 get_penalty_value(doc, reservation),
        policy_arrival:                 get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Arrival')]"),
        policy_departure:               get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Departure')]"),
        policy_children:                get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Children')]"),
        policy_prepayment:              get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Prepayment')]"),
        policy_modification:            get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Modification')]"),
        policy_cancellation:            get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Cancellation')]"),
        policy_noshow:                  get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'No-show')]"),
        policy_refund:                  get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Refund')]"),
        policy_paypal:                  get_page_content(doc, "//a[@id='accordion']/div[1]//p[contains(.,'Paypal')]")
    ]
    if reservation[:is_dwh]
      rsvsn_details.update(Hash[charge_prepayment: reservation[:current_charges][:prepayment]]) if rsvsn_details[:charge_prepayment].nil?
      rsvsn_details.update(Hash[charge_balance_payable: reservation[:current_charges][:balance_payable]]) if rsvsn_details[:charge_balance_payable].nil?
    end
    return rsvsn_details
  end

  def get_page_content(doc, locator, content_type=nil)
    page_element = doc.at(locator)
    return nil if page_element.nil?
    content = page_element.content.strip
    content = content_type.nil? || content_type == 3 ? content : content.split(" ")[content_type - 1]
    return content_type.nil? ? content : (content.nil? ? nil : content.gsub(",", "").gsub("*", "").strip)
  end

  private

  def get_penalty_value(doc, reservation)
    return get_page_content(doc, "//td[contains(., 'Cancellation Charge')]/following::td[1]", 2) if reservation[:is_cancelled]
    return get_page_content(doc, "//td[contains(., 'No Show Charge')]/following::td[1]", 2) if reservation[:is_noshow]
    return get_page_content(doc, "//td[contains(., 'Modification Charge')]/following::td[1]", 2) if reservation[:is_modified]
  end
end
