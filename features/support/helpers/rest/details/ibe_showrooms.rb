# ShowRooms Helper
module ShowRoomsHelper
  def get_policy_copies(response)
    doc = Nokogiri::HTML(response)
    copy = Array[
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Arrival')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Departure')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Children')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Prepayment')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Modification')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Cancellation')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'No-show')][1]"),
      get_page_content(doc, "//div[@id='rpPolicy']//li[contains(., 'Card Fraud')][1]") ]
    return copy.join("\n")
  end

  def get_bpg_copies(response)
    doc = Nokogiri::HTML(response)
    return get_page_content(doc, "//div[@id='bpgModal']//div[@class='modal-body']")
  end
end
