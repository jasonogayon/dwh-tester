# Reservation Details Helper
module ReservationDetailsHelper
  def get_he_rsvsn_details(details)
    he_details = Hash[
      confirmation_no:                details["confirmation_no"],
      transaction_id:                 details["transaction_id"],
      guest_name:                     details["first_name"] + " " + details["last_name"],
      guest_email:                    details["guest_email"],
      guest_mobile:                   details["contact_no"],
      guest_country:                  details["country_name"],
      rsvsn_status:                   details["status_name"],
      rsvsn_madeon:                   details["created_date"],
      rsvsn_modifiedon:               details["modified_date"],
      rsvsn_channel_madeon:           details["channel_creation_date"],
      rsvsn_channel_modifiedon:       details["channel_modification_date"],
      rsvsn_rateplan:                 details["reservation_details"][0]["rate_plan_name"],
      rsvsn_channel_rateplan:         details["reservation_details"][0]["channel_rate_plan_name"],
      rsvsn_eta:                      details["eta"],
      rsvsn_no_guests:                details["adults"].to_i + details["children"].to_i,
      rsvsn_no_rooms:                 details["total_no_of_rooms"],
      rsvsn_no_nights:                details["no_of_nights"],
      rsvsn_checkin:                  details["check_in_date"],
      rsvsn_checkout:                 details["check_out_date"],
      rsvsn_specialrequest:           details["special_request"],
      charge_room_charge_partial:     sprintf("%.2f", details["total_room_charges"]),
      charge_room_charge_total:       sprintf("%.2f", details["total_room_charges"]),
      charge_total_tax:               sprintf("%.2f", details["total_tax"]),
      charge_total_fee:               sprintf("%.2f", details["total_fee"]),
      charge_total_addon_cost:        sprintf("%.2f", details["total_add_on_rate"]),
      charge_total_reservation_cost:  sprintf("%.2f", details["total_amount"]),
      charge_prepayment:              sprintf("%.2f", details["deposit_amount"]),
      charge_balance_payable:         sprintf("%.2f", details["balance_amount"]),
      charge_penalty:                 sprintf("%.2f", details["modification_charge_amount"].to_f + details["cancel_charge_amount"].to_f + details["no_show_charge_amount"].to_f),
      charge_dwh_revenue:             sprintf("%.2f", details["dwh_revenue"].to_f.round(2)),
      policy_children:                details["applied_policy"]["child_policy_desc"],
      policy_prepayment:              details["IBEPolicyCopies"]["prepayment"],
      policy_modification:            details["IBEPolicyCopies"]["modification"],
      policy_cancellation:            details["IBEPolicyCopies"]["cancellation"],
      policy_noshow:                  details["IBEPolicyCopies"]["noshow"],
      acknowledgment:                 details["acknowledged_date"],
      hotel_remittable:               details["hotel_remittable"],
      hotel_remittable_usd:           details["hotel_remittable_usd"],
      rateplan_info:                  details["rate_plan_info"]
    ]
    is_cancelled = @property[:reservation][:rsvsn_type] =~ /cancel|noshow|no-show/
    is_special_revenue = @property[:reservation][:rateplan] =~ /Partial|Arrival/ || @property[:hotel_type] =~ /BDO/
    hotel_revenue = sprintf("%.2f", he_details[:charge_penalty].to_f - he_details[:charge_dwh_revenue].to_f) if is_cancelled && !is_special_revenue
    hotel_revenue ||= is_cancelled && is_special_revenue ? nil : sprintf("%.2f", he_details[:charge_total_reservation_cost].to_f - he_details[:charge_dwh_revenue].to_f)
    he_details.update(Hash[charge_hotel_revenue: hotel_revenue])
    he_details.update(Hash[charge_penalty: nil]) unless he_details[:charge_penalty].to_f > 0
    he_details.update(Hash[charge_total_addon_cost: nil]) if he_details[:charge_total_addon_cost].to_i.zero?
    return he_details
  end
end
