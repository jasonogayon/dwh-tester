# BDO Payment Helper
module AsiapayPaymentHelper
  def get_ap_payment_details(response)
    doc = Nokogiri::HTML(response)
    return Hash[
      merchant_id:        doc.xpath("//input[@name='merchantId']")[0].attr("value"),
      amount:             doc.xpath("//input[@name='amount']")[0].attr("value"),
      order_ref:          doc.xpath("//input[@name='orderRef']")[0].attr("value"),
      currency_code:      doc.xpath("//input[@name='currCode']")[0].attr("value"),
      cc_type:            doc.xpath("//input[@name='pMethod']")[0].attr("value"),
      cc_number:          doc.xpath("//input[@name='cardNo']")[0].attr("value"),
      cc_cvv:             doc.xpath("//input[@name='securityCode']")[0].attr("value"),
      cc_name:            doc.xpath("//input[@name='cardHolder']")[0].attr("value"),
      cc_exp_month:       doc.xpath("//input[@name='epMonth']")[0].attr("value"),
      cc_exp_year:        doc.xpath("//input[@name='epYear']")[0].attr("value"),
      pay_type:           doc.xpath("//input[@name='payType']")[0].attr("value"),
      language:           doc.xpath("//input[@name='lang']")[0].attr("value"),
      ip_address:         doc.xpath("//input[@name='custIPAddress']")[0].attr("value"),
      first_name:         doc.xpath("//input[@name='billingFirstName']")[0].attr("value"),
      last_name:          doc.xpath("//input[@name='billingLastName']")[0].attr("value"),
      email:              doc.xpath("//input[@name='billingEmail']")[0].attr("value"),
      country:            doc.xpath("//input[@name='billingCountry']")[0].attr("value"),
      remark:             doc.xpath("//input[@name='remark']")[0].attr("value"),
      url_success:        doc.xpath("//input[@name='successUrl']")[0].attr("value"),
      url_cancel:         doc.xpath("//input[@name='cancelUrl']")[0].attr("value"),
      url_error:          doc.xpath("//input[@name='errorUrl']")[0].attr("value"),
      url_fail:           doc.xpath("//input[@name='failUrl']")[0].attr("value")
    ]
  end

  def get_ap_redirect_details(response)
    doc = Nokogiri::HTML(response)
    return Hash[
      master_merchant_id: doc.xpath("//input[@name='masterMerId']")[0].attr("value"),
      o_id:               doc.xpath("//input[@name='oId']")[0].attr("value"),
      session_id:         doc.xpath("//input[@name='sessionId']")[0].attr("value"),
      url_redirect:       doc.xpath("//input[@name='urlRedirect']")[0].attr("value")
    ]
  end
end
