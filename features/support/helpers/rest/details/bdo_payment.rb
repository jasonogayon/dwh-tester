# BDO Payment Helper
module BDOPaymentHelper
  def get_bdo_payment_details(response)
    doc = Nokogiri::HTML(response)
    return Hash[
      vpc_version:        doc.search("//input[@name='vpc_Version']").attribute("value"),
      vpc_command:        doc.search("//input[@name='vpc_Command']").attribute("value"),
      vpc_accesscode:     doc.search("//input[@name='vpc_AccessCode']").attribute("value"),
      vpc_txnref:         doc.search("//input[@name='vpc_MerchTxnRef']").attribute("value"),
      vpc_merchant:       doc.search("//input[@name='vpc_Merchant']").attribute("value"),
      vpc_orderinfo:      doc.search("//input[@name='vpc_OrderInfo']").attribute("value"),
      vpc_amount:         doc.search("//input[@name='vpc_Amount']").attribute("value"),
      vpc_returnurl:      doc.search("//input[@name='vpc_ReturnURL']").attribute("value"),
      vpc_locale:         doc.search("//input[@name='vpc_Locale']").attribute("value"),
      vpc_gateway:        doc.search("//input[@name='vpc_gateway']").attribute("value"),
      vpc_card:           doc.search("//input[@name='vpc_card']").attribute("value"),
      vpc_cardnum:        doc.search("//input[@name='vpc_CardNum']").attribute("value"),
      vpc_cardexp:        doc.search("//input[@name='vpc_CardExp']").attribute("value"),
      vpc_cardseccode:    doc.search("//input[@name='vpc_CardSecurityCode']").attribute("value"),
      vpc_securehash:     doc.search("//input[@name='vpc_SecureHash']").attribute("value"),
      vpc_securehashtype: doc.search("//input[@name='vpc_SecureHashType']").attribute("value")
    ]
  end
end
