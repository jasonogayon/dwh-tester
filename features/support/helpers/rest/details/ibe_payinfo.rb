include DatesHelper

# Reservation Charges Helper
module ReservationChargesHelper
  def get_payinfo_details(property, previous_charges=nil)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    doc = Nokogiri::HTML(get(reservation[:desktop_payinfo_url], @rsvsn_headers))
    charges = Hash[
        rateplan_details:           rateplan,
        no_days_until_checkin:      (Date.parse(reservation[:arrival][0]) - DateTime.now).to_i,
        arrival_date:               get_page_content(doc, "//strong[contains(., 'Arrival')]/following::span[1]"),
        departure_date:             get_page_content(doc, "//strong[contains(., 'Departure')]/following::span[1]"),
        no_nights:                  get_page_content(doc, "//strong[contains(., 'Length of stay')]/following::span[1]", 1),
        total_room_charge:          get_page_content(doc, "//div[@id='total-room-cost']//td[contains(., 'Room Cost')]/following::span[1]/following::text()[1]", 2),
        total_tax:                  get_page_content(doc, "//td[contains(., 'Taxes')]/following::span[1]/following::text()[1]", 2),
        total_fee:                  get_page_content(doc, "//td[contains(., 'Fees')]/following::span[1]/following::text()[1]", 2),
        total_addon_cost:           get_page_content(doc, "//td[contains(., 'Total Add-on Charges')]/following::span[1]/following::text()[1]", 2),
        total_reservation_cost:     get_page_content(doc, "//td[contains(., 'Total Reservation Cost')]/following::span[4]", 3),
        prepayment:                 rateplan[:is_no_prepay] ? "0.00" : get_page_content(doc, "//td[contains(., 'PREPAYMENT')]/following::span[1]/following::text()[1]", 2),
        balance_payable:            get_page_content(doc, "//td[contains(., 'PAYABLE AT THE HOTEL')]/following::span[6]", 3),
        penalty:                    get_page_content(doc, "//td[contains(., 'MODIFICATION CHARGE')]/following::td[1]", 2),
        refund:                     get_page_content(doc, "//td[contains(., 'Refund')]/following::span[1]/following::text()[1]", 2),
        policy_prepayment:          get_page_content(doc, "//td[contains(., 'Prepayment')]/following::td[1]"),
        policy_modification:        get_page_content(doc, "//td[contains(., 'Modification')]/following::td[1]"),
        policy_cancellation:        get_page_content(doc, "//td[contains(., 'Cancellation')]/following::td[1]"),
        commission:                 get_commission(property)
    ]
    if reservation[:is_modified]
      charges.update(Hash[prepayment: get_page_content(doc, "//td[contains(., 'PREPAYMENT')]/following::span[1]/following::text()[1]", 2)]) unless reservation[:rateplan_newdetail][:is_no_prepay]
    end
    charges.update(Hash[prepayment_he_display: charges[:prepayment], balance_payable_he_display: charges[:balance_payable]])
    return update_reservation_charges(property, charges, previous_charges)
  end

  def update_reservation_charges(property, charges, previous_charges=nil)
    is_nocc = property[:nocc_sameday] || property[:nocc_nextday]
    charge_base = property[:reservation][:is_modified] ? previous_charges : charges
    charges.update(Hash[penalty:                is_nocc ? nil : get_penalty_amount(property, charge_base)])
    charges.update(Hash[penalty_he:             is_nocc ? nil : get_penalty_amount(property, charge_base, true)])
    charges.update(Hash[refund:                 get_refund_amount(property, charge_base)])
    charges.update(Hash[dwh_revenue:            get_dwh_revenue_amount(property, charges, previous_charges)])
    charges.update(Hash[hotel_revenue:          get_hotel_revenue_amount(property, charges)])
    return charges
  end

  def get_payinfo_details_mobile(property)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    doc = Nokogiri::HTML(get(reservation[:mobile_payinfo_url], @rsvsn_headers))
    charges = Hash[
        rateplan_details:           rateplan,
        no_days_until_checkin:      (Date.parse(reservation[:arrival][0]) - DateTime.now).to_i,
        arrival_date:               get_page_content(doc, "//div[contains(.,'Arrival')][contains(@class,'col-xs-5')]/following::div[1]//small"),
        departure_date:             get_page_content(doc, "//div[contains(.,'Departure')][contains(@class,'col-xs-5')]/following::div[1]//small"),
        no_nights:                  get_page_content(doc, "//div[contains(.,'nights')][contains(@class,'col-xs-5')]/following::div[1]//small", 1),
        total_room_charge:          get_page_content(doc, "//span[@class='total-room-cost']", 3),
        total_tax:                  get_page_content(doc, "//span[@class='taxes']", 3),
        total_fee:                  get_page_content(doc, "//span[@class='fees']", 3),
        total_reservation_cost:     get_page_content(doc, "//span[@class='total-rsvn-cost']", 3),
        prepayment:                 rateplan[:is_no_prepay] ? "0.00" : get_page_content(doc, "//span[@class='prepayment']", 3),
        balance_payable:            get_page_content(doc, "//span[@class='balance']", 3),
        penalty:                    get_page_content(doc, "//div[contains(.,'MODIFICATION')][contains(@class,'col-xs-6')]/following::div[1]/span[1]", 3),
        refund:                     get_page_content(doc, "//div[contains(.,'Refund')][contains(@class,'col-xs-6')]/following::div[1]/span[1]", 3),
        commission:                 get_commission(property)
    ]
    charges.update(Hash[prepayment_he_display: charges[:prepayment], balance_payable_he_display: charges[:balance_payable]])
    room_qty = property[:reservation][:rooms][0][:room_0_qty] ||= 1
    charges.update(Hash[
        total_room_charge:          sprintf("%.2f", charges[:total_room_charge].to_f * room_qty),
        total_tax:                  sprintf("%.2f", charges[:total_tax].to_f * room_qty),
        total_fee:                  sprintf("%.2f", charges[:total_fee].to_f * room_qty),
        total_reservation_cost:     sprintf("%.2f", charges[:total_reservation_cost].to_f * room_qty),
        prepayment:                 sprintf("%.2f", charges[:prepayment].to_f * room_qty),
        prepayment_he_display:      sprintf("%.2f", charges[:prepayment_he_display].to_f * room_qty),
        balance_payable:            sprintf("%.2f", charges[:balance_payable].to_f * room_qty),
        balance_payable_he_display: sprintf("%.2f", charges[:balance_payable_he_display].to_f * room_qty)
    ])
    return update_reservation_charges(property, charges)
  end

  private

  def get_refund_amount(property, charges)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    total_rsvsn_cost = charges[:total_reservation_cost].to_f
    late_modifcancel = rateplan[:has_lead_time_penalty] && charges[:no_days_until_checkin] < 3
    unless reservation[:is_confirmed]
      if reservation[:is_dwh] || (reservation[:is_paypal] && reservation[:is_cancelled] && late_modifcancel)
        if rateplan[:is_full_prepay]
          if rateplan[:has_no_penalty]
            refund = total_rsvsn_cost
          end
          if rateplan[:has_lead_time_penalty]
            refund = late_modifcancel ? total_rsvsn_cost * 0.7 : total_rsvsn_cost
          end
          if rateplan[:has_first_night_penalty]
            refund = charges[:no_nights].to_i > 1 ? total_rsvsn_cost / charges[:no_nights].to_f : 0
          end
          if rateplan[:has_full_charge_penalty]
            refund = 0
          end
          refund ||= nil
        elsif rateplan[:is_partial_prepay] && reservation[:is_modified]
          refund = charges[:prepayment].to_f
        end
      end
    end
    return sprintf("%.2f", refund) unless refund.nil?
  end

  def get_penalty_amount(property, charges, for_he=false)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    no_nights = charges[:no_nights].to_i
    late_modifcancel = rateplan[:has_lead_time_penalty] && charges[:no_days_until_checkin] < 3
    total_rsvsn_cost = charges[:total_reservation_cost].to_f
    unless reservation[:is_confirmed] || rateplan[:has_no_penalty] || reservation[:is_noshow_waived] || reservation[:is_noshow_invalid]
      if reservation[:is_dwh]
        return nil if rateplan[:is_full_prepay] && rateplan[:is_nonrefundable]
        penalty = total_rsvsn_cost * 0.3 if late_modifcancel || for_he
        if rateplan[:has_first_night_penalty]
          penalty = total_rsvsn_cost / no_nights
          penalty -= charges[:prepayment_he_display].to_f if rateplan[:is_partial_prepay] && no_nights == 1
        end
        if rateplan[:has_full_charge_penalty]
          penalty = total_rsvsn_cost
          penalty -= charges[:prepayment_he_display].to_f if rateplan[:is_partial_prepay]
        end
      end
      if reservation[:is_hpp] || (reservation[:is_paypal] && ((for_he && reservation[:is_noshow]) || reservation[:is_cancelled]))
        if rateplan[:is_nonrefundable]
          penalty = charges[:prepayment_he_display] if !rateplan[:is_not_allowed] || reservation[:is_noshow]
        else
          penalty = total_rsvsn_cost * 0.3 if late_modifcancel || reservation[:is_noshow]
          if rateplan[:has_first_night_penalty] || rateplan[:has_full_charge_penalty]
            penalty = total_rsvsn_cost
            penalty = penalty / no_nights if rateplan[:has_first_night_penalty] && no_nights > 1
          end
        end
      end
      if reservation[:is_bdo]
        penalty = charges[:prepayment_he_display].to_f if for_he && (late_modifcancel || rateplan[:is_nonrefundable] || reservation[:is_noshow])
      end
    end
    return sprintf("%.2f", penalty) unless penalty.nil?
  end

  def get_dwh_revenue_amount(property, charges, previous_rsvsn_charges)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    total_room_charge = charges[:total_room_charge].to_f
    commission = charges[:commission]
    if reservation[:is_cancelled] || (reservation[:is_noshow] && !reservation[:is_noshow_invalid])
      if reservation[:is_dwh] && rateplan[:is_partial_prepay]
        dwh_revenue = charges[:prepayment_he_display].to_f
      elsif reservation[:is_dwh] && rateplan[:is_full_prepay] && reservation[:is_noshow]
        dwh_revenue = rateplan[:is_nonrefundable] ? total_room_charge : (charges[:penalty_he].to_f / 1.166).round(2).round(1)
        dwh_revenue = dwh_revenue * commission
      else
        penalty = reservation[:is_bdo] || reservation[:is_paypal] ? charges[:penalty_he] : charges[:penalty]
        dwh_revenue = (penalty.to_f / 1.166).round(2).round(1) * commission
      end
    elsif reservation[:is_modified] && reservation[:is_dwh]
      previous_rateplan = previous_rsvsn_charges[:rateplan_details]
      late_modifcancel = previous_rateplan[:has_lead_time_penalty] && previous_rsvsn_charges[:no_days_until_checkin] < 3
      if previous_rateplan[:is_full_prepay] && (late_modifcancel || previous_rateplan[:has_first_night_penalty])
        dwh_revenue = (total_room_charge + (charges[:penalty].to_f / 1.166).round(2)) * commission
      end
    end
    dwh_revenue ||= total_room_charge * commission
    return sprintf("%.2f", dwh_revenue) unless dwh_revenue.nil?
  end

  def get_hotel_revenue_amount(property, charges)
    reservation = property[:reservation]
    rateplan = reservation[:rateplan_details]
    dwh_revenue = charges[:dwh_revenue].to_f
    if reservation[:is_cancelled] || reservation[:is_noshow]
      hotel_revenue = 0 if reservation[:is_bdo] || rateplan[:is_partial_prepay] || rateplan[:is_no_prepay]
      hotel_revenue = charges[:penalty_he].to_f - dwh_revenue if reservation[:is_dwh] && rateplan[:is_full_prepay] && rateplan[:is_refundable] && reservation[:is_noshow]
      hotel_revenue = charges[:penalty_he].to_f - dwh_revenue if reservation[:is_paypal] && rateplan[:is_full_prepay] && reservation[:is_noshow]
      hotel_revenue ||= charges[:penalty].to_f - dwh_revenue
    end
    hotel_revenue ||= charges[:total_reservation_cost].to_f - dwh_revenue
    return sprintf("%.2f", hotel_revenue) unless hotel_revenue.nil?
  end

  def get_commission(property)
    reservation = property[:reservation]
    commission = FigNewton.dwh_percent_commission if reservation[:is_dwh]
    commission = FigNewton.hpp_percent_commission if reservation[:is_hpp]
    commission = FigNewton.bdo_percent_commission if reservation[:is_bdo]
    commission = FigNewton.paypal_percent_commission if reservation[:is_paypal]
    return (commission.to_f / 100).round(2)
  end
end
