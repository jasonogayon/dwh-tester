# Paypal Payment Helper
module PaypalPaymentHelper
  def get_paypal_payment_details(response)
    doc = Nokogiri::HTML(response)
    return Hash[
      login_email:        FigNewton.paypal_email,
      login_password:     FigNewton.paypal_password,
      currentSession:     doc.at("//input[@id='currentSession']")["value"],
      session:            doc.at("//input[@id='pageSession']")["value"],
      currentDispatch:    doc.at("//input[@id='currentDispatch']")["value"],
      dispatch:           doc.at("//input[@id='pageDispatch']")["value"],
      context:            doc.at("//input[@id='CONTEXT_CGI_VAR']")["value"],
      auth:               doc.at("//input[@name='auth']")["value"]
    ]
  end

  def get_paypal_redirect_login_cookies(session, response)
    return Hash[
      phpsessionid:   session,
      abc_switch:     response.cookies["abc_switch_cross_paypal"],
      navcmd:         response.cookies["navcmd"],
      xpps:           response.cookies["x-pp-s"],
      xppk:           response.cookies["X-PP-K"],
      xppsilover:     response.cookies["X-PP-SILOVER"],
      c9m:            response.cookies["c9MWDuvPtT9GIMyPc3jwol1VSlO"],
      imm:            response.cookies["i-mmSTyTsv6thyfmaQ1oZIPvE98"],
      ilh:            response.cookies["-1ILhdyICORs4hS4xTUr41S8iP0"],
      apache:         response.cookies["Apache"],
      feelcookie:     response.cookies["feel_cookie"]
    ]
  end

  def get_paypal_account_login_cookies(session, redirect_login, account_login)
    return Hash[
      phpsessionid:   session,
      abc_switch:     account_login.cookies["abc_switch_cross_paypal"],
      navcmd:         redirect_login.cookies["navcmd"],
      xpps:           account_login.cookies["x-pp-s"],
      xppk:           redirect_login.cookies["X-PP-K"],
      xppsilover:     account_login.cookies["X-PP-SILOVER"],
      c9m:            account_login.cookies["c9MWDuvPtT9GIMyPc3jwol1VSlO"],
      imm:            account_login.cookies["i-mmSTyTsv6thyfmaQ1oZIPvE98"],
      ilh:            redirect_login.cookies["-1ILhdyICORs4hS4xTUr41S8iP0"],
      apache:         redirect_login.cookies["Apache"],
      feelcookie:     account_login.cookies["feel_cookie"],
      search:         account_login.cookies["INSIDE_SEARCH_PARAMS"],
      lang:           account_login.cookies["LANG"],
      la6:            account_login.cookies["La6vgDMjfK_07f8HuOwmk5fn5w0"],
      segm:           account_login.cookies["SEGM"],
      tv7:            account_login.cookies["Tv7XaFXkAfcLyjkmtYddHHs5nwS"],
      xppads:         account_login.cookies["X-PP-ADS"]
    ]
  end
end
