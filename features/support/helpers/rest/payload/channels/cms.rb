# CMS Payload
module CMSPayload
  def build_cms_request(channel, request_type, start_date, end_date, days_of_week)
    request = get_cms_request_header(channel, request_type)
    request += get_cms_request_body(channel, request_type, start_date, end_date, days_of_week)
    request += get_cms_request_footer(channel, request_type)
  end

  private
  def get_cms_request_header(channel, request)
    if channel.include? "agoda"
      return FigNewton.cms_agoda_header % { request_type: "1" } if request.include? "setari"
      return FigNewton.cms_agoda_header % { request_type: "3" } if request.include? "getbookinglist"
      return FigNewton.cms_agoda_header % { request_type: "4" } if request.include? "getbookingdetails"
    elsif channel.include?("booking")
      return FigNewton.cms_bookingdotcom_header_availnotif if request.include? "availnotif"
      return FigNewton.cms_bookingdotcom_header_rateamountnotif if request.include? "rateamountnotif"
      return FigNewton.cms_bookingdotcom_header_resmodifynotif if request.include? "resmodifynotif"
      return FigNewton.cms_bookingdotcom_header_resnotif if request.include? "resnotif"
    end
  end

  def get_cms_request_footer(channel, request)
    return FigNewton.cms_agoda_footer if channel.include? "agoda"
    if channel.include?("booking")
      return FigNewton.cms_bookingdotcom_footer_availnotif if request.include? "availnotif"
      return FigNewton.cms_bookingdotcom_footer_rateamountnotif if request.include? "rateamountnotif"
      return FigNewton.cms_bookingdotcom_footer_resmodifynotif if request.include? "resmodifynotif"
      return FigNewton.cms_bookingdotcom_footer_resnotif if request.include? "resnotif"
    end
  end

  def get_cms_request_body(channel, request, start_date, end_date, days_of_week)
    info = get_cms_body_information(channel, request, start_date, end_date, days_of_week)
    case channel
    when /agoda/
      case request
      when /getbookinglist/
        return FigNewton.cms_agoda_body_getbookinglist_missingdate % info if request.include? "missing date"
        return FigNewton.cms_agoda_body_getbookinglist_missingproperty % info if request.include? "missing property"
        return FigNewton.cms_agoda_body_getbookinglist_default % info
      when /getbookingdetails/
        return FigNewton.cms_agoda_body_getbookingdetails_missinglanguage % info if request.include? "missing language"
        return FigNewton.cms_agoda_body_getbookingdetails_missingproperty % info if request.include? "missing property"
        return FigNewton.cms_agoda_body_getbookingdetails_missingbooking % info if request.include? "missing booking"
        return FigNewton.cms_agoda_body_getbookingdetails_default % info
      when /setari/
        if request.include? "only"
          return FigNewton.cms_agoda_body_setari_ratesonly % info if request.include? "rates"
          return FigNewton.cms_agoda_body_setari_inventoryonly % info if request.include? "inventory"
          return FigNewton.cms_agoda_body_setari_losonly % info if request.include? "los"
        else
          return FigNewton.cms_agoda_body_setari_all_withextrabed % info if request.include? "extra bed"
          return FigNewton.cms_agoda_body_setari_all_default % info
        end
      end
    when /booking/
      return FigNewton.cms_bookingdotcom_body_availnotif % info if request.include? "availnotif"
      return FigNewton.cms_bookingdotcom_body_rateamountnotif % info if request.include? "rateamountnotif"
      return FigNewton.cms_bookingdotcom_body_resmodifynotif % info if request.include? "resmodifynotif"
      return FigNewton.cms_bookingdotcom_body_resnotif % info if request.include? "resnotif"
    end
  end

  def get_cms_body_information(channel, request, start_date, end_date, days_of_week)
    rate = Hash[
      default:            100.00,
      single_occupancy:   120.50,
      double_occupancy:   230.45,
      full_occupancy:     540.21,
      extra_bed:          54.21,
      is_closed:          false
    ]
    rate.update(rate) { |key, value| 0 } if request.include? "zero"
    rate.update(rate) { |key, value| -123.45 } if request.include? "negative"
    rate.update(rate) { |key, value| 999_999_999_999_999.9999 } if request.include? "over max"
    rate.update(rate) { |key, value| nil } if request.include? "blank"
    rate.update(Hash[single_occupancy: 1000]) if request.include? "bigger single"
    rate.update(Hash[double_occupancy: 1000]) if request.include? "bigger double"
    rate.update(Hash[is_closed: true]) if request.include? "closed"
    rate.update(Hash[default: 1]) if request.include? "low"
    info = Hash[
      hotel_id: request.include?("invalid hotel") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_hotel_id : nil),
      rateplan_id: request.include?("invalid rateplan") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_rateplan_id : FigNewton.cms_bookingdotcom_rateplan_id),
      room_id: request.include?("invalid room") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_room_id : FigNewton.cms_bookingdotcom_room_id),
      currency: request.include?("invalid currency") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_currency : nil),
      booking_id: request.include?("invalid booking") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_booking : FigNewton.cms_bookingdotcom_booking_1),
      booksource: request.include?("invalid booking source") ? (FigNewton.cms_bookingdotcom_bookingsource_1).chop : FigNewton.cms_bookingdotcom_bookingsource_1,
      language: request.include?("invalid language") ? 0 : (channel.include?("agoda") ? FigNewton.cms_agoda_language : nil),
      start_date: start_date,
      end_date: end_date,
      start_date_time: request.include?("invalid timezone") ? "" : "T00:00:00+08:00",
      end_date_time: request.include?("invalid timezone") ? "" : "T14:10:24+08:00",
      booking_limit: channel.include?("booking") ? FigNewton.cms_bookingdotcom_bookinglimit : (request.include?("zero booking limit") ? 0 : -1),
      locator_id: request.include?("zero locator id") ? 0 : (request.include?("negative locator id") ? -1 : (request.include?("availnotif") ? FigNewton.cms_bookingdotcom_locator_id_availnotif : FigNewton.cms_bookingdotcom_locator_id_rateamountnotif)),
      status: request.include?("close") ? "Close" : "Open",
      days_of_week: days_of_week,
      rate_date: start_date,
      rate: rate[:default],
      rate_default: rate[:default],
      rate_singleoccupancy: rate[:single_occupancy],
      rate_doubleoccupancy: rate[:double_occupancy],
      rate_fulloccupancy: rate[:full_occupancy],
      rate_extrabed: rate[:extra_bed],
      rate_isclosed: rate[:is_closed],
      allotment: request.include?("invalid allotment") ? -9_784_628_746_284_628 : (request.include?("over max allotment") ? 1000 : 9),
      allotment_isclosed: request.include?("closed allotment") ? true : false,
      min_los: request.include?("invalid minimum los") ? (request.include?("zero") ? 0 : -5) : 1,
      max_los: request.include?("over max los") ? (channel.include?("booking") ? 194_758_947_896_789_476_948_764_897_694_876_023_840_759_037_895_648_956 : 1004) : 6,
      no_guest: request.include?("occupancy") ? "NumberOfGuests='2'" : "",
      decimal: request.include?("decimal") ? "DecimalPlaces='2'" : "",
      reservation_id_2: FigNewton.cms_bookingdotcom_booking_2,
      reservation_source_2: FigNewton.cms_bookingdotcom_bookingsource_2,
    ]
    info.update(Hash[reservation_id_1: info[:booking_id], reservation_source_1: info[:booksource], los: FigNewton.cms_bookingdotcom_body_availnotif_los % info])
  end
end
