# PMS Payload
module PMSPayload
  def build_pms_request(request_type, start_date, end_date)
    request = get_pms_request_header(request_type)
    request += get_pms_request_body(request_type, start_date, end_date)
    request += get_pms_request_footer(request_type)
  end

  private
  def get_pms_request_header(request)
    summaryonly = request.include?("summary only") ? true : false
    request_header = FigNewton.pms_header_readrq % { max_responses: 5 } if request.include? "readrq"
    request_header = FigNewton.pms_header_notifreportrq if request.include? "notifreportrq"
    request_header = FigNewton.pms_header_rateplanrq % { summary_only: summaryonly.to_s } if request.include? "rateplanrq"
    request_header = FigNewton.pms_header_rateplannotifrq if request.include? "rateplannotifrq"
    request_header = FigNewton.pms_header_invcountrq % { summary_only: false } if request.include? "invcountrq"
    request_header = FigNewton.pms_header_invcountnotifrq if request.include? "invcountnotifrq"
    request_header = FigNewton.pms_header_rateamountrq if request.include? "rateamountnotifrq"
    request_header += FigNewton.pms_pos_credentials % {
      pms_client_id:          FigNewton.pms_client_id,
      pms_client_password:    request.include?("invalid client") ? FigNewton.pms_client_password.chop : FigNewton.pms_client_password,
      pms_api_key:            request.include?("invalid key") ? FigNewton.pms_api_key.chop : FigNewton.pms_api_key
    }
  end

  def get_pms_request_footer(request)
    return FigNewton.pms_footer_readrq if request.include? "readrq"
    return FigNewton.pms_footer_notifreportrq if request.include? "notifreportrq"
    return FigNewton.pms_footer_rateplanrq if request.include? "rateplanrq"
    return FigNewton.pms_footer_rateplannotifrq if request.include? "rateplannotifrq"
    return FigNewton.pms_footer_invcountrq if request.include? "invcountrq"
    return FigNewton.pms_footer_invcountnotifrq if request.include? "invcountnotifrq"
    return FigNewton.pms_footer_rateamountrq if request.include? "rateamountnotifrq"
  end

  def get_pms_request_body(request, start_date, end_date)
    subrequest = (request.include?("rateamountnotifrq") || request.include?("notifreportrq")) ? request : request.split(" ")[1].capitalize
    subrequest = "PreviouslyDelivered" if subrequest.include? "Previouslydelivered"
    info = get_pms_body_information(request, subrequest, start_date, end_date)
    case request
    when /readrq/
      return FigNewton.pms_body_readrq % info
    when /notifreportrq/
      return FigNewton.pms_body_notifreportrq % info
    when /rateplanrq/
      return subrequest.include?("Selected") ? FigNewton.pms_body_rateplanrq_selected % info : FigNewton.pms_body_rateplanrq_all % info
    when /rateplannotifrq/
      return FigNewton.pms_body_rateplannotifrq % info
    when /invcountrq/
      return subrequest.include?("Selected") ? FigNewton.pms_body_invcountrq_selected % info : FigNewton.pms_body_invcountrq_all % info
    when /invcountnotifrq/
      return FigNewton.pms_body_invcountnotifrq_none % info if subrequest.include? "None"
      return FigNewton.pms_body_invcountnotifrq_adjustment % info if subrequest.include? "Adjustment"
      return FigNewton.pms_body_invcountnotifrq_allocation % info if subrequest.include? "Allocation"
      return FigNewton.pms_body_invcountnotifrq_all % info
    when /rateamountnotifrq/
      return FigNewton.pms_body_rateamountrq % info
    end
  end

  def get_pms_body_information(request, subrequest, start_date, end_date)
    Hash[
      hotel_id:           request.include?("invalid hotel") ? 0 : FigNewton.pms_hotel_id,
      rateplan_id:        request.include?("invalid rateplan") ? 0 : FigNewton.pms_rateplan_id,
      room_id:            request.include?("invalid room") ? 0 : FigNewton.pms_room_id,
      currency:           request.include?("invalid currency") ? 0 : FigNewton.pms_currency,
      request_subtype:    subrequest,
      start_date:         start_date,
      end_date:           end_date,
      date_type:          request.include?("lastupdatedate") ? "LastUpdateDate" : "CreateDate",
      adjustment:         subrequest.include?("Adjustment") ? 7 : 10,
      allocation:         subrequest.include?("Allocation") ? 2 : 3,
      room_rate:          2654.23,
      child_age:          10,
      confirmation_no:    FigNewton.pms_reservation_sample,
      acknowledge_by:     [*("A".."Z")].sample(6).join,
      booking_rules:      FigNewton.pms_body_rateplannotifrq_bookingrules % {
        modif_multiplier:   24,
        modif_unit:         "Day",
        modif_type:         "FullStay",
        modif_charge:       25,
        cancel_multiplier:  3,
        cancel_unit:        "Week",
        cancel_type:        "Nights",
        cancel_charge:      100,
        noshow_type:        "FullStay",
        noshow_charge:      50,
        prepay_type:        "FullStay",
        prepay_charge:      10,
        start_date:         start_date,
        end_date:           end_date
      },
      rates:                FigNewton.pms_body_rateplannotifrq_rates % {
        rate_status:        request.include?("close") ? "Close" : "Open",
        room_id:            request.include?("invalid room") ? 0 : FigNewton.pms_room_id,
        start_date:         start_date,
        end_date:           end_date,
        adult_rate:         1234.56,
        adult_qty:          2,
        adult_age:          18,
        child_rate:         78.90,
        child_qty:          3,
        child_age:          2,
        child_age_max:      17
      },
      rateplan_status:    subrequest,
      rateplan_code:      subrequest.include?("Active") ? FigNewton.pms_rateplan_id_old : (request.include?("invalid rateplan") ? FigNewton.pms_rateplan_invalid : rand(99_999_999).to_s),
      supplements:        FigNewton.pms_body_rateplannotifrq_supplements,
      meal_id:            6,
      rateplan_name:      "PMS Partner Test Rate Plan No. #{rand(9999)}",
      rateplan_desc:      "This is a rate plan created via DWHLink."
    ]
  end
end
