# Reservation Add-ons Payload
module ReservationAddonsPayload
  def build_payinfo_addons_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      selected_add_ons[0][id]=14
      &selected_add_ons[0][quantity]=1
      &selected_add_ons[1][id]=15
      &selected_add_ons[1][quantity]=2
      &selected_add_ons[2][id]=13
      &selected_add_ons[2][quantity]=3
    PAYLOAD
  end

  def build_cpage_addons_payload(property)
    addons = property[:public_rateplans][0][:details][:addons].scan(/add_on_id\":(.*?),/)
    addon = ""
    addons.each do |add_on|
      addon = addon + "{" \
        "\"id\":#{add_on[0].to_i}," \
        "\"quantity\":#{rand(20)}," \
        "\"selection_rates\":[]" \
      "},"
    end
    payload = "selectedAddOns=[#{addon}]"
    return payload.gsub(",]", "]")
  end
end
