# Reservation Rooms Payload
module ReservationRoomsPayload
  def build_rooms_payload(property)
    rooms = ""
    room_data = property[:reservation][:rooms]
    for i in 0..room_data.size - 1
      room_id = room_data[i]["room_#{i}_id".to_sym]
      room_qty = room_data[i]["room_#{i}_qty".to_sym]
      room_occ = room_data[i]["room_#{i}_occ".to_sym]
      rooms = rooms + "{" \
          "\"rate_plan_id\":#{property[:reservation][:rateplan_id]}," \
          "\"room_id\":#{room_id}," \
          "\"qty\":#{room_qty}," \
          "\"occupancy\":#{room_occ}," \
          "\"extra_beds\":#{property[:reservation][:extra_beds]}," \
          "\"mobile_exclusive\":#{property[:reservation][:mobile_exclusive]}" \
      "},"
    end
    payload = "data={" \
      "\"entries\":[" + rooms + "]," \
      "\"lang\":\"#{property[:reservation][:language]}\"}" \
      "&reservation_id=#{property[:reservation][:reservation_id]}" \
      "&auth_key=#{property[:reservation][:auth_key]}" \
      "&arrival=#{property[:reservation][:arrival][13]}" \
      "&departure=#{property[:reservation][:departure][13]}" \
      "&property_id=#{property[:id]}" \
      "&language=#{property[:reservation][:language]}" \
      "&user_id=#{property[:reservation][:user_id]}" \
      "&cp_user_id=#{property[:reservation][:cp_user_id]}" \
      "&currency_display_ex=1" \
      "&mobile_exclusive={mobile_exclusive}"
    return payload.gsub(",]", "]")
  end

  def build_rooms_payload_mobile(property)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      language=#{property[:reservation][:language]}
      &arrival=#{property[:reservation][:arrival][13]}
      &departure=#{property[:reservation][:departure][13]}
      &property_id=#{property[:id]}
      &room_id=#{property[:reservation][:rooms][0][:room_0_id]}
      &rateplan_id=#{property[:reservation][:rateplan_id]}
      &refer_from=mobile/getRateplanDetails/#{property[:id]}/
      #{property[:reservation][:arrival][0]}/
      #{property[:reservation][:departure][0]}/
      #{property[:reservation][:rooms][0][:room_0_id]}/
      #{property[:reservation][:language]}
      &adult=
      &is_mixed=0
      &is_public_promo_rp=0
    PAYLOAD
    payload += "&is_public_promo_rp=1" unless custom_promocode.nil?
    return payload
  end
end
