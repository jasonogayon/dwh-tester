# Reservation Process Dates Payload
module ReservationProcessDatesPayload
  def build_processdates_payload(promo_code, data)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      language=#{data[:reservation][:language]}
      &property_id=#{data[:id]}
      &reservation_id=#{data[:reservation][:reservation_id]}
      &auth_key=#{data[:reservation][:auth_key]}
      &arrival=#{data[:reservation][:arrival][4]}
      &departure=#{data[:reservation][:departure][4]}
      &promo_code_value=#{promo_code}
      &user_id=#{data[:reservation][:user_id]}
      &cp_user_id=#{data[:reservation][:cp_user_id]}
      &activefld=arrival"
      &from_step1=1
    PAYLOAD
  end
end
