# Reservation Asiapay Payment Payload
module ReservationAsiapayPaymentPayload
  def build_ap_payload(ap_details)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      merchantId=#{ap_details[:merchant_id]}
      &amount=#{ap_details[:amount]}
      &orderRef=#{ap_details[:order_ref]}
      &currCode=#{ap_details[:currency_code]}
      &pMethod=#{ap_details[:cc_type]}
      &cardNo=#{ap_details[:cc_number]}
      &securityCode=#{ap_details[:cc_cvv]}
      &cardHolder=#{ap_details[:cc_name]}
      &epMonth=#{ap_details[:cc_exp_month]}
      &epYear=#{ap_details[:cc_exp_year]}
      &payType=#{ap_details[:pay_type]}
      &lang=#{ap_details[:language]}
      &custIPAddress=#{ap_details[:ip_address]}
      &billingFirstName=#{ap_details[:first_name]}
      &billingLastName=#{ap_details[:last_name]}
      &billingEmail=#{ap_details[:email]}
      &billingCountry=#{ap_details[:country]}
      &remark=#{ap_details[:remark]}
      &successUrl=#{ap_details[:url_success]}
      &cancelUrl=#{ap_details[:url_cancel]}
      &errorUrl=#{ap_details[:url_error]}
      &failUrl=#{ap_details[:url_fail]}
    PAYLOAD
  end

  def build_ap_redirect_payload(ap_details)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      masterMerId=#{ap_details[:master_merchant_id]}
      &oId=#{ap_details[:o_id]}
      &sessionId=#{ap_details[:session_id]}
      &urlRedirect=#{ap_details[:url_redirect]}
      &pcTimeZone=8
    PAYLOAD
  end
end
