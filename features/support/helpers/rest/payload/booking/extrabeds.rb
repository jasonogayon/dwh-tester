# Reservation Extra Beds Payload
module ReservationExtraBedsPayload
  def build_extrabeds_payload(rsvsn_info)
    extrabeds = ""
    for i in 1..rsvsn_info[:room_1_qty].to_i
      extrabeds = extrabeds + "{\"room_no\":#{i},\"count\":#{rand(rsvsn_info[:extra_beds_qty].to_i + 1)}},"
    end
    payload = "[#{extrabeds}]"
    return payload.gsub(",]", "]")
  end
end
