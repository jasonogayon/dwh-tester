# Reservation Upload Payload
module ReservationUploadPayload
  FILE_ABSOLUTE_PATH = File.absolute_path('../../../../../../', File.dirname(__FILE__))

  def build_credentials_payload(credential_type, property)
    reservation = property[:reservation]
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_front) if credential_type.include? "front"
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_back) if credential_type.include? "back"
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_govid) if credential_type.include? "gov"
    return Hash[
      multipart:      true,
      CCF:            File.new(photo, "rb"),
      property_id:    property[:id],
      reservation_id: reservation[:reservation_id],
      auth_key:       reservation[:reservation_hash],
      lang:           reservation[:language]
    ]
  end

  def build_credentials_submit_payload(property)
    reservation = property[:reservation]
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{property[:id]}
      &auth_key=#{reservation[:reservation_hash]}
      &reservation_id=#{reservation[:reservation_id]}
      &lang=#{reservation[:language]}
    PAYLOAD
  end
end
