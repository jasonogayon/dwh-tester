# Reservation Promo Code Payload
module ReservationPromoCodePayload
  def build_promocode_payload(promo_code)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      promo_code=#{promo_code}
    PAYLOAD
  end
end
