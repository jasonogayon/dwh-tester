# Reservation BDO Payment Payload
module ReservationBDOPaymentPayload
  def build_vpcpay_payload(bdo_details)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      vpc_Version=#{bdo_details[:vpc_version]}
      &vpc_Command=#{bdo_details[:vpc_command]}
      &vpc_AccessCode=#{bdo_details[:vpc_accesscode]}
      &vpc_MerchTxnRef=#{bdo_details[:vpc_txnref]}
      &vpc_Merchant=#{bdo_details[:vpc_merchant]}
      &vpc_OrderInfo=#{bdo_details[:vpc_orderinfo]}
      &vpc_Amount=#{bdo_details[:vpc_amount]}
      &vpc_ReturnURL=#{bdo_details[:vpc_returnurl]}
      &vpc_Locale=#{bdo_details[:vpc_locale]}
      &vpc_gateway=#{bdo_details[:vpc_gateway]}
      &vpc_card=#{bdo_details[:vpc_card]}
      &vpc_CardNum=#{bdo_details[:vpc_cardnum]}
      &vpc_CardExp=#{bdo_details[:vpc_cardexp]}
      &vpc_CardSecurityCode=#{bdo_details[:vpc_cardseccode]}
      &vpc_SecureHash=#{bdo_details[:vpc_securehash]}
      &vpc_SecureHashType=#{bdo_details[:vpc_securehashtype]}
    PAYLOAD
  end
end
