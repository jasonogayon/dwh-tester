# Reservation Payment Payload
module ReservationPaymentPayload
  def build_base_payment_payload(property)
    reservation = property[:reservation]
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      uniquekey=#{reservation[:auth_key]}
      &id=#{reservation[:reservation_id]}
      &authorization_key=#{reservation[:reservation_hash]}
      &property_id=#{property[:id]}
      &mode=#{reservation[:rsvsn_mode]}
      &payment[email]=#{reservation[:email]}
      &payment[email_confirm]=#{reservation[:email_confirm]}
      &payment[country_code]=#{reservation[:country]}
      &payment[contact_number]=#{reservation[:phone]}
      &payment[adults]=#{reservation[:no_adults]}
      &payment[children]=#{reservation[:no_children]}
      &payment[eta]=#{reservation[:eta]}
      &payment[cc_type]=#{reservation[:cc_type]}
      &payment[cc_number]=#{reservation[:cc_number]}
      &payment[cc_exp_month]=#{reservation[:cc_exp_month]}
      &payment[cc_exp_year]=#{reservation[:cc_exp_year]}
      &payment[cc_cvv]=#{reservation[:cc_cvv]}
      &payment[agree]=#{reservation[:terms_agreement]}
      &payment[radio_question]=#{reservation[:payment_agree]}
      &proc_payment=#{reservation[:process_payment]}
      &ab=#{reservation[:ab_test]}
      &lang=#{reservation[:language]}
      &language=#{reservation[:language]}
      &curr=#{property[:currency_code]}
      &cp_user_id=#{reservation[:user_id]}
      &is_valid_day=#{reservation[:is_valid_day]}
    PAYLOAD
    payload += "&payment[first_name]=#{reservation[:first_name]}"
    payload += "&payment[last_name]=#{reservation[:last_name]}"
    payload += "&payment[cc_name]=#{reservation[:cc_name]}"
    payload += "&payment[specialrequest]=#{reservation[:special_request]}"
    if reservation[:is_paypal]
      payload += "&landingPage=Login"
      payload += "&processor=PP"
      payload += "&incontext=1"
    end
    return payload
  end

  def build_payment_payload(property)
    payload = build_base_payment_payload(property)
    qty, occupants = 0, ""
    room_data = property[:reservation][:rooms]
    for i in 0..room_data.size - 1
      room_no = 1
      room_id = room_data[i]["room_#{i}_id".to_sym]
      room_qty = room_data[i]["room_#{i}_qty".to_sym]
      room_occ = room_data[i]["room_#{i}_occ".to_sym]
      rateplan_id = property[:reservation][:rateplan_id]
      first_name = property[:reservation][:first_name]
      last_name = property[:reservation][:last_name]

      for j in (qty.to_i + 1)..(qty.to_i + room_qty.to_i)
        occupants = occupants + "&reservation_room_occupants[#{j - 1}][first_name]=#{first_name}" \
        "&reservation_room_occupants[#{j - 1}][last_name]=#{last_name}" \
        "&reservation_room_occupants[#{j - 1}][adult_count]=1" \
        "&reservation_room_occupants[#{j - 1}][rate_plan_id]=#{rateplan_id}" \
        "&reservation_room_occupants[#{j - 1}][room_id]=#{room_id}" \
        "&reservation_room_occupants[#{j - 1}][room_no]=#{room_no}" \
        "&reservation_room_occupants[#{j - 1}][occupancy]=#{room_occ}"
        room_no += 1
      end
      qty = room_qty
    end
    return payload += occupants
  end

  def build_base_payment_payload_mobile(property)
    reservation = property[:reservation]
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      uniquekey=#{reservation[:auth_key]}
      &id=#{reservation[:reservation_id]}
      &authorization_key=#{reservation[:reservation_hash]}
      &property_id=#{property[:id]}
      &mode=#{reservation[:rsvsn_mode]}
      &payment[email]=#{reservation[:email]}
      &payment[confirm_email]=#{reservation[:email_confirm]}
      &payment[country_code]=#{reservation[:country]}
      &payment[contact_number]=#{reservation[:phone]}
      &payment[adults]=1
      &payment[cc_type]=#{reservation[:cc_type]}
      &payment[cc_number]=#{reservation[:cc_number]}
      &payment[cc_exp_month]=#{reservation[:cc_exp_month]}
      &payment[cc_exp_year]=#{reservation[:cc_exp_year]}
      &payment[cc_cvv]=#{reservation[:cc_cvv]}
      &payment[agree]=#{reservation[:terms_agreement]}
      &proc_payment=#{reservation[:process_payment]}
      &ab=#{reservation[:ab_test]}
      &language=#{reservation[:language]}
      &curr=#{property[:currency_code]}
      &is_valid_day=#{reservation[:is_valid_day]}
      &eta=#{reservation[:eta]}
    PAYLOAD
    payload += "&payment[cc_first_name]=#{reservation[:first_name]}"
    payload += "&payment[cc_last_name]=#{reservation[:last_name]}"
    payload += "&payment[specialrequest]=#{reservation[:special_request]}"
    return payload
  end

  def build_payment_payload_mobile(property)
    occupants = ""
    reservation = property[:reservation]
    room = reservation[:rooms][0]
    for i in 1..room[:room_0_qty].to_i
      occupants = occupants + "&reservation_room_occupants[#{i - 1}][first_name]=#{reservation[:is_onhold] ? 'On-Hold' : reservation[:first_name]}" \
      "&reservation_room_occupants[#{i - 1}][last_name]=#{reservation[:is_onhold] ? 'Reservation' : reservation[:last_name]}" \
      "&reservation_room_occupants[#{i - 1}][adult_count]=#{reservation[:no_adults]}" \
      "&reservation_room_occupants[#{i - 1}][child_count]=#{reservation[:no_children]}" \
      "&reservation_room_occupants[#{i - 1}][rate_plan_id]=#{reservation[:rateplan_id]}" \
      "&reservation_room_occupants[#{i - 1}][room_id]=#{room[:room_0_id]}" \
      "&reservation_room_occupants[#{i - 1}][room_no]=#{i}" \
      "&reservation_room_occupants[#{i - 1}][occupancy]=#{room[:room_0_occ]}"
    end
    payload = build_base_payment_payload_mobile(property)
    payload += "&number_of_rooms=#{room[:room_0_qty]}" + occupants
    return payload
  end
end
