# Reservation Paypal Payment Payload
module ReservationPaypalPaymentPayload
  def build_paypal_login_payload(pp_details)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      cmd=_flow
      &myAllTextSubmitID=
      &reviewPgReturn=1
      &font_option=font_normal
      &currentSession=#{pp_details[:currentSession]}
      &pageState=login
      &currentDispatch=#{pp_details[:currentDispatch]}
      &email_recovery=false
      &password_recovery=false
      &login_email=#{pp_details[:login_email]}
      &login_password=#{pp_details[:login_password]}
      &private_device_checkbox_flag=on
      &SESSION=#{pp_details[:session]}
      &dispatch=#{pp_details[:dispatch]}
      &CONTEXT=#{pp_details[:context]}
      &auth=#{pp_details[:auth]}
      &form_charset=UTF-8
      &flow_name=xpt/Checkout/ec/Login
      &external_remember_me_read_cookie_ids=
      &fso_enabled=26
      &view_requested=MiniPage
    PAYLOAD
    return payload += "&login.x=Log In"
  end

  def build_paypal_payment_payload(pp_details, account_login)
    pp_details.update(Hash[
      currentSession:     pp_details[:session],
      session:            account_login.split("name=\\\"SESSION\\\" value=\\\"")[1].split("\\\"")[0],
      funding_source_id:  account_login.split("name=\\\"funding_source_id\\\" value=\\\"")[1].split("\\\"")[0].gsub("&#x2d;", "-")])
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      cmd=_flow
      &myAllTextSubmitID=
      &reviewPgReturn=1
      &font_option=font_normal
      &currentSession=#{pp_details[:currentSession]}
      &pageState=review
      &currentDispatch=#{pp_details[:currentDispatch]}
      &SESSION=#{pp_details[:session]}
      &dispatch=#{pp_details[:dispatch]}
      &pageServerName=merchantpaymentweb
      &funding_source_id=#{pp_details[:funding_source_id]}
      &CONTEXT=#{pp_details[:context]}
      &auth=#{pp_details[:auth]}
      &form_charset=UTF-8
      &flow_name=xpt/Checkout/ec/Login
      &external_remember_me_read_cookie_ids=
      &fso_enabled=26
      &view_requested=MiniPage
    PAYLOAD
    return payload += "&continue=Pay Now&pageTitle=Review your information"
  end
end
