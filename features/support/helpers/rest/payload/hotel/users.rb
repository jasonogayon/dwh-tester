# Hotel Extranet Users Payload
module HotelExtranetUsersPayload
  def build_user_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      user_id=#{data[:id]}
      &user_name=manager.#{data[:property_id]}.#{data[:name]}
      &user_email=#{data[:email]}
      &role_id=2
    PAYLOAD
    payload += "&user_full_name=#{data[:name]}"
    return payload
  end

  def build_remove_user_payload(user_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      user_id=#{user_id}
    PAYLOAD
  end
end
