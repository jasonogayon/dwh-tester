# Hotel Extranet Policies Payload
module HotelExtranetPoliciesPayload
  def build_edit_policies_payload(rateplan)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{rateplan[:id]}
      &data={
        \"child_not_allowed\":#{rateplan[:details][:child_not_allowed]},
        \"child_policy_age\":#{rateplan[:details][:child_policy_age]},
        \"child_policy_charge\":\"#{rateplan[:details][:child_policy_charge]}\",
        \"child_policy_desc\":\"\",
        \"payment_type\":#{rateplan[:details][:payment_type]},
        \"prepayment_charge_type\":#{rateplan[:details][:prepayment_charge_type]},
        \"prepayment_refundable\":#{rateplan[:details][:prepayment_refundable]},
        \"payment_policy_desc\":\"\",
        \"modify_charge_type\":#{rateplan[:details][:modify_charge_type]},
        \"modify_effectivity_number\":#{rateplan[:details][:modify_effectivity_number]},
        \"modify_effectivity_unit\":#{rateplan[:details][:modify_effectivity_unit]},
        \"modify_desc\":\"\",
        \"late_charge_type\":#{rateplan[:details][:late_charge_type]},
        \"late_effectivity_number\":#{rateplan[:details][:late_effectivity_number]},
        \"late_effectivity_unit\":#{rateplan[:details][:late_effectivity_unit]},
        \"late_desc\":\"\",
        \"no_show_charge_type\":#{rateplan[:details][:no_show_charge_type]},
        \"no_show_desc\":\"\",
        \"no_cc_booking_flag\":\"f\",
        \"same_day_flag\":\"t\",
        \"next_day_flag\":\"t\",
        \"id\":#{rateplan[:policies]['id']},
        \"has_special_period_policy\":false
      }
    PAYLOAD
  end
end
