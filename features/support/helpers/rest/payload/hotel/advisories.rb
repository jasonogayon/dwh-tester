# Hotel Extranet Advisories Payload
module HotelExtranetAdvisoriesPayload
  def build_advisory_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      advisory_cd=#{data[:id]}
      &start_date=#{get_date('TODAY')[0]}
      &end_date=
    PAYLOAD
    payload += "&advisory_name=#{data[:name]}"
    payload += "&advisory_detail=#{data[:description]}"
    payload += data[:rolling].nil? ? "&end_date=#{get_date('500 DAYS FROM NOW')[0]}" : "&disable_end_date=1"
    return payload
  end

  def build_remove_advisory_payload(advisory_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      advisory_cd=#{advisory_id}
    PAYLOAD
  end

  def build_no_credit_card_payload(property, no_cc_type=nil)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      property_cd=#{property[:id]}&check_in_time=56&check_out_time=48
    PAYLOAD
    unless no_cc_type.nil?
      payload += "&same_day_flag=1" if no_cc_type.include?("same")
      payload += "&same_day_flag=1&next_day_flag=1" if no_cc_type.include?("next")
      payload += "&all_time_he_flag=1" if no_cc_type.include?("all-time")
    end
    return payload
  end
end
