# Hotel Extranet Booking Conditions Payload
module HotelExtranetBookingConditionsPayload
  def build_edit_promocode_payload(rateplan, promo_code)
    promocode = promo_code.nil? ? "null" : "\"#{promo_code}\""
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{rateplan[:id]}
      &data={
        \"start_date\":\"#{rateplan[:details][:bc_start_date]}\",
        \"end_date\":\"#{rateplan[:details][:bc_end_date]}\",
        \"promo_code\":#{promocode},
        \"all\":1,
        \"mon\":1,
        \"tue\":1,
        \"wed\":1,
        \"thu\":1,
        \"fri\":1,
        \"sat\":1,
        \"sun\":1,
        \"promo_type\":null,
        \"booking_period\":null,
        \"min_nights\":null,
        \"max_nights\":null,
        \"min_lead_time\":null,
        \"max_lead_time\":null,
        \"book_now_stay_later\":null,
        \"dynamic_pricing\":null
      }
    PAYLOAD
  end
end
