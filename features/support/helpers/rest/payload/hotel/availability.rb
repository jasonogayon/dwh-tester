# Hotel Extranet Availability Payload
module HotelExtranetAvailabilityPayload
  def build_availability_payload(room_id, start_date, end_date, availability)
    inventory = ""
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      inventory += "{\"#{(Date.parse(start_date) + i).to_s.gsub('-', '')}\":#{availability}}," \
    end
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      data=
      [{
        \"room_id\":\"#{room_id}\",
        \"ischannelled\":\"0\",
        \"availability\":[#{inventory}]
      }]
    PAYLOAD
    return payload = payload.gsub(",]", "]")
  end

  def build_view_availability_payload(start_date, end_date)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      start_date=#{start_date}
      &end_date=#{end_date}
    PAYLOAD
  end
end
