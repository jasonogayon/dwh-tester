# Hotel Extranet Reservation Payload
module HotelExtranetReservationPayload
  def build_rsvsn_id_payload(reservation_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{reservation_id}
    PAYLOAD
  end

  def build_rsvsn_details_payload(hotel_id, reservation_id)
    payload = build_rsvsn_id_payload(reservation_id)
    payload += "&property_id=#{hotel_id}"
    return payload
  end

  def build_request_to_cancel_payload(reservation_id, remark)
    payload = build_rsvsn_id_payload(reservation_id)
    payload += "&reason_to_cancel=Insufficient funds&remarks=#{remark}"
    return payload
  end

  def build_request_for_alternate_cc_payload(reservation_id, deadline)
    payload = build_rsvsn_id_payload(reservation_id)
    payload += "&deadline_date=#{deadline}T00:00:00"
    return payload
  end

  def build_update_cc_payload(property)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      uniquekey=#{property[:reservation][:auth_key]}
      &mode=modify
      &id=#{property[:reservation][:reservation_id]}
      &authorization_key=#{property[:reservation][:reservation_hash]}
      &property_id=#{property[:id]}
      &payment[cc_type]=#{property[:reservation][:cc_type]}
      &payment[cc_number]=#{property[:reservation][:cc_number]}
      &payment[cc_exp_month]=#{property[:reservation][:cc_exp_month]}
      &payment[cc_exp_year]=#{property[:reservation][:cc_exp_year]}
      &payment[cc_cvv]=#{property[:reservation][:cc_cvv]}
      &payment[radio_question]=#{property[:reservation][:payment_agree]}
      &ab=#{property[:reservation][:ab_test]}
      &proc_payment=#{property[:reservation][:process_payment]}
      &language=#{property[:reservation][:language]}
      &lang=#{property[:reservation][:language]}
      &payment[agree]=on
    PAYLOAD
    payload += "&payment[first_name]=#{property[:reservation][:first_name]}"
    payload += "&payment[last_name]=#{property[:reservation][:last_name]}"
    payload += "&payment[cc_name]=#{property[:reservation][:cc_name]}"
    return payload
  end
end
