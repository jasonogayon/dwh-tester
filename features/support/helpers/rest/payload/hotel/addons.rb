# Hotel Extranet Add-ons Payload
module HotelExtranetAddonsPayload
  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../../", File.dirname(__FILE__))

  def build_addon_payload(data)
    description = data[:description].nil? ? "Test add-on description" : data[:description]
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_addon_airport_photo) if data[:name].downcase.include? "airport"
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_addon_insurance_photo) if data[:name].downcase.include? "insurance"
    photo ||= File.join(FILE_ABSOLUTE_PATH, FigNewton.image_addon_spa_photo)
    Hash[
      multipart:      true,
      file_name:      File.new(photo, "rb"),
      id:             data[:id],
      name:           data[:name],
      description:    description,
      rate:           data[:price],
      lead_time:      data[:lead_time].to_i ||= 0
    ]
  end

  def build_modify_addon_payload(property)
    addons = property[:public_rateplans][0][:details][:addons].scan(/add_on_id\":(.*?),/)
    addon = ""
    addons.each do |add_on|
      addon = addon + "{" \
          "\"id\":#{add_on[0].to_i}," \
          "\"quantity\":#{rand(20)}," \
          "\"selection_rates\":[]" \
      "},"
    end
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        property_id=#{property[:id]}
        &reservation_id=#{property[:reservation][:reservation_id]}
        &add_ons=[#{addon}]
    PAYLOAD
    return payload.gsub(",]", "]")
  end

  def build_remove_addon_payload(addon_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
        id=#{addon_id}
    PAYLOAD
  end

  def build_remove_addon_from_rateplan_payload(property)
    rateplan = property[:public_rateplans][0]
    return <<-PAYLOAD.gsub(/\s+/, "").strip
        id=#{rateplan[:id]}
        &data={\"add_ons\":[],\"inclusions\":[]}
    PAYLOAD
  end
end
