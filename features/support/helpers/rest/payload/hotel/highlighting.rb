# Hotel Extranet Highlighting Payload
module HotelExtranetHighlightingPayload
  def build_enable_highlight_payload(rateplan)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      rate_plan_id=#{rateplan}
      &flag=NEW&old_rp_id=
    PAYLOAD
  end

  def build_update_highlight_payload(new_rateplan, old_rateplan)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      rate_plan_id=#{new_rateplan}
      &flag=UPDATE&old_rp_id=#{old_rateplan}
    PAYLOAD
  end

  def build_disable_highlight_payload(rateplan)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      rate_plan_id=#{rateplan}
    PAYLOAD
  end
end
