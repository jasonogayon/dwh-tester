# Hotel Extranet Login Payload
module HotelExtranetLoginPayload
  def build_login_payload(username, password)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      user_name=#{username}
      &user_pwd=#{password}
    PAYLOAD
  end

  def build_login_from_cp_payload(hotel_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{hotel_id}
    PAYLOAD
  end
end
