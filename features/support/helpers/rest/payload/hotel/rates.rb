# Hotel Extranet Rates Payload
module HotelExtranetRatesPayload
  def build_rates_payload(rateplan_id, room_id, start_date, end_date, rate)
    rates = ""
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      rates += "{\"#{(Date.parse(start_date) + i).to_s.gsub('-', '')}\":#{rate}}," \
    end
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      data=
      [{
        \"rate_plan_id\":#{rateplan_id},
        \"room_id\":#{room_id},
        \"occupancy\":null,
        \"ischannelled\":0,
        \"rates\":[#{rates}]
      }]
    PAYLOAD
    return payload.gsub(",]", "]")
  end

  def build_view_rates_payload(start_date, end_date)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      start_date=#{start_date}
      &end_date=#{end_date}
      &get_parent_rate_plans_only=1
      &siteminder_flag=0
    PAYLOAD
  end

  def build_extract_rates_payload(rateplan_id, start_date, end_date)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      start_date=#{start_date}
      &end_date=#{end_date}
      &rate_plan_id=#{rateplan_id}
    PAYLOAD
  end
end
