# Hotel Extranet Facilities Payload
module HotelExtranetFaciltiiesPayload
  def build_facility_payload(data)
    return "facility_name=#{data[:name]}"
  end

  def build_remove_facility_payload(facility_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
        facility_id=#{facility_id}
    PAYLOAD
  end
end
