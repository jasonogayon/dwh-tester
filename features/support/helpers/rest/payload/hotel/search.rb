# Hotel Extranet Search Payload
module HotelExtranetSearchPayload
  def build_search_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=check_in_date
      &dir=ASC
      &filter[0][field]=confirmation_no
      &filter[0][data][type]=numeric
      &filter[0][data][comparison]=eq
      &filter[0][data][value]=#{property[:reservation][:confirmation_details][:confirmation_no]}
      &property_id=#{property[:id]}
      &start=0
      &limit=1
    PAYLOAD
  end

  def build_filter_nocc_bookings(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=created_date
      &dir=DESC
      &filter[0][field]=status_id
      &filter[0][data][type]=list
      &filter[0][data][value]=2
      &filter[1][field]=no_cc_booking
      &filter[1][data][type]=boolean
      &filter[1][data][value]=true
      &filter[2][field]=created_date
      &filter[2][data][type]=date
      &filter[2][data][comparison]=ge
      &filter[2][data][value]=#{get_date('TODAY')[4]}
      &property_id=#{property[:id]}
      &start=0
      &limit=1
    PAYLOAD
  end
end
