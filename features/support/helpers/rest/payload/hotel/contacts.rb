# Hotel Extranet Contacts Payload
module HotelExtranetContactsPayload
  def build_contact_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      contact_cd=#{data[:id]}
      &telephone=#{data[:phone]}
      &email=#{data[:email]}
      &role_cd=#{data[:department]}
      &im_type_cd=#{data[:messenger_type]}
      &im_id=#{data[:messenger_id]}
      &report_recipient_flag=#{data[:is_recipient]}
    PAYLOAD
    payload += "&contact_name=#{data[:name]}"
    payload += "&designation=#{data[:designation]}"
    return payload
  end

  def build_remove_contact_payload(contact_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      contact_cd=#{contact_id}
    PAYLOAD
  end
end
