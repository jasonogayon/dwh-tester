# Hotel Extranet Amenities Payload
module HotelExtranetAmenitiesPayload
  def build_amenity_payload(data)
    return "amenity_name=#{data[:name]}"
  end

  def build_remove_amenity_payload(amenity_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      amenity_id=#{amenity_id}
    PAYLOAD
  end
end
