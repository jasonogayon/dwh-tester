# Hotel Extranet Private Rate Plans Payload
module HotelExtranetPrivateRatePlansPayload
  def build_private_rateplan_payload(rateplan)
    unless FigNewton.environment == "local"
      inclusions = "{ \"inclusion_id\":46335, \"description\":\"sample only\", \"sort_no\":0 },
        { \"inclusion_id\":46336, \"description\":null, \"sort_no\":1 },
        { \"inclusion_id\":46337, \"description\":null, \"sort_no\":2 }"
    end
    return <<-PAYLOAD.strip
      policies=
        {
          \"child_not_allowed\":#{rateplan[:child_not_allowed]},
          \"child_policy_age\":\"#{rateplan[:child_policy_age]}\",
          \"child_policy_charge\":\"#{rateplan[:child_policy_charge]}\",
          \"child_policy_desc\":\"\",
          \"prepayment_charge_type\":#{rateplan[:prepayment_charge_type]},
          \"prepayment_refundable\":#{rateplan[:prepayment_refundable]},
          \"modify_charge_type\":#{rateplan[:modify_charge_type]},
          \"modify_effectivity_number\":#{rateplan[:modify_effectivity_number]},
          \"modify_effectivity_unit\":#{rateplan[:modify_effectivity_unit]},
          \"modify_desc\":\"\",
          \"late_charge_type\":#{rateplan[:late_charge_type]},
          \"late_effectivity_number\":#{rateplan[:late_effectivity_number]},
          \"late_effectivity_unit\":#{rateplan[:late_effectivity_unit]},
          \"late_desc\":\"\",
          \"no_show_charge_type\":#{rateplan[:no_show_charge_type]},
          \"no_show_desc\":\"\"
        }
      &inclusions=[#{inclusions}]
      &rates=
        [
          {
            \"room_id\":\"#{rateplan[:room_id]}\",
            \"occupancy\":0,
            \"pricing\":
            {
              \"all\":#{rateplan[:bc_all]},
              \"mon\":#{rateplan[:bc_mon]},
              \"tue\":#{rateplan[:bc_tue]},
              \"wed\":#{rateplan[:bc_wed]},
              \"thur\":#{rateplan[:bc_thu]},
              \"fri\":#{rateplan[:bc_fri]},
              \"sat\":#{rateplan[:bc_sat]},
              \"sun\":#{rateplan[:bc_sun]}
            }
          }
        ]
      &full_payment_flag=#{rateplan[:full_payment_flag]}
      &first_check_in_date=#{rateplan[:bc_start_date]}
      &last_check_out_date=#{rateplan[:bc_end_date]}
      &rate_plan=
        {
          \"type_id\":1,
          \"breakfast_id\":#{rateplan[:rp_breakfast_id]},
          \"meal_inclusion\":\"#{rateplan[:rp_meal_inclusion_name]}\",
          \"rate_plan_name\":\"#{rateplan[:rp_name]}\",
          \"description\":\"#{rateplan[:rp_description]}\",
          \"min_nights\":#{rateplan[:rp_min_nights]},
          \"payment_type\":#{rateplan[:payment_type]},
          \"chkn_mon_flag\":1,
          \"chkn_tue_flag\":1,
          \"chkn_wed_flag\":1,
          \"chkn_thu_flag\":1,
          \"chkn_fri_flag\":1,
          \"chkn_sat_flag\":1,
          \"chkn_sun_flag\":1,
          \"payment_scheme_id\":#{rateplan[:payment_scheme]}
        }
    PAYLOAD
  end

  def build_delete_private_rateplan_payload(rateplan_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      private_rate_plan_id=#{rateplan_id}
    PAYLOAD
  end
end
