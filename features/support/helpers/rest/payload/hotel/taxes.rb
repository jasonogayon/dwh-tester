# Hotel Extranet Taxes Payload
module HotelExtranetTaxesPayload
  def build_tax_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        tax_surcharge_cd=#{data[:id]}
        &type_id=#{data[:tax_type]}
        &taxable_flag=#{data[:is_taxable]}
        &charge_type_id=1
        &charge_amount=#{data[:percent]}
    PAYLOAD
    payload += "&tax_surcharge_name=#{data[:name]}"
    return payload
  end

  def build_remove_tax_payload(tax_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      tax_surcharge_cd=#{tax_id}
    PAYLOAD
  end
end
