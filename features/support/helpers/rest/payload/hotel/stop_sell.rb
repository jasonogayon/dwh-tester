# Hotel Extranet Stop Sell Payload
module HotelExtranetStopSellPayload
  def build_stopsell_payload(rateplan_id, room_id, start_date, end_date, stopsell_status)
    rates = ""
    for i in 0..(Date.parse(end_date) - Date.parse(start_date)).to_i
      rates += "{\"#{(Date.parse(start_date) + i).to_s.gsub('-', '')}stopsell\":#{stopsell_status}}," \
    end
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      data=
      [{
        \"rate_plan_id\":#{rateplan_id},
        \"room_id\":#{room_id},
        \"occupancy\":null,
        \"ischannelled\":0,
        \"rates\":[#{rates}]
      }]
    PAYLOAD
    return payload.gsub(",]", "]")
  end

  def build_view_stopsell_payload(start_date, end_date)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
        start_date=#{start_date}
        &end_date=#{end_date}
        &get_parent_rate_plans_only=1
        &siteminder_flag=0
    PAYLOAD
  end
end
