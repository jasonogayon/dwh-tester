# Hotel Extranet Rooms Payload
module HotelExtranetRoomsPayload
  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../../", File.dirname(__FILE__))

  def build_room_edit_payload(room_id, data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        room_flag=0
        &base_room_type_cd=
        &view_cd=
        &bed_config_cd=
        &room_cd=#{room_id}
        &room_size=
        &room_size_unit_cd=
        &max_adult=#{data[:adult]}
        &max_child=#{data[:child]}
        &number_rooms=#{data[:inventory]}
    PAYLOAD
    payload += "&room_name=#{data[:name]}"
    payload += "&description=#{data[:description]}"
    return payload
  end

  def build_room_payload(data)
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_room_occupancy_photo) if data[:name].downcase.include? "occupancy"
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_room_superior_photo) if data[:name].downcase.include? "superior"
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_room_studio_photo) if data[:name].downcase.include? "studio"
    photo ||= File.join(FILE_ABSOLUTE_PATH, FigNewton.image_room_accommodation_photo)
    return Hash[
      multipart:                true,
      photo:                    File.new(photo, "rb"),
      room_flag:                0,
      room_name:                data[:name],
      max_adult:                data[:adult],
      max_child:                data[:child],
      number_rooms:             data[:inventory],
      online_availability:      data[:availability],
      description:              data[:description].nil? ? "Test room description" : data[:description],
      set_amenities_aftersave:  "on"
    ]
  end

  def build_remove_room_payload(room_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      room_cd=#{room_id}
      &ischannelled=0
    PAYLOAD
  end

  def build_add_room_to_rateplan_payload(room_id, rateplan_id)
    start_date = get_date("TODAY")[0]
    end_date = get_date("100 DAYS FROM NOW")[0]
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      rates=[{
        \"room_id\":\"#{room_id}\",
        \"occupancy\":0,
        \"ischannelled\":\"0\",
        \"max_nights_status\":\"0\",
        \"max_nights_value\":\"500\",
        \"pricing\":{
            \"all\":100,
            \"mon\":100,
            \"tue\":100,
            \"wed\":100,
            \"thur\":100,
            \"fri\":100,
            \"sat\":100,
            \"sun\":100
        }
      }]
      &isPrivateRatePlan=false
      &plan={
        \"rate_plan_id\":#{rateplan_id}
      }
      &valid_from=#{start_date}
      &valid_to=#{end_date}
      &fourteen_month_rolling=0
    PAYLOAD
  end

  def build_remove_room_from_rateplan_payload(room_id, rateplan_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      rateplan_id=#{rateplan_id}
      &room_id=#{room_id}
      &occupancy=
      &ischannelled=0
    PAYLOAD
  end

  def build_add_room_to_child_rateplan_via_calendar_payload(rateplan_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{rateplan_id}
      &exclude_rooms_from_parent=true
    PAYLOAD
  end
end
