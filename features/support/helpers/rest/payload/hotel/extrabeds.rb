# Hotel Extranet Extra Beds Payload
module HotelExtranetExtraBedsPayload
  def build_extrabed_payload(hotel_id, data)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{hotel_id}
      &extra_bed_wo_breakfast_rate=#{data[:without_breakfast_rate]}
      &extra_bed_w_breakfast_rate=#{data[:with_breakfast_rate]}
      &roomtypes[]={\"id\":\"#{data[:room_standard]}\",\"name\":\"Standard Room\",\"max_extra_bed\":\"2\"}
      &roomtypes[]={\"id\":\"#{data[:room_deluxe]}\",\"name\":\"Deluxe Room\",\"max_extra_bed\":\"3\"}
    PAYLOAD
  end

  def build_remove_extrabed_from_rateplan_payload(property)
    rateplan = property[:public_rateplans][0]
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{rateplan[:id]}
      &data={
        \"extra_bed_info\":[{\"id\":#{rateplan[:details][:room_id]}}],
        \"extra_bed_rp_info\":{\"with_breakfast\":0,\"rooms_enabled\":[]}
      }
    PAYLOAD
  end
end
