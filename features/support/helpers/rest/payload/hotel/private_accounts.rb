# Hotel Extranet Private Accounts Payload
module HotelExtranetPrivateAccountsPayload
  def build_privateaccount_payload(data)
    return <<-PAYLOAD.strip
      account_codes={
        \"account_code_details\":
        [{
          \"account_code\":\"#{data[:code]}\",
          \"description\":\"#{data[:name]}\",
          \"contact_person\":\"#{data[:contact]}\",
          \"contact_email\":\"#{data[:email]}\"
        }]
      }
      &details={
        \"account_name\":\"#{data[:name]}\",
        \"contact_person\":\"#{data[:contact]}\",
        \"contact_email\":\"#{data[:email]}\",
        \"contact_primary_number\":\"#{data[:phone]}\",
        \"contract_start\":\"#{get_date('TODAY')[0]}T00:00:00\",
        \"contract_end\":\"#{get_date('500 DAYS FROM NOW')[0]}T00:00:00\",
        \"receive_email\":\"false\",
        \"charge_to_account\":\"#{data[:charge_to_account]}\",
        \"charge_to_individual\":\"#{data[:charge_to_individual]}\",
        \"status\":#{data[:status]},
        \"account_type\":#{data[:account_type]}
      }
      &rate_plans={
        \"rate_plan_ids\":[],
        \"rate_plan_names\":[]
      }
    PAYLOAD
  end

  def build_privateaccount_edit_payload(data)
    return <<-PAYLOAD.strip
      account_codes={
        \"account_code_details\":
        [{
          \"id\":,
          \"account_code\":\"#{data[:code]}\",
          \"description\":\"#{data[:name]}\",
          \"contact_person\":\"#{data[:contact]}\",
          \"contact_email\":\"#{data[:email]}\",
          \"data_status\":\"old\"
        }]
      }
      &details={
        \"id\":\"#{data[:id]}\",
        \"account_name\":\"#{data[:name]}\",
        \"contact_person\":\"#{data[:contact]}\",
        \"contact_email\":\"#{data[:email]}\",
        \"contact_primary_number\":\"#{data[:phone]}\",
        \"contract_start\":\"#{get_date('TODAY')[0]}T00:00:00\",
        \"contract_end\":\"#{get_date('500 DAYS FROM NOW')[0]}T00:00:00\",
        \"receive_email\":\"false\",
        \"charge_to_account\":\"#{data[:charge_to_account]}\",
        \"charge_to_individual\":\"#{data[:charge_to_individual]}\",
        \"account_type\":#{data[:account_type]}
      }
    PAYLOAD
  end

  def build_remove_privateaccount_payload(privateaccount_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      private_acccount_id=#{privateaccount_id}
    PAYLOAD
  end

  def build_privateaccount_rateplan_payload(rateplan_id, privateaccount_id)
    return <<-PAYLOAD.strip
      rate_plans={
        \"new_rate_plan_ids\":[#{rateplan_id}],
        \"old_rate_plan_ids\":[],
        \"bound_rate_plan\":[\"\"],
        \"unbound_rate_plan\":[],
        \"action_id\":true
      }
      &account_detail={
        \"account_id\":#{privateaccount_id},
        \"name\":\"\"
      }
      &has_private_rate_plan=true
  PAYLOAD
  end

  def build_get_privateaccount_info_payload(privateaccount_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{privateaccount_id}&is_have_accnt_code=true
    PAYLOAD
  end
end
