# Hotel Extranet Property Information Payload
module HotelExtranetPropertyInformationPayload
  def build_propertyinfo_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_email[]=#{data[:email]}
      &type_cd=4
      &timezone_cd=
      &telephone=#{data[:phone]}
      &email=#{data[:email]}
      &currency_cd=
      &reservation_email_textfield=
      &longitude=#{data[:longitude]}
      &latitude=#{data[:latitude]}
    PAYLOAD
    payload += "&address=#{data[:address]}"
    return payload
  end
end
