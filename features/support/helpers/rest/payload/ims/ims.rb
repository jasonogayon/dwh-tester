# IMS Payload
module IMSPayload
  def build_ims_request(request_type, ims, key=nil)
    case request_type
    when /login/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "login",
          "membership_number": "#{ims[:member_no]}",
          "password": "#{ims[:password]}"
        }
      PAYLOAD
    when /logoff|logout/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "logoff",
          "key": "#{key}"
        }
      PAYLOAD
    when /reset password|password reset/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "resetpw",
          "membership_number": "#{ims[:member_no]}",
          "birthday_month": "#{ims[:birth_month]}",
          "birthday_day": "#{ims[:birth_day]}"
        }
      PAYLOAD
    when /retrieve session/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "validsession",
          "key": "#{key}"
        }
      PAYLOAD
    when /retrieve voucher/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "membervoucherlist",
          "key": "#{key}"
        }
      PAYLOAD
    when /link facebook|link linkedin/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "linkmember",
          "edm_link_code": "#{ims[:edm]}",
          "membership_number": "#{ims[:member_no]}",
          "membership_ucode": "#{ims[:member_ucode]}",
          "origin_site": "#{ims[:origin_site]}",
          "signin": {
            "login_type": "#{ims[:login_type]}",
            "id": "#{ims[:id]}"
          }
        }
      PAYLOAD
    when /signin via facebook|signin via linkedin/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "signin",
          "system_key": "#{ims[:system_key]}",
          "membership_number": "#{ims[:member_no]}",
          "origin_site": "#{ims[:origin_site]}",
          "login_type": "#{ims[:login_type]}",
          "id": "#{ims[:id]}"
        }
      PAYLOAD
    when /signup/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "signup",
          "name": {
            "salutation": "Mr/Mrs",
            "firstname": "#{ims[:firstname]}",
            "surname": "#{ims[:surname]}",
            "name_on_card": "#{ims[:firstname]} #{ims[:surname]}"
          },
          "gender": "#{ims[:gender]}",
          "mobile_phone": "#{ims[:mobile]}",
          "home_phone": "#{ims[:home]}",
          "biz_phone": "#{ims[:business]}",
          "email": "#{ims[:email]}",
          "birthday": {
            "month": "#{ims[:birth_month]}",
            "day": "#{ims[:birth_day]}"
          },
          "wedding_anniversary": {
            "year": "#{ims[:wedding_year]}",
            "month": "#{ims[:wedding_month]}",
            "day": "#{ims[:wedding_day]}",
            "spouse_name": "#{ims[:wedding_spouse]}"
          },
          "address": {
            "line1": "#{ims[:address_line1]}",
            "line2": "#{ims[:address_line2]}",
            "postcode": "#{ims[:address_postcode]}",
            "city": "#{ims[:address_city]}",
            "state": "#{ims[:address_state]}",
            "country": "#{ims[:address_country]}",
            "address_type": "#{ims[:address_type]}"
          },
          "company": {
            "company_name": "#{ims[:company_name]}",
            "position": "#{ims[:company_position]}"
          },
          "membership_type": "#{ims[:membership_type]}",
          "origin_site": "#{ims[:origin_site]}",
          "signin": {
            "login_type": "#{ims[:login_type]}",
            "id": "#{ims[:id]}"
          }
        }
      PAYLOAD
    when /change password/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "changepw",
          "key": "#{key}",
          "newpw": "#{ims[:new_password]}"
        }
      PAYLOAD
    when /points history/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "pointshistory_member",
          "key": "#{key}"
        }
      PAYLOAD
    when /update member details/
      payload = <<-PAYLOAD.gsub(/\s+/, "").strip
        json={
          "action": "update_mem_details",
          "key": "#{key}",
          "mobile_phone": "#{ims[:mobile]}",
          "home_phone": "#{ims[:home]}",
          "biz_phone": "#{ims[:business]}",
          "address": {
            "line1": "#{ims[:address_line1]}",
            "line2": "#{ims[:address_line2]}",
            "postcode": "#{ims[:address_postcode]}",
            "city": "#{ims[:address_city]}",
            "state": "#{ims[:address_state]}",
            "country": "#{ims[:address_country]}",
            "address_type": "#{ims[:address_type]}"
          }
        }
      PAYLOAD
    end
    return payload
  end

  def get_ims_info(request_type, social_id=nil)
    info = Hash[
      member_no: 6303000000000907,
      password: "RZAJ7958",
      birth_month: 01,
      birth_day: 24,
      edm: "eighhwo435gns3rtho9w43tjgss",
      member_ucode: '180401131IWHV04H',
      origin_site: "bellevue",
      login_type: "password",
      id: social_id,
      system_key: "Egwiletuhjsretriu44$2i834jgserluserusrghjswlsee2@wseww",
      firstname: "DWH",
      surname: "Cukes",
      gender: "M",
      mobile: (SecureRandom.random_number * 10000000).floor + 1,
      home: nil,
      business: nil,
      email: Faker::Internet.email,
      wedding_year: 1984,
      wedding_month: 01,
      wedding_day: 24,
      wedding_spouse: "",
      address_line1: "",
      address_line2: "",
      address_postcode: "",
      address_city: "",
      address_state: "",
      address_country: "",
      address_type: "",
      company_name: "DirectWithHotels",
      company_position: "Software Tester",
      membership_type: "DJ182XJL", # "0EBKJ3QW", #  Platinum: RBZDEQCS (With Suite Upgrade voucher) | Gold: 0EBKJ3QW (Without voucher)
      new_password: "DWHpassw0rd"
    ]
    info.update(Hash[member_no: 6303000000000845]) if request_type.include? "reset"
    info.update(Hash[member_no: 6303000000000844]) if request_type.include? "link"
    info.update(Hash[member_no: 6303000000000844]) if request_type.include? "signin"
    info.update(Hash[member_no: 6303000000001097, password: "DWHpassw0rd"]) if request_type.include? "change"
    info.update(Hash[login_type: "facebook"]) if request_type.include? "facebook"
    info.update(Hash[login_type: "linkedin"]) if request_type.include? "linkedin"
    info.update(Hash[member_no: nil]) if request_type.include? "missing member number"
    info.update(Hash[password: "RZA"]) if request_type.include? "password too short"
    info.update(Hash[member_no: 1234]) if request_type.include? "incorrect member number"
    info.update(Hash[password: "AUXT8831"]) if request_type.include? "incorrect password"
    info.update(Hash[new_password: "dwhpassword"]) if request_type.include? "incorrect change password"
    info.update(Hash[new_password: "dwhpass"]) if request_type.include? "short change password"
    info.update(Hash[new_password: "dwhpasswordtoolong"]) if request_type.include? "too long change password"
    info.update(Hash[birth_day: 01]) if request_type.include? "incorrect birthday"
    info.update(Hash[birth_day: "TEST"]) if request_type.include? "invalid birthday"
    info.update(Hash[edm: "TEST"]) if request_type.include? "incorrect edm code"
    info.update(Hash[member_ucode: "180401131IWHV04I"]) if request_type.include? "incorrect member ucode"
    info.update(Hash[login_type: nil]) if request_type.include? "missing type"
    info.update(Hash[login_type: "TEST"]) if request_type.include? "incorrect type"
    info.update(Hash[id: nil]) if request_type.include? "missing id"
    info.update(Hash[firstname: nil, surname: nil]) if request_type.include? "missing name"
    info.update(Hash[email: nil]) if request_type.include? "missing email"
    info.update(Hash[email: "TEST"]) if request_type.include? "incorrect email"
    info.update(Hash[email: "dwhcukes@gmail.com"]) if request_type.include? "existing email"
    info.update(Hash[mobile: "!@#$%^&*()_"]) if request_type.include? "incorrect mobile"
    info.update(Hash[mobile: "09084520105"]) if request_type.include? "existing mobile"
    info.update(Hash[wedding_month: "TEST"]) if request_type.include? "invalid anniversary"
    info.update(Hash[membership_type: "TEST"]) if request_type.include? "incorrect membership type"
    return info
  end

end
