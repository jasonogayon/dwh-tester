# Control Panel Read Property Payload
module ControlPanelReadPropertyPayload
  def build_read_property_payload(property_name)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=id
      &dir=DESC
      &filter[0][field]=org_name
      &filter[0][data][type]=string
      &start=0
      &view=all
      &limit=1
    PAYLOAD
    return payload += "&filter[0][data][value]=#{property_name}"
  end

  def build_read_property_by_id_payload(property_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=status
      &dir=ASC
      &filter[0][field]=id
      &filter[0][data][type]=string
      &filter[0][data][value]=#{property_id}
      &start=0
      &view=all
      &limit=1
    PAYLOAD
  end
end
