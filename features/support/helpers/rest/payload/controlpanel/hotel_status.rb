# Control Panel Hotel Status Payload
module ControlPanelHotelStatusPayload
  def build_update_hotel_status_payload(hotel_id, status)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{hotel_id}
      &stage_from=#{status[:stage]}
      &status_from=#{status[:from]}
      &status_click=#{status[:to]}
      &remarks=
    PAYLOAD
  end

  def get_property_status(status)
    status = status.downcase
    if status.include? "received"
      if status.include? "web services"
        return Hash[stage: 2, from: 2, to: 11]
      elsif status.include? "go live"
        return Hash[stage: 3, from: 7, to: 12]
      end
    elsif status.include? "accepted"
      if status.include? "submission"
        return Hash[stage: 2, from: 1, to: 2]
      elsif status.include? "web services"
        return Hash[stage: 3, from: 1, to: 2]
      elsif status.include? "go live"
        return Hash[stage: 4, from: 1, to: 2]
      end
    elsif status.include? "website launched"
      return Hash[stage: 3, from: 2, to: 7]
    elsif status.include? "hi training"
      return Hash[stage: 4, from: 2, to: 9]
    elsif status.include? "gi activated"
      return Hash[stage: 4, from: 9, to: 10]
    end
    return status
  end
end
