# Control Panel Reservation Payload
module ControlPanelReservationPayload
  def build_request_noshow_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{property[:reservation][:reservation_id]}
      &property_id=#{property[:id]}
    PAYLOAD
  end

  def build_accept_noshow_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{property[:reservation][:reservation_id]}
      &status_value=6
      &charge_guest=0
      &property_id=#{property[:id]}
      &guest_email=
      &hotelier_email=
    PAYLOAD
  end

  def build_mark_noshow_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{property[:reservation][:reservation_id]}
      &status_value=7
      &property_id=#{property[:id]}
    PAYLOAD
  end

  def build_waived_noshow_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{property[:reservation][:reservation_id]}
      &status_value=10
      &charge_guest=0
      &property_id=#{property[:id]}
      &guest_email=
      &hotelier_email=
    PAYLOAD
  end

  def build_invalid_noshow_payload(property)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      reservation_id=#{property[:reservation][:reservation_id]}
      &status_value=8
      &charge_guest=0
      &property_id=#{property[:id]}
      &guest_email=
      &hotelier_email=
    PAYLOAD
  end

  def build_read_reservations(start_date, end_date, status, payment_scheme, start=0, limit=1)
    status = status.downcase
    payment_scheme = payment_scheme.downcase
    status_id = "2,12" if status.include? "confirm"
    payment_scheme_id = "1" if payment_scheme.include?("dwh") && payment_scheme.include?("full")
    payment_scheme_id = "2" if payment_scheme.include?("dwh") && payment_scheme.include?("partial")
    payment_scheme_id = "3" if payment_scheme.include? "hpp"
    payment_scheme_id = "4" if payment_scheme.include? "bdo"
    payment_scheme_id = "5" if payment_scheme.include? "paypal"
    payment_scheme_id = "1,2,3,4,5" if payment_scheme.include? "all"
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=server_created_date
      &dir=DESC
      &filter[0][field]=server_created_date
      &filter[0][data][type]=date
      &filter[0][data][comparison]=le
      &filter[0][data][value]=#{start_date}
      &filter[1][field]=server_created_date
      &filter[1][data][type]=date
      &filter[1][data][comparison]=ge
      &filter[1][data][value]=#{end_date}
      &filter[2][field]=status_id
      &filter[2][data][parent_id]=0
      &filter[2][data][type]=list
      &filter[2][data][value]=#{status_id}
      &filter[3][field]=payment_scheme
      &filter[3][data][type]=list
      &filter[3][data][value]=#{payment_scheme_id}
      &start=#{start}
      &limit=#{limit}
    PAYLOAD
  end

  def build_get_recent_reservations_payload(limit)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=server_created_date
      &dir=DESC
      &start=0
      &limit=#{limit}
    PAYLOAD
  end

  def build_search_reservation_payload(confirmation_no)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=server_created_date
      &dir=DESC
      &filter[0][field]=confirmation_no
      &filter[0][data][type]=numeric
      &filter[0][data][comparison]=eq
      &filter[0][data][value]=#{confirmation_no}
      &start=0
      &limit=1
    PAYLOAD
  end
end
