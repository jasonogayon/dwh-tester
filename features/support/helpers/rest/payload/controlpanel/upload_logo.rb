# Control Panel Upload Payload
module ControlPanelUploadLogoPayload
  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../../", File.dirname(__FILE__))

  def build_upload_logo_payload(hotel_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{hotel_id}
      &gi_url=http://staging-reservations.directwithhotels.com/
      &color_scheme=
      &extra_css=
      &delete_img_logo=0
      &delete_background=0
    PAYLOAD
  end

  def build_update_hotel_image_payload(hotel_id)
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_hotel_logo)
    Hash[
      multipart:                true,
      image_logo:               File.new(photo, "rb"),
      id:                       hotel_id,
      property_id:              hotel_id,
      hotel_logo_filename:      nil,
      background_filename:      nil,
      tmp_hotel_logo_filename:  nil,
      tmp_background_filename:  nil,
      delete_img_logo:          0,
      delete_background:        0,
      img_logo_flag:            0,
      background_flag:          0,
      background:               nil,
      color_scheme_name:        nil,
      gi_extra_css:             nil
    ]
  end
end
