# Control Panel Hotel Information Payload
module ControlPanelHotelInformationPayload
  def build_update_hotel_info_payload(property)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{property[:id]}
      &image_logo=#{property[:logo]}
      &background=
      &integration_file=
      &country_id=#{property[:country_id]}
      &country_name=#{property[:country]}
      &province_name=
      &city_id=#{property[:city_id]}
      &city_name=#{property[:city]}
      &timezone_id=#{property[:timezone_id]}
      &timezone_name=#{property[:timezone_code]}
      &market_id=170
      &market=
      &group_membership=0
      &property_group_id=
      &group_name=
      &decision_maker_location_id=
      &decision_maker_location_name=
      &currency_id=#{property[:currency_id]}
      &sales_partner_id=122
      &channel_only_flag=0
      &currency_name=#{property[:currency]}
      &market_name=Test Portfolio
      &cluster_id=6
      &cluster_name=PH
      &account_owner_id=
      &account_owner_name=
      &greenie_status=
      &monthly_min_fee=
      &segment_type=
      &wm_name=#{property[:rep_name]}
      &wm_email=#{property[:rep_email]}
      &wm_tel_no=#{property[:rep_phone]}
      &travel_sites_agoda=0
      &travel_sites_bookingcom=0
      &travel_sites_expedia=0
      &travel_sites_gplaces=0
      &travel_sites_hcombined=0
      &travel_sites_splendia=0
      &travel_sites_tripadvisor=0
      &travel_sites_trivago=0
      &travel_sites_other=0
      &other_travel_sites=
      &subscription_date=02 Dec 2016
      &subscribed_by=TEST2 SPI Account2
      &referred_by=
      &paypal_currency_supported=
      &notes=(-^-)
      &pct_commission=#{property[:public_commission]}
      &private_account_enabled=#{property[:private_account]}
      &private_account_rate_commision=#{property[:private_commission]}
      &selected_payment_scheme_id=#{property[:selected_scheme_id]}
      &full_payment_flag=#{property[:full_payment]}
      &orig_full_payment_flag=0
      &orig_full_payment_activation_date=
      &orig_bancnet_payment_flag=false
      &bancnet_payment_flag=#{property[:bancnet]}
      &bancnet_payment_flag_name=#{property[:bancnet_flag]}
      &is_bancnet_candicate=#{property[:bancnet]}
      &payment_scheme_id=#{property[:payment_scheme_id]}
      &pdf_recipients_status=false
      &isRatesAccess=true
      &isLevel2=true
      &switch_to_hpp=0
      &new_payment_scheme_id=#{property[:payment_scheme_id]}
      &original_payment_scheme_id=1
      &cclist=#{property[:cc_list]}
      &bdo_cclist=1,5,6,8
      &bdo_ccname=American Express,Mastercard,JCB,VISA
      &ccname=#{property[:cc_name]}
      &pdf_recipients=
      &pdf_recipients_old=
      &payment_processor=#{property[:payment_processor]}
      &original_bdo_activation_date=
      &prev_payment_processor=
      &paypal_enabled=#{property[:paypal_enabled]}
      &paypal_onboarding=false
      &paypal_onboarding_request_date=false
      &paypal_onboarding_completion_date=false
      &underwriting_rt=false
      &underwriting_activation_date=false
      &pp_bill_agreement_id=
      &pp_bill_agreement_id2=
      &paypal_settings=#{property[:paypal_settings]}
      &orginal_segment_type=
      &ibe_desktop_subdomain=#{property[:ibe_domain_url_desktop]}
      &update_desktop_domain=#{property[:ibe_domain_url_desktop_update]}
      &ibe_mobile_subdomain=#{property[:ibe_domain_url_mobile]}
      &update_mobile_domain=#{property[:ibe_domain_url_mobile_update]}
      &no_prepay_flag=#{property[:no_prepay]}
      &ad_site=
      &ad_site_domain_expiry=
      &website=
      &domain_expiry=
      &website_new=
      &booking_engine=
      &bpg_is_custom=0
      &bpg_copy=
      &bpg_unit=1
      &bpg_number=24
      &bpg_is_update=0
      &ad_site_launch_date=
      &website_launch_date=
      &activation_date=
      &dns_server=
      &dns_user=
      &dns_pass=
      &ftp_host=
      &ftp_user=
      &ftp_pass=
      &url_dir_search=#{property[:url]}
      &gridRowValue=[object Object]
      &booking_flag=0
      &china_union_pay_flag=0
      &ppc_flag=0
      &dns_flag=0
      &ftp_flag=0
      &hosting_flag=0
      &website_flag=0
      &create_website_flag=0
      &integration_req_flag=0
      &ftp_access=0
      &use_preview_images=0
      &delete_img_logo=0
      &delete_background=0
      &img_logo_flag=0
      &background_flag=0
      &color_scheme_id=
      &color_scheme_name=
      &gi_extra_css=
      &gi_url=#{property[:url]}
      &account_name=
      &account_number=
      &bank_name=
      &bank_address=
      &swift_code=
      &iban=
      &sort_code=
      &rate_mixing_flag=#{property[:rate_mixing]}
      &update_desktop_domain=
      &update_mobile_domain=
      &zip_code=
      &hotelier_email_address=
      &hotelier_name=
      &voucher_logo_filename=
      &voucher_hotel_logo_filename=
      &group_commission=
      &voucher_amount=
      &voucher_payment_method=
      &street_address=
    PAYLOAD
    return payload += "&org_name=#{property[:property_name]}"
  end

  def build_update_hotel_ibe_domain_payload(property)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      db=dwhdb
      &query=
    PAYLOAD
    payload += "update core.m_organizations set "
    payload += "ibe_desktop_subdomain = #{property[:ibe_domain_url_desktop]} "
    unless property[:ibe_domain_url_mobile].nil?
      payload += ", ibe_mobile_subdomain = #{property[:ibe_domain_url_mobile]} "
    end
    payload += "where id = #{@property[:id]};"
    return payload
  end
end
