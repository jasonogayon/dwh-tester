# Control Panel Hotel Category Payload
module ControlPanelHotelCategoryPayload
  def build_update_hotel_category_payload(hotel_id)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      property_id=#{hotel_id}
      &category_name=Active&category_id=2
    PAYLOAD
  end

  def build_get_hotels_by_country_payload(start, limit)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      sort=status
      &dir=ASC
      &start=#{start}
      &view=rm
      &limit=#{limit}
      &filter[0][field]=status
      &filter[0][data][type]=list
      &filter[0][data][value]=2
      &filter[1][field]=category_id
      &filter[1][data][type]=list
      &filter[1][data][value]=1
    PAYLOAD
  end
end
