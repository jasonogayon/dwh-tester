# Partners Policies Payload
module PartnersPoliciesPayload
  def build_policies_payload(eform_id, data)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      eform_id=#{eform_id}
      &to_next_tab=1
      &current_tab=5
      &check_in_time=56
      &check_in_time_txt=02:00 PM
      &check_out_time=48
      &check_out_time_txt=12:00 PM
      &child_not_allowed=1
      &child_not_allowed_txt=Not Allowed
      &child_policy_age_txt=
      &child_policy_charge=
      &modify_effectivity_unit=
      &modify_effectivity_unit_txt=
      &modify_effectivity_number_txt=
      &modify_charge_type=1
      &modify_charge_type_txt=No Charge
      &late_effectivity_unit=
      &late_effectivity_number=
      &late_effectivity_unit_txt=
      &late_effectivity_number_txt=
      &late_charge_type=1
      &late_charge_type_txt=No Charge
      &no_show_charge_type=1
      &no_show_charge_type_txt=No Charge
    PAYLOAD
  end
end
