# Hotel Extranet Upload Payload
module PartnersUploadPayload
  def build_images_payload(eform_id, data)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      eform_id=#{eform_id}
      &to_next_tab=1
      &current_tab=6
      &images=[]
    PAYLOAD
  end
end
