# Partners Login Payload
module PartnersLoginPayload
  def build_login_payload(username, password)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      user_name=#{username}
      &user_pwd=#{password}
    PAYLOAD
  end
end
