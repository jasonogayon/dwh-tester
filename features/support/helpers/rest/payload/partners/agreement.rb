# Partners Agreement Payload
module PartnersAgreementPayload
  def build_subscription_agreement_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      eform_id=
      &to_next_tab=1
      &current_tab=1
      &position=#{data[:rep_position]}
      &contact_number=#{data[:rep_phone]}
      &email=#{data[:rep_email]}
      &interested=0
      &accept_terms=1
    PAYLOAD
    payload += "&hotelname=#{data[:property_name]}"
    payload += "&fullname=#{data[:rep_name]}"
    return payload
  end
end
