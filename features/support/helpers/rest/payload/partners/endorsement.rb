# Partners Endorsement Payload
module PartnersEndorsementPayload
  def build_endorsement_payload(data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      id=#{data[:spi_id]}
      &uploaded_file_title=
      &uploaded_file=
      &referred_by=
      &generic_sp_id=122
      &integrate_who=Hotel
      &ftp_host=
      &ftp_username=
      &ftp_password=
      &webmaster_contact_no=#{data[:rep_phone]}
      &webmaster_email=#{data[:rep_email]}
      &room_type_allocation=
      &special_instructions=
    PAYLOAD
    payload += "&webmaster_name=#{data[:rep_name]}"
    payload += "&generic_sp_name=TEST2 SPI Account2"
    return payload
  end
end
