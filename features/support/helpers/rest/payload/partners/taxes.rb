# Partners Taxes Payload
module PartnersTaxesPayload
  def build_taxes_payload(eform_id, data)
    return <<-PAYLOAD.gsub(/\s+/, "").strip
      eform_id=#{eform_id}
      &to_next_tab=1
      &current_tab=4
      &taxes=[]
    PAYLOAD
  end
end
