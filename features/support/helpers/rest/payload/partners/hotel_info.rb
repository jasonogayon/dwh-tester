# Partners Hotel Information Payload
module PartnersHotelInformationPayload
  def build_default_hotelinfo(hotel)
    is_dwh_processed = hotel.include?("Global") || hotel.include?("Asiapay") || hotel.include?("Bancnet")
    is_currency_philippines = hotel.include?("Bancnet") || hotel.include?("Asiapay") || hotel.include?("PHP")
    return Hash[
      hotel_type:         is_dwh_processed ? "DWH" : hotel,
      property_name:      property_name.nil? ? "Trial Hotel #{hotel}" : "#{property_name} -#{hotel}",
      is_dwh:             is_dwh_processed,
      is_asiapay:         hotel.include?("Asiapay") ? true : false,
      currency:           is_currency_philippines ? "Philippines, Pesos" : "United States of America, Dollars",
      currency_code:      is_currency_philippines ? "PHP" : "USD",
      currency_id:        is_currency_philippines ? 114 : 150,
      public_commission:  10,
      private_commission: 10,
      channel_type:       0,
      full_payment:       1,
      private_account:    1,
      rate_mixing:        hotel.include?("Mixing") ? 1 : 0,
      bancnet:            hotel.include?("Bancnet") ? true : false,
      bancnet_flag:       hotel.include?("Bancnet") ? 1 : 0,
      no_prepay:          is_dwh_processed ? 0 : 1,
      payment_processor:  hotel.include?("BDO") ? "BD" : hotel.include?("Paypal") ? "PP" : "",
      payment_scheme_id:  is_dwh_processed ? 1 : 2,
      selected_scheme_id: is_dwh_processed ? 1 : hotel.include?("BDO") ? 3 : hotel.include?("Paypal") ? 4 : 2,
      country:            "Philippines",
      country_id:         174,
      city:               "Makati City",
      city_id:            715,
      timezone:           "China Coastal",
      timezone_code:      "(+08:00) China Coastal Time",
      timezone_id:        22,
      rep_name:           "Jason Ogayon",
      rep_position:       "Software Tester",
      rep_phone:          "1234567890",
      rep_email:          "dwhseide@gmail.com",
      paypal_enabled:     hotel.include?("Paypal") ? 1 : "",
      paypal_settings:    hotel.include?("Paypal") ? FigNewton.pp_settings % { id: FigNewton.pp_merchant_id, pwd: FigNewton.pp_merchant_pwd, sig: FigNewton.pp_merchant_sig } : "",
      cc_list:            hotel.include?("HPP") ? FigNewton.hpp_cc_list : "",
      cc_name:            hotel.include?("HPP") ? FigNewton.hpp_cc_name : "",
      url:                "http://zeuspalace.com/",
      nocc_sameday:       false,
      nocc_nextday:       false,
      nocc_alltime:       false,
      property_info:      get_propertyinfo,
      amenities:          get_amenities,
      facilities:         get_facilities,
      rooms:              get_rooms,
      contacts:           get_contacts,
      addons:             get_addons,
      advisories:         get_advisories,
      extrabeds:          get_extrabeds,
      users:              get_users,
      taxes:              get_taxes,
      private_accounts:   get_private_accounts,
      public_rateplans:   get_public_rateplans(hotel, is_dwh_processed),
      private_rateplans:  get_private_rateplans(hotel, is_dwh_processed)
    ]
  end

  def get_public_rateplans(hotel, is_dwh_processed)
    rateplans = is_dwh_processed ? get_dwh_rateplans : get_hpp_rateplans
    rateplans.delete_if { |rp| rp[:prepay_policy] == "No Prepayment" } if hotel.include?("BDO") || hotel.include?("Paypal")
    rateplans.delete_if { |rp| rp[:noshow_policy] =~ /First Night|Full Charge|No/ } if hotel.include?("BDO")
    return rateplans
  end

  def get_private_rateplans(hotel, is_dwh_processed)
    rateplans = get_public_rateplans(hotel, is_dwh_processed)
    rateplans.delete_if { |rp| rp[:prepay_policy] == "Partial" } if is_dwh_processed
    rateplans.each { |rp| rp.update(Hash[type: "Private #{rp[:type]}"]) }
    return rateplans
  end

  def filter_rateplans(property, rateplans)
    rateplans.delete_if { |rp| rp[:prepay_policy] == "No Prepayment" } if property[:bdo_enabled] || property[:pp_enabled]
    rateplans.delete_if { |rp| rp[:noshow_policy] =~ /First Night|Full Charge|No/ } if property[:bdo_enabled]
    return rateplans
  end

  def build_hotelinfo_payload(eform_id, data)
    payload = <<-PAYLOAD.gsub(/\s+/, "").strip
      eform_id=#{eform_id}
      &to_next_tab=1
      &current_tab=2
      &country=174
      &city=715
      &custom_city=
      &city_txt=Makati City
      &country_txt=Philippines
      &timezone=22
      &timezone_txt=(+08:00) China Coastal Time
      &currency=150
      &currency_txt=United States of America, Dollars
      &preferred_location=
      &existing_website=0
      &website_url=
      &rate_src=0
      &online_travel_agent=
      &account_name=
      &account_number=
      &bank_name=
      &bank_address=
      &swift_code=
      &booking_email=#{data[:rep_email]}
      &copyrep-inputEl=on
      &manager_position=#{data[:rep_position]}
      &manager_landline=#{data[:rep_phone]}
      &manager_email=#{data[:rep_email]}
      &ftp_host=
      &ftp_user=
      &ftp_pass=
    PAYLOAD
    payload += "&manager_name=#{data[:rep_name]}"
    return payload
  end
end
