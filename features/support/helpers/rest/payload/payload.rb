# Payload Helper
module PayloadHelper
  # Reservation
  include ReservationProcessDatesPayload
  include ReservationPromoCodePayload
  include ReservationRoomsPayload
  include ReservationPaymentPayload
  include ReservationBDOPaymentPayload
  include ReservationAsiapayPaymentPayload
  include ReservationPaypalPaymentPayload
  include ReservationAddonsPayload
  include ReservationExtraBedsPayload
  include ReservationUploadPayload

  # Control Panel
  include ControlPanelLoginPayload
  include ControlPanelHotelStatusPayload
  include ControlPanelReadPropertyPayload
  include ControlPanelHotelInformationPayload
  include ControlPanelUploadLogoPayload
  include ControlPanelHotelCategoryPayload
  include ControlPanelReservationPayload

  # Hotel Extranet
  include HotelExtranetLoginPayload
  include HotelExtranetPropertyInformationPayload
  include HotelExtranetContactsPayload
  include HotelExtranetFaciltiiesPayload
  include HotelExtranetAmenitiesPayload
  include HotelExtranetRoomsPayload
  include HotelExtranetExtraBedsPayload
  include HotelExtranetTaxesPayload
  include HotelExtranetAdvisoriesPayload
  include HotelExtranetAddonsPayload
  include HotelExtranetUsersPayload
  include HotelExtranetPrivateAccountsPayload
  include HotelExtranetSearchPayload
  include HotelExtranetReservationPayload
  include HotelExtranetAvailabilityPayload
  include HotelExtranetRatesPayload
  include HotelExtranetStopSellPayload
  include HotelExtranetParentRatePlansPayload
  include HotelExtranetChildRatePlansPayload
  include HotelExtranetPrivateRatePlansPayload
  include HotelExtranetHighlightingPayload
  include HotelExtranetBookingConditionsPayload
  include HotelExtranetPoliciesPayload

  # Sales Partner Interface
  include PartnersLoginPayload
  include PartnersAgreementPayload
  include PartnersHotelInformationPayload
  include PartnersTaxesPayload
  include PartnersPoliciesPayload
  include PartnersUploadPayload
  include PartnersEndorsementPayload

  # Channels
  include CMSPayload
  include PMSPayload

  # Impact Marketing Solutions
  include IMSPayload
end
