# REST Requests Helper
module RestHelper
  def get(url, headers)
    response = RestClient.get(url, headers)
    return response
  rescue RestClient::ExceptionWithResponse => err
    return get_error_response(err)
  end

  def get_without_redirect(url, headers)
    response = RestClient::Request.execute(method: :get, url: url, headers: headers, max_redirects: 0)
    return response
  rescue RestClient::ExceptionWithResponse => err
    return get_error_response(err)
  end

  def post(url, payload, headers=nil)
    response = RestClient.post(url, payload, headers)
    return response
  rescue RestClient::ExceptionWithResponse => err
    return get_error_response(err)
  end

  def post_multipart(url, payload, headers)
    request = RestClient::Request.new(method: :post, url: url, payload: payload, headers: headers)
    response = request.execute
    return response
  rescue RestClient::ExceptionWithResponse => err
    return get_error_response(err)
  end

  def get_error_response(err)
    raise "Internet connection failed. Re-run tests again later." if err.response.nil?
    return Hash[
      setcookie:  err.response.headers[:set_cookie],
      location:   err.response.headers[:location],
      body:       err.response.body,
      error:      err
    ]
  end
end
