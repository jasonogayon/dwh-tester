# Default Data for Test Hotels
module DataLookup
  include NewAddons
  include NewAdvisories
  include NewAmenities
  include NewContacts
  include NewExtraBeds
  include NewFacilities
  include NewPrivateAccounts
  include NewPropertyInformation
  include NewDWHRatePlans
  include NewHPPRatePlans
  include NewRatePlanDetails
  include NewHotelReservation
  include NewRooms
  include NewTaxes
  include NewUsers
  include NewEstimatedTimeOfArrival

  include ExistingAccounts
  include ExistingAddons
  include ExistingGuestNames
  include ExistingHotels
  include ExistingRatePlans
  include ExistingRooms
end
