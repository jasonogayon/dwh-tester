# New Private Accounts
module NewPrivateAccounts
  def get_private_accounts
    return Array[
      Hash[
        id:                         nil,
        name:                       "Privados",
        code:                       "privados",
        contact:                    "Jason Ogayon",
        email:                      "dwhseide@gmail.com",
        phone:                      "09084520105",
        status:                     "null",
        charge_to_account:          false,
        charge_to_individual:       true,
        account_type:               0,
        to_edit:                    false,
        new_account_details:        Hash[],
        to_delete:                  false
      ],
      Hash[
        id:                         nil,
        name:                       "Test Private Account",
        code:                       "testprivaccount",
        contact:                    "Jason Ogayon",
        email:                      "dwhseide@gmail.com",
        phone:                      "09084520105",
        status:                     "null",
        charge_to_account:          false,
        charge_to_individual:       true,
        account_type:               0,
        to_edit:                    true,
        new_account_details:        Hash[
            name:                   "Edited Test Private Account",
            code:                   "testaccount",
            contact:                "Jason Ogayon",
            email:                  "dwhcukes@gmail.com",
            phone:                  "1234567890",
            status:                 "null",
            charge_to_account:      false,
            charge_to_individual:   true,
            account_type:           0
        ],
        to_delete:                  true
      ]
    ]
  end
end
