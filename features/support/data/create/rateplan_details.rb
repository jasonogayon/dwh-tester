# New Rate Plan Details
module NewRatePlanDetails
  def get_rateplan_data(property, rateplan, rateplan_type="Nightly")
    rateplan = update_rateplan(rateplan, rateplan_type)
    stay_nights = rateplan[:min_nights_stay].to_i + rateplan[:no_free_nights].to_i
    room = property[:rooms].select { |rm| rm[:name].nil? ? rm["room_name"] =~ /Accommodation|Standard/ : rm[:name] =~ /Accommodation|Standard/ }[0]
    room_id = room[:id] ||= room["id"]
    return Hash[
      type:                           rateplan[:type],
      name:                           rateplan[:name],
      is_published:                   rateplan_type.downcase.include?("inactive") ? 0 : 1,
      is_parent:                      rateplan[:is_parent],
      addons:                         rateplan[:with_addons] ? "{ \"add_on_id\":#{get_addon_id(property[:hotel_type], 'insurance')}, \"sort_no\":0 },{ \"add_on_id\":#{get_addon_id(property[:hotel_type], "spa")}, \"sort_no\":1 },{ \"add_on_id\":#{get_addon_id(property[:hotel_type], 'airport transfer') }, \"sort_no\":2 }" : nil,
      extrabeds:                      rateplan[:with_extrabeds] ? room_id : nil,
      promo_code:                     rateplan[:promo_code].nil? ? "null" : "\"#{rateplan[:promo_code]}\"",
      promo_type:                     get_promo_type(rateplan),
      discount_type:                  rateplan[:discount_type].nil? ? "null" : (rateplan[:discount_type].downcase.include?("percent") ? 1 : 2),
      discount_value:                 rateplan[:discount_amount] ||= "null",
      room_id:                        room_id,
      user_id:                        19_510,
      is_custom_policy:               rateplan[:inherits_policy] ? 0 : 1,
      parent_rate_plan_id:            custom_rateplan_id.nil? ? get_parent_id(property[:hotel_type], rateplan) : custom_rateplan_id,
      max_extra_nights:               rateplan[:max_extra_nights].nil? ? 500 : rateplan[:max_extra_nights],
      max_nights:                     stay_nights,
      max_nights_flag:                rateplan[:max_extra_nights].nil? ? 1 : 0,
      additional_nightly:             rateplan[:max_extra_nights].nil? ? 0 : 500,
      additional_nightly_flag:        rateplan[:max_extra_nights].nil? ? 0 : 1,
      full_payment_flag:              rateplan[:prepay_policy].downcase.include?("full") ? 1 : 0,
      channel_only_flag:              property[:channel_type],
      pct_commission:                 property[:public_commission],
      payment_scheme:                 property[:hotel_type].include?("DWH") ? 1 : 2,
      hotel_type:                     property[:hotel_type],
      rp_type:                        rateplan[:type].downcase.include?("nightly") ? 1 : (rateplan[:type].downcase.include?("fixed") ? 2 : 3),
      rp_name:                        rateplan[:name],
      rp_description:                 rateplan[:rateplan_description] ||= "Test rate plan description",
      rp_breakfast_id:                rateplan[:meal_inclusion].include?("breakfast") ? 1 : "null",
      rp_meal_inclusion_name:         "No Breakfast",
      rp_meal_inclusion_description:  rateplan[:meal_description] ||= "Test meal description",
      rp_min_nights:                  stay_nights,
      rp_free_nights:                 rateplan[:no_free_nights],
      rp_stay_nights:                 rateplan[:min_nights_stay],
      rp_channel_flag:                rateplan_type.downcase.include?("channel") ? 1 : 0,
      rp_special_flag:                rateplan_type.downcase.include?("special") ? 1 : 0,
      rp_mobile_exclusive_flag:       rateplan_type.downcase.include?("mobile") ? 1 : 0,
      bc_start_date:                  get_date(rateplan[:first_checkin])[0],
      bc_end_date:                    get_date(rateplan[:last_checkout])[0],
      bc_all:                         rateplan[:all].nil? ? 1 : 0,
      bc_mon:                         rateplan[:mon].nil? ? 1 : 0,
      bc_tue:                         rateplan[:tue].nil? ? 1 : 0,
      bc_wed:                         rateplan[:wed].nil? ? 1 : 0,
      bc_thu:                         rateplan[:thu].nil? ? 1 : 0,
      bc_fri:                         rateplan[:fri].nil? ? 1 : 0,
      bc_sat:                         rateplan[:sat].nil? ? 1 : 0,
      bc_sun:                         rateplan[:sun].nil? ? 1 : 0,
      bc_promo_type:                  rateplan[:promo_type] ||= "null",
      bc_booking_period:              rateplan[:booking_period_start] ||= "null",
      bc_booking_period_start:        rateplan[:booking_period_start] ||= "null",
      bc_booking_period_end:          rateplan[:booking_period_end] ||= "null",
      bc_min_nights:                  rateplan[:promo_min_nights] ||= stay_nights,
      bc_max_nights:                  rateplan[:promo_max_nights] ||= "null",
      bc_min_lead_time:               rateplan[:promo_min_leadtime] ||= "null",
      bc_max_lead_time:               rateplan[:promo_max_leadtime] ||= "null",
      child_not_allowed:              rateplan[:child_policy].downcase.include?("not allowed") ? 1 : 2,
      child_policy_age:               rateplan[:child_policy].downcase.include?("below") ? rateplan[:child_policy].downcase.split("below")[0].gsub(/[^0-9]/, "") : 0,
      child_policy_charge:            rateplan[:child_policy].downcase.include?("for ") ? rateplan[:child_policy].downcase.split("for ")[1] : 0,
      payment_type:                   property[:hotel_type].include?("DWH") ? (rateplan[:prepay_policy].downcase.include?("partial") ? 10 : 100) : (rateplan[:prepay_policy].downcase.include?("no prepay") ? 200 : 201),
      prepayment_charge_type:         get_prepay_charge_type(property[:hotel_type], rateplan),
      prepayment_refundable:          rateplan[:refund_policy].nil? || rateplan[:refund_policy].downcase.include?("non") ? 0 : 1,
      modify_charge_type:             get_policies_charge_type(rateplan, 0),
      modify_effectivity_number:      get_policies_effectivity_number(property[:hotel_type], rateplan, 0),
      modify_effectivity_unit:        get_policies_effectivity_unit(property[:hotel_type], rateplan, 0),
      late_charge_type:               get_policies_charge_type(rateplan, 1),
      late_effectivity_number:        get_policies_effectivity_number(property[:hotel_type], rateplan, 1),
      late_effectivity_unit:          get_policies_effectivity_unit(property[:hotel_type], rateplan, 1),
      no_show_charge_type:            get_policies_charge_type(rateplan, 2),
      daily_rate:                     property[:currency_code] == "PHP" ? 1000 : 5
    ]
  end

  private

  def update_rateplan(rateplan, rateplan_type)
    rateplan.update(Hash[
      type:                   rateplan[:type].gsub("Nightly", "Fixed Nights").gsub("Free", "Fixed"),
      name:                   "Fixed #{rateplan[:name]}".gsub("Fixed Free ", "Fixed "),
      min_nights_stay:        2,
      max_extra_nights:       3,
      no_free_nights:         0
    ]) if rateplan_type.downcase.include? "fixed"
    rateplan.update(Hash[
      type:                   rateplan[:type].gsub("Nightly", "Free Nights").gsub("Fixed", "Free"),
      name:                   "Free #{rateplan[:name]}".gsub("Free Fixed ", "Free "),
      min_nights_stay:        2,
      max_extra_nights:       nil,
      no_free_nights:         1
    ]) if rateplan_type.downcase.include? "free"
    return rateplan
  end

  def get_prepay_charge_type(hotel_type, rateplan)
    return 0 if rateplan[:prepay_policy].nil?
    policy = rateplan[:prepay_policy].downcase
    if hotel_type.include? "DWH"
      return policy.include?("partial") ? 7 : 3
    else
      return 0 if policy.include? "no prepay"
      return 2 if policy.include? "first night"
      return 3 if policy.include? "full"
      return 9
    end
  end

  def get_policies_charge_type(rateplan, policy_type)
    policy = get_policy(rateplan, policy_type)
    refund = rateplan[:refund_policy].nil? ? "" : rateplan[:refund_policy].downcase
    if policy.nil? || (policy.include?("effective immediately") && refund.include?("non"))
      return rateplan[:type].include?("Private") ? 3 : 0
    else
      return 1 if policy.include? "no penalty"
      return 2 if policy.include? "first night"
      return 3 if policy.include? "full charge"
      return 12
    end
  end

  def get_policy(rateplan, policy_type)
    return rateplan[:modification_policy].downcase if !rateplan[:modification_policy].nil? && policy_type.zero?
    return rateplan[:cancellation_policy].downcase if !rateplan[:cancellation_policy].nil? && policy_type == 1
    return rateplan[:noshow_policy].downcase if !rateplan[:noshow_policy].nil? && policy_type == 2
  end

  def get_policies_effectivity_number(hotel_type, rateplan, policy_type)
    policy = get_policy(rateplan, policy_type)
    return 0 if rateplan[:prepay_policy].nil?
    prepayment = rateplan[:prepay_policy].downcase
    refund = rateplan[:refund_policy].nil? ? "" : rateplan[:refund_policy].downcase
    if policy.nil? || (policy.include?("effective immediately") && refund.include?("non")) || (policy.include?("effective immediately") && prepayment.include?("partial") && hotel_type.include?("DWH"))
      return 0
    else
      return policy.include?("lead time") ? 3 : 20
    end
  end

  def get_policies_effectivity_unit(hotel_type, rateplan, policy_type)
    policy = get_policy(rateplan, policy_type)
    return 0 if rateplan[:prepay_policy].nil?
    prepayment = rateplan[:prepay_policy].downcase
    refund = rateplan[:refund_policy].nil? ? "" : rateplan[:refund_policy].downcase
    if policy.nil? || (policy.include?("effective immediately") && refund.include?("non")) || (policy.include?("effective immediately") && prepayment.include?("partial") && hotel_type.include?("DWH"))
      return rateplan[:type].include?("Private") ? "null" : 0
    else
      return policy.include?("not allowed") && !hotel_type.include?("DWH") ? -1 : (policy.include?("days") ? 1 : 3)
    end
  end

  def get_promo_type(rateplan)
    return "null" if rateplan[:promo_type].nil?
    promo_type = rateplan[:promo_type].downcase
    return 1 if promo_type.include? "advanced purchase"
    return 2 if promo_type.include? "last minute"
    return 3 if promo_type.include? "flash deal"
    return 4 if promo_type.include? "minimum night"
    return 5 if promo_type.include? "limited offer"
  end

  def get_parent_id(hotel_type, rateplan)
    rateplan_type = rateplan[:type]
    if hotel_type.include? "DWH"
      return FigNewton.dwh_parent_nightly if rateplan_type.include? "Nightly"
      return FigNewton.dwh_parent_fixed if rateplan_type.include? "Fixed"
      return FigNewton.dwh_parent_free if rateplan_type.include? "Free"
    elsif hotel_type.include? "HPP"
      return FigNewton.hpp_parent_nightly if rateplan_type.include? "Nightly"
      return FigNewton.hpp_parent_fixed if rateplan_type.include? "Fixed"
      return FigNewton.hpp_parent_free if rateplan_type.include? "Free"
    end
  end
end
