# New Facilities
module NewFacilities
  def get_facilities
    return Array[
      Hash[
        id:         nil,
        name:       "Parking",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Airport transfer",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Room service",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "WiFi",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Spa Features and Treatments",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Swimming Pool and Gym",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Laundry service",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Test Facility",
        to_delete:  true
      ]
    ]
  end
end
