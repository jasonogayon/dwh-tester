# New Contacts
module NewContacts
  def get_contacts
    return Array[
      Hash[
        id:                     nil,
        name:                   "Test Contact",
        designation:            "Software Tester",
        department:             14,
        phone:                  "09084520105",
        email:                  "dwhseide@gmail.com",
        messenger_type:         4,
        messenger_id:           "jasonogayon",
        is_recipient:           1,
        to_edit:                true,
        new_contact_details:    Hash[
            name:               "Edited Test Contact",
            designation:        "Software Tester",
            department:         14,
            phone:              "1234567890",
            email:              "dwhcukes@gmail.com",
            messenger_type:     4,
            messenger_id:       "jasonogayon",
            is_recipient:       0
        ],
        to_delete:              true
      ]
    ]
  end
end
