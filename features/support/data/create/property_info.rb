# New Property Information
module NewPropertyInformation
  def get_propertyinfo
    Hash[
      type:       "Condo Resort",
      address:    "Legaspi Village, Makati City 1229",
      phone:      "09084520105",
      email:      "dwhseide@gmail.com",
      longitude:  121.0216356,
      latitude:   14.5694186
    ]
  end
end
