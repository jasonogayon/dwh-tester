# New Users
module NewUsers
  def get_users
    return Array[
      Hash[
          id:                 nil,
          name:               "Jason Ogayon",
          email:              "dwhcukes@gmail.com",
          to_edit:            false,
          new_user_details:   Hash[],
          to_delete:          false
      ],
      Hash[
          id:                 nil,
          name:               "Test User",
          email:              "dwhcukes@gmail.com",
          to_edit:            true,
          new_user_details:   Hash[
              name:           "Edited Test User",
              email:          "dwhseide@gmail.com"
          ],
          to_delete:          true
      ]
    ]
  end
end
