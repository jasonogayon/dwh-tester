# New Add-ons
module NewAddons
  def get_addons
    return Array[
      Hash[
        id:                 nil,
        name:               "Airport Transfer",
        price:              10,
        description:        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " \
                            "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " \
                            "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        to_edit:            false,
        new_addon_details:  Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Insurance Hotel Guest Cover",
        price:              20,
        description:        "Introducing Tune Insurance Hotel Guest Cover, the perfect cover for every Tune Hotels guest. " \
                            "We're not only covering you against room cancellations but have taken it further to cover you for events " \
                            "such as accidental death and permanent disablement.",
        to_edit:            false,
        new_addon_details:  Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Spa Service",
        price:              10,
        description:        "Test addon description",
        to_edit:            false,
        new_addon_details:  Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Test Add-on",
        price:              15,
        description:        "Test addon description",
        to_edit:            true,
        new_addon_details:  Hash[
          name:             "Edited Test Add-on",
          price:            199,
          description:      "Edited Test addon description"
        ],
        to_delete:          true
      ]
    ]
  end
end
