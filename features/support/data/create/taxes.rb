# New Taxes
module NewTaxes
  def get_taxes
    return Array[
      Hash[
        id:                 nil,
        name:               "Government Tax",
        percent:            6,
        tax_type:           1,
        is_taxable:         0,
        to_edit:            false,
        new_tax_details:    Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Test Tax",
        percent:            6,
        tax_type:           1,
        is_taxable:         0,
        to_edit:            true,
        new_tax_details:    Hash[
            name:           "Edited Test Tax",
            percent:        10,
            tax_type:       1,
            is_taxable:     0
        ],
        to_delete:          true
      ],
      Hash[
        id:                 nil,
        name:               "Service Charge",
        percent:            10,
        tax_type:           2,
        is_taxable:         1,
        to_edit:            false,
        new_tax_details:    Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Test Surcharge",
        percent:            10,
        tax_type:           2,
        is_taxable:         1,
        to_edit:            true,
        new_tax_details:    Hash[
            name:           "Edited Test Surcharge",
            percent:        5,
            tax_type:       2,
            is_taxable:     0
        ],
        to_delete:          true
      ]
    ]
  end
end
