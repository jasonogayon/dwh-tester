# New Extra Beds
module NewExtraBeds
  def get_extrabeds
    return Hash[
      without_breakfast_rate:     10,
      with_breakfast_rate:        20,
      room_accommodation:         nil,
      room_occupancy:             nil
    ]
  end
end
