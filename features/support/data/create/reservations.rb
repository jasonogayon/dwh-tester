# New Hotel Reservations
module NewHotelReservation
  def get_rateplan(property, rateplan_type, rooms="", payment="")
    if rateplan_type.nil?
      rateplan = property[:public_rateplans][0]
    else
      rateplan_alias = rateplan_type.gsub(Regexp.union(["Public", "Private", "Nightly", "Fixed", "Free", "Nights", "Inactive", "Active", "Mobile-Only", "Special", "Channel-Only"]), "").strip
      rateplan = property[:public_rateplans].select { |rp| rp[:alias].include?(rateplan_alias) }[0]
      raise "Error: The rate plan described in the test does not exist. Please check." if rateplan.nil?
    end
    rateplan.update(Hash[
      id:             custom_rateplan_id.nil? ? get_rateplan_id(property[:hotel_type], rateplan_type, property[:is_asiapay]) : custom_rateplan_id,
      name:           rateplan_type,
      is_private:     rateplan_type.nil? ? false : rateplan_type.downcase =~ /private/ ? true : false,
      is_onhold:      payment =~ /different credit card|different CC/ ? true : false,
      images_sent:    payment =~ /credentials sent|images uploaded/ ? true : false,
      rooms:          get_room_ids(property[:hotel_type], rooms.downcase.split("and"), property[:is_asiapay]),
      details:        get_rateplan_data(property, rateplan, rateplan_type ||= "Nightly")
    ])
    return rateplan
  end

  def get_default_rsvsn_info(hotel_type, rateplan, arrival=nil, departure=nil, guest=nil)
    if !custom_arrival.nil? && !custom_no_nights.nil?
      custom_departure = Date.parse(custom_arrival) + custom_no_nights.to_i
      custom_departure = "#{custom_departure.year}-#{custom_departure.month}-#{custom_departure.day}"
      arrival = "#{(Date.parse(custom_arrival) - Date.parse(get_date('TODAY')[0])).to_i} DAYS FROM NOW"
      departure = "#{(Date.parse(custom_departure) - Date.parse(get_date('TODAY')[0])).to_i} DAYS FROM NOW"
    end
    reservation = Hash[
      arrival:            arrival.nil? ? get_date("TOMORROW") : get_date(arrival),
      departure:          departure.nil? ? get_date("#{rateplan[:details][:rp_min_nights].to_i + 1} DAYS FROM NOW") : get_date(departure),
      rateplan:           rateplan[:name] ||= nil,
      rateplan_id:        rateplan[:id] ||= nil,
      rateplan_details:   get_rateplan_policy(rateplan[:name]),
      rooms:              rateplan[:rooms] ||= nil,
      rsvsn_type:         "confirmed",
      rsvsn_mode:         "new",
      language:           "en",
      reservation_id:     0,
      reservation_hash:   0,
      auth_key:           0,
      user_id:            0,
      cp_user_id:         0,
      ab_test:            0,
      mobile_exclusive:   0,
      extra_beds:         rateplan[:details][:extrabeds].nil? ? "null" : "[{\"room_no\":1,\"count\":#{rand(1..10)}}]",
      first_name:         property_id.nil? ? (guest.nil? ? rateplan[:type] : guest[:first_name]) : Faker::Pokemon.name,
      last_name:          property_id.nil? ? (guest.nil? ? rateplan[:alias][0] : guest[:last_name]) : Faker::Pokemon.name,
      email:              guest.nil? ? FigNewton.email : guest[:email],
      email_confirm:      guest.nil? ? FigNewton.email : guest[:email],
      phone:              guest.nil? ? "639#{rand(1_000_000_000)}" : guest[:phone],
      no_adults:          guest.nil? ? rand(100) : guest[:no_adults],
      no_children:        guest.nil? ? rand(100) : guest[:no_children],
      special_request:    property_id.nil? ? (guest.nil? ? FigNewton.special_request : guest[:special_request]) : "<marquee>#{Faker::Lorem.paragraph}</marquee>",
      eta:                guest.nil? ? rand(94) : guest[:eta],
      country:            guest.nil? ? rand(246) : guest[:country],
      cc_type:            hotel_type.include?("BDO") ? FigNewton.bdo_cc_type : custom_cc.nil? ? FigNewton.cc_type : custom_cc,
      cc_number:          hotel_type.include?("BDO") ? FigNewton.bdo_cc_number : custom_cc.nil? ? FigNewton.cc_number : 2_223_000_048_400_011,
      cc_exp_month:       hotel_type.include?("BDO") ? FigNewton.bdo_cc_expiry_month : FigNewton.cc_expiry_month,
      cc_exp_year:        hotel_type.include?("BDO") ? FigNewton.bdo_cc_expiry_year : FigNewton.cc_expiry_year,
      cc_cvv:             hotel_type.include?("BDO") ? FigNewton.bdo_cc_seccode : 3.times.map { (0..9).to_a.sample }.join,
      terms_agreement:    "on",
      payment_agree:      "yes",
      process_payment:    true,
      is_valid_day:       1,
      is_private:         rateplan[:is_private] ||= false,
      is_onhold:          rateplan[:is_onhold] ||= false,
      images_sent:        rateplan[:images_sent] ||= false,
      is_confirmed:       true,
      is_not_confirmed:   false,
      is_modified:        false,
      is_cancelled:       false,
      is_noshow:          false,
      is_dwh:             hotel_type.include?("DWH"),
      is_hpp:             hotel_type.include?("HPP"),
      is_bdo:             hotel_type.include?("BDO"),
      is_paypal:          hotel_type.include?("Paypal"),
      is_hotel_processed: hotel_type =~ /HPP|BDO|Paypal/ ? true : false,
      is_custom:          !property_id.nil? && !custom_rateplan_id.nil? && !custom_arrival.nil? && !custom_no_nights.nil?
    ]
    reservation.update(Hash[cc_name: reservation[:is_onhold] ? "OnHold Reservation" : "#{reservation[:first_name]} #{reservation[:last_name]}"])
    return reservation
  end

  def get_modified_rsvsn_info(rateplan, arrival=nil, departure=nil)
    return Hash[
      rsvsn_type:         "modified",
      rsvsn_mode:         "modify",
      user_id:            2,
      cp_user_id:         2,
      arrival:            arrival.nil? ? get_date("TOMORROW") : get_date(arrival),
      departure:          departure.nil? ? get_date("#{rateplan[:details][:rp_min_nights].to_i + 1} DAYS FROM NOW") : get_date(departure),
      rateplan:           rateplan[:name],
      rateplan_id:        rateplan[:id] ||= nil,
      rooms:              rateplan[:rooms] ||= nil,
      is_confirmed:       false,
      is_not_confirmed:   true,
      is_modified:        true,
      rateplan_newdetail: get_rateplan_policy(rateplan[:name]),
    ]
  end

  private

  def get_rateplan_policy(rateplan_name)
    rateplan = Hash[
      is_private:                 rateplan_name =~ /Private/ ? true : false,
      has_no_penalty:             rateplan_name =~ /NC|No Charge|No Penalty/ ? true : false,
      has_lead_time_penalty:      rateplan_name =~ /LT|Early|Late|Lead Time/ ? true : false,
      has_first_night_penalty:    rateplan_name =~ /FNEI|First Night Penalty|First Night Effective Immediately/ ? true : false,
      has_full_charge_penalty:    rateplan_name =~ /FCEI|Full Charge Penalty|Full Charge Effective Immediately/ ? true : false,
      is_no_prepay:               rateplan_name =~ /Arrival|No Prepay/ ? true : false,
      is_partial_prepay:          rateplan_name =~ /Partial|10-|10P|10% Prepay/ ? true : false,
      is_first_night_prepay:      rateplan_name =~ /FN-|First Night Prepay/ ? true : false,
      is_full_prepay:             rateplan_name =~ /FC-|Full-|Full Charge Prepay|Full Prepay/ ? true : false,
      is_nonrefundable:           rateplan_name =~ /NotR|Non-Refundable|Nonrefundable|NonRefundable/ ? true : false,
      is_not_allowed:             rateplan_name =~ /NotAllowed|Not Allowed/ ? true : false
    ]
    rateplan.update(Hash[is_public: !rateplan[:is_private], is_refundable: !rateplan[:is_nonrefundable]])
    return rateplan
  end
end
