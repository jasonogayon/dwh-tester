# New Amenities
module NewAmenities
  def get_amenities
    return Array[
      Hash[
        id:         nil,
        name:       "Hairdryer",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Iron and Ironing board",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Living room/lounge area",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Telephone with IDD",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Minibar",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Private bathroom with separate bathtub",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "In-room safe",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Coffee and Tea maker",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "High-speed internet and WiFi",
        to_delete:  false
      ],
      Hash[
        id:         nil,
        name:       "Test Amenity",
        to_delete:  true
      ]
    ]
  end
end
