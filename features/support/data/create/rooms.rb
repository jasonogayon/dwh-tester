# New Rooms
module NewRooms
  def get_rooms
    return Array[
      Hash[
        id:                 nil,
        name:               "Deluxe Room",
        size:               21,
        unit:               "Square Meters",
        adult:              2,
        child:              1,
        description:        "Deluxe rooms offer prolific modern and heritage décor, " \
                            "comfort and a full array of amenities that make your stay comfortable and warm. " \
                            "* Free WiFi  in the room and hotel premise " \
                            "* Deluxe Room from 2nd Floor till 5th Floor only.",
        inventory:          999,
        availability:       999,
        to_edit:            false,
        new_room_details:   Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Standard Room",
        size:               70,
        unit:               "Square Meters",
        adult:              5,
        child:              4,
        description:        "Comes with Club facilities and benefits (Breakfast, Tea Break, " \
                            "Evening Cocktails from 6 pm to 7.30pm, WIFI, Complimentary Laundry and Pressing for 4 pieces daily. " \
                            "Children under 12 years not allowed in the Club’s Lounge",
        inventory:          999,
        availability:       999,
        to_edit:            false,
        new_room_details:   Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Superior",
        size:               40,
        unit:               "Square Meters",
        adult:              2,
        child:              0,
        description:        "Superior rooms offer prolific modern and heritage décor, " \
                            "comfort and a full array of amenities that make your stay comfortable and warm. " \
                            "* Free WiFi in the room and hotel premise " \
                            "*Superior Room from 1st Floor till 5th Floor only.",
        inventory:          100,
        availability:       100,
        to_edit:            false,
        new_room_details:   Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Studio",
        size:               24,
        unit:               "Square Meters",
        adult:              5,
        child:              3,
        description:        "Test room description",
        inventory:          999,
        availability:       999,
        to_edit:            false,
        new_room_details:   Hash[],
        to_delete:          false
      ],
      Hash[
        id:                 nil,
        name:               "Test Room",
        size:               nil,
        unit:               "",
        adult:              4,
        child:              2,
        description:        "Test room description",
        inventory:          999,
        availability:       999,
        to_edit:            true,
        new_room_details:   Hash[
            name:           "Edited Test Room",
            size:           25,
            unit:           "Square Meters",
            adult:          5,
            child:          3,
            description:    "Edited test room description",
            inventory:      100,
            availability:   100
        ],
        to_delete:          true
      ]
    ]
  end
end
