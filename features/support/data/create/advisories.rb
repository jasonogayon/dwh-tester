# New Advisories
module NewAdvisories
  def get_advisories
    return Array[
      Hash[
        id:                     nil,
        name:                   "Baby Cot Policy",
        rolling:                "yes",
        description:            "Baby crib/cot is available at no extra charge, subject to availability.",
        to_edit:                false,
        new_advisory_details:   Hash[],
        to_delete:              false
      ],
      Hash[
        id:                     nil,
        name:                   "Best Price Guarantee",
        rolling:                "yes",
        description:            "In the unlikely event that a lower price at The Royale Chulan Hotel " \
                                "is made available on a non-The Royale Chulan website (the 'Competing Price'), " \
                                "upon its receipt of a claim that satisfies these Best Price Guarantee terms and conditions (the 'BPG Terms'), " \
                                "The Royale Chulan will honor that Competing Price and provide the individual that submitted the valid claim one of the following: " \
                                "(1) an additional 10% discount off the Competing Price per room per night; or " \
                                "(2) a voucher that the guest can use during the stay, the amount of the voucher will be equal to the difference " \
                                "between the original reservation rate and the competing price.  " \
                                "Terms and Conditions of the Best Price Guarantee can be viewed in http://www.theroyalechulanhotel.com/",
        to_edit:                false,
        new_advisory_details:   Hash[],
        to_delete:              false
      ],
      Hash[
        id:                     nil,
        name:                   "Connecting Rooms Policy",
        rolling:                "yes",
        description:            "Connecting rooms are available upon request, subject to availability",
        to_edit:                false,
        new_advisory_details:   Hash[],
        to_delete:              false
      ],
      Hash[
        id:                     nil,
        name:                   "Exchange Rate Policy",
        rolling:                "yes",
        description:            "Exchange rate will be based on property's current posting.",
        to_edit:                false,
        new_advisory_details:   Hash[],
        to_delete:              false
      ],
      Hash[
        id:                     nil,
        name:                   "Extra Bed Policy",
        rolling:                "yes",
        description:            "Extra bed is available at RM 185.60Nett, inclusive of one buffet breakfast. Payable at the hotel and subject to availability.",
        to_edit:                false,
        new_advisory_details:   Hash[],
        to_delete:              false
      ],
      Hash[
        id:                     nil,
        name:                   "Test Advisory",
        rolling:                "yes",
        description:            "Test advisory description",
        to_edit:                false,
        new_advisory_details:   Hash[
            name:               "Edited Test Advisory",
            rolling:            "yes",
            description:        "Edited test advisory description"
        ],
        to_delete:              true
      ]
    ]
  end
end
