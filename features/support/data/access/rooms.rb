# Existing Rooms
module ExistingRooms
  def get_room_ids(hotel_type, rooms, is_asiapay=false)
    unless rooms.size < 1
      room_ids = []
      rooms.each do |room|
        room = room.downcase.strip
        if room.include? "accommodation"
          acc_room_id = {}
          acc_room_id.update(Hash[room_0_qty: room.split("accommodation")[0].strip.to_i])
          acc_room_id.update(Hash[room_0_id: FigNewton.dwh_accommodation]) if hotel_type.include? "DWH"
          acc_room_id.update(Hash[room_0_id: FigNewton.ap_accommodation]) if is_asiapay
          acc_room_id.update(Hash[room_0_id: FigNewton.hpp_accommodation]) if hotel_type.include? "HPP"
          acc_room_id.update(Hash[room_0_id: FigNewton.bdo_accommodation]) if hotel_type.include?("BDO") && !hotel_type.include?("Paypal")
          acc_room_id.update(Hash[room_0_id: FigNewton.paypal_accommodation]) if !hotel_type.include?("BDO") && hotel_type.include?("Paypal")
          acc_room_id.update(Hash[room_0_id: FigNewton.bdo_paypal_accommodation]) if hotel_type.include?("BDO") && hotel_type.include?("Paypal")
          acc_room_id.update(Hash[room_0_occ: 0])
          room_ids.push(acc_room_id)
        end
        if room.include? "occupancy"
          occ_room_id = {}
          occ_room_id.update(Hash[room_1_qty: room.split("occupancy")[0].strip.to_i])
          occ_room_id.update(Hash[room_1_id: FigNewton.dwh_occupancy]) if hotel_type.include? "DWH"
          occ_room_id.update(Hash[room_1_id: FigNewton.ap_occupancy]) if is_asiapay
          occ_room_id.update(Hash[room_1_id: FigNewton.hpp_occupancy]) if hotel_type.include? "HPP"
          occ_room_id.update(Hash[room_1_id: FigNewton.bdo_occupancy]) if hotel_type.include?("BDO") && !hotel_type.include?("Paypal")
          occ_room_id.update(Hash[room_1_id: FigNewton.paypal_occupancy]) if !hotel_type.include?("BDO") && hotel_type.include?("Paypal")
          occ_room_id.update(Hash[room_1_id: FigNewton.bdo_paypal_occupancy]) if hotel_type.include?("BDO") && hotel_type.include?("Paypal")
          occ_room_id.update(Hash[room_1_occ: 3])
          room_ids.push(occ_room_id)
        end
      end
    end
    return room_ids
  end
end
