# Existing Guest Names
module ExistingGuestNames
  def get_guest_firstname(hotel_type, rateplan_name)
    return "Test" if rateplan_name.nil?
    rateplan = get_rateplan_policy_info(rateplan_name)
    if hotel_type.include? "DWH"
      first_name = rateplan[:is_full_prepay] && rateplan[:is_nonrefundable] ? FigNewton.firstname_full_notr : FigNewton.firstname_full_yesr
      first_name = FigNewton.firstname_partial if rateplan[:is_partial_prepay]
      first_name = FigNewton.firstname_onhold if rateplan[:is_onhold]
      first_name += " Pending" if rateplan[:is_pending]
    else
      first_name = FigNewton.firstname_pa if rateplan[:is_no_prepay]
      first_name = rateplan[:is_nonrefundable] ? FigNewton.firstname_pb_notr : FigNewton.firstname_pb_yesr
    end
    first_name = "#{first_name} #{hotel_type}"
    first_name || FigNewton.firstname_default
  end

  def get_guest_lastname(rateplan_name)
    return "Reservation" if rateplan_name.nil?
    rateplan = get_rateplan_policy_info(rateplan_name)
    last_name = FigNewton.lastname_nc if rateplan[:has_no_penalty]
    last_name = FigNewton.lastname_lt if rateplan[:has_lead_time_penalty]
    last_name = FigNewton.lastname_fnei if rateplan[:has_first_night_penalty]
    last_name = FigNewton.lastname_fcei if rateplan[:has_full_charge_penalty]
    last_name = FigNewton.lastname_10ei if rateplan[:is_nonrefundable]
    last_name = FigNewton.lastname_notallowed if rateplan[:is_not_allowed]
    last_name || FigNewton.lastname_default
  end
end
