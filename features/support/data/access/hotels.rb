# Existing Hotels
module ExistingHotels
  def get_hotel_id(hotel, is_asiapay)
    return FigNewton.unsubscribed_hotel_id if hotel.include? "Unsubscribed"
    if hotel.include? "Rate Mixing"
      return FigNewton.dwh_ratemixing_hotel_id if hotel.include? "DWH"
    else
      return FigNewton.ap_hotel_id if is_asiapay
      return FigNewton.dwh_hotel_id if hotel.include? "DWH"
      return FigNewton.hpp_hotel_id if hotel.include? "HPP"
      return hotel.include?("BDO") ? (hotel.include?("Paypal") ? FigNewton.bdo_paypal_hotel_id : FigNewton.bdo_hotel_id) : FigNewton.paypal_hotel_id
    end
  end

  def get_hotel_name(hotel, is_asiapay)
    return FigNewton.unsubscribed_hotel_name if hotel.include? "Unsubscribed"
    return FigNewton.ap_hotel_name if is_asiapay
    return FigNewton.dwh_hotel_name if hotel.include? "DWH"
    return FigNewton.hpp_hotel_name if hotel.include? "HPP"
    return hotel.include?("BDO") ? (hotel.include?("Paypal") ? FigNewton.bdo_paypal_hotel_name : FigNewton.bdo_hotel_name) : FigNewton.paypal_hotel_name
  end
end
