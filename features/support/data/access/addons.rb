# Existing Add-ons
module ExistingAddons
  def get_addon_id(hotel_type, addon_name)
    if hotel_type.include? "DWH"
      return FigNewton.dwh_addon_spa if addon_name.include? "spa"
      return FigNewton.dwh_addon_airport if addon_name.include? "airport transfer"
      return FigNewton.dwh_addon_insurance if addon_name.include? "insurance"
    elsif  hotel_type.include? "HPP"
      return FigNewton.hpp_addon_spa if addon_name.include? "spa"
      return FigNewton.hpp_addon_airport if addon_name.include? "airport transfer"
      return FigNewton.hpp_addon_insurance if addon_name.include? "insurance"
    elsif  hotel_type.include? "BDO"
      return FigNewton.bdo_addon_spa if addon_name.include? "spa"
      return FigNewton.bdo_addon_airport if addon_name.include? "airport transfer"
      return FigNewton.bdo_addon_insurance if addon_name.include? "insurance"
    end
  end
end
