# Existing Accounts
module ExistingAccounts
  def get_username(hotel_type)
    return FigNewton.username_dwh if hotel_type.include? "DWH"
    return FigNewton.username_hpp if hotel_type.include? "HPP"
    return FigNewton.username_bdo if hotel_type.include?("BDO") && !hotel_type.include?("Paypal")
    return FigNewton.username_paypal if !hotel_type.include?("BDO") && hotel_type.include?("Paypal")
    return FigNewton.username_bdo_paypal if hotel_type.include?("BDO") && hotel_type.include?("Paypal")
  end

  def get_password(hotel_type)
    return FigNewton.password_dwh if hotel_type.include? "DWH"
    return FigNewton.password_hpp if hotel_type.include? "HPP"
    return FigNewton.password_bdo if hotel_type.include?("BDO") && !hotel_type.include?("Paypal")
    return FigNewton.password_paypal if !hotel_type.include?("BDO") && hotel_type.include?("Paypal")
    return FigNewton.password_bdo_paypal if hotel_type.include?("BDO") && hotel_type.include?("Paypal")
  end
end
