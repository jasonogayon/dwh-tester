# Existing Rate Plans
module ExistingRatePlans
  def get_rateplan_id(hotel_type, rateplan_name, is_asiapay=false)
    rateplan = get_rateplan_policy_info(rateplan_name)
    return ap_rp_id(rateplan) if is_asiapay
    return dwh_rp_id(rateplan) if hotel_type.include? "DWH"
    return hpp_rp_id(rateplan) if hotel_type.include? "HPP"
    return hotel_type.include?("BDO") ? (hotel_type.include?("Paypal") ? bdo_paypal_rp_id(rateplan) : bdo_rp_id(rateplan)) : paypal_rp_id(rateplan)
  end

  private
  def get_rateplan_policy_info(rateplan_name)
    Hash[
        is_public:                  rateplan_name =~ /Public/,
        has_no_penalty:             rateplan_name =~ /NC|No Charge|No Penalty/,
        has_lead_time_penalty:      rateplan_name =~ /LT|Early|Late|Lead Time/,
        has_first_night_penalty:    rateplan_name =~ /FNEI|First Night Penalty|First Night Effective Immediately/,
        has_full_charge_penalty:    rateplan_name =~ /FCEI|Full Charge Penalty|Full Charge Effective Immediately/,
        is_no_prepay:               rateplan_name =~ /Arrival|No Prepay/,
        is_partial_prepay:          rateplan_name =~ /Partial|10-|10P|10% Prepay/,
        is_first_night_prepay:      rateplan_name =~ /FN-|First Night Prepay/,
        is_full_prepay:             rateplan_name =~ /FC-|Full-|Full Charge Prepay|Full Prepay/,
        is_nonrefundable:           rateplan_name =~ /NotR|Non-Refundable|Nonrefundable|NonRefundable/,
        is_not_allowed:             rateplan_name =~ /NotAllowed|Not Allowed/,
        is_onhold:                  rateplan_name =~ /OnHold|Onhold|On Hold|On-Hold/,
        is_pending:                 rateplan_name =~ /Pending/
    ]
  end

  def dwh_rp_id(rateplan)
    if rateplan[:is_partial_prepay]
      return FigNewton.dwh_public_partial_nc if rateplan[:has_no_penalty]
      return FigNewton.dwh_public_partial_lt if rateplan[:has_lead_time_penalty]
      return FigNewton.dwh_public_partial_fnei if rateplan[:has_first_night_penalty]
      return FigNewton.dwh_public_partial_fcei if rateplan[:has_full_charge_penalty]
    else
      return rateplan[:is_public] ? FigNewton.dwh_public_full_notr : FigNewton.dwh_private_full_notr if rateplan[:is_nonrefundable]
      return rateplan[:is_public] ? FigNewton.dwh_public_full_yesr_nc : FigNewton.dwh_private_full_yesr_nc if rateplan[:has_no_penalty]
      return rateplan[:is_public] ? FigNewton.dwh_public_full_yesr_lt : FigNewton.dwh_private_full_yesr_lt if rateplan[:has_lead_time_penalty]
      return rateplan[:is_public] ? FigNewton.dwh_public_full_yesr_fnei : FigNewton.dwh_private_full_yesr_fnei if rateplan[:has_first_night_penalty]
      return rateplan[:is_public] ? FigNewton.dwh_public_full_yesr_fcei : FigNewton.dwh_private_full_yesr_fcei if rateplan[:has_full_charge_penalty]
    end
  end

  def ap_rp_id(rateplan)
    if rateplan[:is_partial_prepay]
      return FigNewton.ap_public_partial_nc if rateplan[:has_no_penalty]
      return FigNewton.ap_public_partial_lt if rateplan[:has_lead_time_penalty]
      return FigNewton.ap_public_partial_fnei if rateplan[:has_first_night_penalty]
      return FigNewton.ap_public_partial_fcei if rateplan[:has_full_charge_penalty]
    else
      return FigNewton.ap_public_full_notr if rateplan[:is_nonrefundable]
      return FigNewton.ap_public_full_yesr_nc if rateplan[:has_no_penalty]
      return FigNewton.ap_public_full_yesr_lt if rateplan[:has_lead_time_penalty]
      return FigNewton.ap_public_full_yesr_fnei if rateplan[:has_first_night_penalty]
      return FigNewton.ap_public_full_yesr_fcei if rateplan[:has_full_charge_penalty]
    end
  end

  def hpp_rp_id(rateplan)
    if rateplan[:is_no_prepay]
      return rateplan[:is_public] ? FigNewton.hpp_public_pa_nc : FigNewton.hpp_private_pa_nc if rateplan[:has_no_penalty]
      return rateplan[:is_public] ? FigNewton.hpp_public_pa_lt : FigNewton.hpp_private_pa_lt if rateplan[:has_lead_time_penalty]
      return rateplan[:is_public] ? FigNewton.hpp_public_pa_fnei : FigNewton.hpp_private_pa_fnei if rateplan[:has_first_night_penalty]
      return rateplan[:is_public] ? FigNewton.hpp_public_pa_fcei : FigNewton.hpp_private_pa_fcei if rateplan[:has_full_charge_penalty]
    else
      if rateplan[:is_nonrefundable]
        if rateplan[:is_partial_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_notr_notallowed : FigNewton.hpp_private_pb_10p_notr_notallowed if rateplan[:is_not_allowed]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_notr_10ei : FigNewton.hpp_private_pb_10p_notr_10ei
        elsif rateplan[:is_first_night_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_notr_notallowed : FigNewton.hpp_private_pb_fn_notr_notallowed if rateplan[:is_not_allowed]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_notr_10ei : FigNewton.hpp_private_pb_fn_notr_10ei
        elsif rateplan[:is_full_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_notr_notallowed : FigNewton.hpp_private_pb_full_notr_notallowed if rateplan[:is_not_allowed]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_notr_10ei : FigNewton.hpp_private_pb_full_notr_10ei
        end
      else
        if rateplan[:is_partial_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_yesr_nc : FigNewton.hpp_private_pb_10p_yesr_nc if rateplan[:has_no_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_yesr_lt : FigNewton.hpp_private_pb_10p_yesr_lt if rateplan[:has_lead_time_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_yesr_fnei : FigNewton.hpp_private_pb_10p_yesr_fnei if rateplan[:has_first_night_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_10p_yesr_fcei : FigNewton.hpp_private_pb_10p_yesr_fcei if rateplan[:has_full_charge_penalty]
        elsif rateplan[:is_first_night_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_yesr_nc : FigNewton.hpp_private_pb_fn_yesr_nc if rateplan[:has_no_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_yesr_lt : FigNewton.hpp_private_pb_fn_yesr_lt if rateplan[:has_lead_time_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_yesr_fnei : FigNewton.hpp_private_pb_fn_yesr_fnei if rateplan[:has_first_night_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_fn_yesr_fcei : FigNewton.hpp_private_pb_fn_yesr_fcei if rateplan[:has_full_charge_penalty]
        elsif rateplan[:is_full_prepay]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_yesr_nc : FigNewton.hpp_private_pb_full_yesr_nc if rateplan[:has_no_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_yesr_lt : FigNewton.hpp_private_pb_full_yesr_lt if rateplan[:has_lead_time_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_yesr_fnei : FigNewton.hpp_private_pb_full_yesr_fnei if rateplan[:has_first_night_penalty]
          return rateplan[:is_public] ? FigNewton.hpp_public_pb_full_yesr_fcei : FigNewton.hpp_private_pb_full_yesr_fcei if rateplan[:has_full_charge_penalty]
        end
      end
    end
  end

  def bdo_rp_id(rateplan)
    if rateplan[:is_partial_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_10p_notr_notallowed : FigNewton.bdo_private_pb_10p_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_10p_notr_10ei : FigNewton.bdo_private_pb_10p_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_10p_yesr_lt : FigNewton.bdo_private_pb_10p_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    elsif rateplan[:is_first_night_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_fn_notr_notallowed : FigNewton.bdo_private_pb_fn_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_fn_notr_10ei : FigNewton.bdo_private_pb_fn_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_fn_yesr_lt : FigNewton.bdo_private_pb_fn_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    elsif rateplan[:is_full_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_full_notr_notallowed : FigNewton.bdo_private_pb_full_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_full_notr_10ei : FigNewton.bdo_private_pb_full_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.bdo_public_pb_full_yesr_lt : FigNewton.bdo_private_pb_full_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    end
  end

  def paypal_rp_id(rateplan)
    if rateplan[:is_partial_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_notr_notallowed : FigNewton.pp_private_pb_10p_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_notr_10ei : FigNewton.pp_private_pb_10p_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_yesr_nc : FigNewton.pp_private_pb_10p_yesr_nc if rateplan[:has_no_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_yesr_lt : FigNewton.pp_private_pb_10p_yesr_lt if rateplan[:has_lead_time_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_yesr_fnei : FigNewton.pp_private_pb_10p_yesr_fnei if rateplan[:has_first_night_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_10p_yesr_fcei : FigNewton.pp_private_pb_10p_yesr_fcei if rateplan[:has_full_charge_penalty]
      end
    elsif rateplan[:is_first_night_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_notr_notallowed : FigNewton.pp_private_pb_fn_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_notr_10ei : FigNewton.pp_private_pb_fn_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_yesr_nc : FigNewton.pp_private_pb_fn_yesr_nc if rateplan[:has_no_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_yesr_lt : FigNewton.pp_private_pb_fn_yesr_lt if rateplan[:has_lead_time_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_yesr_fnei : FigNewton.pp_private_pb_fn_yesr_fnei if rateplan[:has_first_night_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_fn_yesr_fcei : FigNewton.pp_private_pb_fn_yesr_fcei if rateplan[:has_full_charge_penalty]
      end
    elsif rateplan[:is_full_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_notr_notallowed : FigNewton.pp_private_pb_full_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_notr_10ei : FigNewton.pp_private_pb_full_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_yesr_nc : FigNewton.pp_private_pb_full_yesr_nc if rateplan[:has_no_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_yesr_lt : FigNewton.pp_private_pb_full_yesr_lt if rateplan[:has_lead_time_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_yesr_fnei : FigNewton.pp_private_pb_full_yesr_fnei if rateplan[:has_first_night_penalty]
        return rateplan[:is_public] ? FigNewton.pp_public_pb_full_yesr_fcei : FigNewton.pp_private_pb_full_yesr_fcei if rateplan[:has_full_charge_penalty]
      end
    end
  end

  def bdo_paypal_rp_id(rateplan)
    if rateplan[:is_partial_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_10p_notr_notallowed : FigNewton.bp_private_pb_10p_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_10p_notr_10ei : FigNewton.bp_private_pb_10p_notr_10ei
      else
        return rateplan[:is_public] ? FigNewton.bp_public_pb_10p_yesr_lt : FigNewton.bp_private_pb_10p_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    elsif rateplan[:is_first_night_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_fn_notr_notallowed : FigNewton.bp_private_pb_fn_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_fn_notr_fnei : FigNewton.bp_private_pb_fn_notr_fnei
      else
        return rateplan[:is_public] ? FigNewton.bp_public_pb_fn_yesr_lt : FigNewton.bp_private_pb_fn_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    elsif rateplan[:is_full_prepay]
      if rateplan[:is_nonrefundable]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_full_notr_notallowed : FigNewton.bp_private_pb_full_notr_notallowed if rateplan[:is_not_allowed]
        return rateplan[:is_public] ? FigNewton.bp_public_pb_full_notr_fcei : FigNewton.bp_private_pb_full_notr_fcei
      else
        return rateplan[:is_public] ? FigNewton.bp_public_pb_full_yesr_lt : FigNewton.bp_private_pb_full_yesr_lt if rateplan[:has_lead_time_penalty]
      end
    end
  end
end
