# Partners Dashboard Page
class DashboardPage
  include PageObject
  include ControlPanelHelper

  page_url FigNewton.login_cp_url

  # Element Locators
  link(:logoff, id: "lnklogout")
  cell(:overview, xpath: "//div[@id='selectview']//tr[contains(.,'Sales Overview')]/td")
  cell(:reservation, xpath: "//div[@id='selectview']//tr[contains(.,'Reservation')]/td")
  cell(:room_night, xpath: "//div[@id='selectview']//tr[contains(.,'Room Night')]/td")
  cell(:dwh_revenue, xpath: "//div[@id='selectview']//tr[contains(.,'DWH Revenue')]/td")
  cell(:hotel_revenue, xpath: "//div[@id='selectview']//tr[contains(.,'Hotel Revenue')]/td")
  cell(:endorsement, xpath: "//div[@id='selectview']//tr[contains(.,'Hotel Endorsement')]/td")
  cell(:subscription, xpath: "//div[@id='selectview']//tr[contains(.,'e-Subscription')]/td")
  cell(:logs, xpath: "//div[@id='selectview']//tr[contains(.,'Log')]/td")
  cell(:reports, xpath: "//div[@id='selectview']//tr[contains(.,'Report')]/td")
  text_field(:username, id: "loginUsername")

  # Methods
  def goto(tab_name)
    one_moment_please_element.wait_while_present(timeout: 300)
    page_loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    if tab_name.include? "overview"
      overview_element.click
    elsif tab_name.include? "reservation"
      reservation_element.click
    elsif tab_name.include? "room"
      room_night_element.click
    elsif tab_name.include? "revenue"
      tab_name.include?("dwh") ? dwh_revenue_element.click : hotel_revenue_element.click
    elsif tab_name.include? "endorse"
      endorsement_element.click
    elsif tab_name.include? "subscription"
      subscription_element.click
    elsif tab_name.include? "log"
      logs_element.click
    elsif tab_name.include? "report"
      reports_element.click
    end
    loading_mask_element.wait_while_present(timeout: 300)
  end

  def logout
    logoff_element.wait_until_present(timeout: 120)
    logoff
  end
end
