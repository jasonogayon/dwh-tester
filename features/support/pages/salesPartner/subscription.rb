# Partners Subscription Page
class SubscriptionPage
  include PageObject

  page_url FigNewton.login_cp_url

  # Element Locators
  image(:open_eform, xpath: "//div[@id='salesSubscriptionAccts']/div[3]/div[3]/div[2]/div/table/tbody/tr[3]/td[6]/div/img[2]")

  # Methods
  def open_eform
    open_eform_element.wait_until_present(timeout: 120)
    open_eform_element.click
  end
end
