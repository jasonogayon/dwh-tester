# Partners Endorsement Page
class EndorsementPage
  include PageObject
  include ControlPanelHelper

  page_url FigNewton.login_cp_url

  # Element Locators
  div(:endorse_successful, xpath: "//td[contains(@id,'messagebox')][contains(.,'A hotel has been successfully endorsed')]/div")
  button(:refresh_hotel_list, xpath: "//div[@id='sales-endorse_header']/following::div[1]//button[contains(.,'Refresh Hotel List')]")
  button(:endorse, xpath: "//button[contains(.,'Endorse')]")
  button(:endorse_OK, xpath: "//button[contains(.,'OK')]")
  list_item(:sales_partner_default, xpath: "//li[contains(@class,'x-boundlist-item')][contains(., 'TEST2 SPI Account2')]")
  list_item(:booking_integration_default, xpath: "//li[contains(@class,'x-boundlist-item')][contains(., 'Hotel')]")
  text_field(:sales_partner, name: "generic_sp_id")
  text_field(:booking_integration, name: "integrate_who")
  text_field(:hotel_webmaster_name, name: "webmaster_name")
  text_field(:hotel_webmaster_phone, name: "webmaster_contact_no")
  text_field(:hotel_webmaster_email, name: "webmaster_email")

  # Methods
  def endorse_property(property_name)
    open_endorsement_popup(property_name)
    select_sales_partner
    select_booking_integration
    input_webmaster_details
    endorse
    endorse_successful_element.wait_until_present(timeout: 120)
    endorse_OK
  end

  private

  def open_endorsement_popup(property_name)
    loading_element.wait_while_present(timeout: 300)
    refresh_hotel_list_element.wait_until_present(timeout: 120)
    refresh_hotel_list
    loading_element.wait_while_present(timeout: 300)
    self.class.link(:first_hotel, xpath: "//div[@id='endorsementgrid-body']//tr//a[contains(.,'#{property_name}')]")
    first_hotel
  end

  def select_sales_partner
    sales_partner_element.wait_until_present(timeout: 120)
    sales_partner_element.click
    loading_element.wait_while_present(timeout: 300)
    sales_partner_default_element.wait_until_present(timeout: 120)
    sales_partner_default_element.click
  end

  def select_booking_integration
    booking_integration_element.wait_until_present(timeout: 120)
    booking_integration_element.click
    loading_element.wait_while_present(timeout: 300)
    booking_integration_default_element.wait_until_present(timeout: 120)
    booking_integration_default_element.click
  end

  def input_webmaster_details
    hotel_webmaster_name_element.wait_until_present(timeout: 120)
    hotel_webmaster_name = FigNewton.cc_cardowner
    hotel_webmaster_phone = FigNewton.phone
    hotel_webmaster_email = FigNewton.email
  end
end
