# Partners Endorsement Form Page
class EndorsementFormPage
  include PageObject

  page_url FigNewton.eform_url

  # Element Locators
  link(:create, id: "create-new")
  span(:please_wait_saving, xpath: "//span[contains(@id,'messagebox')][contains(.,'Please wait')]")
  button(:accept_terms, xpath: "//label[contains(.,'I accept the terms')]/preceding::input[1]")
  button(:existing_website_confirm, xpath: "//table[@id='chk_existing_website-innerCt']/tbody/tr/td[1]/table/tbody/tr/td[2]/input")
  button(:existing_website_cancel, xpath: "//table[@id='chk_existing_website-innerCt']/tbody/tr/td[2]/table/tbody/tr/td[2]/input")
  button(:copy_authorized_representative_details, xpath: "//label[contains(., 'Copy the same contact details')]/preceding::input[1]")
  button(:back, xpath: "//div[@id='btnpanel-innerCt']/div[contains(.,'Back')]/em/button")
  button(:save_and_continue_later, xpath: "//div[@id='btnpanel-innerCt']/div[contains(.,'Save and continue later')]/em/button")
  button(:cancel, xpath: "//div[@id='btnpanel-innerCt']/div[contains(.,'Cancel')]/em/button")
  button(:save_and_proceed, xpath: "//div[@id='btnpanel-innerCt']/div[contains(.,'Save and proceed')]/em/button")
  button(:submit, xpath: "//div[@id='btnpanel-innerCt']/div[contains(.,'Submit')]/em/button")
  button(:thank_you, xpath: "//div[contains(@id,'messagebox')][contains(.,'Thank You, we have received your information')]//button[contains(.,'OK')]")
  button(:header_agreement, xpath: "//div[@id='mainbody']//button[contains(.,'Subscription Agreement')]")
  button(:header_hotelinfo, xpath: "//div[@id='mainbody']//button[contains(.,'Hotel Information')]")
  button(:header_taxes, xpath: "//div[@id='mainbody']//button[contains(.,'Taxes')]")
  button(:header_policies, xpath: "//div[@id='mainbody']//button[contains(.,'Policies')]")
  button(:header_images, xpath: "//div[@id='mainbody']//button[contains(.,'Images')]")
  text_field(:hotel_name, name: "hotelname")
  text_field(:rep_fullname, name: "fullname")
  text_field(:rep_position, name: "position")
  text_field(:rep_phone, name: "contact_number")
  text_field(:rep_email, name: "email")
  text_field(:option_to_cms, name: "interested")
  text_field(:country, name: "country")
  text_field(:city, name: "city")
  text_field(:timezone, name: "timezone")
  text_field(:currency, name: "currency")
  text_field(:booking_email, name: "booking_email")
  text_field(:arrival_time, name: "check_in_time")
  text_field(:departure_time, name: "check_out_time")
  text_field(:child_policy, name: "child_not_allowed")
  text_field(:modify_charge_type, name: "modify_charge_type")
  text_field(:cancel_charge_type, name: "late_charge_type")
  text_field(:noshow_charge_type, name: "no_show_charge_type")

  # Methods
  def create_property(info)
    browser.windows.last.use
    create_element.wait_until_present(timeout: 120)
    create
    browser.windows.last.use
    hotelname = set_authorized_representative(info)
    select_option_to_cms(info)
    accept_terms
    save_and_continue
    set_hotel_info(info)
    save_and_continue
    save_and_continue
    set_default_policies
    save_and_continue
    submit_hotel
    return hotelname
  end

  private

  def save_and_continue
    please_wait_saving_element.wait_while_present(timeout: 300)
    save_and_proceed_element.wait_until_present(timeout: 120)
    save_and_proceed
  end

  def assign_authorized_representative(info)
    hotelname = "#{info['property_name']} No. #{rand(1_000_000)}"
    header_agreement_element.wait_until_present(timeout: 120)
    hotel_name_element.wait_until_present(timeout: 120)
    hotel_name = hotelname
    rep_fullname = info["rep_name"]
    rep_position = info["rep_position"]
    rep_phone = info["rep_phone"]
    rep_email = info["rep_email"]
    return hotelname
  end

  def select_option_to_cms(info)
    option_to_cms_element.click
    info["option_to_cms"].downcase.include?("yes") ? select_eform_option("cms", "Yes") : select_eform_option("cms", "No")
  end

  def assign_hotel_info(info)
    # Set Location and Currency
    country_element.wait_until_present(timeout: 120)
    country_element.click
    select_eform_option("info", info["country"])
    city_element.click
    select_eform_option("info", info["city"])
    timezone_element.click
    select_eform_option("info", info["timezone"])
    currency_element.click
    select_eform_option("info", info["currency"])

    # Set Existing Website Option
    existing_website_cancel

    # Set Direct Bookings Email
    booking_email = info["rep_email"]

    # Copy Authorized Representative Details
    copy_authorized_representative_details
  end

  def assign_default_policies
    arrival_time_element.wait_until_present(timeout: 120)
    arrival_time_element.click
    select_eform_option("arrival", "02:00 PM")
    departure_time_element.click
    select_eform_option("departure", "12:00 PM")
    child_policy_element.click
    select_eform_option("child", "Not Allowed")
    modify_charge_type_element.click
    select_eform_option("modify", "No Charge")
    cancel_charge_type_element.click
    select_eform_option("cancel", "No Charge")
    noshow_charge_type_element.click
    select_eform_option("noshow", "No Charge")
  end

  def select_eform_option(option_type, option)
    locator = "li[contains(@class,'x-boundlist-item')][contains(., '#{option}')]"
    locator_modified = "//#{locator}/following::#{locator}"
    if option_type.include? "noshow"
      self.class.list_item(:option, xpath: locator_modified + "[2]")
    elsif option_type.include?("cancel") || option_type.include?("departure")
      self.class.list_item(:option, xpath: locator_modified + "[1]")
    else
      self.class.list_item(:option, xpath: "//" + locator)
    end
    option_element.wait_until_present(timeout: 120)
    option_element.click
    option_element.wait_while_present(timeout: 300)
  end

  def submit_hotel
    please_wait_saving_element.wait_while_present(timeout: 300)
    submit_element.wait_until_present(timeout: 120)
    submit
    thank_you_element.wait_until_present(timeout: 120)
    thank_you
    browser.windows.first.use
    create_element.wait_until_present(timeout: 120)
  end
end
