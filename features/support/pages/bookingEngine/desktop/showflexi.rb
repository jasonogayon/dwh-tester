# Desktop Booking Engine ShowFlexiblePage
class ShowFlexiblePage
  include PageObject
  include BookingEngineHelper

  page_url "#{FigNewton.showflexi_url}/<%=params[:id]%>/en/0/0/0/0"

  # Element Locators
  span(:copy_taxes, xpath: "//span[contains(@class, 'promo-tfees')]")
  span(:label_exclusivepackages, xpath: "//span[contains(.,'Exclusive Packages')]")
  link(:promocode, id: "promo-code-link")
  image(:promocode_success, xpath: ".//div[@id='promo-success-icon']/img")
  button(:promocode_apply, id: "btn-apply-promo-code")
  paragraph(:validity_checkin, xpath: "//td[contains(., 'Validity')]/p[contains(., 'Valid for check-in')]")
  paragraph(:validity_booking, xpath: "//td[contains(., 'Validity')]/p[contains(., 'Booking Period')]")
  paragraph(:validity_stay, xpath: "//td[contains(., 'Validity')]/p[contains(., 'Stay Period')]")
  text_field(:promocode_input, id: "promo-code-textbox")

  # Methods
  def apply_promocode(code)
    promocode_element.wait_until_present(timeout: 120)
    promocode
    promocode_input = code ||= FigNewton.promo_code
    promocode_success_element.wait_until_present(timeout: 120)
    promocode_apply
    label_exclusivepackages_element.wait_until_present(timeout: 120)
  end

  def tax_inclusion_copy
    return "incl" + copy_taxes.split("incl")[1]
  end

  def valid_days_for_checkin_copy
    validity_checkin_element.wait_until_present(timeout: 120)
    return validity_checkin
  end

  def valid_booking_dates_copy
    validity_booking_element.wait_until_present(timeout: 120)
    return validity_booking
  end

  def valid_stay_dates_copy
    validity_stay_element.wait_until_present(timeout: 120)
    return validity_stay
  end
end
