# Desktop Booking Engine Image Upload Page
class ImageUploadPage
  include PageObject
  include BookingEngineHelper

  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../", File.dirname(__FILE__))

  # Element Locators
  h1(:temporarily_unavailable, xpath: "//h1[contains(., '503 Service Temporarily Unavailable')]")
  h1(:error_encountered, xpath: "//h1[contains(., 'An Error Was Encountered')]")
  paragraph(:error_page, xpath: "//h1[contains(., 'An Error Was Encountered')]/following::p[1]")
  h2(:rsvsn_header, xpath: "//h2")
  link(:upload_later, css: "div#details-guest > h3 + p + p + p + div + p + div + p + div + p + div + div + div + div > button + br + b > a")
  button(:upload_now, id: "btnUpload")
  file_field(:creditcard_front, id: "front_file")
  file_field(:creditcard_back, id: "back_file")
  file_field(:creditcard_govt, id: "photo_file")

  # Methods
  def upload_images_later
    upload_later_element.wait_until_present(timeout: 120)
    upload_later
  end

  def upload_images_now
    upload_now_element.wait_until_present(timeout: 120)
    display_upload_fields
    creditcard_front = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_front)
    creditcard_back = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_back)
    creditcard_govt = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_creditcard_govid)
    upload_now
    begin
      tries ||= 5
      preloader_element.wait_while_present(timeout: 300)
      @browser.refresh
      rsvsn_header_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
  rescue
    raise error_encountered_element.exists? ? "Page error: " + error_page : "Unexpected error found in page. Please check."
  end

  def imageupload_form_copy
    copy = []
    for i in 1..7
      i == 7 ? self.class.div(:onhold_key, xpath: "//*[@id='details-guest']/div[#{(i - 1)}]") : self.class.paragraph(:onhold_key, xpath: "//*[@id='details-guest']/p[#{i}]")
      wait_until(10, "Reservation has been redirected back to the Payment Page. Please check.") { onhold_key_element.present? }
      copy[i] = onhold_key_element.text
    end
    copy = copy.join("\n")
    return copy
  end

  private

  def display_upload_fields
    script = <<-JS
      document.getElementsByName('CCF')[0].style.display=''
      document.getElementsByName('CCB')[0].style.display=''
      document.getElementsByName('IDF')[0].style.display=''
    JS
    @browser.execute_script(script)
  end
end
