# Desktop Booking Engine Payment Page
class PayPage
  include PageObject
  include DataLookup
  include BookingEngineHelper

  INVALID_EMAIL = "a@a..com"
  INVALID_CC_NUMBER = "0"
  INVALID_CC_SECCODE = "1"
  INVALID_NAME = "12345678901234567890123456789012345678901234567890123456789012345678901234567890"

  # Element Locators
  div(:popup_securitycode, id: "securemodal")
  div(:popup_preauth, css: "div#idPreAUTHFAQs > div.modal-body")
  div(:popup_cardownerfraud, id: "ccfraudmodal")
  div(:copy_noprepay, id: "total-deposit")
  div(:copy_securitycode, xpath: "//*[@id='securemodal']/div[2]")
  div(:copy_cardnumbersecurity, xpath: "//div[@id='secmodal']/div[2]")
  div(:copy_cardownerfraud, xpath: "//div[@id='ccfraudmodal']/div[2]")
  div(:error_details, id: "error-message")
  div(:error_cc, id: "ctypeerror")
  span(:termspopup, xpath: "//span[contains(., 'Terms of Reservation')]")
  span(:termspopup_hpp_booking, xpath: "//input[@id='pa-cc-payment-agree']/following::a/span[contains(., 'Terms of Reservation')]")
  span(:error_firstname, id: "firstnameerror")
  span(:error_lastname, id: "lastnameerror")
  span(:error_email, id: "emailerror_req")
  span(:error_country, id: "countryerror")
  span(:error_phone, id: "mobileerror")
  span(:error_guests, id: "occuerror_num")
  span(:error_cc_type, id: "carderror")
  span(:error_cc_number, id: "cnumbererror")
  span(:error_cc_cardowner_option, id: "ccnameopterror")
  span(:error_cc_cardowner_name, id: "fnameerror")
  span(:error_cc_expiry_month, id: "experror1")
  span(:error_cc_expiry_year, id: "experror2")
  span(:error_cc_seccode, id: "seccodeerror")
  span(:error_accept_terms, id: "accepterror")
  span(:error_email_invalid, id: "emailerror_inv")
  span(:error_email_doesnotmatch, id: "emailerror_match")
  span(:error_email_confirm, id: "cemailerror")
  span(:error_cc_invalid, id: "invalidcc")
  link(:cta_public, id: "btnconfirmbooking")
  link(:cta_private, id: "pa-cc-btnconfirmbooking")
  link(:cta_account, id: "pa-acct-btnconfirmbooking")
  link(:cta_paypal_cc, id: "btnCheckoutPaypalCC")
  link(:cta_paypal_account, id: "btnCheckoutPaypal")
  link(:cta_bancnet, id: "btnBancnet")
  link(:toggle_creditcard_dwh, xpath: "//a[contains(., 'Pay with Credit')][1]")
  link(:toggle_creditcard_hpp, xpath: "//a[contains(., 'Charge to Credit')][1]")
  link(:toggle_account, xpath: "//a[contains(., 'Charge to Account')][1]")
  link(:toggle_paypal, xpath: "//a[contains(., 'Pay with PayPal')][1]")
  link(:securitycode_open, id: "securecode")
  link(:securitycode_close, css: "div#securemodal > div > a")
  link(:cardnumbersecurity_open, id: "sec_cc")
  link(:cardnumbersecurity_close, css: "div#secmodal > div > a")
  link(:preauth_open, id: "idPreAUTHFAQ")
  link(:preauth_close, css: "div#idPreAUTHFAQs > div > a")
  link(:cardownerfraud_open, id: "fraud_cc")
  link(:cardownerfraud_close, css: "div#ccfraudmodal > div > a")
  link(:onhold_confirm, id: "ccfraud_btn_continue")
  link(:onhold_close, id: "ccfraud_btn_close")
  link(:view_private_cc, id: "btn-charge-to-credit-card")
  span(:error_payment, xpath: "//strong[contains(., 'Your payment could not be processed.')]")
  element(:payoption_cc, xpath: "//select[@id='payment_cc_type']/optgroup[@data-group='creditcard']")
  element(:payoption_bancnet, xpath: "//select[@id='payment_cc_type']/optgroup[@data-group='debitcard']")
  element(:payoption_mastercard, xpath: "//select[@id='payment_cc_type']/option[@value='MC']")
  element(:payoption_visa, xpath: "//select[@id='payment_cc_type']/option[@value='VI']")
  element(:payoption_amex, xpath: "//select[@id='payment_cc_type']/option[@value='AX']")
  element(:payoption_jcb, xpath: "//select[@id='payment_cc_type']/option[@value='JC']")
  element(:payoption_electron, xpath: "//select[@id='payment_cc_type']/option[@value='VE']")
  checkbox(:accept_terms_default_public, id: "payment_agree")
  checkbox(:accept_terms_default_private, id: "pa-cc-payment-agree")
  checkbox(:accept_terms_default_account, id: "pa-acct-payment-agree")
  textarea(:special_request, id: "payment_specialrequest")
  paragraph(:copy_digicert, class: "footnote")
  paragraph(:error_processing, xpath: "//div[@class='span12']/p")
  text_field(:first_name, id: "payment_first_name")
  text_field(:last_name, id: "payment_last_name")
  text_field(:email, id: "payment_email")
  text_field(:email_confirm, id: "payment_email_confirm")
  text_field(:phone, id: "payment_contact_number")
  text_field(:cc_number, id: "payment_cc_number")
  text_field(:cc_cardowner, id: "payment_cc_name")
  text_field(:cc_seccode, id: "payment_cc_cvv")
  select_list(:cc_type, id: "payment_cc_type")
  select_list(:cc_expiry_month, id: "payment_cc_exp_month")
  select_list(:cc_expiry_year, id: "payment_cc_exp_year")
  select_list(:country, id: "payment_country")
  select_list(:adult, id: "payment_adults")
  select_list(:children, id: "payment_children")
  select_list(:eta, id: "payment_eta")
  radio_button(:cc_cardowner_yes, id: "opt_yes")
  radio_button(:cc_cardowner_no, id: "opt_no")

  # Methods
  def input_guest_details(rateplan, payment_type="credit card", no_occupants=0)
    wait_for_cta(rateplan, payment_type)
    first_name = get_guest_firstname(rateplan)
    last_name = get_guest_lastname(rateplan)
    email = FigNewton.email
    email_confirm = FigNewton.email
    country_element.select_value(FigNewton.country)
    phone = FigNewton.phone
    input_occupants(rateplan, no_occupants)
    special_request = FigNewton.special_request
  end

  def input_creditcard_details(rateplan, payment_type="credit card")
    unless payment_type.include? "account"
      wait_for_cta(rateplan, payment_type)
      if rateplan.include? "bdo"
        cc_type_element.select_value(FigNewton.bdo_cc_type)
        cc_number = FigNewton.bdo_cc_number
        cc_expiry_month_element.select_value(FigNewton.bdo_cc_expiry_month)
        cc_expiry_year_element.select_value(FigNewton.bdo_cc_expiry_year)
        cc_seccode = FigNewton.bdo_cc_seccode
      else
        cc_type_element.select_value(FigNewton.cc_type)
        cc_number = FigNewton.cc_number
        cc_expiry_month_element.select_value(FigNewton.cc_expiry_month)
        cc_expiry_year_element.select_value(FigNewton.cc_expiry_year)
        cc_seccode = FigNewton.cc_seccode
      end
      rateplan.include?("onhold") ? change_cardowner_name : select_cc_cardowner_yes
    end
  end

  def accept_terms_and_conditions(rateplan, payment_type="credit card")
    rateplan = rateplan.downcase
    if rateplan.include?("public") || rateplan.include?("error") || rateplan.include?("availability")
      check_accept_terms_default_public
    else
      payment_type.include?("account") ? check_accept_terms_default_account : check_accept_terms_default_private
    end
  end

  def confirm_reservation(rateplan, payment_type="credit card", allow_paymentfailure=false)
    wait_for_cta(rateplan, payment_type)
    click_cta(rateplan, payment_type, allow_paymentfailure)
  end

  def change_cardowner_name
    select_cc_cardowner_no if cc_cardowner_yes?
    cc_cardowner = FigNewton.cc_cardowner
  end

  def input_invalid_email
    email_element.clear
    email = INVALID_EMAIL
  end

  def input_invalid_card
    cc_number_element.clear
    cc_number = INVALID_CC_NUMBER
  end

  def input_invalid_seccode
    cc_seccode_element.clear
    cc_seccode = INVALID_CC_SECCODE
  end

  def input_invalid_guest
    first_name_element.clear
    first_name = INVALID_NAME
    last_name_element.clear
    last_name = INVALID_NAME
  end

  def payment_page_exist
    return @browser.url.include? "reservation/pay"
  end

  def digicert_copy
    copy_digicert_element.wait_until_present(timeout: 120)
    return copy_digicert
  end

  def noprepay_copy
    copy_noprepay_element.wait_until_present(timeout: 120)
    return copy_noprepay
  end

  def securitycode_copy
    begin
      tries ||= 3
      securitycode_open_element.wait_until_present(timeout: 120)
      securitycode_open
      copy_securitycode_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    return copy_securitycode
  end

  def cardnumbersecurity_copy
    begin
      tries ||= 3
      cardnumbersecurity_open_element.wait_until_present(timeout: 120)
      cardnumbersecurity_open
      copy_cardnumbersecurity_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    return copy_cardnumbersecurity
  end

  def cardownerfraud_copy
    begin
      tries ||= 3
      cardownerfraud_open_element.wait_until_present(timeout: 120)
      cardownerfraud_open
      copy_cardownerfraud_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    return copy_cardownerfraud
  end

  def preauth_copy
    preauth_open_element.wait_until_present(timeout: 120)
    preauth_open
    copy = []
    for i in 1..5
      copy_key = i.to_s
      self.class.list_item(:preauth_key, xpath: "//*[@id='idPreAUTHFAQs']/div[2]/div/ol/li[#{copy_key}]")
      preauth_key_element.wait_until_present(timeout: 120)
      copy[i] = preauth_key_element.text
      copy[i] = copy[i].gsub("\u0080\u0099", "")
    end
    copy = copy.join("\n")
    return copy
  end

  def termsofreservation_copy(rateplan, payment_type="credit card")
    terms_id = "tTerms2"
    terms_id = "ca_#{terms_id}" if rateplan.include?("hpp") && rateplan.include?("private") && payment_type.include?("account")
    copy = []
    begin
      tries ||= 3
      open_rsvsnterms_popup(rateplan, payment_type)
      for i in 1..12
        copy_key = i.to_s
        self.class.paragraph(:terms_key, xpath: "//div[@id='#{terms_id}']/p[#{copy_key}]")
        terms_key_element.wait_until_present(timeout: 120)
        copy[i] = terms_key_element.text
      end
    rescue
      retry unless (tries -= 1).zero?
    end
    copy = copy.join("\n")
    return copy
  end

  def input_missingdetails_copy(rateplan)
    error_firstname_element.wait_until_present(timeout: 120)
    copy = Array[
      error_firstname,
      error_lastname,
      error_email,
      error_country,
      error_phone,
      error_guests,
      error_cc_type,
      error_cc_number,
      error_cc_cardowner_option,
      error_cc_cardowner_name,
      error_cc_expiry_month,
      error_cc_expiry_year,
      error_cc_seccode,
      error_accept_terms,
      error_details
    ]
    copy = copy.join("\n")
    return copy
  end

  def invalid_email_copy
    copy = Array[
      error_email_invalid,
      error_email_doesnotmatch,
      error_email_confirm,
      error_details
    ]
    copy = copy.join("\n")
    return copy
  end

  def invalid_card_copy
    return error_cc_invalid
  end

  def default_invalid_rsvsn_copy
    return error_cc
  end

  private

  def wait_for_cta(rateplan, payment_type)
    rateplan = rateplan.downcase
    if rateplan.include?("public") || rateplan.include?("error") || rateplan.include?("availability")
      cta_public_element.wait_until_present(timeout: 120)
    else
      if payment_type.include? "account"
        cta_account_element.wait_until_present(timeout: 120)
      else
        open_view(rateplan, payment_type)
        cta_private_element.wait_until_present(timeout: 120)
      end
    end
  end

  def click_cta(rateplan, payment_type, allow_paymentfailure)
    rateplan = rateplan.downcase
    if rateplan.include?("public") || rateplan.include?("error") || rateplan.include?("availability")
      cta_public_element.focus
      cta_public
    else
      if payment_type.include? "account"
        cta_account_element.focus
        cta_account
      else
        cta_private_element.focus
        cta_private
      end
    end
    if rateplan.include? "onhold"
      onhold_confirm_element.wait_until_present(timeout: 120)
      onhold_confirm_element.focus
      onhold_confirm
    end
    handle_alert
    loading_element.wait_while_present(timeout: 300)
    raise "Reservation has been redirected back to the Payment Page. Please check." if !rateplan.include?("onhold") && @browser.html.include?("Your payment could not be processed.") && !allow_paymentfailure
  end

  def input_occupants(rateplan, no_occupants)
    rateplan = rateplan.downcase
    if rateplan.include?("public") || rateplan.include?("error") || rateplan.include?("availability")
      adult_element.select_value(FigNewton.adult)
      if children_element.exists?
        children_element.wait_until_present(timeout: 120)
        children_element.select_value(FigNewton.children)
      end
      if FigNewton.env_type.include? "dev"
        for i in 0..(no_occupants - 1) do
          self.class.text_field(:occupant_firstname, name: "reservation_room_occupants[#{i}][first_name]")
          occupant_firstname = get_guest_firstname(rateplan) + (i + 1).to_s
          self.class.text_field(:occupant_lastname, name: "reservation_room_occupants[#{i}][last_name]")
          occupant_lastname = get_guest_lastname(rateplan)
          self.class.select_list(:private_adult, name: "reservation_room_occupants[#{i}][adult_count]")
          private_adult_element.select_value(FigNewton.adult)
          self.class.select_list(:private_children, name: "reservation_room_occupants[#{i}][child_count]")
          if private_children_element.exists?
            private_children_element.wait_until_present(timeout: 120)
            private_children_element.select_value(FigNewton.children)
          end
        end
      end
    else
      for i in 0..(no_occupants - 1) do
        self.class.text_field(:occupant_firstname, name: "reservation_room_occupants[#{i}][first_name]")
        occupant_firstname = get_guest_firstname(rateplan) + (i + 1).to_s
        self.class.text_field(:occupant_lastname, name: "reservation_room_occupants[#{i}][last_name]")
        occupant_lastname = get_guest_lastname(rateplan)
        self.class.select_list(:private_adult, name: "reservation_room_occupants[#{i}][adult_count]")
        private_adult_element.select_value(FigNewton.adult)
        self.class.select_list(:private_children, name: "reservation_room_occupants[#{i}][child_count]")
        if private_children_element.exists?
          private_children_element.wait_until_present(timeout: 120)
          private_children_element.select_value(FigNewton.children)
        end
      end
    end
  end

  def open_view(rateplan, payment_type)
    if rateplan.include?("private") && !rateplan.include?("dwh") && !payment_type.include?("account")
      view_private_cc_element.wait_until_present(timeout: 120)
      view_private_cc
    end
  end

  def open_rsvsnterms_popup(rateplan, payment_type)
    if rateplan.include?("private") && !rateplan.include?("dwh") && !payment_type.include?("account")
      open_view(rateplan, payment_type)
      termspopup_hpp_booking_element.wait_until_present(timeout: 120)
      @browser.execute_script("window.scrollBy(0,1000)")
      termspopup_hpp_booking_element.focus
      termspopup_hpp_booking_element.click
    else
      termspopup_element.wait_until_present(timeout: 120)
      termspopup_element.click
    end
  end
end
