# Desktop Booking Engine Policies Page
class PoliciesPage
  include PageObject
  include DataLookup
  include BookingEngineHelper
  include DataTransformHelper

  page_url "#{FigNewton.policies_url}/<%=params[:id]%>/<%=params[:rateplan_id]%>/0/1/0/0/<%=params[:arrival_date]%>/<%=params[:departure_date]%>/en/0"

  # Element Locators
  paragraph(:policy_header, xpath: "//div[@id='rpPolicy']/p")
  list_item(:policy_arrival, xpath: "//div[@id='rpPolicy']//li[contains(., 'Arrival')][1]")
  list_item(:policy_departure, xpath: "//div[@id='rpPolicy']//li[contains(., 'Departure')][1]")
  list_item(:policy_children, xpath: "//div[@id='rpPolicy']//li[contains(., 'Children')][1]")
  list_item(:policy_prepayment, xpath: "//div[@id='rpPolicy']//li[contains(., 'Prepayment')][1]")
  list_item(:policy_modification, xpath: "//div[@id='rpPolicy']//li[contains(., 'Modification')][1]")
  list_item(:policy_cancellation, xpath: "//div[@id='rpPolicy']//li[contains(., 'Cancellation')][1]")
  list_item(:policy_noshow, xpath: "//div[@id='rpPolicy']//li[contains(., 'No-show')][1]")
  list_item(:policy_refund, xpath: "//div[@id='rpPolicy']//li[contains(., 'Refund')][1]")
  list_item(:policy_paypal, xpath: "//div[@id='rpPolicy']//li[contains(., 'PayPal')][1]")
  list_item(:policy_cardfraud, xpath: "//div[@id='rpPolicy']//li[contains(., 'Card Fraud')][1]")

  # Methods
  def policies_copy
    copy = Array[
      policy_arrival,
      policy_departure,
      policy_children,
      policy_prepayment,
      policy_modification,
      policy_cancellation,
      policy_noshow,
      get_detail(policy_refund_element),
      get_detail(policy_paypal_element),
      policy_cardfraud
    ]
    copy = copy.join("\n")
    return copy
  end
end
