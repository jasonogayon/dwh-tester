# Desktop Booking Engine SelectDates Page
class SelectDatesPage
  include PageObject
  include BookingEngineHelper

  page_url "#{FigNewton.selectdates_url}/<%=params[:id]%>"

  # Element Locators
  div(:promocode_success, id: "promo-success-icon")
  link(:showflexi, id: "flexible")
  link(:promocode, id: "promo-code-link")
  button(:cta, id: "btnNext")
  text_field(:promocode_input, id: "promo-code-textbox")

  # Methods
  def goto_showrooms
    cta_element.wait_until_present(timeout: 120)
    cta
    handle_alert
  end

  def goto_showflexi
    showflexi
  end

  def input_promocode(promocode)
    promocode
    promocode_input = promocode ||= FigNewton.promo_code
    promocode_success_element.wait_until_present(timeout: 120) unless (neo == :true || mobile == :true)
  end

  def promocode_copy
    return promocode_input_element.value
  end
end
