# Desktop Booking Engine PayInfo Page
class PayInfoPage
  include PageObject
  include BookingEngineHelper
  include DataTransformHelper

  # Element Locators
  link(:cta, id: "btnguestdetails")
  cell(:arrival, xpath: "//td[contains(., 'Arrival')]/following::td[1]")
  cell(:departure, xpath: "//td[contains(., 'Departure')]/following::td[1]")
  cell(:no_nights, xpath: "//td[contains(., 'Length of stay')]/following::td[1]")
  cell(:total_room_charge, xpath: "//div[@id='total-room-cost']//td[contains(., 'Room Cost')]/following::td[1]")
  cell(:total_tax, xpath: "//td[contains(., 'Taxes')]/following::td[1]")
  cell(:total_fee, xpath: "//td[contains(., 'Fees')]/following::td[1]")
  cell(:total_addon_charges, xpath: "//td[contains(., 'Total Add-on Charges')]/following::td[1]")
  cell(:total_reservation_cost, xpath: "//td[contains(., 'Total Reservation Cost')]/following::td[1]")
  cell(:prepayment, xpath: "//td[contains(., 'PREPAYMENT')]/following::td[1]")
  cell(:balance_payable, xpath: "//td[contains(., 'PAYABLE AT THE HOTEL')]/following::td[1]")
  cell(:modification_charge, xpath: "//td[contains(., 'MODIFICATION CHARGE')]/following::td[1]")
  cell(:refund, xpath: "//td[contains(., 'Refund')]/following::td[1]")
  cell(:policy_prepayment, xpath: "//td[contains(., 'Prepayment')]/following::td[1]")
  cell(:policy_modification, xpath: "//td[contains(., 'Modification')]/following::td[1]")
  cell(:policy_cancellation, xpath: "//td[contains(., 'Cancellation')]/following::td[1]")
  cell(:toggle_addons, xpath: "//td[contains(.,'Enhance your stay with these Add-ons')]")

  # Methods
  def goto_payment
    cta_element.wait_until_present(timeout: 120)
    cta_element.focus
    cta
    handle_alert
  end

  def include_addons(addons)
    unless addons.nil? || addons.zero?
      # Pending: Add addons
    end
  end

  def get_rsvsn_charges(rateplan, arrival, rsvsn_type="confirmed", previous_rsvsn_charges=nil)
    arrival_element.wait_until_present(timeout: 120)
    @browser.execute_script("window.scrollBy(0,300)")
    rsvsn_charges = Hash[
      rateplan:                     rateplan,
      arrival:                      arrival,
      no_days_until_checkin:        get_no_of_days_until_checkin(arrival, previous_rsvsn_charges),
      total_room_charge:            get_detail(total_room_charge_element),
      total_tax:                    get_detail(total_tax_element),
      total_fee:                    get_detail(total_fee_element),
      total_addon_cost:             get_detail(total_addon_charges_element),
      total_reservation_cost:       get_detail(total_reservation_cost_element),
      prepayment:                   convert(rsvsn_type, get_detail(prepayment_element)),
      prepayment_he_display:        get_prepayment_he_display(rateplan),
      balance_payable:              convert(rsvsn_type, get_detail(balance_payable_element)),
      balance_payable_he_display:   get_detail(balance_payable_element),
      penalty:                      get_detail(modification_charge_element),
      refund:                       get_detail(refund_element),
      policy_prepayment:            policy_prepayment,
      policy_modification:          policy_modification,
      policy_cancellation:          policy_cancellation,
      no_nights:                    no_nights.split(" ")[0],
      percent_commission:           get_percent_commission(rateplan)
    ]

    # Compute penalty and refund since refund value not displayed in page
    refund = Hash[
      penalty:                      get_penalty(rsvsn_type, rsvsn_charges),
      penalty_he_display:           get_penalty(rsvsn_type, rsvsn_charges, true),
      refund:                       get_refund(rsvsn_type, rsvsn_charges),
      refund_email_display:         get_refund(rsvsn_type, rsvsn_charges, true)
    ]
    rsvsn_charges.update(refund)

    # Update penalty and refund for modified reservations
    if rsvsn_type.include? "modif"
      refund = Hash[
        penalty:                    get_penalty(rsvsn_type, previous_rsvsn_charges),
        penalty_he_display:         get_penalty(rsvsn_type, previous_rsvsn_charges, true),
        refund:                     get_refund(rsvsn_type, previous_rsvsn_charges),
        refund_email_display:       get_refund(rsvsn_type, previous_rsvsn_charges, true)
      ]
      rsvsn_charges.update(refund)
    end

    # Compute expected DWH revenue
    dwh_revenue = Hash[dwh_revenue: get_dwh_revenue(rsvsn_type, rsvsn_charges, previous_rsvsn_charges)]
    rsvsn_charges.update(dwh_revenue)

    # Compute expected Hotel revenue
    hotel_revenue = Hash[hotel_revenue: get_hotel_revenue(rsvsn_type, rsvsn_charges)]
    rsvsn_charges.update(hotel_revenue)
    return rsvsn_charges
  end

  def recompute_rsvsn_charges(rsvsn_type, rsvsn_charges, previous_rsvsn_charges)
    if rsvsn_type.include?("cancel") || rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show")
      prepayment = nil
      balance_payable = nil
    end
    new_rsvsn_charges = Hash[
      prepayment:             prepayment,
      balance_payable:        balance_payable,
      penalty:                get_penalty(rsvsn_type, rsvsn_charges),
      penalty_he_display:     get_penalty(rsvsn_type, rsvsn_charges, true),
      refund:                 get_refund(rsvsn_type, rsvsn_charges),
      refund_email_display:   get_refund(rsvsn_type, rsvsn_charges, true)
    ]
    rsvsn_charges.update(new_rsvsn_charges)

    # Compute expected DWH revenue
    dwh_revenue = Hash[dwh_revenue: get_dwh_revenue(rsvsn_type, rsvsn_charges, previous_rsvsn_charges)]
    rsvsn_charges.update(dwh_revenue)

    # Compute expected Hotel revenue
    hotel_revenue = Hash[hotel_revenue: get_hotel_revenue(rsvsn_type, rsvsn_charges)]
    rsvsn_charges.update(hotel_revenue)
    return rsvsn_charges
  end

  private

  def get_no_of_days_until_checkin(arrival_date, previous_rsvsn_charges)
    return (Date.parse(arrival_date) - DateTime.now).to_i
  rescue
    return previous_rsvsn_charges[:no_days_until_checkin]
  end

  def get_percent_commission(rateplan)
    percent_commission = FigNewton.dwh_percent_commission if rateplan.include? "dwh"
    percent_commission = FigNewton.hpp_percent_commission if rateplan.include? "hpp"
    percent_commission = FigNewton.bdo_percent_commission if rateplan.include? "bdo"
    percent_commission ||= FigNewton.paypal_percent_commission
    return percent_commission
  end

  def get_refund(rsvsn_type, rsvsn_charges, isfor_email_display=false)
    rateplan = rsvsn_charges[:rateplan]
    no_days_until_checkin = rsvsn_charges[:no_days_until_checkin]
    prepayment_ibe = rsvsn_charges[:prepayment].to_f
    prepayment_he = rsvsn_charges[:prepayment_he_display].to_f
    total_rsvsn_cost = rsvsn_charges[:total_reservation_cost].to_f
    no_nights = rsvsn_charges[:no_nights].to_f
    if rateplan.include?("dwh") && !rsvsn_type.include?("confirm")
      if rateplan.include? "full_yesr"
        refund = total_rsvsn_cost * 0.7 if rateplan.include?("_lt") && no_days_until_checkin < 3
        refund = total_rsvsn_cost / no_nights.to_f if rateplan.include? "fnei"
        refund = total_rsvsn_cost if rateplan.include?("nc") || (rateplan.include?("_lt") && no_days_until_checkin >= 3)
        refund ||= 0
      elsif rateplan.include?("partial") && rsvsn_type.include?("modif")
        refund = prepayment_ibe
      end
    elsif isfor_email_display && rateplan.include?("bdo") && !rsvsn_type.include?("confirm")
      refund = prepayment_he if rateplan.include?("_lt") && no_days_until_checkin >= 3
    end
    return refund.nil? ? nil : sprintf("%.2f", refund)
  end

  def get_penalty(rsvsn_type, rsvsn_charges, isfor_he_display=false)
    rateplan = rsvsn_charges[:rateplan]
    no_days_until_checkin = rsvsn_charges[:no_days_until_checkin]
    prepayment = rsvsn_charges[:prepayment_he_display].to_f
    total_rsvsn_cost = rsvsn_charges[:total_reservation_cost].to_f
    no_nights = rsvsn_charges[:no_nights].to_f
    unless rsvsn_type.include?("confirm")
      if rsvsn_type.include?("modif") && rsvsn_charges[:rateplan].include?("dwh")
        return get_detail(modification_charge_element)
      else
        penalty = prepayment if rateplan.include?("_lt") && no_days_until_checkin < 3 && rateplan.include?("bdo")
        penalty = total_rsvsn_cost * 0.3 if rateplan.include?("_lt") && no_days_until_checkin < 3 && !rateplan.include?("bdo")
        penalty = total_rsvsn_cost / no_nights if rateplan.include?("fnei") || rateplan.include?("fn_notr_10ei") || (rateplan.include?("fn_notr_notallowed") && (rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show") || rateplan.include?("bdo")))
        penalty = total_rsvsn_cost if rateplan.include?("fcei") || rateplan.include?("full_notr_10ei") || (rateplan.include?("full_notr_notallowed") && (rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show") || rateplan.include?("bdo")))
        penalty = total_rsvsn_cost * 0.1 if rateplan.include?("10p_notr_10ei") || (rateplan.include?("10p_notr_notallowed") && (rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show") || (rateplan.include?("bdo") && !rateplan.include?("private"))))
        penalty = nil if !isfor_he_display && rsvsn_charges[:rateplan].include?("bdo")
      end
    end
    return penalty.nil? ? nil : sprintf("%.2f", penalty)
  end

  def get_prepayment_he_display(rateplan)
    return rateplan.include?("_pa_") ? "0.00" : get_detail(prepayment_element)
  end

  def get_dwh_revenue(rsvsn_type, rsvsn_charges, previous_rsvsn_charges)
    rateplan = rsvsn_charges[:rateplan]
    total_room_charge = rsvsn_charges[:total_room_charge].to_f
    prepayment = rsvsn_charges[:prepayment_he_display].to_f
    penalty = rsvsn_charges[:penalty].to_f
    percent_commission = (rsvsn_charges[:percent_commission].to_f / 100).round(2)
    if rsvsn_type.include?("cancel") || rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show")
      penalty = (penalty / 1.166).round(2).round(1) * percent_commission
      dwh_revenue = prepayment if rateplan.include?("partial")
      dwh_revenue ||= penalty
    elsif rsvsn_type.include? "modif"
      dwh_revenue = (total_room_charge + (penalty / 1.166).round(2)) * percent_commission if previous_rsvsn_charges[:rateplan].include?("dwh") && previous_rsvsn_charges[:rateplan].include?("full") && ((previous_rsvsn_charges[:rateplan].include?("_lt") && previous_rsvsn_charges[:no_days_until_checkin] < 3) || previous_rsvsn_charges[:rateplan].include?("fnei"))
    end
    dwh_revenue ||= total_room_charge * percent_commission
    return sprintf("%.2f", dwh_revenue)
  end

  def get_hotel_revenue(rsvsn_type, rsvsn_charges)
    rateplan = rsvsn_charges[:rateplan]
    total_rsvsn_cost = rsvsn_charges[:total_reservation_cost].to_f
    penalty = rsvsn_charges[:penalty].to_f
    dwh_revenue = rsvsn_charges[:dwh_revenue].to_f
    hotel_revenue = penalty - dwh_revenue if (rsvsn_type.include?("cancel") || rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show")) && !(rateplan.include?("partial") || rateplan.include?("_pa_") || rateplan.include?("bdo"))
    hotel_revenue ||= total_rsvsn_cost - dwh_revenue
    hotel_revenue = nil if (rsvsn_type.include?("cancel") || rsvsn_type.include?("noshow") || rsvsn_type.include?("no-show")) && (rateplan.include?("partial") || rateplan.include?("_pa_") || rateplan.include?("bdo"))
    return hotel_revenue.nil? ? nil : sprintf("%.2f", hotel_revenue)
  end
end
