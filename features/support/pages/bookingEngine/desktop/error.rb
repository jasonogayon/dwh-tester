# Destkop Booking Engine Error Page
class ErrorPage
  include PageObject
  include BookingEngineHelper

  # Element Locators
  paragraph(:error_prepayment, css: "div.span12 > p")

  # Methods
  def error_page_exist
    return @browser.url.include?("reservation/paymentError")
  end

  def prepayment_error_copy
    error_prepayment_element.wait_until_present(timeout: 120)
    return error_prepayment
  end

  def php_error_copy
    error_php_element.wait_until_present(timeout: 120)
    return error_php
  end
end
