# Desktop Booking Engine Update Credit Card Details Page
class UpdateCreditCardDetailsPage < PayPage
  include PageObject
  include BookingEngineHelper

  page_url "#{FigNewton.update_cc_url}/<%=params[:id]%>/<%=params[:rsvsn_id]%>/<%=params[:rsvsn_hash]%>/en/1"

  # Element Locators
  h2(:page_header, xpath: "//h2")
end
