# Desktop Booking Engine Confirmation Page
class ConfirmationPage
  include PageObject
  include BookingEngineHelper
  include DataTransformHelper

  page_url "#{FigNewton.details_url}/<%=params[:id]%>/<%=params[:rsvsn_id]%>/<%=params[:rsvsn_hash]%>/en/2"

  # Element Locators
  h2(:rsvsn_header, xpath: "//h2")
  div(:rsvsn_meal_name, xpath: "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Meal')][1]")
  div(:rsvsn_meal_description, xpath: "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Meal Description')]/following::div[1]")
  div(:rsvsn_specialrequest, xpath: "//*[@id='reservation-details']/div/table/tbody/tr/td[1]/div/div[contains(., 'Special Request')][1]")
  div(:copy_modify_popup, xpath: "//*[@id='modifyDialog']/div[2]/div[2]")
  cell(:confirmation_no, xpath: "//td[contains(., 'Confirmation Number')]/following::td[1]")
  cell(:transaction_id, xpath: "//td[contains(., 'Transaction ID')]/following::td[1]")
  cell(:guest_name, xpath: "//td[contains(., 'Guest Name')]/following::td[1]")
  cell(:guest_email, xpath: "//td[contains(., 'Email Address')]/following::td[1]")
  cell(:guest_mobile, xpath: "//td[contains(., 'Mobile Number')]/following::td[1]")
  cell(:guest_country, xpath: "//td[contains(., 'Country')]/following::td[1]")
  cell(:paypal_owner, xpath: "//td[contains(., 'PayPal Account Owner')]/following::td[1]")
  cell(:paypal_email, xpath: "//td[contains(., 'PayPal Account Email')]/following::td[1]")
  cell(:paypal_mobile, xpath: "//td[contains(., 'PayPal Account Mobile')]/following::td[1]")
  cell(:private_account, xpath: "//td[contains(., 'Private Account')]/following::td[1]")
  cell(:private_promocode, xpath: "//td[contains(., 'Promo Code')]/following::td[1]")
  cell(:creditcard_owner, xpath: "//td[contains(., 'Name of Card Owner')]/following::td[1]")
  cell(:creditcard_number, xpath: "//td[contains(., 'Card Number')]/following::td[1]")
  cell(:creditcard_expiry, xpath: "//td[contains(., 'Credit Card Expiry')]/following::td[1]")
  cell(:rsvsn_madeon, xpath: "//td[contains(., 'Reservation Made On')]/following::td[1]")
  cell(:rsvsn_modifiedon, xpath: "//td[contains(., 'Reservation Modified On')]/following::td[1]")
  cell(:rsvsn_cancelledon, xpath: "//td[contains(., 'Reservation Cancelled On')]/following::td[1]")
  cell(:rsvsn_details, xpath: "//td[contains(., 'Reservation Details')]/following::td[1]")
  cell(:rsvsn_eta, xpath: "//td[contains(., 'Estimated Time of Arrival')]/following::td[1]")
  cell(:rsvsn_checkin, xpath: "//td[contains(., 'Check In')]/following::td[1]")
  cell(:rsvsn_checkout, xpath: "//td[contains(., 'Check Out')]/following::td[1]")
  cell(:charge_modification, xpath: "//td[contains(., 'Modification Charge')]/following::td[1]")
  cell(:charge_cancellation, xpath: "//td[contains(., 'Cancellation Charge')]/following::td[1]")
  cell(:charge_noshow, xpath: "//td[contains(., 'No Show Charge')]/following::td[1]")
  cell(:charge_room_charge, xpath: "//td[contains(., 'Room Cost')]/following::td[1]")
  cell(:charge_total_tax, xpath: "//td[contains(., 'Taxes')]/following::td[1]")
  cell(:charge_total_fee, xpath: "//td[contains(., 'Fees')]/following::td[1]")
  cell(:charge_total_addon_cost, xpath: "//td[contains(., 'Total Add-on Charges')]/following::td[1]")
  cell(:charge_total_reservation_cost, xpath: "//td[contains(., 'Total Reservation Cost')]/following::td[1]")
  cell(:charge_prepayment, xpath: "//td[contains(., 'Prepayment')]/following::td[1]")
  cell(:charge_balance_payable, xpath: "//td[contains(., 'Payable at the Hotel')]/following::td[1]")
  cell(:charge_refund, xpath: "//td[contains(., 'Refund')]/following::td[1]")
  h3(:toggle_details, xpath: "//h3[contains(.,'Reservation Details')]")
  h3(:toggle_policies, xpath: "//h3[contains(.,'Hotel Policies')]")
  link(:make_new_reservation, xpath: "//a[contains(.,'Make a new reservation')]")
  link(:save, id: "savepage")
  link(:print, id: "printpage")
  link(:modify_datesrooms, id: "conModify")
  link(:modify_datesrooms_yes, id: "btnModifyYes")
  link(:modify_datesrooms_no, id: "btnModifyNo")
  link(:modify_guestdetails, id: "modifyguest")
  link(:include_or_modify_addons, id: "modify-addon")
  link(:cancel, id: "conCancel")
  link(:cancel_yes, id: "cancelYes")
  link(:cancel_no, css: "a#cancelYes + a")
  link(:submit_cc_info, id: "submitcc")
  paragraph(:policy_header, xpath: "//*[@id='reservation-hotel-policies']/p[1]")
  paragraph(:policy_cardfraud, xpath: "//*[@id='reservation-hotel-policies']//p[contains(., 'Fraud')][1]/following::p[1]")
  paragraph(:copy_cancel_popup, xpath: "//*[@id='cancelDialog']//p[4]")
  list_item(:policy_arrival, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Arrival')][1]")
  list_item(:policy_departure, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Departure')][1]")
  list_item(:policy_children, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Children')][1]")
  list_item(:policy_prepayment, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Prepayment')][1]")
  list_item(:policy_modification, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Modification')][1]")
  list_item(:policy_cancellation, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Cancellation')][1]")
  list_item(:policy_noshow, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'No-show')][1]")
  list_item(:policy_refund, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'Refund')][1]")
  list_item(:policy_paypal, xpath: "//*[@id='reservation-hotel-policies']//li[contains(., 'PayPal')][1]")
  list_item(:rsvsn_adults, xpath: "//*[@id='reservation-details']//ul[contains(., 'Adult')][1]/li[1]")
  list_item(:rsvsn_children, xpath: "//*[@id='reservation-details']//ul[contains(., 'Adult')][1]/li[2]")

  # Methods
  def open_details_and_policies_toggles
    @browser.execute_script("window.scrollBy(0,1000)")
    toggle_policies_element.focus
    toggle_policies_element.click
    toggle_details_element.focus
    toggle_details_element.click
  end

  def cancel_rsvsn
    cancel_element.wait_until_present(timeout: 120)
    begin
      tries ||= 3
      cancel
      cancel_yes_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    cancel_yes_element.focus
    cancel_yes
    rsvsn_header_element.wait_until_present(timeout: 120)
  end

  def modify_rsvsn_datesrooms
    modify_datesrooms_element.wait_until_present(timeout: 120)
    begin
      tries ||= 3
      modify_datesrooms
      modify_datesrooms_yes_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    modify_datesrooms_yes_element.focus
    modify_datesrooms_yes
  end

  def modify_rsvsn_guestdetails
    modify_guestdetails
    # Pending: Edit Guest Details info and then Save changes
  end

  def modify_addons
    # Pending: Edit Add-ons and then Save changes
  end

  def confirmation_no
    confirmation_no_element.wait_until_present(timeout: 120)
    return confirmation_no
  end

  def confirmation_url
    return @browser.url
  end

  def reservation_id
    return @browser.url.split("details/")[1].split("/")[1]
  end

  def reservation_hash
    return @browser.url.split("details/")[1].split("/")[2]
  end

  def reservation_header
    rsvsn_header_element.wait_until_present(timeout: 120)
    return rsvsn_header
  end

  def modify_popup_copy
    begin
      tries ||= 3
      modify_datesrooms_element.wait_until_present(timeout: 120)
      modify_datesrooms
      copy_modify_popup_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    return copy_modify_popup
  end

  def cancel_popup_copy
    begin
      tries ||= 3
      cancel_element.wait_until_present(timeout: 120)
      cancel
      copy_cancel_popup_element.wait_until_present(timeout: 120)
    rescue
      retry unless (tries -= 1).zero?
    end
    return copy_cancel_popup
  end

  def reservation_details(reservation_type)
    rsvsn_header_element.wait_until_present(timeout: 120)
    rsvsn_details = Hash[
      date:                           DateTime.now,
      rsvsn_type:                     reservation_type,
      rsvsn_header:                   rsvsn_header,
      confirmation_no:                confirmation_no,
      transaction_id:                 transaction_id,
      guest_name:                     guest_name,
      guest_email:                    guest_email,
      guest_mobile:                   guest_mobile,
      guest_country:                  guest_country,
      paypal_owner:                   get_detail(paypal_owner_element),
      paypal_email:                   get_detail(paypal_email_element),
      paypal_mobile:                  get_detail(paypal_mobile_element),
      private_account:                get_detail(private_account_element),
      private_promocode:              get_detail(private_promocode_element),
      creditcard_owner:               get_detail(creditcard_owner_element),
      creditcard_number:              get_detail(creditcard_number_element),
      creditcard_expiry:              get_detail(creditcard_expiry_element),
      rsvsn_madeon:                   rsvsn_madeon,
      rsvsn_modifiedon:               get_detail(rsvsn_modifiedon_element),
      rsvsn_cancelledon:              get_detail(rsvsn_cancelledon_element),
      rsvsn_details:                  rsvsn_details,
      rsvsn_eta:                      rsvsn_eta,
      rsvsn_adults:                   get_detail(rsvsn_adults_element),
      rsvsn_children:                 get_detail(rsvsn_children_element),
      rsvsn_checkin:                  rsvsn_checkin,
      rsvsn_checkout:                 rsvsn_checkout,
      rsvsn_meal_name:                rsvsn_meal_name,
      rsvsn_meal_description:         get_detail(rsvsn_meal_description_element),
      rsvsn_specialrequest:           rsvsn_specialrequest,
      charge_room_charge:             get_detail(charge_room_charge_element),
      charge_total_tax:               get_detail(charge_total_tax_element),
      charge_total_fee:               get_detail(charge_total_fee_element),
      charge_total_addon_cost:        get_detail(charge_total_addon_cost_element),
      charge_total_reservation_cost:  get_detail(charge_total_reservation_cost_element),
      charge_prepayment:              get_detail(charge_prepayment_element),
      charge_balance_payable:         get_detail(charge_balance_payable_element),
      charge_refund:                  get_detail(charge_refund_element),
      charge_penalty:                 get_penalty(reservation_type),
      policy_arrival:                 policy_arrival,
      policy_departure:               policy_departure,
      policy_children:                policy_children,
      policy_prepayment:              policy_prepayment,
      policy_modification:            policy_modification,
      policy_cancellation:            policy_cancellation,
      policy_noshow:                  policy_noshow,
      policy_refund:                  get_detail(policy_refund_element),
      policy_paypal:                  get_detail(policy_paypal_element),
      policy_cardfraud:               get_detail(policy_cardfraud_element),
      confirmation_url:               @browser.url
    ]
    return rsvsn_details
  end

  def create_reservation_link_exist
    return make_new_reservation_element.present?
  end

  def print_reservation_link_exist
    return print_element.present?
  end

  def save_reservation_link_exist
    return save_element.present?
  end

  def modify_guest_details_link_exist
    return modify_guestdetails_element.present?
  end

  def modify_dates_and_rooms_link_exist
    return modify_datesrooms_element.present?
  end

  def include_addons_link_exist
    return include_addons_element.present?
  end

  def modify_addons_link_exist
    return include_or_modify_addons_element.present?
  end

  def cancel_reservation_link_exist
    return cancel_element.present?
  end

  def submit_cc_info_link_exist
    return submit_cc_info_element.present?
  end

  private

  def get_penalty(reservation_type)
    penalty = get_detail(charge_cancellation_element) if reservation_type.include? "cancel"
    penalty = get_detail(charge_noshow_element) if reservation_type.include?("noshow") || reservation_type.include?("no-show")
    penalty = get_detail(charge_modification_element) if reservation_type.include? "modif"
    return penalty
  end
end
