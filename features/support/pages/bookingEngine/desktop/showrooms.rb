# Desktop Booking Engine ShowRooms Page
class ShowRoomsPage
  include PageObject
  include DataLookup
  include BookingEngineHelper

  page_url "#{FigNewton.showrooms_url}/<%=params[:id]%>/<%=params[:arrival_date]%>/<%=params[:departure_date]%>/en/0/0/0/0/<%=params[:rateplan_id]%>"

  # Element Locators
  div(:highlight, xpath: "//*[@class='valuedeal']")
  div(:public_promocode, xpath: "//*[@class='public_promo_code']")
  div(:extra_beds_disabled, xpath: "//div[@class='extra-bed-dropdown']/div[@class='dropdown disabled']")
  div(:extra_beds_enabled, xpath: "//div[@class='extra-bed-dropdown']/div[@class='dropdown emphasis']")
  link(:cta, id: "ctaReserve")
  link(:change_dates, id: "btnChangeDate")
  link(:promocode, id: "promo-code-link")
  link(:policy_popup_rsvsn, xpath: "//div[@id='rateplanContent']//a[contains(.,'Reservation Policies')]")
  link(:policy_popup_close, xpath: "//*[@class='close']")
  link(:promocode_rateplan_name, xpath: "//div[@class='special-rateplans public-rateplans']//h2[@class='element-rate-plan-name']/a")
  span(:arrival_value, css: ".well > span")
  span(:departure_value, css: ".well > span + span")
  span(:no_nights_value, css: "td.date-detalye > div > span + span + span > span")
  span(:copy_taxfee_exclusive, xpath: "//td[contains(@class,'emphasize')]/span[1]")
  span(:copy_taxfee_inclusive, xpath: "//td[contains(@class,'emphasize')]/span[2]")
  span(:promocode_title, xpath: "//div[@class='special-rateplans public-rateplans']//div[@class='public_promo_code']/span[contains(.,'Exclusive for')]")
  span(:promocode_error, xpath: "//div[contains(@class,'alert-error no-private-rate-plan')]/span")
  button(:promocode_apply, id: "btn-apply-promo-code")
  button(:view_rooms_and_prices, id: "btnNext")
  text_field(:promocode_input, id: "promo-code-textbox")

  # Methods
  def choose_room(rateplan_id, room_id, no_rooms)
    raise "Error: Page has been redirected to the No Availability page" if @browser.url.downcase.include? "noavail"
    cta_element.wait_until_present(timeout: 120)
    self.class.select_list(:room_accommodation, id: "room_#{rateplan_id}_#{room_id}_0")
    room_accommodation_element.select_value(no_rooms.to_s)
  end

  def choose_rooms(rateplan, no_rooms_acc=0, no_rooms_occ=0, no_rooms_sup=0, no_rooms_std=0, rsvsn_type="confirmed")
    raise "Error: Page has been redirected to the No Availability page" if @browser.url.downcase.include? "noavail"
    cta_element.wait_until_present(timeout: 120)
    rateplan_id = get_rateplan_id(rateplan, rsvsn_type)
    if no_rooms_acc.to_i > 0
      self.class.select_list(:room_accommodation, id: "room_#{rateplan_id}_#{get_room_id(rateplan, 'accommodation', rsvsn_type)}_0")
      room_accommodation_element.wait_until_present(timeout: 120)
      room_accommodation_element.select_value(no_rooms_acc)
    end
    if no_rooms_occ.to_i > 0
      self.class.select_list(:room_occupancy, id: "room_#{rateplan_id}_#{get_room_id(rateplan, 'occupancy', rsvsn_type)}_3")
      room_occupancy_element.wait_until_present(timeout: 120)
      room_occupancy_element.select_value(no_rooms_occ)
    end
    if no_rooms_sup.to_i > 0 # For DWH reservation tests with less than minimum or greater than maximum payments
      occupancy = no_rooms_sup.include?("maximum pay") ? 2 : 1
      self.class.select_list(:room_superior, id: "room_#{rateplan_id}_#{get_room_id(rateplan, 'superior', rsvsn_type)}_#{occupancy}")
      room_superior_element.wait_until_present(timeout: 120)
      room_superior_element.select_value(1)
    end
    if no_rooms_std.to_i > 0 # For HE calendar availability tests
      self.class.select_list(:room_studio, id: "room_#{rateplan_id}_#{get_room_id(rateplan, 'studio', rsvsn_type)}_0")
      room_studio_element.wait_until_present(timeout: 120)
      room_studio_element.select_value(no_rooms_std)
    end
  end

  def promo_rateplan
    promocode_title_element.wait_until_present(timeout: 120)
    promocode_rateplan_name_element.wait_until_present(timeout: 120)
    promo_header = promocode_title_element.text
    promo_rateplan_name = promocode_rateplan_name_element.text
    return promo_header, promo_rateplan_name
  end

  def promocode_choose_rooms(rateplan_id, rateplan_name, no_rooms_acc, no_rooms_occ)
    raise "Error: Page has been redirected to the No Availability page" if @browser.url.downcase.include? "noavail"
    cta_element.wait_until_present(timeout: 120)
    if no_rooms_acc.to_i > 0
      self.class.select_list(:room_accommodation, id: "room_#{rateplan_id}_#{get_room_id(rateplan_name, 'accommodation', nil)}_0")
      room_accommodation_element.wait_until_present(timeout: 120)
      room_accommodation_element.select_value(no_rooms_acc.to_s)
    end
    if no_rooms_occ.to_i > 0
      self.class.select_list(:room_occupancy, id: "room_#{rateplan_id}_#{get_room_id(rateplan_name, 'occupancy', nil)}_3")
      room_occupancy_element.wait_until_present(timeout: 120)
      room_occupancy_element.select_value(no_rooms_occ)
    end
  end

  def get_room_availability(rateplan, room, rsvsn_type="confirmed")
    rateplan_id = get_rateplan_id(rateplan, rsvsn_type)
    raise "Error: Page has been redirected to the No Availability page" if @browser.url.downcase.include? "noavail"
    self.class.select_list(:room, id: "room_#{rateplan_id}_#{get_room_id(rateplan, room, rsvsn_type)}_0")
    room_element.wait_until_present(timeout: 120)
    return room_element.attribute("data-available")
  end

  def get_room_rates(rateplan, room, rsvsn_type="confirmed")
    rateplan_id = get_rateplan_id(rateplan, rsvsn_type)
    room_id = get_room_id(rateplan, room, rsvsn_type)
    raise "Error: Page has been redirected to the No Availability page" if @browser.url.downcase.include? "noavail"
    self.class.span(:rate_per_night, xpath: "//tr[contains(@data-rateplan-id,'#{rateplan_id}')][contains(@data-room-id,'#{room_id}')]//div[@class='price-per-night-incl']/span[@class='convert']")
    begin
      rate_per_night_element.wait_until_present(timeout: 120)
    rescue
      raise "Error: Cannot find inclusive rate per night information on page. Locator must have changed."
    end
    return rate_per_night.gsub(",", "").gsub(".00", "")
  end

  def goto_payinfo
    cta_element.wait_until_present(timeout: 120)
    cta_element.focus
    cta
    handle_alert
  end

  def include_extrabeds(extrabeds)
    unless extrabeds.nil? || extrabeds.zero?
      # Pending: Add extra beds
    end
  end

  def view_policies(rateplan, rsvsn_type)
    self.class.link(:rateplan_link, id: "rpName_#{get_rateplan_id(rateplan, rsvsn_type)}")
    rateplan_link_element.wait_until_present(timeout: 120)
    rateplan_link
    policy_popup_rsvsn_element.wait_until_present(timeout: 120)
    policy_popup_rsvsn
  end

  def tax_inclusion_copy
    copy = copy_taxfee_inclusive
    copy = "Incl" + copy.split("Incl")[1]
    copy.gsub!(".", ". of") if !copy.include?("GST")
    return copy
  end

  def apply_promocode(code)
    promocode
    promocode_input = code||=FigNewton.promo_code
    promocode_apply
  end

  def change_staydates(arrival_date, departure_date)
    change_dates_element.wait_until_present(timeout: 120)
    change_dates
    view_rooms_and_prices_element.wait_until_present(timeout: 120)
    choose_staydates(arrival_date, departure_date)
    view_rooms_and_prices
  end

  def highlight_status
    return highlight_element.present?
  end
end
