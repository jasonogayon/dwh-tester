# Mobile Booking Engine Upload Page
class ImageUploadPageMobile < ImageUploadPage
  include PageObject
  include BookingEngineHelper

  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../../", File.dirname(__FILE__))

  # Element Locators
  h4(:rsvsn_header, xpath: "//div[contains(@class,'col-xs-12')]/h4")
  link(:upload_later, xpath: "//a[@class='btn cta']/following::p[1]/a")
  link(:upload_now, xpath: "//a[@class='btn cta']")
end
