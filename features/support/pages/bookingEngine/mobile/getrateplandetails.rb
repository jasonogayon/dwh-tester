# Mobile Booking Engine GetRatePlanDetails Page
class GetRatePlanDetailsPageMobile < ShowRoomsPage
  include PageObject
  include DataLookup
  include BookingEngineHelper

  page_url "#{FigNewton.getrateplandetails_url_mobile_neo}/<%=params[:id]%>/<%=params[:arrival_date]%>/<%=params[:departure_date]%>/<%=params[:room_id]%>/en"

  # Methods
  def choose_rateplan(rateplan, rsvsn_type)
    raise "Page has been redirected to the No Availability page. Please check." if @browser.url.downcase.include? "noavail"
    self.class.button(:rateplan, xpath: "//button[@data-rate-plan-id='#{get_rateplan_id(rateplan, rsvsn_type)}']")
    rateplan_element.wait_until_present(timeout: 120)
    rateplan_element.focus
    rateplan
    handle_alert
    preloader_element.wait_while_present(timeout: 300)
  end
end
