# Mobile Booking Engine Confirmation Page
class ConfirmationPageMobile < ConfirmationPage
  include PageObject
  include BookingEngineHelper
  include DataTransformHelper

  # Element Locators
  h4(:rsvsn_header, xpath: "//div[contains(@class,'col-xs-12')]/h4")
  link(:make_new_reservation, id: "cta-btn")
  link(:save, xpath: "//a[contains(.,'Save')]")
  link(:print, xpath: "//a[contains(.,'Print')]")
  link(:modify_datesrooms, xpath: "//a[contains(.,'Modify dates')]")
  link(:modify_guestdetails, xpath: "//a[contains(.,'Modify guest')]")
  link(:include_or_modify_addons, xpath: "//a[contains(.,'add-ons')]")
  link(:cancel, xpath: "//a[contains(.,'Cancel reservation')]")
  link(:submit_cc_info, xpath: "//a[contains(.,'Complete Your Reservation')]")
  span(:creditcard_owner, xpath: "//div[@class='reservation-divider']/div[1]/div[2]//span")
  span(:creditcard_number, xpath: "//div[@class='reservation-divider']/div[2]/div[2]//span")
  span(:creditcard_expiry, xpath: "//div[@class='reservation-divider']/div[3]/div[2]//span")
  span(:charge_prepayment, xpath: "//div[contains(@class,'row right')][contains(.,'Prepayment')]/div[2]/span")
  span(:charge_balance_payable, xpath: "//div[contains(@class,'row right')][contains(.,'Payable')]/div[2]/span")
  element(:toggle_policies, xpath: "//a[@id='accordion']/div[contains(.,'Policies')]//h4/small")
  element(:toggle_rateplan, xpath: "//a[@id='accordion']/div[contains(.,'Rate plan')]//h4/small")
  element(:toggle_bpg, xpath: "//a[@id='accordion']/div[contains(.,'Best Price')]//h4/small")
  element(:toggle_hotel, xpath: "//a[@id='accordion']/div[contains(.,'Hotel Details')]//h4/small")
  paragraph(:confirmation_no, xpath: "//p[contains(., 'Confirmation Number')]/following::p[1]")
  paragraph(:transaction_id, xpath: "//p[contains(., 'Transaction ID')]/following::p[1]")
  paragraph(:rsvsn_meal_name, xpath: "//a[@id='accordion']/div[2]/div[2]//p[2]")
  paragraph(:charge_total_reservation_cost, xpath: "//div[contains(.,'Total reservation cost')][contains(@class,'col-xs-6')]/following::p[1]")
  paragraph(:rsvsn_madeon, xpath: "//p[contains(., 'Reservation Date')]/following::p[1]")
  paragraph(:rsvsn_detail, xpath: "//p[contains(., 'Reservation Details')]/following::p[1]")
  paragraph(:rsvsn_checkin, xpath: "//p[contains(., 'Check In')]/following::p[1]")
  paragraph(:rsvsn_checkout, xpath: "//p[contains(., 'Check Out')]/following::p[1]")
  paragraph(:guest_name, xpath: "//p[contains(.,'Guest Name')]")
  paragraph(:guest_email, xpath: "//p[contains(.,'Email')]/following::p[1]")
  paragraph(:guest_mobile, xpath: "//p[contains(.,'Mobile')]/following::p[1]")
  paragraph(:guest_country, xpath: "//p[contains(.,'Country')]/following::p[1]")
  paragraph(:rsvsn_specialrequest, xpath: "//p[contains(.,'Special Request')]/following::p[1]")
  paragraph(:rsvsn_rooms, xpath: "//p[contains(.,'Guest Details')]/following::p[1]")
  paragraph(:policy_header, xpath: "//a[@id='accordion']/div[1]//p[2]")
  paragraph(:policy_arrival, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Arrival')]")
  paragraph(:policy_departure, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Departure')]")
  paragraph(:policy_children, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Children')]")
  paragraph(:policy_prepayment, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Prepayment')]")
  paragraph(:policy_modification, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Modification')]")
  paragraph(:policy_cancellation, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Cancellation')]")
  paragraph(:policy_noshow, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'No-show')]")
  paragraph(:policy_refund, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Refund')]")
  paragraph(:policy_paypal, xpath: "//a[@id='accordion']/div[1]//p[contains(.,'Paypal')]")

  # Methods
  def get_rsvsn_details(reservation_type)
    rsvsn_header_element.wait_until_present(timeout: 120)
    @browser.execute_script("window.scrollBy(0,1000)")
    toggle_policies_element.wait_until_present(timeout: 120)
    toggle_policies_element.focus
    toggle_policies_element.click
    rsvsn_details = Hash[
        date:                           DateTime.now,
        rsvsn_type:                     reservation_type,
        rsvsn_header:                   rsvsn_header,
        confirmation_no:                confirmation_no,
        transaction_id:                 transaction_id,
        guest_name:                     guest_name,
        guest_email:                    guest_email,
        guest_mobile:                   guest_mobile,
        guest_country:                  guest_country,
        paypal_owner:                   get_detail(paypal_owner_element),
        paypal_email:                   get_detail(paypal_email_element),
        paypal_mobile:                  get_detail(paypal_mobile_element),
        private_account:                get_detail(private_account_element),
        private_promocode:              get_detail(private_promocode_element),
        creditcard_owner:               get_detail(creditcard_owner_element),
        creditcard_number:              get_detail(creditcard_number_element),
        creditcard_expiry:              get_detail(creditcard_expiry_element),
        rsvsn_madeon:                   rsvsn_madeon,
        rsvsn_modifiedon:               get_detail(rsvsn_modifiedon_element),
        rsvsn_cancelledon:              get_detail(rsvsn_cancelledon_element),
        rsvsn_details:                  rsvsn_detail,
        rsvsn_checkin:                  rsvsn_checkin,
        rsvsn_checkout:                 rsvsn_checkout,
        rsvsn_meal_name:                get_detail(rsvsn_meal_name_element),
        rsvsn_meal_description:         get_detail(rsvsn_meal_description_element),
        rsvsn_specialrequest:           get_special_request(reservation_type),
        charge_total_reservation_cost:  get_detail(charge_total_reservation_cost_element),
        charge_prepayment:              get_detail(charge_prepayment_element),
        charge_balance_payable:         get_detail(charge_balance_payable_element),
        policy_arrival:                 policy_arrival,
        policy_departure:               policy_departure,
        policy_children:                policy_children,
        policy_prepayment:              policy_prepayment,
        policy_modification:            policy_modification,
        policy_cancellation:            policy_cancellation,
        policy_noshow:                  policy_noshow,
        policy_refund:                  get_detail(policy_refund_element),
        policy_paypal:                  get_detail(policy_paypal_element),
        confirmation_url:               @browser.url
    ]
    return rsvsn_details
  end

  def create_reservation_link_exist
    @browser.execute_script("window.scrollBy(0,1000)")
    return make_new_reservation_element.present?
  end

  def submit_cc_info_link_exist
    @browser.execute_script("window.scrollBy(0,-1000)")
    return submit_cc_info_element.present?
  end

  private

  def get_special_request(reservation_type)
    return reservation_type.include?("public") ? rsvsn_specialrequest : nil
  end
end
