# Mobile Booking Engine ShowRooms Page
class ShowRoomsPageMobile < ShowRoomsPage
  include PageObject
  include DataLookup
  include BookingEngineHelper

  page_url "#{FigNewton.showrooms_url_mobile_neo}/<%=params[:id]%>/<%=params[:arrival_date]%>/<%=params[:departure_date]%>/en/0"

  # Element Locators
  div(:public_promocode, xpath: "//*[@class='promo-tag']")
  button(:promocode_submit, id: "btnPromoCodeSubmit")
  text_field(:promocode_input, id: "txtPromoCodeShowRooms")

  # Methods
  def choose_room(rateplan, room_type, rsvsn_type)
    raise "Page has been redirected to the No Availability page. Please check." if @browser.url.downcase.include? "noavail"
    self.class.element(:room_name, xpath: "//div[@class='room-list']//div[@id='#{get_room_id(rateplan, room_type, rsvsn_type)}']//p[1]/strong")
    room_name_element.wait_until_present(timeout: 120)
    room_name_element.click
    handle_alert
    preloader_element.wait_while_present(timeout: 300)
  end
end
