# Mobile Booking Engine SelectDates Page
class SelectDatesPageMobile < SelectDatesPage
  include PageObject
  include BookingEngineHelper
  include DatesHelper

  page_url "#{FigNewton.selectdates_url_mobile_neo}/<%=params[:id]%>"

  # Element Locators
  link(:cta, id: "cta-btn")
  link(:calendar_arrival_close, xpath: "//div[@id='arrivalCalendar']//a[contains(@class,'close')]")
  link(:calendar_departure_close, xpath: "//div[@id='departureCalendar']//a[contains(@class,'close')]")
  link(:promocode, id: "lblPromocode")
  paragraph(:calendar_arrival, id: "lblcheckin")
  paragraph(:calendar_departure, id: "lblcheckout")
  paragraph(:no_nights, id: "lblnights")
  text_field(:promocode_input, id: "promo_code_value")

  # Methods
  def select_staydates(arrival_date, departure_date)
    calendar_arrival_element.wait_until_present(timeout: 120)
    calendar_arrival_element.click
    self.class.link(:arrival, xpath: "//div[@id='arrivalCalendar']//td[contains(.,'#{get_date(arrival_date)[1]}')]/a")
    arrival_element.wait_until_present(timeout: 120)
    arrival_element
    calendar_arrival_close
    calendar_departure_element.wait_until_present(timeout: 120)
    calendar_departure_element.click
    self.class.link(:departure, xpath: "//div[@id='departureCalendar']//td[contains(.,'#{get_date(departure_date)[1]}')]/a")
    departure_element.wait_until_present(timeout: 120)
    departure_element
    calendar_departure_close
  end
end
