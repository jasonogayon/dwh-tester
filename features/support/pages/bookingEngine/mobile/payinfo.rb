# Mobile Booking Engine PayInfo Page
class PayInfoPageMobile < PayInfoPage
  include PageObject
  include DataLookup
  include BookingEngineHelper
  include DataTransformHelper

  # Element Locators
  span(:total_room_charge, xpath: "//div[contains(.,'Total Room Cost')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  span(:total_tax, xpath: "//div[contains(.,'Taxes')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  span(:total_fee, xpath: "//div[contains(.,'Fees')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  span(:prepayment, xpath: "//div[contains(.,'PREPAYMENT')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  span(:balance_payable, xpath: "//div[contains(.,'PAYABLE')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  span(:modification_charge, xpath: "//div[contains(.,'MODIFICATION')][contains(@class,'col-xs-6')]/following::div[1]/span[1]")
  link(:cta, id: "btnconfirmbooking")
  element(:arrival, xpath: "//div[contains(.,'Arrival')][contains(@class,'col-xs-5')]/following::div[1]//small")
  element(:departure, xpath: "//div[contains(.,'Departure')][contains(@class,'col-xs-5')]/following::div[1]//small")
  element(:no_nights, xpath: "//div[contains(.,'nights')][contains(@class,'col-xs-5')]/following::div[1]//small")
  paragraph(:total_reservation_cost, xpath: "//div[contains(.,'Total reservation cost')][contains(@class,'col-xs-6')]/following::div[1]/p[1]")

  div(:copy_securitycode, xpath: "//div[@id='securitycodeBlock']/div[contains(@id,'popover')]/div[2]")
  div(:copy_cardnumbersecurity, xpath: "//div[@id='myCCsecurity']/div[2]")
  div(:copy_cardownerfraud, xpath: "//div[@id='GuestBlock']/div[contains(@id,'popover')][2]/div[2]")
  link(:popup_preauth, xpath: "//a[contains(.,'Pre-authorization')]")
  link(:proceed_payment, id: "proceed_payment")
  span(:error_payment, xpath: "//strong[contains(., 'Your payment could not be processed.')]")
  strong(:termspopup, xpath: "//strong[contains(., 'Terms of Reservation')]")
  element(:popup_securitycode, id: "lblsecCode")
  element(:popup_cardownerfraud, id: "lblidentity")
  checkbox(:accept_terms, id: "payment_agree")
  textarea(:special_request, id: "payment_specialrequest")
  text_field(:first_name, id: "ccard_first_name")
  text_field(:last_name, id: "ccard_last_name")
  text_field(:email, id: "payment_email")
  text_field(:email_confirm, id: "payment_confirm_email")
  text_field(:phone, id: "payment_contact_number")
  text_field(:cc_number, id: "txtCreditCard")
  text_field(:cc_seccode, id: "payment_security_code")
  text_field(:occupant_firstname, id: "guest_first_name")
  text_field(:occupant_lastname, id: "guest_last_name")
  text_field(:no_rooms, name: "quantity")
  select_list(:cc_expiry_month, id: "payment_cc_exp_month")
  select_list(:cc_expiry_year, id: "payment_cc_exp_year")
  select_list(:country, id: "payment_country")
  select_list(:eta, id: "payment_eta")

  # Methods
  def get_rsvsn_charges(rateplan, arrival, previous_rsvsn_charges=nil)
    arrival_element.wait_until_present(timeout: 120)
    rsvsn_charges = Hash[
      rateplan: rateplan,
      arrival: arrival,
      no_days_until_checkin: get_no_of_days_until_checkin(arrival, previous_rsvsn_charges),
      total_room_charge: get_detail(total_room_charge_element),
      total_tax: get_detail(total_tax_element),
      total_fee: get_detail(total_fee_element),
      total_reservation_cost: get_detail(total_reservation_cost_element),
      prepayment: get_prepayment(rateplan),
      prepayment_he_display: get_prepayment_he_display(rateplan),
      balance_payable: get_balance_payable(rateplan),
      balance_payable_he_display: get_detail(balance_payable_element),
      penalty: get_detail(modification_charge_element),
      no_nights: no_nights.split(" ")[0],
      percent_commission: get_percent_commission(rateplan)
    ]
    return rsvsn_charges
  end

  def input_guest_details(rateplan)
    proceed_payment if proceed_payment_element.present?
    cta_element.wait_until_present(timeout: 120)
    first_name = get_guest_firstname(rateplan)
    last_name = get_guest_lastname(rateplan)
    email = FigNewton.email
    email_confirm = FigNewton.email
    country_element.select_value(FigNewton.country)
    phone = FigNewton.phone
    special_request = FigNewton.special_request
    for i in 0..(no_rooms.to_i - 1)
      if rateplan.include? "onhold"
        self.class.text_field(:adult_firstname, name: "reservation_room_occupants[#{i}][first_name]")
        adult_firstname = "Jason"
        self.class.text_field(:adult_lastname, name: "reservation_room_occupants[#{i}][last_name]")
        adult_lastname = "Ogayon"
      end
      self.class.button(:adult, xpath: "//button[@data-field='reservation_room_occupants[#{i}][adult_count]'][@data-type='plus']")
      adult
    end
  end

  def input_creditcard_details(rateplan)
    cta_element.wait_until_present(timeout: 120)
    if rateplan.include? "bdo"
      cc_number = FigNewton.bdo_cc_number
      cc_expiry_month_element.select_value(FigNewton.bdo_cc_expiry_month)
      cc_expiry_year_element.select_value(FigNewton.bdo_cc_expiry_year)
      cc_seccode = FigNewton.bdo_cc_seccode
    else
      cc_number = FigNewton.cc_number
      cc_expiry_month_element.select_value(FigNewton.cc_expiry_month)
      cc_expiry_year_element.select_value(FigNewton.cc_expiry_year)
      cc_seccode = FigNewton.cc_seccode
    end
  end

  def accept_terms_and_conditions
    check_accept_terms
  end

  def confirm_reservation
    cta_element.focus
    cta
    handle_alert
    preloader_element.wait_while_present(timeout: 300)
  end

  private

  def get_prepayment(rateplan)
    return rateplan.include?("onhold") ? nil : get_detail(prepayment_element)
  end

  def get_prepayment_he_display(rateplan)
    return rateplan.include?("_pa_") ? "0.00" : get_detail(prepayment_element)
  end

  def get_balance_payable(rateplan)
    return rateplan.include?("onhold") ? nil : get_detail(balance_payable_element)
  end
end
