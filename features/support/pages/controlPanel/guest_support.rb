# Control Panel Guest Support Page
class GuestSupportPage
  include PageObject
  include ControlPanelHelper
  include DatesHelper

  # Element Locators
  div(:total_no_rsvsn, xpath: "//div[@id='reservationsListGrid']//td[@class='x-toolbar-right']//div[@class='xtb-text']")
  div(:latest_rsvsn_transaction_date, xpath: "//div[@id='reservationsListGrid']//div[1]/table/tbody/tr[1]/td[8]/div")
  button(:advanced_search, xpath: "//table[@id='guest_advance_search']//button")
  button(:advanced_search_go, xpath: "//table[@id='btn_search']//button")
  button(:search_go, xpath: "//table[@id='quick_search_btn']//button")
  checkbox(:filter_onhold, id: "onhold")
  checkbox(:filter_confirmed, id: "confirmed")
  checkbox(:filter_modified, id: "modified")
  checkbox(:filter_cancelled, id: "cancelled")
  checkbox(:filter_noshow, id: "noshow")
  text_field(:guest_confirmation_no, id: "quick_search_confirm_no")
  text_field(:guest_transaction_id, id: "quick_search_txn_id")
  text_field(:guest_email_address, id: "quick_search_emailadd")
  text_field(:filter_date_from, id: "filter_search_from")
  text_field(:filter_date_to, id: "filter_search_to")
  radio_button(:filter_transaction_date, xpath: "//div[@id='panelReservationsSearch']//label[contains(.,'Transaction')]/preceding::input[@name='datetype'][1]")

  # Methods
  def search_reservation(confirmation_no)
    loading_element.wait_while_present(timeout: 300)
    guest_confirmation_no_element.wait_until_present(timeout: 120)
    guest_confirmation_no = confirmation_no
    search_go
    loading_element.wait_while_present(timeout: 300)
  end

  def open_reservation(confirmation_no)
    search_reservation(confirmation_no)
    view_reservation_details(confirmation_no)
  end

  def filter_reservation_by_transaction_date(rsvsn_type, start_date, end_date)
    loading_element.wait_while_present(timeout: 300)
    advanced_search_element.wait_until_present(timeout: 120)
    advanced_search
    filter_transaction_date_element.wait_until_present(timeout: 120)
    select_filter_transaction_date
    filter_date_from_element.clear
    filter_date_from = get_date(start_date)[12]
    filter_date_to_element.clear
    filter_date_to = get_date(end_date)[12]
    filter_reservation_status(rsvsn_type)
    advanced_search_go
    loading_element.wait_until_present(timeout: 120)
    loading_element.wait_while_present(timeout: 300)
    no_rsvsn = total_no_rsvsn_element.text.split("of ")[1]
    create_total_no_rsvsn_csv(get_date(start_date)[5], no_rsvsn)
    return no_rsvsn
  end

  def latest_reservation
    loading_element.wait_while_present(timeout: 300)
    return DateTime.httpdate(get_date("TODAY")[7].strftime("%a").to_s + ", " + latest_rsvsn_transaction_date_element.text + " GMT")
  end

  private

  def view_reservation_details(confirmation_no)
    loading_element.wait_while_present(timeout: 300)
    self.class.div(:rsvsn, xpath: "//div[@id='reservationsListGrid']//div[contains(.,'#{confirmation_no}')][@class='ux-cell-value']//div[@class='ux-cell-action edit']")
    loading_element.wait_while_present(timeout: 300)
    rsvsn_element.click
  end

  def create_total_no_rsvsn_csv(date, total_no_rsvsn)
    filename = "reports/cp/total_no_rsvsns.csv"
    unless File.file?(filename)
      CSV.open(filename, "wb") { |csv| csv << ["Date", "Number of Reservations"] }
    end
    CSV.open(filename, "a+") { |csv| csv << [date, total_no_rsvsn] }
  end

  def filter_reservation_status(rsvsn_type)
    check_filter_onhold if rsvsn_type.include? "on-hold"
    check_filter_confirmed if rsvsn_type.include? "confirmed"
    check_filter_modified if rsvsn_type.include? "modified"
    check_filter_cancelled if rsvsn_type.include? "cancelled"
    check_filter_noshow if rsvsn_type.include? "noshow"
  end
end
