# Control Panel Hotel Management Page
class HotelManagementPage
  include PageObject
  include ControlPanelHelper

  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../", File.dirname(__FILE__))

  # Element Locators
  div(:current_status, xpath: "//legend[contains(.,'Current Status')]/following::div[1]/div")
  div(:retrieving_property_info, xpath: "//div[contains(.,'Retrieving property information')]")
  div(:market_default, xpath: "//div[contains(@class,'x-combo-list-item')][contains(., 'Undefined')]")
  div(:logs, xpath: "//div[@class='ux-cell-action lastupdate']")
  span(:uploading_logo, xpath: "//span[contains(.,'Uploading logo')]")
  link(:tab_propertyinfo, xpath: "//li[@id='tabpanel_property__property_info']/a[2]")
  link(:tab_launchtype, xpath: "//li[@id='tabpanel_property__property_launchtype']/a[2]")
  link(:tab_guestinterface, xpath: "//li[@id='tabpanel_property__property_guest_interface']/a[2]")
  link(:channel_manager, xpath: "//a[contains(.,'ChannelManager')]")
  link(:finance_interface, xpath: "//a[contains(.,'Finance Interface')]")
  link(:dashboard, xpath: "//a[contains(.,'Dashboard')]")
  link(:my_account, xpath: "//a[contains(.,'My Account')]")
  image(:hotel_logo_preview, xpath: "//img[@id='logoheader_image'][contains(@style,'block')]")
  image(:hotel_logo_image, xpath: "//div[@id='property_logo_image']/img")
  button(:search, xpath: "//div[@id='propertyPanel']/div/div[1]//button[contains(.,'Search')]")
  button(:search_go, xpath: "//div[@id='panelSearch']//button[contains(.,'Search')]")
  button(:accept, xpath: "//div[@id='property_status_form']//button[contains(.,'Accept')]")
  button(:submit_to_online_prod, xpath: "//div[@id='property_status_form']//button[contains(.,'Submit to Online Production')]")
  button(:website_launch, xpath: "//div[@id='property_status_form']//button[contains(.,'Website Launch')]")
  button(:submit_to_go_live, xpath: "//div[@id='property_status_form']//button[contains(.,'Submit to Go Live')]")
  button(:hi_training, xpath: "//div[@id='property_status_form']//button[contains(.,'HI Training')]")
  button(:gi_activate, xpath: "//div[@id='property_status_form']//button[contains(.,'GI Activate')]")
  button(:edit_guest_interface_design, xpath: "//button[contains(.,'Edit Guest Interface Design')]")
  button(:upload_hotel_logo, xpath: "//div[@id='guest_interface_preview']//form/div/div[1]//button[contains(.,'Upload')]")
  button(:edit_guest_interface_design_save, xpath: "//div[@id='guest_interface_preview']//button[contains(.,'Save')]")
  button(:property_info_save, xpath: "//div[contains(@style,'block')]//button[contains(.,'Save')]")
  checkbox(:integration_guidelines, name: "integration_req_flag")
  checkbox(:full_payment, id: "full_payment_flag")
  checkbox(:bancnet, id: "bancnet_payment_flag")
  textarea(:status_change_remark, id: "property_statuschange_remarks")
  text_field(:hotel_id, name: "id")
  text_field(:hotel_name, name: "org_name")
  text_field(:hotel_market_name, xpath: "//input[@name='market_name']/following::input[1]")
  text_field(:guest_interface_url, name: "gi_url")
  text_field(:directory_search_url, name: "url_dir_search")
  file_field(:hotel_logo, xpath: "//div[contains(.,'Please limit to under 30kb file size')]//input[@name='image_logo']")

  # Methods
  def search_property(property_name)
    loading_element.wait_while_present(timeout: 300)
    one_moment_please_element.wait_while_present(timeout: 300)
    search_element.wait_until_present(timeout: 120)
    loading_mask_element.wait_while_present(timeout: 300)
    search
    hotel_name_element.wait_until_present(timeout: 120)
    hotel_name = property_name
    search_go
    loading_element.wait_while_present(timeout: 300)
  end

  def search_property_by_id(property_id)
    loading_element.wait_while_present(timeout: 300)
    one_moment_please_element.wait_while_present(timeout: 300)
    search_element.wait_until_present(timeout: 120)
    loading_mask_element.wait_while_present(timeout: 300)
    search
    hotel_id_element.wait_until_present(timeout: 120)
    hotel_id = property_id
    search_go
    loading_element.wait_while_present(timeout: 300)
  end

  def update_property_info(property_name)
    set_property_status(property_name, "Submission Accepted")
    open_property_info_popup(property_name)
    update_property_info_data
    update_launch_type_data
    update_guest_interface_design
    save_property_info_changes
  end

  def set_property_status(property_name, status)
    if status.downcase.include?("submission") && status.downcase.include?("accepted")
      open_update_status_popup(property_name)
      change_property_status(property_name)
    else
      self.class.div(:property_status, xpath: "//tr[contains(.,'#{property_name}')]//td[14]/div")
      property_status_element.wait_until_present(timeout: 120)
      current_status = property_status
      until current_status.downcase.include? "activated"
        open_update_status_popup(property_name)
        change_property_status(property_name)
        property_status_element.wait_until_present(timeout: 120)
        current_status = property_status
      end
    end
  end

  def open_property_info_popup(property_name)
    self.class.div(:property_name, xpath: "//tr[contains(.,'#{property_name}')]//td[5]/div")
    loading_element.wait_while_present(timeout: 300)
    property_name_element.wait_until_present(timeout: 120)
    property_name_element.click
    retrieving_property_info_element.wait_while_present(timeout: 300)
  end

  def update_bancnet_payment_setting(bancnet_setting)
    tab_propertyinfo_element.wait_until_present(timeout: 120)
    bancnet_element.wait_until_present(timeout: 120)
    bancnet_setting == true ? check_bancnet : uncheck_bancnet
    save_property_info_changes
  end

  def view_logs(property_name)
    bancnet_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    logs_element.wait_until_present(timeout: 120)
    logs_element.click
    loading_element.wait_while_present(timeout: 300)
    self.class.span(:logs_window, xpath: "//span[contains(.,'Logs for #{property_name}')]")
    logs_window_element.wait_until_present(timeout: 120)
    self.class.div(:log_1, xpath: "//div[contains(.,'Logs for #{property_name}')]//div[contains(@class,'x-grid3-row')][1]//td[contains(@class,'x-grid3-td-col_log_by')]/div")
    self.class.div(:log_2, xpath: "//div[contains(.,'Logs for #{property_name}')]//div[contains(@class,'x-grid3-row')][2]//td[contains(@class,'x-grid3-td-col_log_by')]/div")
    self.class.button(:logs_close, xpath: "//div[contains(.,'Logs for #{property_name}')]//button[contains(.,'Close')]")
    logs_close_element.wait_until_present(timeout: 120)
    log_2_element.wait_until_present(timeout: 120)
    log_1_element.wait_until_present(timeout: 120)
    loading_element.wait_while_present(timeout: 300)
    logs = log_1_element.text + "\n" + log_2_element.text
    logs_close
    return logs
  end

  private

  def update_property_info_data
    tab_propertyinfo_element.wait_until_present(timeout: 120)
    hotel_market_name_element.wait_until_present(timeout: 120)
    hotel_market_name_element.click
    loading_element.wait_while_present(timeout: 300)
    market_default_element.wait_until_present(timeout: 120)
    market_default_element.click
  end

  def update_launch_type_data
    tab_launchtype_element.wait_until_present(timeout: 120)
    tab_launchtype
    integration_guidelines_element.wait_until_present(timeout: 120)
    uncheck_integration_guidelines
    directory_search_url = FigNewton.adsite_url
  end

  def update_guest_interface_design
    tab_guestinterface_element.wait_until_present(timeout: 120)
    tab_guestinterface
    edit_guest_interface_design_element.wait_until_present(timeout: 120)
    edit_guest_interface_design
    upload_hotel_logo_element.wait_until_present(timeout: 120)
    Watir.relaxed_locate = false
    hotel_logo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_hotel_logo)
    Watir.relaxed_locate = true
    upload_hotel_logo
    uploading_logo_element.wait_while_present(timeout: 300)
    edit_guest_interface_design_save
    hotel_logo_image_element.wait_until_present(timeout: 120)
  end

  def save_property_info_changes
    property_info_save_element.wait_until_present(timeout: 120)
    property_info_save
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def change_property_status(property_name)
    status = current_status.downcase
    if status.include? "received"
      accept_element.wait_until_present(timeout: 120)
      accept
    elsif status.include? "accepted"
      if status.include? "submission"
        submit_to_online_prod_element.wait_until_present(timeout: 120)
        submit_to_online_prod
      elsif status.include? "web services"
        website_launch_element.wait_until_present(timeout: 120)
        website_launch
      elsif status.include? "golive"
        hi_training_element.wait_until_present(timeout: 120)
        hi_training
      end
    elsif status.include? "web site launch"
      submit_to_go_live_element.wait_until_present(timeout: 120)
      submit_to_go_live
    elsif status.include? "hi training"
      gi_activate_element.wait_until_present(timeout: 120)
      gi_activate
    end
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def open_update_status_popup(property_name)
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    self.class.div(:edit_property_status, xpath: "//tr[contains(.,'#{property_name}')]//td[14]//div[contains(@class,'status_edit')]")
    edit_property_status_element.wait_until_present(timeout: 120)
    edit_property_status_element.click
    current_status_element.wait_until_present(timeout: 120)
  end
end
