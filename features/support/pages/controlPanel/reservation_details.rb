# Control Panel Reservation Details Page
class ReservationDetailsPage
  include PageObject
  include ControlPanelHelper

  # Element Locators
  div(:close_reservationdetails, xpath: "//span[contains(.,'Reservation Details')][contains(@class, 'x-window-header-text')]/preceding::div[1]")
  button(:cancel, xpath: "//*[@id='btn_cancel_reservation']/tbody/tr[2]/td[2]/em/button")
  button(:cancel_confirm, xpath: "//button[contains(., 'Proceed')]")
  button(:report_noshow, xpath: "//table[@id='btn_request_for_no_show']//button[contains(.,'Report as No show')]")
  button(:report_noshow_confirm, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//td[2]//td[2]//button[contains(.,'Ok')]")
  checkbox(:send_remarks_to_finance, id: "send_to_finance")
  textarea(:guestsupport_remark, id: "guest_support_remarks")

  # Methods
  def cancel_reservation(rateplan)
    one_moment_please_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    cancel_element.wait_until_present(timeout: 120)
    cancel
    cancel_confirm_element.wait_until_present(timeout: 120)
    guestsupport_remark_element.clear
    guestsupport_remark = "Cancelling reservation for testing purposes"
    uncheck_send_remarks_to_finance if rateplan.include?("dwh") && rateplan.include?("full_notr")
    cancel_confirm
    please_wait_element.wait_while_present(timeout: 300) if rateplan.include?("onhold") || rateplan.include?("notallowed")
    loading_element.wait_until_present(timeout: 120)
    loading_element.wait_while_present(timeout: 300)
    raise "Reservation cancellation window did not close properly after cancellation. Please check." if cancel_element.present?
  end

  def mark_as_noshow
    loading_element.wait_while_present(timeout: 300)
    report_noshow_element.wait_until_present(timeout: 120)
    report_noshow
    report_noshow_confirm_element.wait_until_present(timeout: 120)
    report_noshow_confirm_element.focus
    report_noshow_confirm
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def close_reservationdetails
    close_reservationdetails_element.wait_until_present(timeout: 120)
    close_reservationdetails_element.click
  end

  def goto_confirmation(url)
    @browser.goto url
    rsvsn_header_element.wait_until_present(timeout: 120)
  end
end
