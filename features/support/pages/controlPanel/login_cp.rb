# Control Panel Login Page
class LoginPage
  include PageObject
  include ControlPanelHelper

  page_url FigNewton.login_cp_url

  # Element Locators
  div(:login, id: "loginsubmit")
  link(:logoff, id: "lnklogout")
  text_field(:username, id: "loginUsername")
  text_field(:password, id: "loginPass")

  # Methods
  def login(user_type)
    user_type ||= "admin" if user_type.nil?
    tries ||= 5
    username_element.wait_until_present(timeout: 120)
    username = user_type.include?("admin") && user_type.include?("non") ? FigNewton.username_cp_nonadmin : FigNewton.username_cp
    password = user_type.include?("admin") && user_type.include?("non") ? FigNewton.password_cp_nonadmin : FigNewton.password_cp
    login_element.click
  rescue
    @browser.refresh
    (tries -= 1).zero? ? raise("Unable to log-in to CP.") : retry
  end

  def login_as_salespartner
    tries ||= 5
    username_element.wait_until_present(timeout: 120)
    username = FigNewton.username_spi
    password = FigNewton.password_spi
    login_element.click
  rescue
    @browser.refresh
    (tries -= 1).zero? ? raise("Unable to log-in to SPI.") : retry
  end

  def logout
    tries ||= 5
    page_loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    logoff
    username_element.wait_until_present(timeout: 120)
  rescue Selenium::WebDriver::Error::UnknownError
    @browser.refresh
    (tries -= 1).zero? ? raise("Unable to log-out from CP.") : retry
  end
end
