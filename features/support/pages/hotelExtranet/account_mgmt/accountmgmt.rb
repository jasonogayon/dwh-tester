# Hotel Extranet Account Management Page
class AccountManagementPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  span(:users, xpath: "//*[@id='treeNavAcctMan']//li[1]//a/span")
  span(:logs, xpath: "//*[@id='treeNavAcctMan']//li[2]//a/span")
  span(:help, xpath: "//*[@id='treeNavAcctMan']//li[3]//a/span")

  # Methods
  def goto(page)
    loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    if page.include? "users"
      users_element.click
    elsif page.include? "logs"
      logs_element.click
    elsif page.include? "help"
      help_element.click
    end
    loading_element.wait_while_present(timeout: 300)
  end
end
