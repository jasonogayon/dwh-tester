# Hotel Extranet Users Page
class UsersPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridUsers']/div[2]/div[2]/div/div/div[2]/div/div/table/tbody/tr[contains(.,'"
  LOCATOR_MID = "')]/td"
  LOCATOR_END = "/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate username')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridUsers']//button[contains(.,'Add User')]")
  button(:save, xpath: "//*[@id='winUsersform']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='winUsersform']//button[contains(.,'Cancel')]")
  text_field(:name, namh: "user_full_name")
  text_field(:username, namh: "user_name")
  text_field(:email, namh: "user_email")
  text_field(:designation, xpath: "//div[@id='winUsersform']/div/form/div[4]/div/div/input[2]")

  # Methods
  def add_user(info)
    loading_element.wait_while_present(timeout: 300)
    add
    user = "#{info['name']} #{rand(1000)}"
    input_user_details("add", user, info)
    save
    verify_feedback(info)
    return user
  end

  def update_user(info)
    loading_element.wait_while_present(timeout: 300)
    user = info["name"]
    self.class.div(:old_user, xpath: LOCATOR_BEGIN + user + LOCATOR_MID + LOCATOR_END)
    old_user_element.wait_until_present(timeout: 120)
    old_user_element.click
    name_element.wait_until_present(timeout: 120)
    please_wait_element.wait_while_present(timeout: 300)
    input_user_details("edit", user, info)
    save
    verify_feedback(info)
    return user
  end

  def delete_user(user)
    self.class.image(:option, xpath: "//td[contains(., '#{user}')]/following::td[5]/div/img[2]")
    option_element.when_visible.click
    click_confirm_element.when_visible.click
    feedback_element.wait_until_present(timeout: 120)
    option_element.wait_while_present(timeout: 300)
  end

  def get_userinfo(user, element)
    info = {}
    for i in 1..5
      key = i.to_s
      self.class.div(:user_key, xpath: "#{LOCATOR_BEGIN}#{user}#{LOCATOR_MID}[#{key}]#{LOCATOR_END}")
      info[key.to_sym] = user_key_element.text
    end
    actual = info[:"#{1}"] if element.include?("name")
    actual = info[:"#{2}"] if element.include?("username")
    actual = info[:"#{3}"] if element.include?("email")
    actual = info[:"#{4}"] if element.include?("designation")
    actual = info[:"#{5}"] if element.include?("property")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_user_status(user)
    self.class.div(:user, xpath: LOCATOR_BEGIN + user.to_s + LOCATOR_MID + LOCATOR_END)
    return !user_element.present?
  end

  private

  def input_user_details(update_type, user, info)
    name_element.wait_until_present(timeout: 120)
    name_element.clear
    sleep 1
    name = user
    username = "#{info["username"]}.#{rand(1_000_000)}" if update_type.include? "add"
    email_element.clear
    email = info["email"]
    designation_element.click
    loading_element.wait_while_present(timeout: 300)
    select_option(info["designation"])
  end

  def verify_feedback(info)
    loading_element.wait_until_present(timeout: 20)
    loading_element.wait_while_present(timeout: 300)
  rescue
    begin
      duplicate_confirm_element.wait_until_present(timeout: 3)
      duplicate_confirm_element.click
      username = "#{info['username']}.#{rand(1_000_000)}"
      save
    rescue
    end
  end
end
