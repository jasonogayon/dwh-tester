# Hotel Extranet Activity Logs
class ActivityLogs
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:createdpromocode, xpath: "//div[@id='gridLogs']//tr[contains(.,'Promo code')]//td[4]/div")
  div(:editlog_promocode, xpath: "//div[@id='gridLogs']//div[@class='x-grid3-scroller']//div[2]//tr[contains(.,'Promo code for Rate plan for Promocode only')]//td[4]/div")
  cell(:date_label, xpath: "//div[@id='gridLogs']//tr[@class='x-grid3-hd-row']/td[1]")

  # Methods
  def log_new_promocode
    date_label_element.wait_until_present(timeout: 120)
    createdpromocode_element.wait_until_present(timeout: 120)
    return createdpromocode
  end

  def log_update_promocode
    date_label_element.wait_until_present(timeout: 120)
    editlog_promocode_element.wait_until_present(timeout: 120)
    return editlog_promocode
  end

  def log_delete_promocode(promocode)
    self.class.div(:deletedpromocode, xpath: "//div[@id='gridLogs']//tr[contains(.,'Promo code #{promocode} removed')]//td[4]/div")
    date_label_element.wait_until_present(timeout: 120)
    deletedpromocode_element.wait_until_present(timeout: 120)
    return deletedpromocode
  end
end
