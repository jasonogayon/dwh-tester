# Hotel Extranet Login Page
class LoginPage
  include PageObject
  include DataLookup
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:login, id: "loginsubmit")
  link(:logoff, id: "lnklogout")
  text_field(:username, id: "loginUsername")
  text_field(:password, id: "loginPass")

  # Methods
  def login(hotel_type, rsvsn_type)
    begin
      tries ||= 5
      username_element.wait_until_present(timeout: 120)
      username = get_username(hotel_type, rsvsn_type)
      password = get_password(hotel_type, rsvsn_type)
      login_element.click
    rescue
      @browser.refresh
      (tries -= 1).zero? ? raise("Unable to log-in to HE.") : retry
    end
    page_loading_element.wait_while_present(timeout: 300)
  end

  def logout
    tries ||= 5
    page_loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
    logoff
    username_element.wait_until_present(timeout: 120)
  rescue Selenium::WebDriver::Error::UnknownError
    @browser.refresh
    (tries -= 1).zero? ? raise("Unable to log-out from HE.") : retry
  end
end
