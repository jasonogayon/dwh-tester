# Hotel Extranet Extra Beds Page
class ExtraBedsPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  span(:rate_currency, xpath: "//div[@id='panelExtraBeds']/div[2]/form/div/div/div/div/div[1]/div/div/fieldset/legend/span")
  button(:save, xpath: "//table[@id='extra_bed_save_btn']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='panelExtraBeds']//button[contains(.,'Cancel')]")
  text_field(:without_breakfast_rate, id: "extra_bed_wo_breakfast_rate")
  text_field(:with_breakfast_rate, id: "extra_bed_w_breakfast_rate")

  # Methods
  def update_extrabed(info)
    without_breakfast_rate = info["without_breakfast_rate"] unless info["without_breakfast_rate"].nil? || info["without_breakfast_rate"].empty?
    with_breakfast_rate = info["with_breakfast_rate"] unless info["with_breakfast_rate"].nil? && info["with_breakfast_rate"].empty?
    save_element.wait_until_present(timeout: 120)
    save
    feedback_element.wait_until_present(timeout: 120)
    feedback.include?("Extra bed configuration successfully updated.").should == true
  end

  def get_extrabedinfo(element)
    actual = rate_currency if element.include?("currency")
    actual = with_breakfast_rate if element.include?("with")
    actual = without_breakfast_rate if element.include?("without")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end
end
