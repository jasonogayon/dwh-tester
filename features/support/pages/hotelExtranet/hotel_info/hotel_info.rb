# Hotel Extranet Hotel Information Page
class HotelInformationPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  span(:property_info, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Property Information')]")
  span(:contacts, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Contacts')]")
  span(:facilities, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Hotel Facilities & Services')]")
  span(:amenities, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Room Amenities')]")
  span(:rooms, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Rooms')]")
  span(:extrabeds, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Extra Beds')]")
  span(:taxes, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Taxes & Surcharges')]")
  span(:advisories, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Hotel Policies')]")
  span(:gallery, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Image gallery')]")
  span(:addons, xpath: "//*[@id='treeNavHotel']//span[contains(.,'Reservation Add-ons')]")

  # Methods
  def goto(page)
    loading_element.wait_while_present(timeout: 300)
    loading_mask_element.wait_while_present(timeout: 300)
    if page.include? "property"
      property_info_element.click
    elsif page.include? "contact"
      contacts_element.click
    elsif page.include? "facility"
      facilities_element.click
    elsif page.include? "amenity"
      amenities_element.click
    elsif page.include? "room"
      rooms_element.click
    elsif page.include? "extrabed"
      extrabeds_element.click
    elsif page.include? "tax"
      taxes_element.click
    elsif page.include? "advisory"
      advisories_element.click
    elsif page.include? "gallery"
      gallery_element.click
    elsif page.include? "addon"
      addons_element.click
    end
    loading_element.wait_while_present(timeout: 300)
  end
end
