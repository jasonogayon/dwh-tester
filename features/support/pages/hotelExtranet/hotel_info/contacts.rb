# Hotel Extranet Contacts Page
class HotelExtranet_ContactsPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridContacts']/div[2]/div[2]/div/div/div[2]/div/div/table/tbody/tr"
  LOCATOR_MID = "/td[contains(., '"
  LOCATOR_END = "')]/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate contact name')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridContacts']//button[contains(.,'Add Contacts')]")
  button(:save, xpath: "//*[@id='winContactsform']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='winContactsform']//button[contains(.,'Cancel')]")
  checkbox(:recipient_flag, name: "report_recipient_flag")
  text_field(:name, id: "contact_name")
  text_field(:designation, id: "contact_designation")
  text_field(:department, id: "role_cd")
  text_field(:phone, id: "contact_tel")
  text_field(:email, id: "contact_email")
  text_field(:messenger, id: "improtocol")
  text_field(:messenger_id, id: "imid")

  # Methods
  def add_contact(info)
    loading_element.wait_while_present(timeout: 300)
    contact = "#{info['name']} #{rand(1000)}"
    add
    input_contact_details(contact, info)
    save
    verify_feedback(info)
    return contact
  end

  def update_contact(info)
    loading_element.wait_while_present(timeout: 300)
    contact = info["name"]
    self.class.div(:old_contact, xpath: LOCATOR_BEGIN + LOCATOR_MID + contact + LOCATOR_END)
    old_contact_element.wait_until_present(timeout: 120)
    old_contact_element.click
    input_contact_details(contact, info)
    save
    verify_feedback(info)
    return contact
  end

  def delete_contact(contact)
    loading_element.wait_while_present(timeout: 300)
    delete_item("contact", contact)
  end

  def get_contactinfo(contact, element)
    element = element.downcase
    info = {}
    for i in 1..7
      key = i.to_s
      self.class.div(:contact_key, xpath: "#{LOCATOR_BEGIN}[contains(., '#{contact}')]/td[#{key}]/div")
      info[key.to_sym] = contact_key_element.text
    end
    actual = info[:"#{1}"] if element.include?("name")
    actual = info[:"#{2}"] if element.include?("designation")
    actual = info[:"#{3}"] if element.include?("phone")
    actual = info[:"#{4}"] if element.include?("email")
    actual = info[:"#{5}"] if element.include?("department")
    actual = info[:"#{6}"] if element.include?("messenger")
    actual = info[:"#{7}"] if element.include?("id")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_contact_recipient_status(contact)
    loading_element.wait_while_present(timeout: 300)
    self.class.div(:old_contact, xpath: LOCATOR_BEGIN + LOCATOR_MID + contact + LOCATOR_END)
    old_contact_element.wait_until_present(timeout: 120)
    old_contact_element.click
    recipient_flag_element.wait_until_present(timeout: 120)
    sleep 1
    return recipient_flag_element.checked?
  end

  def get_contact_status(contact)
    self.class.div(:contact, xpath: LOCATOR_BEGIN + LOCATOR_MID + contact.to_s + LOCATOR_END)
    return !contact_element.present?
  end

  private

  def input_contact_details(contact, info)
    name_element.wait_until_present(timeout: 120)
    name_element.clear
    sleep 1
    name = contact
    designation_element.clear
    sleep 1
    designation = info["designation"]
    department_element.click
    loading_element.wait_while_present(timeout: 300)
    select_option(info["department"])
    phone = info["phone"]
    email = info["email"]
    unless messenger.nil? || messenger_id.nil? || messenger.empty? || messenger_id.empty?
      messenger_id = info["messenger_id"]
      messenger_element.click
      loading_element.wait_while_present(timeout: 300)
      select_option(info["messenger"])
    end
    check_recipient_flag unless info["isrecipient"].nil? || info["isrecipient"].empty?
  end

  def verify_feedback(info)
    feedback_element.wait_until_present(timeout: 10)
  rescue
    begin
      duplicate_confirm_element.wait_until_present(timeout: 3)
      duplicate_confirm_element.click
      name = "#{info['name']}.#{rand(1_000_000)}"
      save
    rescue
    end
  end
end
