# Hotel Extranet Facilities Page
class FacilitiesPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridFacilities']/div[2]/div[2]/div/div/div[2]/div/div/table/tbody/tr"
  LOCATOR_MID = "/td[contains(., '"
  LOCATOR_END = "')]/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate facility')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridFacilities']//button[contains(.,'Add')]")
  button(:save, xpath: "//*[@id='win_addfacility']//button[contains(.,'OK')]")
  button(:cancel, xpath: "//*[@id='win_addfacility']//button[contains(.,'Cancel')]")
  text_field(:name, xpath: "//*[@name='facility_name']/following::input")

  # Methods
  def add_facility(info)
    loading_element.wait_while_present(timeout: 300)
    facility = "#{info['name']} #{rand(1000)}"
    add
    name_element.wait_until_present(timeout: 120)
    name = facility
    save
    verify_feedback(info)
    return facility
  end

  def delete_facility(facility)
    loading_element.wait_while_present(timeout: 300)
    delete_item("facility", facility)
  end

  def get_facilityinfo(facility)
    info = {}
    for i in 1..2
      key = i.to_s
      self.class.div(:facility_key, xpath: "#{LOCATOR_BEGIN}[contains(., '#{facility}')]/td[#{key}]/div")
      info[key] = facility_key_element.text
    end
    return info
  end

  def get_facility_status(facility)
    self.class.div(:facility, xpath: LOCATOR_BEGIN + LOCATOR_MID + facility.to_s + LOCATOR_END)
    return !facility_element.present?
  end

  private

  def verify_feedback(info)
    duplicate_confirm_element.wait_until_present(timeout: 3)
    duplicate_confirm_element.click
    name = "#{info['name']}.#{rand(1_000_000)}"
    save
  rescue
  end
end
