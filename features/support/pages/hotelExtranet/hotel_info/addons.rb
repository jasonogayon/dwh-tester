# Hotel Extranet Add-ons Page
class AddonsPage
  include PageObject
  include HotelExtranetHelper

  FILE_ABSOLUTE_PATH = File.absolute_path("../../../../../", File.dirname(__FILE__))

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridReservationAddOns']/div/div[2]/div/div/div[2]/div/div/table/tbody/tr"
  LOCATOR_MID = "/td[contains(.,'"
  LOCATOR_END = "')]/div/span"

  # Element Locators
  button(:add, xpath: "//*[@id='gridReservationAddOns']//button[contains(.,'Create New Reservation Add-on')]")
  button(:add_save, xpath: "//*[@id='winCreateAddOns']//button[contains(.,'Save')]")
  button(:add_cancel, xpath: "//*[@id='winCreateAddOns']//button[contains(.,'Cancel')]")
  button(:edit_save, xpath: "//table[@id='reservation_addons_save_btn']//button[contains(.,'Save')]")
  button(:edit_cancel, xpath: "//div[@id='form_edit_addons']//button[contains(.,'Cancel')]")
  textarea(:add_description, xpath: "//div[@id='form_create_addons']//textarea")
  textarea(:edit_description, xpath: "//div[@id='form_edit_addons']//textarea")
  text_field(:add_addon_name, xpath: "//div[@id='form_create_addons']//input[@name='name']")
  text_field(:add_price, xpath: "//div[@id='form_create_addons']//input[@name='rate']")
  text_field(:add_leadtime, xpath: "//div[@id='form_create_addons']//input[@name='lead_time']")
  text_field(:edit_addon_name, xpath: "//div[@id='form_edit_addons']//input[@name='name']")
  text_field(:edit_price, xpath: "//div[@id='form_edit_addons']//input[@name='rate']")
  text_field(:edit_leadtime, xpath: "//div[@id='form_edit_addons']//input[@name='lead_time']")
  file_field(:photo, xpath: "//div[@id='form_create_addons']//input[@name='file_name']")

  # Methods
  def add_addon(info)
    loading_element.wait_while_present(timeout: 300)
    add
    addon = "#{info['name']} #{rand(1000)}"
    add_addon_name_element.wait_until_present(timeout: 120)
    add_addon_name = addon
    add_description = "Test addon description"
    add_price = info["price"]
    add_leadtime = info["leadtime"] unless info["leadtime"].nil? || info["leadtime"].empty?
    Watir.relaxed_locate = false
    photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_addon_spa_photo)
    Watir.relaxed_locate = true
    add_save
    verify_feedback
    return addon
  end

  def update_addon(info)
    loading_element.wait_while_present(timeout: 300)
    addon = info["name"]
    self.class.span(:old_addon, xpath: LOCATOR_BEGIN + LOCATOR_MID + addon + LOCATOR_END)
    old_addon_element.wait_until_present(timeout: 120)
    old_addon_element.click
    edit_addon_name_element.wait_until_present(timeout: 120)
    edit_addon_name_element.clear
    edit_addon_name = addon
    edit_description_element.clear
    edit_description = "Test addon description"
    edit_price = info["price"]
    edit_leadtime = info["leadtime"] unless info["leadtime"].nil? || info["leadtime"].empty?
    edit_save
    verify_feedback
    return addon
  end

  def delete_addon(addon)
    loading_element.wait_while_present(timeout: 300)
    delete_item("addon", addon)
  end

  def get_addoninfo(addon, element)
    element = element.downcase
    actual = edit_addon_name if element.include? "name"
    actual = edit_description if element.include? "description"
    actual = edit_price if element.include? "price"
    actual = edit_leadtime if element.include? "lead time"
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_addon_status(addon)
    self.class.span(:addon, xpath: LOCATOR_BEGIN + LOCATOR_MID + addon.to_s + LOCATOR_END)
    return !addon_element.present?
  end

  private

  def verify_feedback
    feedback_element.wait_until_present(timeout: 10)
  end
end
