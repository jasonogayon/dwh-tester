# Hotel Extranet Taxes Page
class TaxesPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridTaxes']/div[2]/div[2]/div/div/div[2]/div/div/table/tbody/tr"
  LOCATOR_MID = "/td[contains(., '"
  LOCATOR_END = "')]/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate tax or surcharge name')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  span(:select_tax, xpath: "//span[@class='x-menu-item-text'][contains(., 'Tax')]")
  span(:select_surcharge, xpath: "//span[@class='x-menu-item-text'][contains(., 'Surcharge')]")
  button(:add, xpath: "//div[@id='gridTaxes']//td[@class='x-btn-mc'][contains(., 'Add')]/em/button")
  button(:save, xpath: "//*[@id='winTaxesform']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='winTaxesform']//button[contains(.,'Cancel')]")
  text_field(:name, name: "tax_surcharge_name")
  text_field(:percent_amount, name: "charge_amount")
  radio_button(:taxable_confirm, xpath: "//*[@id='radiotaxable']/div/div[1]//input")
  radio_button(:taxable_cancel, xpath: "//*[@id='radiotaxable']/div/div[2]//input")

  # Methods
  def add_tax(info)
    loading_element.wait_while_present(timeout: 300)
    tax = "#{info['name']} #{rand(1000)}"
    add
    select_tax_type(info)
    input_tax_details(tax, info)
    save
    verify_feedback(info)
    return tax
  end

  def update_tax(info)
    loading_element.wait_while_present(timeout: 300)
    tax = info["name"]
    self.class.div(:old_tax, xpath: LOCATOR_BEGIN + LOCATOR_MID + tax + LOCATOR_END)
    old_tax_element.wait_until_present(timeout: 120)
    old_tax_element.click
    name_element.wait_until_present(timeout: 120)
    please_wait_element.wait_while_present(timeout: 300)
    input_tax_details(tax, info)
    save
    verify_feedback(info)
    return tax
  end

  def delete_tax(tax)
    loading_element.wait_while_present(timeout: 300)
    delete_item("tax", tax)
  end

  def get_taxinfo(tax, element)
    info = {}
    for i in 1..5
      key = i.to_s
      self.class.div(:tax_key, xpath: "#{LOCATOR_BEGIN}[contains(., '#{tax}')]/td[#{key}]/div")
      info[key.to_sym] = tax_key_element.text
    end
    actual = info[:"#{1}"] if element.include?("name")
    actual = info[:"#{2}"] if element.include?("type")
    actual = info[:"#{3}"] if element.include?("scheme")
    actual = info[:"#{4}"] if element.include?("percent")
    actual = (info[:"#{5}"] == "NA" ? "be non-taxable" : "be taxable") if element.include?("taxability")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_tax_status(tax)
    self.class.div(:tax, xpath: LOCATOR_BEGIN + LOCATOR_MID + tax + LOCATOR_END)
    return !tax_element.present?
  end

  private

  def select_tax_type(info)
    if info["tax_type"].downcase.include? "tax"
      select_tax_element.wait_until_present(timeout: 120)
      select_tax_element.click
    else
      select_surcharge_element.wait_until_present(timeout: 120)
      select_surcharge_element.click
    end
  end

  def input_tax_details(tax, info)
    name_element.wait_until_present(timeout: 120)
    name_element.clear
    sleep 1
    name = tax
    percent_amount_element.clear
    sleep 1
    percent_amount = info["percent"]
    unless info["tax_type"].downcase.include? "tax"
      info["istaxable"].nil? || info["istaxable"].empty? ? select_taxable_cancel : select_taxable_confirm
    end
  end

  def verify_feedback(info)
    duplicate_confirm_element.wait_until_present(timeout: 3)
    duplicate_confirm_element.click
    name = "#{info['name']}.#{rand(1_000_000)}"
    save
  rescue
  end
end
