# Hotel Extranet Property Information Page
class PropertyInformationPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:api_key, id: "api_key")
  button(:add_reservation_email, xpath: "//*[@id='gridReservationEmails']//button[contains(.,'Add')]")
  button(:save, xpath: "//*[@id='panelPropertyInfo']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='panelPropertyInfo']//button[contains(.,'Cancel')]")
  textarea(:address, id: "property_address")
  text_field(:name, name: "property_name")
  text_field(:group, name: "property_group")
  text_field(:type, id: "property_type")
  text_field(:country, id: "property_country")
  text_field(:timezone, id: "property_timezone")
  text_field(:phone, id: "property_telno")
  text_field(:email, id: "property_email")
  text_field(:currency, id: "property_currency")
  text_field(:input_reservation_email, id: "reservation_email_textfield")
  text_field(:longitude, id: "property_longitude")
  text_field(:latitude, id: "property_latitude")

  # Methods
  def update_propertyinfo(info)
    edit_property_type(info["type"])
    edit_property_address(info["address"])
    edit_property_phone(info["phone"])
    edit_property_email(info["email"])
    edit_property_longitude(info["longitude"])
    edit_property_latitude(info["latitude"])
    save_element.wait_until_present(timeout: 120)
    save
    feedback_element.wait_until_present(timeout: 120)
    feedback.include?("Property information successfully updated.").should == true
  end

  def get_propertyinfo(element)
    element = element.downcase
    actual = name if element.include?("name")
    actual = group if element.include?("group")
    actual = type if element.include?("type")
    actual = country if element.include?("country")
    actual = address if element.include?("address")
    actual = timezone if element.include?("timezone")
    actual = phone if element.include?("phone")
    actual = email if element.include?("email")
    actual = currency if element.include?("currency")
    actual = api_key if element.include?("api")
    actual = longitude if element.include?("longitude")
    actual = latitude if element.include?("latitude")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  private

  def edit_property_type(hotel_type)
    loading_element.wait_while_present(timeout: 300)
    unless hotel_type.nil? || hotel_type.empty?
      type_element.click
      loading_element.wait_while_present(timeout: 300)
      select_option(hotel_type)
    end
  end

  def edit_property_address(hotel_address)
    loading_element.wait_while_present(timeout: 300)
    address = hotel_address unless hotel_address.nil?
  end

  def edit_property_phone(phone_number)
    loading_element.wait_while_present(timeout: 300)
    phone = phone_number unless phone_number.nil?
  end

  def edit_property_email(email_address)
    loading_element.wait_while_present(timeout: 300)
    email = email_address unless email_address.nil?
  end

  def edit_property_longitude(location)
    loading_element.wait_while_present(timeout: 300)
    longitude = location unless location.nil?
  end

  def edit_property_latitude(location)
    loading_element.wait_while_present(timeout: 300)
    latitude = location unless location.nil?
  end
end
