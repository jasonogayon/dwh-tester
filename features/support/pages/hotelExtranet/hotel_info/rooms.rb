# Hotel Extranet Rooms Page
class RoomsPage
  include PageObject
  include HotelExtranetHelper

  FILE_ABSOLUTE_PATH = File.absolute_path('../../../../../', File.dirname(__FILE__))

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridRoomTypes']/div/div[2]/div/div/div[2]/div/div/table/tbody/tr"
  LOCATOR_MID = "/td[contains(., '"
  LOCATOR_END = "')]/div/span"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate room name')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridRoomTypes']//button[contains(.,'Add Room Type')]")
  button(:add_save, xpath: "//*[@id='winRoomType']//button[contains(.,'Save')]")
  button(:add_cancel, xpath: "//*[@id='winRoomType']//button[contains(.,'Cancel')]")
  button(:edit_save, xpath: "//table[@id='room_save_btn']//button[contains(.,'Save')]")
  button(:edit_cancel, xpath: "//div[@id='panelRoomTypesInfo']//button[contains(.,'Cancel')]")
  button(:edit_max_adult_confirm, xpath: "//span[contains(.,'Changing the maximum number of adults')]/preceding::div[6]/following::button[contains(.,'Yes')]")
  textarea(:add_description, id: "roomtype_description")
  textarea(:edit_description, name: "description")
  file_field(:add_photo, id: "first_roomtype_photo_file-file")
  text_field(:add_standard_name, id: "roomtype_add_base")
  text_field(:add_standard_view, id: "roomtype_add_view")
  text_field(:add_standard_bed, id: "roomtype_add_bedconfig")
  text_field(:add_custom_name, id: "roomtype_name")
  text_field(:add_size_value, xpath: "//*[@id='add_roomtypes']/div/form/div[1]/div/input")
  text_field(:add_size_unit, xpath: "//*[@id='add_roomtypes']/div/form/div[2]/div/div/input[2]")
  text_field(:add_max_adult, xpath: "//*[@id='add_roomtypes']/div/form/div[3]/div/input")
  text_field(:add_max_child, xpath: "//*[@id='add_roomtypes']/div/form/div[4]/div/div/input[2]")
  text_field(:add_inventory, id: "roomtype_add_roomcount")
  text_field(:add_initial_availability, id: "roomtype_add_onlineavailability")
  text_field(:edit_standard_name, id: "roomtype_edit_name")
  text_field(:edit_standard_view, id: "roomtype_edit_view")
  text_field(:edit_standard_bed, id: "roomtype_edit_bed")
  text_field(:edit_custom_name, id: "roomtype_edit_customname")
  text_field(:edit_size_value, name: "room_size")
  text_field(:edit_size_unit, xpath: "//input[@name='room_size_unit_cd']/following::input[1]")
  text_field(:edit_max_adult, name: "max_adult")
  text_field(:edit_max_child, xpath: "//input[@name='max_child']/following::input[1]")
  text_field(:edit_inventory, name: "number_rooms")
  radio_button(:add_use_standard_name, id: "roomtype_add_base")
  radio_button(:add_use_custom_name, xpath: "//*[@id='grouproomflag']/div/div/div[2]/div/div/input")
  radio_button(:edit_use_standard_name, xpath: "//label[contains(.,'Use custom name')]/preceding::input[@name='room_flag'][2]")
  radio_button(:edit_use_custom_name, xpath: "//label[contains(.,'Use custom name')]/preceding::input[@name='room_flag'][1]")

  # Methods
  def add_room(info)
    loading_element.wait_while_present(timeout: 300)
    room = "#{info['name']} #{rand(1000)}"
    add
    add_custom_name_element.wait_until_present(timeout: 120)
    select_add_use_custom_name
    add_custom_name = room
    add_size_value = info["size"]
    add_size_unit_element.click
    select_option(info["unit"])
    add_max_adult = info["adult"]
    add_max_child_element.click
    select_option(info["child"])
    add_inventory = info["inventory"]
    add_initial_availability = info["availability"]
    add_description = "Test room description"
    Watir.relaxed_locate = false
    add_photo = File.join(FILE_ABSOLUTE_PATH, FigNewton.image_room_accommodation_photo)
    Watir.relaxed_locate = true
    begin
      tries ||= 3
      add_save_element.wait_until_present(timeout: 120)
      add_save
    rescue
      authentication_server_failure_element.wait_until_present(timeout: 120)
      authentication_server_failure
      puts "Warning: It seems that an Authentication Server Error was encountered when the test ran. Please verify if not an actual error."
      retry unless (tries -= 1).zero?
    end
    verify_feedback(info)
    return room
  end

  def update_room(info)
    loading_element.wait_while_present(timeout: 300)
    room = info["name"]
    self.class.span(:old_room, xpath: LOCATOR_BEGIN + LOCATOR_MID + info["name"] + LOCATOR_END)
    old_room_element.wait_until_present(timeout: 120)
    old_room_element.click
    edit_custom_name_element.wait_until_present(timeout: 120)
    select_edit_use_custom_name
    edit_custom_name = room
    edit_size_value = info["size"]
    edit_size_unit_element.click
    select_option(info["unit"])
    adult = edit_max_adult
    edit_max_adult = info["adult"]
    edit_max_adult_element.send_keys :tab
    if adult != info["adult"]
      edit_max_adult_confirm_element.wait_until_present(timeout: 120)
      edit_max_adult_confirm
    end
    edit_max_child_element.click
    select_option(info["child"])
    edit_inventory = info["inventory"]
    edit_description = "Test room description"
    edit_save
    verify_feedback(info)
    return room
  end

  def delete_room(room)
    loading_element.wait_while_present(timeout: 300)
    delete_item("room", room)
  end

  def get_roominfo(room, element)
    element = element.downcase
    actual = edit_custom_name if element.include?("name")
    actual = edit_size_value if element.include?("size")
    actual = edit_size_unit if element.include?("unit")
    actual = edit_max_adult if element.include?("adult")
    actual = edit_max_child if element.include?("child")
    actual = edit_inventory if element.include?("inventory")
    actual = edit_description if element.include?("description")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_room_status(room)
    self.class.div(:room, xpath: LOCATOR_BEGIN + LOCATOR_MID + room.to_s + LOCATOR_END)
    return !room_element.present?
  end

  private

  def verify_feedback(info)
    raise "It seems that an Authentication Server Error was encountered when the test ran. Please verify if not an actual error." if authentication_server_failure_element.present?
    feedback_element.wait_until_present(timeout: 10)
  rescue
    begin
      duplicate_confirm_element.wait_until_present(timeout: 3)
      duplicate_confirm_element.click
      custom_name = "#{info['name']}.#{rand(1_000_000)}"
      save
    rescue
    end
  end
end
