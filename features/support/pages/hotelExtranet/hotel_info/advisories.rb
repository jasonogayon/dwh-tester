# Hotel Extranet Advisories Page
class AdvisoriesPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridAdvisory']/div/div[2]/div/div/div[2]/div/div[contains(., '"
  LOCATOR_MID = "')]/table/tbody/tr/td"
  LOCATOR_END = "/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Hotel policy name must be unique')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridAdvisory']//button[contains(.,'Add Hotel Policy')]")
  button(:save, xpath: "//*[@id='winAdvisoryform']//button[contains(.,'Save')]")
  button(:cancel, xpath: "//*[@id='winAdvisoryform']//button[contains(.,'Cancel')]")
  checkbox(:disable_end_date, id: "advisory_form_disable_end_date")
  textarea(:description, name: "advisory_detail")
  text_field(:name, name: "advisory_name")
  text_field(:start_date, id: "advisory_form_startdate")
  text_field(:end_date, id: "advisory_form_enddate")

  # Methods
  def add_advisory(info)
    loading_element.wait_while_present(timeout: 300)
    advisory = "#{info['name']} #{rand(1000)}"
    add_element.wait_until_present(timeout: 120)
    add
    input_advisory_details(advisory, info)
    save
    verify_feedback(info)
    return advisory
  end

  def update_advisory(info)
    loading_element.wait_while_present(timeout: 300)
    advisory = info["name"]
    self.class.div(:old_advisory, xpath: LOCATOR_BEGIN + advisory + LOCATOR_MID + LOCATOR_END)
    old_advisory_element.wait_until_present(timeout: 120)
    old_advisory_element.click
    name_element.wait_until_present(timeout: 120)
    please_wait_element.wait_while_present(timeout: 300)
    input_advisory_details(advisory, info)
    save
    verify_feedback(info)
    return advisory
  end

  def delete_advisory(advisory)
    loading_element.wait_while_present(timeout: 300)
    delete_item("advisory", advisory)
  end

  def get_advisoryinfo(advisory, element)
    info = {}
    for i in 1..4
      key = i.to_s
      self.class.div(:advisory_key, xpath: "#{LOCATOR_BEGIN }#{advisory}#{LOCATOR_MID}[#{key}]/div")
      info[key.to_sym] = advisory_key_element.text
    end
    actual = info[:"#{1}"] if element.include?("name")
    actual = info[:"#{2}"] if element.include?("description")
    actual = info[:"#{3}"] if element.include?("start")
    actual = (info[:"#{4}"].nil? || info[:"#{4}"].empty? ? "anytime" : info[:"#{4}"]) if element.include?("end") || element.include?("availability")
    actual = "blank" if actual.nil? || actual.empty?
    return actual
  end

  def get_advisory_status(advisory)
    self.class.div(:advisory, xpath: LOCATOR_BEGIN + advisory.to_s + LOCATOR_MID + LOCATOR_END)
    return !advisory_element.present?
  end

  private

  def input_advisory_details(advisory, info)
    name_element.wait_until_present(timeout: 120)
    name_element.clear
    sleep 1
    name = advisory
    description = "Test advisory description"
    if info["rolling"].nil? || info["rolling"].empty?
      start_date = info["start"]
      end_date = info["end"]
    else
      check_disable_end_date
    end
  end

  def verify_feedback(info)
    feedback_element.wait_until_present(timeout: 10)
  rescue
    begin
      duplicate_confirm_element.wait_until_present(timeout: 3)
      duplicate_confirm_element.click
      name = "#{info["name"]}.#{rand(1_000_000)}"
      save
    rescue
    end
  end
end
