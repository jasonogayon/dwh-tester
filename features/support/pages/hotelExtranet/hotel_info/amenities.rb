# Hotel Extranet Amenities Page
class AmenitiesPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  LOCATOR_BEGIN = "//div[@id='gridAmenities']/div[2]/div[2]/div/div/div[2]/div/div/table/tbody/tr/td[contains(., '"
  LOCATOR_END = "')]/div"

  # Element Locators
  cell(:duplicate_confirm, xpath: "//span[contains(., 'Duplicate amenity')]/preceding::div[6]/following::div[1]/div[2]/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")
  button(:add, xpath: "//*[@id='gridAmenities']//button[contains(.,'Add')]")
  button(:save, xpath: "//*[@id='win_addamenity']//button[contains(.,'OK')]")
  button(:cancel, xpath: "//*[@id='win_addamenity']//button[contains(.,'Cancel')]")
  text_field(:name, xpath: "//*[@name='amenity_name']/following::input")

  # Methods
  def add_amenity(info)
    loading_element.wait_while_present(timeout: 300)
    amenity = "#{info['name']} #{rand(1000)}"
    add
    name_element.wait_until_present(timeout: 120)
    name = amenity
    save
    verify_feedback(info)
    return amenity
  end

  def delete_amenity(amenity)
    loading_element.wait_while_present(timeout: 300)
    delete_item("amenity", amenity)
  end

  def get_amenity_status(amenity)
    self.class.div(:amenity, xpath: LOCATOR_BEGIN + amenity.to_s + LOCATOR_END)
    return !amenity_element.present?
  end

  private

  def verify_feedback(info)
    duplicate_confirm_element.wait_until_present(timeout: 3)
    duplicate_confirm_element.click
    name = "#{info['name']}.#{rand(1_000_000)}"
    save
  rescue
  end
end
