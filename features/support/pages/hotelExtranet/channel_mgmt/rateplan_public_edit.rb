# Hotel Extranet Update Public Rate Plan
class EditPublicRatePlanPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  button(:editpanel_bookingconditions_editpromocode, xpath: "//div[@id='rate_plan_edit_tabpanel']//div[@class='x-column-inner']/div[4]//td[@class='x-btn-mc']/em/button")
  button(:editpanel_bookingconditions_deletepromocode, xpath: "//div[@id='rate_plan_edit_tabpanel']//div[@class='x-column-inner']/div[5]//td[@class='x-btn-mc']/em/button")
  button(:editpanel_promocode_yesdelete_modal, xpath: "//div[contains(@class,'x-window-bl')]//td[@class='x-btn-mc']//button[contains(.,'Yes')]")
  button(:editpanel_save_toolbar, xpath: "//div[@id='rate_plan_edit_panel']//div[@class='x-panel x-panel-noborder x-form-label-left']//button[contains(.,'Save')]")
  button(:editpanel_bookingconditions_pencil, xpath: "//div[@id='panel_rate_plans']//div[contains(@class,'x-column-inner')]//div[4]//button[@class='x-btn-text']")
  div(:label_rateplan_code, xpath: "//div[@id='rate_plan_list_card']//table//div[contains(.,'Rate Plan Code')]")
  div(:promocode_avail_copy, xpath: "//div[@id='panel_rate_plans']//div[@id='displayFieldPromoCode']")
  div(:editpanel_bookingconditions_rateplan_caption, xpath: "//div[@id='rate_plan_list_card']//div//form//div//div[contains(.,'This rate plan is valid for reservations with check-in')]")
  div(:label_promocode_rateplan, xpath: "//div[@id='rate_plan_edit_panel']//form//div[@class='x-column-inner']/div[contains(.,'Setup a promo code for this rate plan')]")
  div(:promocode_error_message, xpath: "//div[contains(@id,'panel_rate_plans')]//div/input[contains(@class,'txtPublicPromoCode')]/following::div[1]")
  div(:editpanel_bookingconditions_copypromocodelink, xpath: "//div[@id='panel_rate_plans']//div[@id='rate_plan_edit_panel']//div[contains(@class,'public_code_ibe_link')]")
  label(:editpanel_parent_bookingconditions_modal_promocodecaption, xpath: "//div/label[contains(.,'Please use this link for promotional campaign:')]")
  label(:label_rateplan_type, xpath: "//div[contains(@id,'rate_plan_edit_tabpanel')]//label[contains(.,'Rate Plan Type:')]")
  span(:highlight_enabledisable, xpath: "//div[@id='rate_plan_list_card']//span[contains(.,'rate plan highlighting')]")
  span(:editpanel_parent_bookingconditions_header, xpath: "//div[contains(@id,'rate_plan_edit_tabpanel')]/div/div/ul/li//a/em//span[contains(.,'Booking Conditions')]")
  span(:editpanel_parent_bookingconditions_delete_alertmessage, xpath: "//div[contains(@class,'x-dlg-icon')]/div[2]/span[contains(.,'Are you sure you want to delete this Promo code?')]")
  text_field(:promocode_editpanel, xpath: "//div[contains(@id,'rate_plan_edit_panel')]//div[contains(@class,'x-panel-body x-panel-body-noheader x-panel-body-noborder')]/div/div/input[contains(@class,'txtPublicPromoCode')]")
  text_field(:promocode, xpath: "//div[contains(@id,'panel_rate_plans')]//div/input[contains(@class,'txtPublicPromoCode')]")
  text_field(:editpanel_bookingconditions_promocodelink, xpath: "//div[contains(@class,'x-window-mc')]//div/input[contains(@class,'calendar-link')]")

  # Methods
  def rateplan_name_link(promorateplan_name)
    self.class.div(:rateplan_name_list, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{promorateplan_name}')]//td[4]/div")
    loading_element.wait_while_present(timeout: 300)
    mask_element.wait_while_present(timeout: 300)
    label_rateplan_code_element.wait_until_present(timeout: 120)
    rateplan_name_list_element.wait_until_present(timeout: 120)
    rateplan_name_list_element.scroll_into_view
    mask_element.wait_while_present(timeout: 300)
    rateplan_name_list_element.click
  end

  def editpanel_bookingconditions_copypromocodelink
    editpanel_bookingconditions_copypromocodelink_element.wait_until_present(timeout: 120)
    return editpanel_bookingconditions_copypromocodelink_element.text
  end

  def editpanel_bookingconditions_promocodelink
    editpanel_bookingconditions_copypromocodelink_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_copypromocodelink_element.click
    editpanel_bookingconditions_promocodelink_element.wait_until_present(timeout: 120)
    return editpanel_bookingconditions_promocodelink_element.value
  end

  def editpanel_parent_bookingconditions_header
    highlight_enabledisable_element.wait_until_present(timeout: 120)
    label_rateplan_type_element.wait_until_present(timeout: 120)
    editpanel_parent_bookingconditions_header_element.click
  end

  def promocode_readonly
    return promocode_editpanel
  end

  def assign_promocode(promocode)
    label_promocode_rateplan_element.wait_until_present(timeout: 120)
    promocode = promocode
  end

  def promocode_copy
    promocode_element.wait_until_present(timeout: 120)
    promocode_element.value
  end

  def promocode_avail_copy_editsection
    editpanel_bookingconditions_rateplan_caption_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_rateplan_caption_element.click
    promocode_avail_copy_element.wait_until_present(timeout: 120)
    promocode_avail_copy
  end

  def promocode_error_copy
    editpanel_bookingconditions_rateplan_caption_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_rateplan_caption_element.click
    promocode_error_message
  end

  def save_toolbar
    editpanel_save_toolbar_element.wait_until_present(timeout: 120)
    editpanel_save_toolbar_element.click
  end

  def editpanel_bookingconditions_pencil
    editpanel_bookingconditions_pencil_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_pencil
  end

  def editpanel_parent_bookingconditions_enable_editpromocode
    promocode_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_editpromocode_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_editpromocode
  end

  def editpanel_bookingconditions_deletepromocode
    promocode_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_deletepromocode_element.wait_until_present(timeout: 120)
    editpanel_bookingconditions_deletepromocode_element.click
    editpanel_parent_bookingconditions_delete_alertmessage_element.wait_until_present(timeout: 120)
    editpanel_promocode_yesdelete_modal_element.wait_until_present(timeout: 120)
    editpanel_promocode_yesdelete_modal
  end
end
