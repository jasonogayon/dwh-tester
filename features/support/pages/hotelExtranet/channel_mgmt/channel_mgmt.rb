# Hotel Extranet Channel Management Page
class ChannelManagementPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  span(:calendar, xpath: "//div[@id='treeNavAvailability']//span[contains(., 'Calendar')]")
  span(:public_rateplan, xpath: "//div[@id='treeNavAvailability']//li[1]//li[2]//span[contains(., 'Rate Plans')]")
  span(:private_rateplan, xpath: "//div[@id='treeNavAvailability']//span[contains(., 'Private Rate Plans')]")

  # Methods
  def goto_calendar
    loading_element.wait_while_present(timeout: 300)
    calendar_element.wait_until_present(timeout: 120)
    calendar_element.click
  end

  def goto_rateplan(rateplan)
    rateplan = rateplan.nil? ? "public" : rateplan.downcase
    loading_element.wait_while_present(timeout: 300)
    if rateplan.include? "private"
      private_rateplan_element.wait_until_present(timeout: 120)
      private_rateplan_element.click
    else
      public_rateplan_element.wait_until_present(timeout: 120)
      public_rateplan_element.click
    end
  end
end
