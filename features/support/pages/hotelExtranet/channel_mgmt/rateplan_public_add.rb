# Hotel Extranet Add Public Rate Plan
class AddPublicRatePlanPage
  include PageObject
  include DatesHelper
  include HotelExtranetHelper
  include DataTransformHelper

  page_url FigNewton.login_he_url

  # Constants
  OPTION_DAY = "Day"
  OPTION_MONTH = "Month"
  OPTION_10 = "10"
  OPTION_25 = "25"
  OPTION_30 = "30"
  OPTION_50 = "50"
  OPTION_FULL = "Full Charge"
  OPTION_FIRST_NIGHT = "First Night"
  OPTION_NOCHARGE = "No Charge"
  OPTION_NOTALLOWED = "Not Allowed"
  OPTION_EFFECTIVE_IMMEDIATELY = "Effective Immediately"
  MAX_ROOM_RATE = 5000
  MAX_EXTRANIGHT = 100
  MAX_FREENIGHT = 10

  # Element Locators
  div(:wizard_parent_rateplantype_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[1]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_details_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[2]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_bookingconditions_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[3]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_roomsrates_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[4]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_policies_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[5]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_inclusions_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[6]/div/form/div[1]/div[1]/div")
  div(:wizard_parent_addons_header, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[6]/div/form/div[4]/div[1]/div")
  div(:wizard_child_details_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div[1]/div/form/div[1]/div[1]/div")
  div(:wizard_child_bookingconditions_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div[2]/div/form/div[1]/div[1]/div")
  div(:wizard_child_roomsrates_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div[3]/div/form/div[1]/div[1]/div[1]/div[1]/div[1]")
  div(:wizard_child_policies_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div[4]/div/form/div[1]/div[1]/div")
  div(:wizard_child_inclusions_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div/form/div[1]/div[1]/div[contains(.,'inclusions')]")
  div(:wizard_child_addons_header, xpath: "//div[@id='derived_rp_wizard_tabpanel']//div/form/div[4]/div[1]/div[contains(.,'add-on')]")
  div(:parent_room_list, xpath: "//div[@id='rate_plan_wizard_tabpanel']//form/div[5]//table/tbody//td[2]/div")
  div(:child_room_list, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[1]//table/tbody/tr/td[2]/div")
  div(:feedback, xpath: "//div[@class='divfeedbackmsg']")
  div(:parent_room_checkbox_accommodation, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[contains(., 'Accommodation')]/table//td[1]/div/div/div")
  div(:parent_room_checkbox_occupancy, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[contains(., 'Occupancy')]/table//td[1]/div/div/div")
  div(:parent_room_charging_occupancy, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[contains(., 'Occupancy')]/table//td[2]/div/div/div/div")
  div(:parent_room_extradayrates_key_accommodation, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[contains(., 'Accommodation')]/table//td[11]/div")
  div(:parent_room_extradayrates_key_occupancy, xpath: "//div[@id='rate_plan_wizard_tabpanel']//div[contains(., '1 person')]/table//td[11]/div")
  div(:child_room_checkbox_accommodation, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[1]//table//tr[contains(.,'Accommodation')]/td[1]/div")
  div(:child_room_checkbox_occupancy, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[1]//table//tr[contains(.,'Occupancy')]/td[1]/div")
  div(:promocode_error_message, xpath: "//div[contains(@id,'rate_plan_wizard_card')]//div/input[contains(@class,'txtPublicPromoCode')]/following::div[1]")
  div(:label_promocode_rateplan, xpath: "//div[@id='panel_rate_plans']//form//div[@class='x-column-inner']/div[contains(.,'Setup a promo code for this rate plan')]")
  div(:promocode_avail_copy, xpath: "//div[contains(@id,'rate_plan_wizard_card')]//div[contains(@class,'x-form-item x-hide-label')]//div[contains(@class,'x-form-display-field')]")
  divs(:parent_no_rooms, xpath: "//div[@id='rate_plan_wizard_tabpanel']//form/div[5]//table/tbody//td[2]/div")
  divs(:parent_no_occupancy, xpath: "//div[@id='rate_plan_wizard_tabpanel']//form/div[5]//table/tbody//td[2]/div[contains(.,'person')]")
  divs(:child_no_rooms, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[1]//table/tbody/tr/td[2]/div")
  span(:wizard_parent_title, xpath: "//div[@id='rate_plan_wizard_card']/div/span")
  span(:wizard_child_title, xpath: "//div[@id='derived_rp_wizard_card']/div/span")
  span(:promocodelink_bookingconditions, xpath: "//div[@id='panel_rate_plans']//span[contains(@class,'public_code_ibe_link')]")
  cell(:promocode_previewpanel, xpath: "//td[contains(.,'Promo Code:')]")
  button(:parent_next, xpath: "//div[@id='rate_plan_wizard_tabpanel']//button[contains(., 'Next')]")
  button(:parent_rates_next, xpath: "//div[@id='rate_plan_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Next')]")
  button(:parent_save, xpath: "//div[@id='rate_plan_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Save')]")
  button(:parent_save_and_publish, xpath: "//div[@id='rate_plan_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Save and Publish')]")
  button(:child_next, xpath: "//div[@id='derived_rp_wizard_tabpanel']//button[contains(., 'Next')]")
  button(:child_rates_next, xpath: "//div[@id='derived_rp_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Next')]")
  button(:child_save, xpath: "//div[@id='derived_rp_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Save')]")
  button(:child_save_and_publish, xpath: "//div[@id='derived_rp_wizard_tabpanel']/div[2]/div[2]//button[contains(., 'Save and Publish')]")
  checkbox(:parent_tag_specialpromo, name: "special_flag")
  checkbox(:parent_tag_mobileexclusive, name: "mobile_exclusive")
  checkbox(:child_tag_specialpromo, name: "special_flag")
  checkbox(:child_tag_mobileexclusive, name: "mobile_exclusive")
  checkbox(:child_set_booking_period, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Booking Period')]/preceding::input[1]")
  checkbox(:child_set_min_nights, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Minimum Nights')]/preceding::input[1]")
  checkbox(:child_set_max_nights, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Maximum Nights')]/preceding::input[1]")
  checkbox(:child_set_min_leadtime, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Minimum Lead Time')]/preceding::input[1]")
  checkbox(:child_set_max_leadtime, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Maximum Lead Time')]/preceding::input[1]")
  textarea(:parent_rateplan_description, name: "description")
  textarea(:child_rateplan_description, xpath: "//div[@id='derived_rp_wizard_tabpanel']//textarea[@name='description']")
  textarea(:meal_description, name: "meal_description")
  textarea(:children_charge, name: "charge_for_children")
  text_field(:parent_rateplan_name, name: "rate_plan_name")
  text_field(:child_rateplan_name, xpath: "//div[@id='derived_rp_wizard_tabpanel']//input[@name='rate_plan_name']")
  text_field(:rateplan_los_default, name: "minimum_nights")
  text_field(:rateplan_los_stay, name: "nights_stay")
  text_field(:rateplan_los_free, name: "nights_free")
  text_field(:rateplan_meal, xpath: "//input[@name='breakfast_id']/following::input[1]")
  text_field(:meal_custom, name: "meal_description_other")
  text_field(:parent_first_checkin, xpath: "//div[@id='rate_plan_wizard_tabpanel']//form/div[contains(.,'Start date')]//div[1]/div/div/div/div[1]//input")
  text_field(:parent_last_checkout, xpath: "//div[@id='rate_plan_wizard_tabpanel']//form/div[contains(.,'Start date')]//div[2]/div/div/div/div[1]//input")
  text_field(:child_first_checkin, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[contains(.,'Start date')]//div[1]/div/div/div/div[1]//input")
  text_field(:child_last_checkout, xpath: "//div[@id='derived_rp_wizard_tabpanel']//form/div[contains(.,'Start date')]//div[2]/div/div/div/div[1]//input")
  text_field(:promo_type, xpath: "//label[contains(.,'Select a pre-defined promo')]/following::div[1]//input")
  text_field(:rates_input, xpath: "//input[contains(@class,'x-form-text') and contains(@class ,'x-form-field') and contains(@class ,'x-form-num-field') and contains(@class ,'x-form-focus')]")
  text_field(:children_allowed, name: "children_allowed")
  text_field(:children_age, name: "child_age")
  text_field(:prepayment, name: "prepayment")
  text_field(:modification_period_type, name: "modify_effectivity_unit")
  text_field(:modification_period_value, name: "modify_effectivity_number")
  text_field(:modification_charge, name: "modify_charge_type")
  text_field(:cancellation_period_type, name: "cancel_effectivity_unit")
  text_field(:cancellation_period_value, name: "cancel_effectivity_number")
  text_field(:cancellation_charge, name: "cancel_charge_type")
  text_field(:noshow_charge, name: "no_show_charge_type")
  text_field(:max_extranights, xpath: "//label[contains(., 'Maximum no. of additional nights:')]/following::input[1]")
  text_field(:child_booking_period_start, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Booking Period')]/following::input[1]")
  text_field(:child_booking_period_end, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Booking Period')]/following::input[2]")
  text_field(:child_min_nights, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Minimum Nights')]/following::input[1]")
  text_field(:child_max_nights, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Maximum Nights')]/following::input[1]")
  text_field(:child_min_leadtime, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Minimum Lead Time')]/following::input[1]")
  text_field(:child_max_leadtime, xpath: "//fieldset[contains(@class,'booking-conditions-promo')]//label[contains(.,'Maximum Lead Time')]/following::input[1]")
  text_field(:child_discount_type, name: "adjustment_type")
  text_field(:child_discount_amount, name: "adjustment_amount")
  text_field(:promocode, xpath: "//div[contains(@id,'panel_rate_plans')]//div/input[contains(@class,'txtPublicPromoCode')]")
  radio_button(:inherit_policy, xpath: "//input[@name='policy_option'][@value='0']")
  radio_button(:custom_policy, xpath: "//input[@name='policy_option'][@value='1']")
  radio_button(:nightly, xpath: "//input[@name='type_id'][@value='1']")
  radio_button(:fixed_nights, xpath: "//input[@name='type_id'][@value='2']")
  radio_button(:free_nights, xpath: "//input[@name='type_id'][@value='3']")
  radio_button(:refundable_yes, xpath: "//label[contains(., 'Refundable')]/following::input[1]")
  radio_button(:refundable_no, xpath: "//label[contains(., 'Refundable')]/following::input[2]")
  radio_button(:prepayment_arrival, xpath: "//label[contains(., 'upon arrival')]/preceding::input[@name='deposit'][1]")
  radio_button(:prepayment_booking, xpath: "//label[contains(., 'upon booking')]/preceding::input[@name='deposit'][1]")
  radio_button(:allow_extranights_cancel, xpath: "//label[contains(., 'No')]/preceding::input[@name='allow_extra_nights'][1]")
  radio_button(:allow_extranights_confirm, xpath: "//label[contains(., 'No')]/preceding::input[@name='allow_extra_nights'][2]")
  radio_button(:max_extranights_forever, xpath: "//label[contains(., 'No')]/preceding::input[@name='max_additional_nights'][1]")
  radio_button(:max_extranights_finite, xpath: "//label[contains(., 'No')]/preceding::input[@name='max_additional_nights'][2]")
  radio_button(:free_nights_extrabed_charge_cancel, xpath: "//label[contains(., 'No')]/preceding::input[@name='charge_free_nights_extra_bed'][1]")
  radio_button(:free_nights_extrabed_charge_confirm, xpath: "//label[contains(., 'No')]/preceding::input[@name='charge_free_nights_extra_bed'][2]")

  # Methods
  def assign_rateplan_type(data)
    is_parent = data[:parent_name].nil?
    if is_parent
      rateplan_type = data[:rateplan_type].downcase
      wizard_parent_title_element.wait_until_present(timeout: 120)
      wizard_parent_rateplantype_header_element.wait_until_present(timeout: 120)
      rateplan_type.include?("free") ? select_free_nights : select_nightly
      select_fixed_nights if rateplan_type.include? "fixed"
      parent_next
    end
  end

  def assign_rateplan_details(rateplan_name, data)
    rateplan_type = data[:rateplan_type].downcase
    is_parent = data[:parent_name].nil?
    if is_parent
      wizard_parent_details_header_element.wait_until_present(timeout: 120)
      parent_rateplan_name = rateplan_name
      parent_rateplan_description = "Test description"
      assign__rateplan_custom_meal(data[:meal_inclusion])
      assign_rateplan_min_nights_stay(data)
      check_parent_tag_specialpromo if rateplan_type.include? "special"
      check_parent_tag_mobileexclusive if rateplan_type.include? "mobile"
    else
      wizard_child_details_header_element.wait_until_present(timeout: 120)
      child_rateplan_name = rateplan_name
      child_rateplan_description = "Test description"
      check_child_tag_specialpromo if rateplan_type.include? "special"
      check_child_tag_mobileexclusive if rateplan_type.include? "mobile"
    end
    is_parent ? parent_next : child_next
  end

  def assign_promocode(promocode)
    label_promocode_rateplan_element.wait_until_present(timeout: 120)
    promocode = promocode
    if wizard_parent_title_element.present? || wizard_child_title_element.present?
      parent_next
    end
  end

  def assign_bookingconditions(data)
    is_parent = data[:parent_name].nil?
    first_checkin = get_date(data[:first_checkin])[4]
    last_checkout = get_date(data[:last_checkout])[4]
    if is_parent
      parent_first_checkin_element.wait_until_present(timeout: 120)
      promocode = data[:promocode] if !data[:promocode].nil?
      parent_first_checkin = first_checkin
      parent_last_checkout = last_checkout
      # Pending: Set Days Allowed
      parent_next
    else
      child_first_checkin_element.wait_until_present(timeout: 120)
      promocode = data[:promocode] if !data[:promocode].nil?
      child_first_checkin = first_checkin
      child_last_checkout = last_checkout
      # Pending: Set Days Allowed
      assign_child_promo_info(data)
      child_next
    end
  end

  def assign_child_promo_info(data)
    promo_type_element.click
    select_dropdown_option(data[:promo_type], 0)
    assign_child_promo_booking_period(data)
    assign_child_promo_nights(data)
    assign_child_promo_min_leadtime(data)
    assign_child_promo_max_leadtime(data)
  end

  def assign_rooms_rates(data)
    is_parent = data[:parent_name].nil?
    if is_parent
      wizard_parent_roomsrates_header_element.wait_until_present(timeout: 120)
      parent_room_list_element.wait_until_present(timeout: 120)
      assign_rateplan_nightly_rates(data)
      assign_rateplan_extra_night_rates(data)
      assign_extrabeds(data)
      parent_rates_next
    else
      wizard_child_roomsrates_header_element.wait_until_present(timeout: 120)
      child_room_list_element.wait_until_present(timeout: 120)
      assign_discount(data)
      select_rooms_to_derived_rates_from(data)
      child_rates_next
    end
  end

  def assign_extrabeds(data)
    # Pending
    # select_free_nights_extrabed_charge_cancel if data[:rateplan_type].downcase.include? 'free' # Default for Free Nights without extra beds
  end

  def assign_policies(data)
    is_parent = data[:parent_name].nil?
    inherit_policy = data[:prepay_policy].nil? && data[:modification_policy].nil? && data[:cancellation_policy].nil? && data[:noshow_policy].nil? && data[:child_policy].nil?
    if is_parent
      wizard_parent_policies_header_element.wait_until_present(timeout: 120)
    else
      wizard_child_policies_header_element.wait_until_present(timeout: 120)
      unless inherit_policy
        sleep 1
        custom_policy_element.wait_until_present(timeout: 120)
        mask_element.wait_while_present(timeout: 300)
        select_custom_policy
      end
    end
    unless inherit_policy
      assign_children_policy(data)
      assign_prepayment_policy(data)
      assign_refund_policy(data)
      assign_modification_policy(data)
      assign_cancellation_policy(data)
      assign_noshow_policy(data)
    end
    is_parent ? parent_rates_next : child_rates_next
  end

  def assign_inclusions(data)
    is_parent = data[:parent_name].nil?
    if is_parent
      wizard_parent_inclusions_header_element.wait_until_present(timeout: 120)
    else
      wizard_child_inclusions_header_element.wait_until_present(timeout: 120)
    end
  end

  def assign_addons(data)
    is_parent = data[:parent_name].nil?
    if is_parent
      wizard_parent_addons_header_element.wait_until_present(timeout: 120)
    else
      wizard_child_addons_header_element.wait_until_present(timeout: 120)
    end
  end

  def save_rateplan(data, is_active)
    is_parent = data[:parent_name].nil?
    if is_parent
      parent_save_and_publish_element.wait_until_present(timeout: 120)
      is_active ? parent_save_and_publish : parent_save
    else
      child_save_and_publish_element.wait_until_present(timeout: 120)
      is_active ? child_save_and_publish : child_save
    end
    feedback_element.wait_until_present(timeout: 120)
  end

  def promocode
    promocode_element.wait_until_present(timeout: 120)
    parent_next
    promocode_avail_copy
    promocode_element.value
  end

  def promocode_error_message
    promocode_element.wait_until_present(timeout: 120)
    parent_next
    promocode_error_message
  end

  def promocode_avail_copy
    promocode_element.wait_until_present(timeout: 120)
    if wizard_parent_title_element.present? || wizard_child_title_element.present?
      parent_next_element.wait_until_present(timeout: 120)
      parent_next
    end
    promocode_avail_copy_element.text
  end

  def promocode_previewpanel
    return promocode_previewpanel.gsub("Promo Code: ", "")
  end

  def promocode_link
    promocodelink_bookingconditions_element.wait_until_present(timeout: 120)
    promocodelink_bookingconditions.click
  end

  private

  def get_option(option)
    return "div[contains(@class,'x-combo-list-item')][contains(., '#{option}')]"
  end

  def select_dropdown_option(option, i)
    option_locator = i > 0 ? "//#{get_option(option)}/following::#{get_option(option)}[#{i}]" : "//#{get_option(option)}"
    self.class.div(:option, xpath: option_locator)
    option_element.wait_until_present(timeout: 120)
    option_element.click
    option_element.wait_while_present(timeout: 300)
  end

  def assign__rateplan_custom_meal(meal)
    rateplan_meal_element.click
    loading_element.wait_while_present(timeout: 300)
    select_dropdown_option("Custom", 0)
    meal_custom = meal
    meal_description = "Test meal description"
  end

  def assign_rateplan_min_nights_stay(rateplan_info)
    if rateplan_info[:rateplan_type].downcase.include? "free"
      rateplan_los_stay = rateplan_info[:min_nights_stay]
      rateplan_los_free = rateplan_info[:no_free_nights]
    else
      rateplan_los_default = rateplan_info[:min_nights_stay]
    end
  end

  def assign_child_promo_booking_period(data)
    booking_period_start = data[:booking_period_start]
    booking_period_end = data[:booking_period_end]
    unless data[:promo_type].downcase.include? "flash deal"
      check_child_set_booking_period unless booking_period_start.nil? && booking_period_end.nil?
    end
    child_booking_period_start = get_date(booking_period_start)[4] unless booking_period_start.nil?
    child_booking_period_end = get_date(booking_period_end)[4] unless booking_period_end.nil?
  end

  def assign_child_promo_nights(data)
    promo_min_nights = data[:promo_min_nights]
    promo_max_nights = data[:promo_max_nights]
    child_min_nights = promo_min_nights unless promo_min_nights.nil?
    unless promo_max_nights.nil?
      check_child_set_max_nights
      child_max_nights = promo_max_nights
    end
  end

  def assign_child_promo_min_leadtime(data)
    promo = data[:promo_type].downcase
    promo_min_leadtime = data[:promo_min_leadtime]
    unless promo.include?("last minute")
      unless promo_min_leadtime.nil?
        check_child_set_min_leadtime unless promo.include?("advanced purchase") || promo.include?("early bird")
        child_min_leadtime = promo_min_leadtime
      end
    end
  end

  def assign_child_promo_max_leadtime(data)
      promo = data[:promo_type].downcase
      promo_max_leadtime = data[:promo_max_leadtime]
      unless promo_max_leadtime.nil?
          check_child_set_max_leadtime unless promo.include? "last minute"
          child_max_leadtime = promo_max_leadtime
      end
  end

  def get_room_name_locator(is_parent, i)
    if is_parent
      return "//div[@id='rate_plan_wizard_tabpanel']//div[#{i}]/table/tbody/tr/td[2]/div/div"
    else
      return "//div[@id='derived_rp_wizard_tabpanel']//div[#{i}]/table/tbody/tr/td[2]/div"
    end
  end

  def assign_rateplan_nightly_rates(data)
    is_parent = data[:parent_name].nil?
    no_rooms = parent_no_rooms_elements.count
    for i in 1..no_rooms
      self.class.div(:room_name, xpath: get_room_name_locator(is_parent, i))
      room_name = room_name
      if room_name.downcase.include? "accommodation"
        parent_room_checkbox_accommodation_element.wait_until_present(timeout: 120)
        parent_room_checkbox_accommodation_element.click
        input_random_rates(MAX_ROOM_RATE)
      end
      if room_name.downcase.include? "occupancy"
        parent_room_checkbox_occupancy_element.wait_until_present(timeout: 120)
        parent_room_checkbox_occupancy_element.click
        parent_room_charging_occupancy_element.wait_until_present(timeout: 120)
        parent_room_charging_occupancy_element.click
        no_occupancy = parent_no_occupancy_elements.count
        for i in 1..no_occupancy
          input_random_rates(MAX_ROOM_RATE)
        end
      end
    end
  end

  def input_random_rates(max_rate)
    rates_input_element.wait_until_present(timeout: 5)
    rates_input = rand(max_rate).to_s
    rates_input_element.send_keys :enter
  end

  def assign_rateplan_extra_night_rates(data)
    no_rooms = parent_no_rooms_elements.count
    is_parent = data[:parent_name].nil?
    rateplan_type = data[:rateplan_type].downcase
    if rateplan_type.include? "fixed"
      unless data[:max_extra_nights].nil?
        no_extra_nights = data[:max_extra_nights].downcase
        has_no_extra_nights_limit = no_extra_nights.include?("forever") || no_extra_nights.include?("infinite")
      end
      if no_extra_nights.to_i > 0 || has_no_extra_nights_limit
        select_allow_extranights_confirm
        if has_no_extra_nights_limit
          select_max_extranights_forever
        else
          select_max_extranights_finite
          max_extranights = no_extra_nights
        end
        for i in 1..no_rooms
          self.class.div(:room_name, xpath: get_room_name_locator(is_parent, i))
          room_name = room_name
          if room_name.downcase.include? "accommodation"
            parent_room_extradayrates_key_accommodation_element.wait_until_present(timeout: 120)
            parent_room_extradayrates_key_accommodation_element.click
            input_random_rates(MAX_ROOM_RATE)
          end
          if room_name.downcase.include? "occupancy"
            parent_room_extradayrates_key_occupancy_element.wait_until_present(timeout: 120)
            parent_room_extradayrates_key_occupancy_element.click
            no_occupancy = parent_no_occupancy_elements.count
            for i in 1..no_occupancy
              input_random_rates(MAX_ROOM_RATE)
            end
          end
        end
      else
        select_allow_extranights_cancel
      end
    end
  end

  def assign_discount(data)
    unless data[:discount_type].nil? || data[:discount_amount].nil?
      child_discount_type_element.wait_until_present(timeout: 120)
      child_discount_type_element.click
      select_dropdown_option(data[:discount_type], 0)
      child_discount_amount_element.wait_until_present(timeout: 120)
      child_discount_amount = data[:discount_amount]
    end
  end

  def select_rooms_to_derived_rates_from(data)
    no_rooms = child_no_rooms_elements.count
    is_parent = data[:parent_name].nil?
    for i in 1..no_rooms
      self.class.div(:room_name, xpath: get_room_name_locator(is_parent, i))
      room_name = room_name
      if room_name.downcase.include? "accommodation"
        child_room_checkbox_accommodation_element.wait_until_present(timeout: 120)
        child_room_checkbox_accommodation_element.click
      end
      if room_name.downcase.include? "occupancy"
        child_room_checkbox_occupancy_element.wait_until_present(timeout: 120)
        child_room_checkbox_occupancy_element.click
      end
    end
  end

  def assign_children_policy(data)
    children_allowed_element.click
    loading_element.wait_while_present(timeout: 300)
    policy = data[:child_policy].downcase
    if policy.include?("not") && policy.include?("allowed")
      select_dropdown_option("Not Allowed", 0)
    else
      policy.include?("free") ? select_dropdown_option("Allowed", 1) : select_dropdown_option("Yes with charges", 0)
      children_age_element.wait_until_present(timeout: 120)
      children_age_element.click
      child_age = policy.split("below")[0].gsub(/[^0-9]/, "")
      select_dropdown_option(child_age, 0)
      unless policy.include? "free"
        child_charge = policy.split("child charge: ")[1]
        children_charge_element.wait_until_present(timeout: 120)
        children_charge = child_charge
      end
    end
  end

  def assign_prepayment_policy(data)
    policy = data[:prepay_policy].downcase
    if data[:hotel_type].downcase.include? "dwh"
      prepayment_element.click
      policy.include?("full") ? select_dropdown_option(OPTION_FULL, 0) : select_dropdown_option(OPTION_10, 0)
    else
      if policy.include? "no prepay"
        select_prepayment_arrival
      else
        select_prepayment_booking
        prepayment_element.click
        select_dropdown_option(OPTION_10, 0) if policy.include?("10") || policy.include?("partial")
        select_dropdown_option(Option_25, 0) if policy.include? "25"
        select_dropdown_option(OPTION_50, 0) if policy.include? "50"
        select_dropdown_option(OPTION_FIRST_NIGHT, 0) if policy.include? "first night"
        select_dropdown_option(OPTION_FULL, 0) if policy.include? "full"
      end
    end
  end

  def assign_refund_policy(data)
    hotel_type = data[:hotel_type].downcase
    prepay_policy = data[:prepay_policy].downcase
    unless (hotel_type.include?("dwh") && prepay_policy.include?("partial")) || (hotel_type.include?("hpp") && prepay_policy.include?("no prepayment"))
      is_refundable = check_if_rateplan_is_refundable(data)
      is_refundable ? select_refundable_yes : select_refundable_no
    end
  end

  def check_if_rateplan_is_nonrefundable(data)
    refund_policy = data[:refund_policy].nil? ? "N/A" : data[:refund_policy].downcase
    return (refund_policy.include?("refundable") && refund_policy.include?("non"))
  end

  def check_if_rateplan_is_refundable(data)
    prepay_policy = data[:prepay_policy].downcase
    refund_policy = data[:refund_policy].nil? ? "N/A" : data[:refund_policy].downcase
    return (!prepay_policy.include?("no prepayment") && refund_policy.include?("refundable") && !refund_policy.include?("non"))
  end

  def check_if_rateplan_is_hotel_refundable(data)
    hotel_type = data[:hotel_type].downcase
    is_refundable = check_if_rateplan_is_refundable(data)
    return (!hotel_type.include?("dwh") && is_refundable)
  end

  def assign_modification_policy(data)
    hotel_type = data[:hotel_type].downcase
    prepay_policy = data[:prepay_policy].downcase
    is_nonrefundable = check_if_rateplan_is_nonrefundable(data)
    is_hotel_refundable = check_if_rateplan_is_hotel_refundable(data)
    policy = data[:modification_policy].nil? ? "N/A" : data[:modification_policy].downcase
    unless hotel_type.include?("dwh") && prepay_policy.include?("full") && is_nonrefundable
      if policy.include? "no penalty"
        modification_charge_element.click
        loading_element.wait_while_present(timeout: 300)
        select_dropdown_option(OPTION_NOCHARGE, 0)
      elsif hotel_type.include?("hpp") && is_nonrefundable
        modification_period_type_element.click
        loading_element.wait_while_present(timeout: 300)
        policy.include?("effective immediately") ? select_dropdown_option(OPTION_EFFECTIVE_IMMEDIATELY, 0) : select_dropdown_option(OPTION_NOTALLOWED, 1)
      else
        modification_period_type_element.click
        loading_element.wait_while_present(timeout: 300)
        if policy.include? "lead time"
          select_dropdown_option(OPTION_DAY, 0)
          modification_period_value = 3
          modification_charge_element.click
          loading_element.wait_while_present(timeout: 300)
          select_dropdown_option(OPTION_30, 0)
        elsif (policy.include?("first night") || policy.include?("full charge")) && policy.include?("effective immediately")
          if hotel_type.include?("dwh") && prepay_policy.include?("partial")
            select_dropdown_option(OPTION_EFFECTIVE_IMMEDIATELY, 0)
          else
            select_dropdown_option(OPTION_MONTH, 0)
            modification_period_value = 20
          end
          modification_charge_element.click
          loading_element.wait_while_present(timeout: 300)
          if policy.include? "first night"
            is_hotel_refundable ? select_dropdown_option(OPTION_FIRST_NIGHT, 1) : select_dropdown_option(OPTION_FIRST_NIGHT, 0)
          end
          if policy.include? "full charge"
            is_hotel_refundable || hotel_type.include?("dwh") ? select_dropdown_option(OPTION_FULL, 1) : select_dropdown_option(OPTION_FULL, 0)
          end
        end
      end
    end
  end

  def assign_cancellation_policy(data)
    hotel_type = data[:hotel_type].downcase
    prepay_policy = data[:prepay_policy].downcase
    is_nonrefundable = check_if_rateplan_is_nonrefundable(data)
    is_hotel_refundable = check_if_rateplan_is_hotel_refundable(data)
    policy = data[:cancellation_policy].nil? ? "N/A" : data[:cancellation_policy].downcase
    unless hotel_type.include?("dwh") && prepay_policy.include?("full") && is_nonrefundable
      if policy.include? "no penalty"
        cancellation_charge_element.click
        loading_element.wait_while_present(timeout: 300)
        select_dropdown_option(OPTION_NOCHARGE, 1)
      elsif hotel_type.include?("hpp") && is_nonrefundable
        cancellation_period_type_element.click
        loading_element.wait_while_present(timeout: 300)
        policy.include?("effective immediately") ? select_dropdown_option(OPTION_EFFECTIVE_IMMEDIATELY, 1) : select_dropdown_option(OPTION_NOTALLOWED, 2)
      else
        cancellation_period_type_element.click
        loading_element.wait_while_present(timeout: 300)
        if policy.include? "lead time"
          select_dropdown_option(OPTION_DAY, 1)
          cancellation_period_value = 3
          cancellation_charge_element.click
          loading_element.wait_while_present(timeout: 300)
          select_dropdown_option(OPTION_30, 1)
        elsif (policy.include?("first night") || policy.include?("full charge")) && policy.include?("effective immediately")
          if hotel_type.include?("dwh") && prepay_policy.include?("partial")
            select_dropdown_option(OPTION_EFFECTIVE_IMMEDIATELY, 1)
          else
            select_dropdown_option(OPTION_MONTH, 1)
            cancellation_period_value = 20
          end
          cancellation_charge_element.click
          loading_element.wait_while_present(timeout: 300)
          if policy.include? "first night"
            is_hotel_refundable ? select_dropdown_option(OPTION_FIRST_NIGHT, 2) : select_dropdown_option(OPTION_FIRST_NIGHT, 1)
          end
          if policy.include? "full charge"
            is_hotel_refundable || hotel_type.include?("dwh") ? select_dropdown_option(OPTION_FULL, 2) : select_dropdown_option(OPTION_FULL, 1)
          end
        end
      end
    end
  end

  def assign_noshow_policy(data)
    hotel_type = data[:hotel_type].downcase
    is_nonrefundable = check_if_rateplan_is_nonrefundable(data)
    is_hotel_refundable = check_if_rateplan_is_hotel_refundable(data)
    policy = data[:noshow_policy].nil? ? "N/A" : data[:noshow_policy].downcase
    unless is_nonrefundable
      noshow_charge_element.click
      loading_element.wait_while_present(timeout: 300)
      select_dropdown_option(OPTION_30, 2) if policy.include? "30"
      select_dropdown_option(OPTION_NOCHARGE, 2) if policy.include? "no penalty"
      if policy.include? "first night"
        is_hotel_refundable ? select_dropdown_option(OPTION_FIRST_NIGHT, 3) : select_dropdown_option(OPTION_FIRST_NIGHT, 2)
      end
      if policy.include? "full charge"
        is_hotel_refundable || hotel_type.include?("dwh") ? select_dropdown_option(OPTION_FULL, 3) : select_dropdown_option(OPTION_FULL, 2)
      end
    end
  end
end
