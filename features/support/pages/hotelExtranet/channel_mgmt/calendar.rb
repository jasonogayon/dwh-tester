# Hotel Extranet Calendar Page
class CalendarPage
  include PageObject
  include HotelExtranetHelper
  include DatesHelper
  include DataTransformHelper

  page_url FigNewton.login_he_url

  STOP_SELL_STYLE = "[contains(@style,'color:#ffffff;background-color:#C11B17')]"

  # Element Locators
  button(:save_availability, xpath: "//table[@id='availability_save_btn']//button[contains(.,'Save')]")
  button(:cancel_availability, xpath: "//table[@id='availability_save_btn']//button[contains(.,'Cancel')]")
  button(:save_rate, xpath: "//table[@id='onlineselling_save_btn']//button[contains(.,'Save')]")
  button(:cancel_rate, xpath: "//table[@id='onlineselling_save_btn']//button[contains(.,'Cancel')]")
  text_field(:calendar_start_from, id: "availability_matrix")
  text_field(:input, xpath: "//input[contains(@class,'x-form-text')][contains(@class,'x-form-num-field')][contains(@class,'x-form-focus')]")

  # Methods
  def assign_calendar_start_date(start_date)
    loading_element.wait_while_present(timeout: 300)
    calendar_start_from_element.clear
    calendar_start_from = get_date(start_date)[4]
    loading_element.wait_while_present(timeout: 300)
  end

  def set_default_availability(room_name, start_date, end_date)
    update_availability(room_name, 500, start_date, end_date)
  end

  def set_default_rates(room_name, rateplan, start_date, end_date)
    update_rates(room_name, rateplan, 1, start_date, end_date)
  end

  def remove_stop_sell(room_name, rateplan, start_date, end_date)
    update_stop_sell(room_name, rateplan, false, start_date, end_date)
  end

  def update_availability(room_name, availability, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    for i in 2..(no_nights + 2)
      select_room_availability(room_name, i)
      room_availability_element.click
      input_element.wait_until_present(timeout: 10)
      input = availability
      @browser.send_keys :enter
    end
    save_availability_updates
  end

  def update_rates(room_name, rateplan, rate, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    for i in 5..(no_nights + 5)
      select_room_rate_for_rateplan(rateplan, room_name, i)
      room_rate_element.click
      input_element.wait_until_present(timeout: 10)
      input = rate
      @browser.send_keys :enter
    end
    save_rate_updates
  end

  def update_stop_sell(room_name, rateplan, stop_sell_status, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    for i in 5..(no_nights + 5)
      select_room_rate_for_rateplan(rateplan, room_name, i)
      room_rate_element.right_click
      self.class.span(:stop_sell, xpath: get_stop_sell_locator(stop_sell_status, i))
      stop_sell_element.wait_until_present(timeout: 10)
      stop_sell_element.click
    end
    save_rate_updates
  end

  def get_room_availability(room_name, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    availability = []
    for i in 2..(no_nights + 2)
      select_room_availability(room_name, i)
      availability[i - 2] = room_availability
    end
    return availability
  end

  def get_room_availability_stop_sell_status(room_name, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    availability_stop_sell_status = []
    for i in 2..(no_nights + 2)
      self.class.cell(:stop_sell_status, xpath: "//div[@id='gridRatesAvailability']//tr[contains(.,'#{capitalize(room_name)}')]/td[#{i}]#{STOP_SELL_STYLE}")
      availability_stop_sell_status[i - 2] = stop_sell_status_element.present?
    end
    return availability_stop_sell_status
  end

  def get_room_rates(room_name, rateplan, start_date, end_date)
    no_nights = get_no_nights(start_date, end_date)
    rates = []
    for i in 5..(no_nights + 5)
      select_room_rate_for_rateplan(rateplan, room_name, i)
      rates[i - 5] = room_rate
    end
    return rates
  end

  def get_room_rates_stop_sell_status(room_name, rateplan, start_date, end_date)
    loading_element.wait_while_present(timeout: 300)
    no_nights = get_no_nights(start_date, end_date)
    rates_stop_sell_status = []
    for i in 5..(no_nights + 5)
      self.class.cell(:stops_sell_status, xpath: "//div[@id='gridOnlineRatesAvailability']//div[contains(.,'#{capitalize(rateplan)}')]//tr[contains(.,'#{capitalize(room_name)}')]/td[#{i}]#{STOP_SELL_STYLE}")
      rates_stop_sell_status[i - 5] = stops_sell_status_element.present?
    end
    return rates_stop_sell_status
  end

  private

  def get_no_nights(start_date, end_date)
    return get_difference_of(end_date, start_date).gsub(" night/s", "").to_i
  end

  def get_stop_sell_locator(stop_sell_status, i)
    option = stop_sell_status ? get_stop_sell_option("Stop sell") : get_stop_sell_option("Cancel stop sell")
    locator = "//#{option}"
    while i > 5
      locator += "/following::#{option}"
      i -= 1
    end
    return locator
  end

  def get_stop_sell_option(option)
    return "span[contains(@class,'x-menu-item-text')][contains(.,'#{option}')]"
  end

  def wait_until_changes_have_been_saved
    please_wait_element.wait_while_present(timeout: 300)
    saving_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def save_rate_updates
    save_rate_element.wait_until_present(timeout: 10)
    save_rate
    click_confirm_element.wait_until_present(timeout: 10)
    click_confirm
    wait_until_changes_have_been_saved
  rescue
    return false
  end

  def save_availability_updates
    save_availability_element.wait_until_present(timeout: 10)
    save_availability
    wait_until_changes_have_been_saved
  rescue
    return save_availability_element.present?
  end

  def select_room_availability(room_name, i)
    self.class.div(:room_availability, xpath: "//div[@id='gridRatesAvailability']//tr[contains(.,'#{capitalize(room_name)}')]/td[#{i}]/div[contains(@class,'x-grid3-cell-inner')]")
    room_availability_element.wait_until_present(timeout: 120)
  end

  def select_room_rate_for_rateplan(rateplan, room_name, i)
    self.class.div(:room_rate, xpath: "//div[@id='gridOnlineRatesAvailability']//div[contains(.,'#{capitalize(rateplan)}')]//tr[contains(.,'#{capitalize(room_name)}')]/td[#{i}]/div")
    room_rate_element.wait_until_present(timeout: 120)
  end
end
