# Hotel Extranet Private Rate Plans Page
class PrivateRatePlansPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  span(:list, xpath: "//div[@id='gridPrivateRatePlans']/div/div[2]/div/div[1]/div[2]/div/div[1]/table/tbody/tr/td[3]/div/span")

  # Methods
  def wait_for_rateplanlist
    loading_element.wait_while_present(timeout: 300)
    list_element.wait_until_present(timeout: 120)
  end
end
