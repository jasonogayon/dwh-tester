# Hotel Extranet Public Rate Plans Page
class PublicRatePlansPage
  include PageObject
  include DataLookup
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:mask, xpath: "//div[contains(@class, 'ext-el-mask')]")
  span(:list, xpath: "//div[@id='rate_plan_list_card']//td[4]//span")
  span(:highlight_enabledisable, xpath: "//div[@id='rate_plan_list_card']//span[contains(.,'rate plan highlighting')]")
  span(:highlight_change, xpath: "//div[@id='rate_plan_list_card']//span[contains(.,'Change highlighted rate plan ')]")
  button(:add_parent_rateplan, xpath: "//div[@id='rate_plan_list_card']//button[contains(.,'Add Rate Plan')]")
  button(:add_child_rateplan, xpath: "//div[@id='rate_plan_edit_panel']//button[contains(.,'Create a derived')]")
  button(:highlight_confirm, xpath: "//table[contains(@class, 'x-toolbar-ct')]//button[contains(.,'Yes')]")
  button(:highlight_proceed, xpath: "//table[contains(@class, 'x-toolbar-ct')]//button[contains(.,'Proceed')]")
  button(:highlight_cancel, xpath: "//table[contains(@class, 'x-toolbar-ct')]//button[contains(.,'No')]")

  # Methods
  def highlight(rateplan)
    mask_element.wait_while_present(timeout: 300)
    highlight_enabledisable_element.click
    choose_rateplan_to_highlight(rateplan)
  end

  def update_highlight(rateplan)
    mask_element.wait_while_present(timeout: 300)
    highlight_change_element.wait_until_present(timeout: 120)
    loading_element.wait_while_present(timeout: 300)
    highlight_change_element.click
    choose_rateplan_to_highlight(rateplan)
  end

  def remove_highlight
    if disable_highlight_exists
      mask_element.wait_while_present(timeout: 300)
      highlight_enabledisable_element.click
      confirm_update_highlight
    end
    loading_element.wait_while_present(timeout: 300)
  end

  def open_add_parent_rateplan_wizard
    loading_element.wait_while_present(timeout: 300)
    list_element.wait_until_present(timeout: 120)
    add_parent_rateplan_element.wait_until_present(timeout: 120)
    add_parent_rateplan
  end

  def open_add_child_rateplan_wizard(data)
    loading_element.wait_while_present(timeout: 300)
    list_element.wait_until_present(timeout: 120)
    self.class.div(:parent_rateplan, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{data[:parent_name]}')]//td[4]/div")
    parent_rateplan_element.click
    add_child_rateplan_element.wait_until_present(timeout: 120)
    add_child_rateplan
  end

  def get_new_rateplan_id(rateplan_name)
    self.class.div(:rateplan_id, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{rateplan_name}')]//td[3]/div")
    rateplan_id_element.wait_until_present(timeout: 120)
    new_rateplan_id = rateplan_id
    return new_rateplan_id.strip
  end

  def get_new_rateplan_status(rateplan_name)
    self.class.span(:rateplan_status, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{rateplan_name}')]//td[5]//span")
    rateplan_status_element.wait_until_present(timeout: 120)
    new_rateplan_status = rateplan_status
    return new_rateplan_status.strip
  end

  def get_new_rateplan_published_date(rateplan_name)
    self.class.div(:rateplan_published_date, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{rateplan_name}')]//td[6]/div")
    rateplan_published_date_element.wait_until_present(timeout: 120)
    new_rateplan_published_date = rateplan_published_date
    return new_rateplan_published_date.strip
  end

  def get_new_rateplan_type(rateplan_name)
    self.class.div(:rateplan_type, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{rateplan_name}')]//td[7]/div")
    rateplan_type_element.wait_until_present(timeout: 120)
    new_rateplan_type = rateplan_type
    return new_rateplan_type.strip
  end

  def remove_rateplan(rateplan_name)
    loading_element.wait_while_present(timeout: 300)
    list_element.wait_until_present(timeout: 120)
    self.class.image(:rateplan_to_delete, xpath: "//div[@id='rate_plan_list_card']//table[contains(.,'#{rateplan_name}')]//td[15]//img")
    rateplan_to_delete_element.wait_until_present(timeout: 120)
    rateplan_to_delete_element.click
    click_confirm_element.wait_until_present(timeout: 120)
    click_confirm
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def get_new_rateplan_name(data)
    hotel_type = data[:hotel_type].downcase
    rateplan_type = data[:rateplan_type].gsub("Public ", "").gsub("Private ", "")
    prepay_policy = data[:prepay_policy].nil? ? "" : data[:prepay_policy]
    refund_policy = data[:refund_policy].nil? ? nil : data[:refund_policy]
    policy = data[:modification_policy].nil? ? "Inherit Policy" : data[:modification_policy].gsub("No PENALTY", "NC").gsub(" PENALTY", "").gsub(" LEAD TIME", " LT").gsub("FIRST NIGHT", "FN").gsub("EFFECTIVE IMMEDIATELY", "Immediate")
    rateplan_name = data[:parent_name].nil? ? "Parent " : "Child "
    if hotel_type.include? "dwh"
      rateplan_name += "#{rateplan_type} #{prepay_policy} "
      rateplan_name += prepay_policy.downcase.include?("partial") ? policy : "#{refund_policy} #{policy}"
      rateplan_name = rateplan_name.gsub("Inherit Policy", "") if prepay_policy.downcase.include?("full") && refund_policy.downcase.include?("non")
    else
      rateplan_name += "#{rateplan_type} "
      rateplan_name += prepay_policy.downcase.include?("no prepayment") ? "Arrival #{policy}" : "Booking #{prepay_policy} #{refund_policy} #{policy}"
    end
    rateplan_name += " No. #{rand(100)}" if data_setup == :false
    return rateplan_name
  end

  private

  def choose_rateplan_to_highlight(rateplan)
    highlight_confirm_element.wait_until_present(timeout: 120)
    highlight_confirm
    self.class.radio_button(:rateplan, xpath: "//input[@name='rate_plans'][@value='#{get_rateplan_id(rateplan, nil)}']")
    rateplan_element.wait_until_present(timeout: 120)
    select_rateplan
    highlight_proceed
    confirm_update_highlight
  end

  def confirm_update_highlight
    click_confirm_element.wait_until_present(timeout: 120)
    click_confirm
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def disable_highlight_exists
    highlight_enabledisable_element.wait_until_present(timeout: 120)
    return highlight_enabledisable_element.text.include?("Disable")
  end
end
