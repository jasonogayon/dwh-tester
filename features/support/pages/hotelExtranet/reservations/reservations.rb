# Hotel Extranet Reservations Page
class ReservationsPage
  include PageObject
  include HotelExtranetHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:rsvsn_mask, xpath: "//div[contains(@class, 'ext-el-mask')]")
  div(:acknowledgment, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr//td[contains(.,'Acknowledged')]/div")
  div(:payment_status, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr//td[13]/div")
  div(:rsvsn_status, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr//td[3]/div")
  link(:rsvsn_confirmation_no, xpath: "//label[contains(., 'Confirmation No')]/following::div[1]/div/a")
  cells(:rsvsns, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr/td[2]")
  image(:acknowledge_checkbox, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr//td[last()-1]/div/img")
  image(:payment_status_checkbox, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div/table/tbody/tr//td[last()-5]/div/img")
  button(:search_open, xpath: "//*[@id='panelReservations']//button[contains(.,'Search')]")
  button(:search_go, xpath: "//*[@id='reservationFiltersPanel']//button[contains(.,'Search')]")
  button(:search_clear, xpath: "//*[@id='panelReservationsSearch']//button[contains(.,'Clear')]")
  button(:search_close, xpath: "//*[@id='panelReservationsSearch']//button[contains(.,'Search')]")
  text_field(:guest_confirmation_no, name: "confirmation_no")

  # Methods
  def open_reservation(confirmation_no)
    search_reservation(confirmation_no)
    begin
      tries ||= 10
      rsvsn_element.wait_until_present(timeout: 120)
      rsvsn_element.click
      loading_element.wait_while_present(timeout: 300)
      rsvsn_confirmation_no_element.wait_until_present(timeout: 120)
    rescue
      (tries -= 1).zero? ? raise("Unable to view reservation details") : retry
    end
  end

  def search_reservation(confirmation_no)
    self.class.cell(:rsvsn, xpath: "//div[@id='gridReservations']/div/div/div/div/div[2]/div/div[contains(.,'#{confirmation_no}')]/table/tbody/tr/td[2]")
    search_open_element.wait_until_present(timeout: 120)
    search_open
    guest_confirmation_no_element.wait_until_present(timeout: 120)
    guest_confirmation_no = confirmation_no
    search_go
    loading_element.wait_while_present(timeout: 300)
    rsvsn_mask_element.wait_while_present(timeout: 300)
    rsvsn_element.wait_until_present(timeout: 120)
  end

  def acknowledge_reservation(confirmation_no)
    search_reservation(confirmation_no)
    rsvsn_element.focus
    acknowledge_checkbox_element.scroll_into_view
    acknowledge_checkbox_element.focus
    acknowledge_checkbox_element.click
    click_confirm_element.wait_until_present(timeout: 120)
    click_confirm_element.click
    loading_element.wait_while_present(timeout: 300)
    rsvsn_element.wait_until_present(timeout: 120)
    acknowledge_checkbox_element.wait_while_present(timeout: 300)
  end

  def acknowledgment_status
    acknowledgment_element.wait_until_present(timeout: 120)
    return acknowledgment
  end

  def reservation_payment_status
    payment_status_element.wait_until_present(timeout: 120)
    return payment_status
  end

  def reservation_status
    rsvsn_status_element.wait_until_present(timeout: 120)
    return rsvsn_status
  end
end
