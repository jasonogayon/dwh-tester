# Hotel Extranet Reservation Details
class ReservationDetailsPage
  include PageObject
  include HotelExtranetHelper
  include DataTransformHelper

  page_url FigNewton.login_he_url

  # Element Locators
  div(:tab_rsvsnhistory, xpath: "//*[@id='winReservationDetails']/div[2]/div[1]")
  div(:grid_paymenthistory, id: "paymentHistoryGrid")
  div(:guest_name, name: "guest_name")
  div(:guest_email, xpath: "//*[@id='reservation_audit_log_form']/div/form/fieldset[2]/div/div/div[2]/div/div")
  div(:guest_mobile, name: "contact_no")
  div(:guest_country, name: "country_name")
  div(:rsvsn_eta, name: "eta")
  div(:rsvsn_no_guests, name: "total_guests")
  div(:rsvsn_specialrequest, name: "special_request")
  div(:rsvsn_status, name: "status_name")
  div(:rsvsn_ibe_madeon, name: "created_date")
  div(:rsvsn_ibe_modifiedon, name: "modified_date")
  div(:rsvsn_channel_madeon, name: "channel_creation_date")
  div(:rsvsn_channel_modifiedon, name: "channel_modification_date")
  div(:rsvsn_checkin, name: "check_in_date_time")
  div(:rsvsn_checkout, name: "check_out_date_time")
  div(:rsvsn_no_nights, name: "no_of_nights")
  div(:rsvsn_ibe_rateplan, name: "rate_plan_name")
  div(:rsvsn_channel_rateplan, name: "channel_rate_plan_name_code")
  div(:rsvsn_no_rooms, name: "total_no_of_rooms")
  div(:charge_roomcost_partial, name: "total_room_charges")
  div(:charge_roomcost_total, name: "total_room_cost")
  div(:charge_total_tax, name: "total_tax")
  div(:charge_total_fee, name: "total_fee")
  div(:charge_total_addon_cost, name: "total_add_on_rate")
  div(:charge_rsvsncost_ibe, xpath: "//label[contains(.,'Total reservation cost')]/following::div[1]/div/div/div[1]")
  div(:charge_prepay_ibe_dwh, xpath: "//label[contains(.,'Payment')]/following::div[1]/div/div/div[1]")
  div(:charge_prepay_ibe_hpp, xpath: "//label[contains(.,'Payment')]/following::div[1]/div[1]")
  div(:charge_penalty, name: "penalty_charge_amount")
  div(:charge_balance_payable, name: "balance_amount")
  div(:charge_amount_remittable, name: "hotel_remittable")
  div(:charge_revenue_dwh, xpath: "//label[contains(.,'DirectWithHotels fee')]/following::div[1]/div/div/div[1]")
  div(:charge_revenue_hpp, xpath: "//label[contains(.,'DirectWithHotels fee')]/following::div[1]/div[1]")
  div(:policy_children, name: "child_policy")
  div(:policy_prepayment, name: "payment_policy")
  div(:policy_modification, name: "modification_policy")
  div(:policy_cancellation, name: "cancellation_policy")
  div(:policy_noshow, name: "no_show_policy")
  div(:acknowledgment, name: "acknowledged_details")
  div(:markedcharged, name: "payment_status_details")
  div(:notif_noshow, xpath: "//div[contains(., 'The reservation has been marked as a no show.')]")
  divs(:row_payment, xpath: "//div[@id='paymentHistoryGrid']/div/div/div/div/div[2]/div/div/table/tbody/tr")
  link(:rsvsn_confirmation_no, xpath: "//label[contains(., 'Confirmation No')]/following::div[1]/div/a")
  link(:tab_paymenthistory, xpath: "//a[contains(., 'Payment/Transaction History')]")
  link(:rsvsn_alternate_cc, xpath: "//label[@id='alternate_cc']/a")
  image(:rsvsn_alternate_cc_deadline, xpath: "//input[@id='deadline_date']/following::img")
  button(:request_to_cancel, xpath: "//div[@id='winReservationDetails']//button[contains(.,'Request to Cancel')]")
  button(:request_to_cancel_confirm, xpath: "//div[@id='winRequestToCancel']//button[contains(.,'Proceed')]")
  button(:request_to_cancel_cancel, xpath: "//div[@id='winRequestToCancel']//button[contains(.,'Close')]")
  button(:mark_as_charged, xpath: "//table[@id='mark_as_charged']//button[contains(.,'Mark as Charged')]")
  button(:mark_as_preauth, xpath: "//table[@id='mark_as_preauthorized']//button[contains(.,'Mark as Pre-authorized')]")
  button(:mark_payment_status_confirm, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//button[contains(.,'OK')]")
  button(:mark_payment_status_cancel, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//button[contains(.,'Cancel')]")
  button(:report_noshow, xpath: "//*[@id='winReservationDetails']//button[contains(.,'Report as No Show')]")
  button(:report_noshow_confirm, xpath: "//*[contains(@class, 'x-window')][contains(@class, 'x-window-plain')][contains(@class, 'x-window-dlg')]//button[contains(.,'Yes')]")
  button(:close_reservationdetails, xpath: "//div[@id='winReservationDetails']//button[contains(., 'Close')][contains(@class, 'cancelicon')]")
  button(:date_today, xpath: "//button[contains(.,'Today')]")
  button(:rsvsn_alternate_cc_confirm, xpath: "//div[@id='winRequestAlternateCCDetails']//button[contains(.,'Send Request')]")
  button(:rsvsn_alternate_cc_cancel, xpath: "//div[@id='winRequestAlternateCCDetails']//button[contains(.,'Close')]")
  textarea(:request_to_cancel_remark, name: "remarks")

  # Methods
  def mark_as_noshow
    report_noshow_element.wait_until_present(timeout: 120)
    report_noshow
    report_noshow_confirm_element.wait_until_present(timeout: 120)
    report_noshow_confirm
    loading_element.wait_while_present(timeout: 300)
  end

  def goto_confirmation(url)
    @browser.goto url
    rsvsn_header_element.wait_until_present(timeout: 120)
  end

  def goto_paymenthistory
    tab_spaymenthistory_element.wait_until_present(timeout: 120)
    tab_paymenthistory_element.click
  end

  def paymenthistory_size
    grid_paymenthistory_element.wait_until_present(timeout: 120)
    return row_payment_elements.size
  end

  def get_reservation_details(reservation_type)
    loading_element.wait_while_present(timeout: 300)
    rsvsn_details = Hash[
      date:                             DateTime.now,
      rsvsn_type:                       reservation_type,
      confirmation_no:                  rsvsn_confirmation_no_element.text,
      guest_name:                       guest_name,
      guest_email:                      guest_email,
      guest_mobile:                     guest_mobile,
      guest_country:                    guest_country,
      rsvsn_status:                     rsvsn_status,
      rsvsn_madeon:                     rsvsn_ibe_madeon,
      rsvsn_modifiedon:                 rsvsn_ibe_modifiedon,
      rsvsn_channel_madeon:             get_detail(rsvsn_channel_madeon_element),
      rsvsn_channel_modifiedon:         get_detail(rsvsn_channel_modifiedon_element),
      rsvsn_rateplan:                   rsvsn_ibe_rateplan,
      rsvsn_channel_rateplan:           get_detail(rsvsn_channel_rateplan_element),
      rsvsn_eta:                        rsvsn_eta,
      rsvsn_no_guests:                  rsvsn_no_guests,
      rsvsn_no_rooms:                   rsvsn_no_rooms,
      rsvsn_no_nights:                  rsvsn_no_nights,
      rsvsn_checkin:                    rsvsn_checkin,
      rsvsn_checkout:                   rsvsn_checkout,
      rsvsn_specialrequest:             rsvsn_specialrequest,
      charge_room_charge_partial:       get_detail(charge_roomcost_partial_element),
      charge_room_charge_total:         get_detail(charge_roomcost_total_element),
      charge_total_tax:                 get_detail(charge_total_tax_element),
      charge_total_fee:                 get_detail(charge_total_fee_element),
      charge_total_addon_cost:          convert("he_" + reservation_type, get_detail(charge_total_addon_cost_element)),
      charge_total_reservation_cost:    get_detail(charge_rsvsncost_ibe_element),
      charge_prepayment:                get_detail(charge_prepay_ibe_dwh_element),
      charge_balance_payable:           get_detail(charge_balance_payable_element),
      charge_penalty:                   get_detail(charge_penalty_element),
      charge_dwh_revenue:               get_detail(charge_revenue_dwh_element),
      charge_hotel_revenue:             get_detail(charge_amount_remittable_element),
      policy_children:                  policy_children,
      policy_prepayment:                policy_prepayment,
      policy_modification:              policy_modification,
      policy_cancellation:              policy_cancellation,
      policy_noshow:                    policy_noshow,
      acknowledgment:                   get_detail(acknowledgment_element),
      mark_as_charged:                  get_detail(markedcharged_element)
    ]

    # Update prepayment for non-DWH reservations
    unless reservation_type.include?("dwh")
      prepayment = Hash[charge_prepayment: get_detail(charge_prepay_ibe_hpp_element)]
      rsvsn_details.update(prepayment)
    end

    # Update balance for reservations where it is not displayed
    if rsvsn_details[:charge_balance_payable].nil?
      balance = Hash[charge_balance_payable: get_balance(rsvsn_details)]
      rsvsn_details.update(balance)
    end

    # Update hotel revenue for reservations where it is not displayed
    if rsvsn_details[:charge_balance_payable].nil?
      hotel_revenue = Hash[charge_hotel_revenue: get_hotel_revenue(rsvsn_details)]
      rsvsn_details.update(hotel_revenue)
    end
    return rsvsn_details
  end

  def close_reservation_details
    tries ||= 3
    close_reservationdetails_element.wait_until_present(timeout: 120)
    close_reservationdetails
  rescue
    authentication_server_failure_element.wait_until_present(timeout: 120)
    authentication_server_failure
    puts "Warning: It seems that an Authentication Server Error appeared while the test ran. Please verify if not an actual error."
    retry unless (tries -= 1).zero?
  end

  def request_to_cancel_status
    return request_to_cancel_element.present?
  end

  def request_reservation_for_cancellation
    request_to_cancel_element.wait_until_present(timeout: 120)
    request_to_cancel
    request_to_cancel_remark_element.wait_until_present(timeout: 120)
    request_to_cancel_remark = "Requesting to cancel reservation for testing purposes."
    request_to_cancel_confirm
    click_confirm_element.wait_until_present(timeout: 120)
    click_confirm_element.click
    saving_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def request_for_alternate_cc_status
    return rsvsn_alternate_cc_element.present?
  end

  def request_alternate_cc_from_guest
    rsvsn_alternate_cc_element.wait_until_present(timeout: 120)
    rsvsn_alternate_cc_element.scroll_into_view
    rsvsn_alternate_cc
    rsvsn_alternate_cc_deadline_element.wait_until_present(timeout: 120)
    rsvsn_alternate_cc_deadline_element.click
    date_today
    rsvsn_alternate_cc_confirm_element.wait_until_present(timeout: 120)
    rsvsn_alternate_cc_confirm
    please_wait_element.wait_while_present(timeout: 300)
    loading_element.wait_while_present(timeout: 300)
  end

  def mark_as_charged_existence
    loading_element.wait_while_present(timeout: 300)
    rsvsn_confirmation_no_element.wait_until_present(timeout: 120)
    return mark_as_charged_element.present?
  end

  def mark_as_preauthorized_existence
    loading_element.wait_while_present(timeout: 300)
    rsvsn_confirmation_no_element.wait_until_present(timeout: 120)
    return mark_as_preauth_element.present?
  end

  def mark_reservation_payment_status(tag_type)
    if tag_type.include? "charged"
      mark_as_charged_element.wait_until_present(timeout: 120)
      mark_as_charged
    else
      mark_as_preauth_element.wait_until_present(timeout: 120)
      mark_as_preauth
    end
    mark_payment_status_confirm_element.wait_until_present(timeout: 120)
    mark_payment_status_confirm
    saving_element.wait_while_present(timeout: 300)
    loading_element.wait_until_present(timeout: 20)
    loading_element.wait_while_present(timeout: 300)
    close_reservation_details
  end

  private

  def get_balance(rsvsn_details)
    return sprintf("%.2f", rsvsn_details[:charge_total_reservation_cost].to_f - rsvsn_details[:charge_prepayment].to_f)
  end

  def get_hotel_revenue(rsvsn_details)
    return sprintf("%.2f", rsvsn_details[:charge_total_reservation_cost].to_f - rsvsn_details[:charge_dwh_revenue].to_f)
  end
end
