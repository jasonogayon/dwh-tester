DWH-Tester
==========

DWH-Tester is a collection of test suites for checking running business rules and user flows for applications built by DirectWithHotels Philippines, Ltd. Additionally, some test suites are meant to be used as tools to build or get data from said applications with the goal of making it easier for the tester or developer to build test data or get information from desired scenarios.

Note: Configuration files to make the code work are not included in this repository and are manually sent.

## Setup

1. Install Ruby and the Ruby Development Kit
2. Clone or download this repository
3. Open a terminal / command line of your choice and navigate to your local repository
4. Run `gem install bundler` to install the bundler gem
5. Run `bundle install` to install gem dependencies
6. Install Docker for building a standalone image of the running test code (Optional). Run `rake build` to build and push the latest standalone image. Use the Docker Quickstart Terminal on Windows.

## Server Name Options

* `-p staging` for Staging
* `-p dev` for Dev
* `-p uat` for UAT
* `-p uat2` for UAT2

## Tools

#### View Existing Property Information

To view information from an existing property, for any test server:
`cucumber -p [server_name] -t @view_all PROP_ID=[property_id]`

For example:
`cucumber -p dev -t @view_all PROP_ID=18442`

For viewing only a specific information from the hotel, replace the `-t @view_all` tag with any of the following:

* `-t @view_rooms` for rooms
* `-t @view_public_rateplans` for public rate plans
* `-t @view_private_rateplans` for private rate plans
* `-t @view_private_accounts` for private accounts
* `-t @view_taxes` for taxes and surcharges
* `-t @view_users` for users

#### Create Test Reservations

To create test reservations on any desired property, for any test server, do the following:
`cucumber -p [server_name] -t @custom_rsvsn PROP_ID=[property_id] RP_ID=[rateplan_id] CHECKIN=[arrival_date] NIGHTS=[no_nights]`

For example:
`cucumber -p uat2 -t @custom_rsvsn PROP_ID=18442 RP_ID=21360 CHECKIN=2017-07-01 NIGHTS=2`

Add a `PROMOCODE=[promocode_value]` parameter for rate plans with promo code. Meanwhile add a `NOCC=true` parameter for testing no-credit-card reservations.

Leaving out the `-p` option will automatically run the test on the `staging` server.

If you like, you can add some tags for targetting specific reservation types:

* `-t @confirm`
* `-t @cancel`
* `-t @noshow`
* `-t @mobile`
* `-t @desktop`

#### View Reservation Information

To view information from an existing reservation, for any test server:
`cucumber -p [server_name] -t @view_rsvsn CONFIRMATION_NO=[confirmation_no]`

For example:
`cucumber -t @view_rsvsn CONFIRMATION_NO=4319192`


#### Create A New Test Property

To create a new Global Collect test property, for any test server, do this:
`cucumber -p [server_name] -t @gc_hotel`

For example:
`cucumber -t @gc_hotel`

For other types of property, replace the `-t @gc_hotel` tag with any of the following:

* `-t @ap_hotel` for Asiapay
* `-t @hpp_hotel` for HPP
* `-t @bdo_hotel` for BDO
* `-t @bp_hotel` for BDO-Paypal
* `-t @pp_hotel` for Paypal-Only
* `-t @bn_hotel` for Bancnet

#### Create Rate Plans on An Existing Property

To create new generic public and private nightly rate plans, for any test server:
`cucumber -p [server_name] -t @new_rateplans PROP_ID=[property_id]`

For example:
`cucumber -p uat2 -t @new_rateplans PROP_ID=18442`

Meanwhile, to create new child rate plans given a rate plan ID:
`cucumber -p [server_name] -t @new_child_rateplans PROP_ID=[property_id] RP_ID=[rateplan_id]`

#### Run A Database Query Via Terminal

To query the database, using the Control Panel's query feature:
`cucumber -p [server_name] -t @query DB=[database_name] QUERY="[SQL_query]"`

To query the database and create an Excel report of it afterwards (filed under the reports directory), just change the tag from `-t @query` to `-t @queryreport`


Author: **Jason B. Ogayon (Software Tester)**